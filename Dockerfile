FROM debian:11.8-slim

#TODO: multi-stage for the selenium stuff

# Taken from the docker compose django docs...
ENV PYTHONUNBUFFERED 1

EXPOSE 8000/tcp

# System packages and cleanup
RUN apt-get update && \
    apt-get install -y --force-yes python3 python3-pip python3-venv
RUN apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/apt /var/lib/cache /var/lib/log

# venv
ENV VIRTUAL_ENV=/opt/momw
RUN mkdir -p $VIRTUAL_ENV && python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Requirements
COPY requirements .
RUN python -m pip install -r app.in && \
    python -m pip install -r tests.in
# RUN python -m pip install --require-hashes -r app.txt && \
#     python -m pip install --require-hashes -r tests.txt

# App env setup begins
VOLUME /app
WORKDIR /app

# Bootstrap the user
RUN export user=momw home=/home/momw uid=1000 gid=1000 && \
    mkdir -p ${home} && \
    echo "${user}:x:${uid}:${gid}:${user},,,:${home}:/bin/bash" >> /etc/passwd && \
    echo "${user}:x:${uid}:" >> /etc/group
USER momw

ENTRYPOINT [ "make" ]
