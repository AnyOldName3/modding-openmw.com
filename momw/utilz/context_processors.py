"""
TODO: break out the other modules' stuff
"""
import datetime
import pytz

from django.conf import settings
from chroniko import ADD_CATEGORY_PERM, ADD_ENTRY_PERM
from media import ADD_MEDIA_PERM, CHANGE_MEDIA_PERM


TZ = pytz.timezone(settings.TIME_ZONE)


def site_wide(request):
    # Do this try/except so tests don't fail on missing an HTTP_HOST key.
    if hasattr(request, "user"):
        add_category_perm = request.user.has_perm(ADD_CATEGORY_PERM)
        add_entry_perm = request.user.has_perm(ADD_ENTRY_PERM)
        add_media_perm = request.user.has_perm(ADD_MEDIA_PERM)
        edit_media_perm = request.user.has_perm(CHANGE_MEDIA_PERM)
    else:
        add_category_perm = False
        add_entry_perm = False
        add_media_perm = False
        edit_media_perm = False
    try:
        this_domain = request.META["HTTP_HOST"]
    except KeyError:
        this_domain = None
    return {
        # TODO: need change category/entry.... and make it work...
        "can_post_new_category": add_category_perm,
        "can_post_new_entry": add_entry_perm,
        "can_post_new_media": add_media_perm,
        "can_change_media": edit_media_perm,
        "DEBUG": settings.DEBUG,
        "now": datetime.datetime.now(TZ),
        "request": request,
        "this_domain": this_domain,
        "this_site": settings.SITE_NAME,
    }
