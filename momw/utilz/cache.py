import http.client
import urllib.request

from bs4 import BeautifulSoup
from django.conf import settings
from django.views.decorators.cache import cache_page


class CacheCtl(object):
    """
    Thanks:
    https://stackoverflow.com/a/26858638
    """

    def dispatch(self, *args, **kwargs):
        return cache_page(settings.CACHE_MIDDLEWARE_SECONDS)(super().dispatch)(
            *args, **kwargs
        )


def warm_cache(just_print=False):
    """
    Read the sitemap XML data to produce a list of URL, then
    request them so as to populate the cache.
    """
    _xml = ""
    _hostname = settings.PROJECT_HOSTNAME
    _insecure = "staging." in _hostname or "dev." in _hostname

    if settings.DEBUG:
        _hostname += ":8666"
        _http = "http"
        _insecure = False
    else:
        _http = "https"

    _url = "{0}://{1}/sitemap.xml".format(_http, _hostname)
    urls = []

    if _insecure:
        insecure_cert = http.client.ssl.SSLContext()
        insecure_cert.verify_mode = http.client.ssl.CERT_NONE
        _u1 = urllib.request.urlopen(_url, context=insecure_cert)
    else:
        _u1 = urllib.request.urlopen(_url)
    for line in _u1.readlines():
        _xml += line.decode("utf-8")

    _u1.close()
    y = BeautifulSoup(_xml, "html.parser")

    for line in y.findAll("loc"):
        urls.append(line.string)

    if just_print:
        for url in urls:
            print(url)

    else:
        for url in urls:
            print("Caching '{}' ...".format(url))
            if _insecure:
                g = urllib.request.urlopen(url, context=insecure_cert)
            else:
                g = urllib.request.urlopen(url)
            g.close()

    return True
