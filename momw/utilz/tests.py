from django.contrib.auth.models import (
    AnonymousUser,
    User,
)  # TODO: get user function thing
from django.shortcuts import reverse
from django.contrib.sites.models import Site
from django.test import RequestFactory, TestCase
from selenium.common.exceptions import WebDriverException
from .git import APP_SHA, GIT_TAG
from .views import app_info, app_info_json, contact


HTTP_OK = 200


class BaseSelenium:
    """
    This class implements some boilerplate/helpful methods for writing tests
    that use Selenium.  Handles settings up and tearing down as required by the
    varisou Selenium drivers (but specifically Firefox and Chrome.)
    """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.er500 = "500 Internal Server Error"

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()

    def check_http_status_code(
        self,
        named_url,
        view,
        sitemaps=None,
        slug=None,
        step=None,
        template=None,
        **kwargs,
    ):
        if kwargs:
            r = self.f.get(reverse(named_url, kwargs=kwargs))
        elif slug and step:
            r = self.f.get(reverse(named_url, kwargs={"slug": slug, "step": step}))
        elif slug:
            r = self.f.get(reverse(named_url, kwargs={"slug": slug}))
        else:
            r = self.f.get(reverse(named_url))
        r.user = AnonymousUser()
        if sitemaps:
            if template:
                response = view(r, sitemaps=sitemaps, template=template)
            else:
                response = view(r, sitemaps=sitemaps)
        elif slug and step:
            if template:
                response = view(r, slug=slug, step=step, template=template)
            else:
                response = view(r, slug=slug, step=step)
        elif slug:
            if template:
                response = view(r, slug=slug, template=template)
            else:
                response = view(r, slug=slug)
        else:
            if template:
                response = view(r, template=template)
            else:
                response = view(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def check_for_500(self, named_url, slug=None):
        if slug:
            self.driver.get(
                self.live_server_url + reverse(named_url, kwargs={"slug": slug})
            )
        else:
            self.driver.get(self.live_server_url + reverse(named_url))
        self.assertFalse(
            self.er500 in self.driver.find_element_by_css_selector("body").text
        )

    def check_for_text(
        self, named_url, text, slug=None, step=None, where="body", exists=True
    ):
        if slug and step:
            self.driver.get(
                self.live_server_url
                + reverse(named_url, kwargs={"slug": slug, "step": step})
            )
        elif slug:
            self.driver.get(
                self.live_server_url + reverse(named_url, kwargs={"slug": slug})
            )
        else:
            self.driver.get(self.live_server_url + reverse(named_url))

        test = text in self.driver.find_element_by_css_selector(where).text

        if exists:
            self.assertTrue(test)
        else:
            self.assertFalse(test)


class UtilzSeleniumTestCase:
    """
    To use this class, subclass it and the above BaseSelenium class as seen
    below.  Ordering matters:

    class YourFirefoxTestCase(BaseSelenium, YourSeleniumTestCase,
                              UtilzSeleniumTestCase, StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.driver = FFDriver()

    Where 'YourSeleniumTestCase' is a class that implements Selenium test methods.
    """

    def setUp(self):
        super().setUp()
        self.f = RequestFactory()

    def test_app_info(self):
        u = "app-info"
        v = app_info
        self.check_http_status_code(u, v)
        self.check_for_500(u)

    def test_app_info_json(self):
        u = "app-info-json"
        v = app_info_json
        self.check_http_status_code(u, v)
        self.check_for_500(u)

    def test_contact(self):
        u = "contact"
        v = contact
        self.check_http_status_code(u, v)
        self.check_for_500(u)


# TODO: many if not all of the below are obsoleted by the above selenium tests
class UtilzTestCase(TestCase):
    def setUp(self):
        self.f = RequestFactory()
        self.s = Site.objects.create(name="test", domain="test.pizza")
        self.u = User.objects.create(
            first_name="Test", last_name="User", username="testuser", password="12345"
        )

    def test_contact_page(self):
        r = self.f.get(reverse("contact"))
        r.user = AnonymousUser()
        response = contact(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_app_info(self):
        r = self.f.get(reverse("app-info"))
        r.user = AnonymousUser()
        response = app_info(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_app_info_has_sha_or_tag(self):
        r = self.f.get(reverse("app-info"))
        r.user = AnonymousUser()
        response = app_info(r)
        try:
            self.assertContains(response, APP_SHA)
        except AssertionError:
            self.assertContains(response, GIT_TAG)

    def test_app_info_json(self):
        r = self.f.get(reverse("app-info-json"))
        r.user = AnonymousUser()
        response = app_info_json(r)
        self.assertEqual(response.status_code, HTTP_OK)

    def test_app_info_json_has_sha_or_tag(self):
        r = self.f.get(reverse("app-info-json"))
        r.user = AnonymousUser()
        response = app_info_json(r)
        self.assertTrue(
            APP_SHA in response.content.decode("utf-8")
            or GIT_TAG in response.content.decode("utf-8")
        )

    def test_app_info_json_is_json(self):
        r = self.f.get(reverse("app-info-json"))
        r.user = AnonymousUser()
        response = app_info_json(r)
        self.assertTrue(
            "application/json; charset=utf-8" in response.headers["content-type"]
        )


def start_driver_wrapper(cls, DriverClass):
    """
    Needed because chromedriver is known to be flakey and will sometimes fail
    to connect for no reason.  https://stackoverflow.com/a/40528075
    """
    while not hasattr(cls, "driver"):
        try:
            cls.driver = DriverClass()
        except (ConnectionResetError, WebDriverException):
            pass
