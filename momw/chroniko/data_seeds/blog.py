import pytz

from datetime import datetime
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from rikeripsum import rikeripsum
from chroniko.models import BlogCategory, BlogEntry, BlogTag, TaggedEntry

TZ = pytz.timezone(settings.TIME_ZONE)
User = get_user_model()


def generate_category(title, slug, desc):
    c = BlogCategory.objects.create(title=title, slug=slug, description=desc)
    c.save()
    return c


def generate_entry(body, date_added, slug, status, title, author, category, excerpt=""):
    """
    'body': rikeripsum.generate_paragraph(100),
     'date_added': datetime.strptime('2010-12-18 14:34:34 -0500', datetime_format),
     'slug': 'blog-entry-one',
     'status': 1,
     'title': 'Blog Entry One LIVE',
     'author': u,
     'category': cat1
    """
    e = BlogEntry.objects.create(
        body=body,
        date_added=date_added,
        slug=slug,
        status=status,
        title=title,
        author=author,
        category=category,
        excerpt=excerpt,
    )
    e.save()
    return e


def blog_category_factory():
    BlogCategory(
        title="Category One",
        slug="category-one",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Two",
        slug="category-two",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Three",
        slug="category-three",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Four",
        slug="category-four",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Five",
        slug="category-five",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Six",
        slug="category-six",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Seven",
        slug="category-seven",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Eight",
        slug="category-eight",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Nine",
        slug="category-nine",
        description=rikeripsum.generate_paragraph(10),
    ).save()
    BlogCategory(
        title="Category Ten",
        slug="category-ten",
        description=rikeripsum.generate_paragraph(10),
    ).save()


def blog_tag_factory():
    BlogTag(name="Tag1").save()
    BlogTag(name="Tag2").save()
    BlogTag(name="Tag3").save()
    BlogTag(name="Tag4").save()
    BlogTag(name="Tag5").save()
    BlogTag(name="Tag6").save()
    BlogTag(name="Tag7").save()
    BlogTag(name="Tag8").save()
    BlogTag(name="Tag9").save()
    BlogTag(name="Tag10").save()
    BlogTag(name="Tag11").save()
    BlogTag(name="Tag12").save()
    BlogTag(name="Tag13").save()
    BlogTag(name="Tag14").save()
    BlogTag(name="Tag15").save()
    BlogTag(name="Tag16").save()
    BlogTag(name="Tag17").save()
    BlogTag(name="Tag18").save()
    BlogTag(name="Tag19").save()
    BlogTag(name="Tag20").save()
    BlogTag(name="Tag21").save()
    BlogTag(name="Tag22").save()
    BlogTag(name="Tag23").save()
    BlogTag(name="Tag24").save()
    BlogTag(name="Tag25").save()
    BlogTag(name="Tag26").save()
    BlogTag(name="Tag27").save()
    BlogTag(name="Tag28").save()
    BlogTag(name="Tag29").save()
    BlogTag(name="Tag30").save()
    BlogTag(name="Tag31").save()
    BlogTag(name="Tag32").save()
    BlogTag(name="Tag33").save()
    BlogTag(name="Tag34").save()
    BlogTag(name="Tag35").save()
    BlogTag(name="Tag36").save()
    BlogTag(name="Tag37").save()
    BlogTag(name="Tag38").save()
    BlogTag(name="Tag39").save()
    BlogTag(name="Tag40").save()


def blog_entry_factory():
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    entry_content_type = ContentType.objects.get_for_model(BlogEntry)
    u = User.objects.get(pk=1)
    cat1 = BlogCategory.objects.get(pk=1)
    cat2 = BlogCategory.objects.get(pk=2)
    cat3 = BlogCategory.objects.get(pk=3)
    cat4 = BlogCategory.objects.get(pk=4)
    cat5 = BlogCategory.objects.get(pk=5)
    cat6 = BlogCategory.objects.get(pk=6)
    cat7 = BlogCategory.objects.get(pk=7)
    cat8 = BlogCategory.objects.get(pk=8)
    cat9 = BlogCategory.objects.get(pk=9)
    cat10 = BlogCategory.objects.get(pk=10)
    tag1 = BlogTag.objects.get(pk=1)
    tag2 = BlogTag.objects.get(pk=2)
    tag3 = BlogTag.objects.get(pk=3)
    tag4 = BlogTag.objects.get(pk=4)
    tag5 = BlogTag.objects.get(pk=5)
    tag6 = BlogTag.objects.get(pk=6)
    tag7 = BlogTag.objects.get(pk=7)
    tag8 = BlogTag.objects.get(pk=8)
    tag9 = BlogTag.objects.get(pk=9)
    tag10 = BlogTag.objects.get(pk=10)
    tag11 = BlogTag.objects.get(pk=11)
    tag12 = BlogTag.objects.get(pk=12)
    tag13 = BlogTag.objects.get(pk=13)
    tag14 = BlogTag.objects.get(pk=14)
    tag15 = BlogTag.objects.get(pk=15)
    tag16 = BlogTag.objects.get(pk=16)
    tag17 = BlogTag.objects.get(pk=17)
    tag18 = BlogTag.objects.get(pk=18)
    tag19 = BlogTag.objects.get(pk=19)
    tag20 = BlogTag.objects.get(pk=20)
    tag21 = BlogTag.objects.get(pk=21)
    tag22 = BlogTag.objects.get(pk=22)
    tag23 = BlogTag.objects.get(pk=23)
    tag24 = BlogTag.objects.get(pk=24)
    tag25 = BlogTag.objects.get(pk=25)
    tag26 = BlogTag.objects.get(pk=26)
    tag27 = BlogTag.objects.get(pk=27)
    tag28 = BlogTag.objects.get(pk=28)
    tag29 = BlogTag.objects.get(pk=29)
    tag30 = BlogTag.objects.get(pk=30)
    tag31 = BlogTag.objects.get(pk=31)
    tag32 = BlogTag.objects.get(pk=32)
    tag33 = BlogTag.objects.get(pk=33)
    tag34 = BlogTag.objects.get(pk=34)
    tag35 = BlogTag.objects.get(pk=35)
    tag36 = BlogTag.objects.get(pk=36)
    tag37 = BlogTag.objects.get(pk=37)
    tag38 = BlogTag.objects.get(pk=38)
    tag39 = BlogTag.objects.get(pk=39)
    tag40 = BlogTag.objects.get(pk=40)
    e1 = {
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2010-12-18 14:34:34 -0500", datetime_format),
        "slug": "blog-entry-one",
        "status": 1,
        "title": "Blog Entry One LIVE",
        "author": u,
        "category": cat1,
    }
    e2 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2011-01-19 10:34:34 -0500", datetime_format),
        "slug": "blog-entry-two",
        "status": 2,
        "title": "Blog Entry Two DRAFT",
        "author": u,
        "category": cat2,
    }
    e3 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2011-02-22 16:34:34 -0500", datetime_format),
        "slug": "blog-entry-three",
        "status": 3,
        "title": "Blog Entry Three HIDDEN",
        "author": u,
        "category": cat3,
    }
    e4 = {
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2011-02-22 16:34:34 -0500", datetime_format),
        "slug": "blog-entry-four",
        "status": 1,
        "title": "Blog Entry Four LIVE",
        "author": u,
        "category": cat4,
    }
    e5 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2011-03-04 03:04:34 -0500", datetime_format),
        "slug": "blog-entry-five",
        "status": 2,
        "title": "Blog Entry Five DRAFT",
        "author": u,
        "category": cat5,
    }
    e6 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2011-04-20 04:20:34 -0500", datetime_format),
        "slug": "blog-entry-six",
        "status": 3,
        "title": "Blog Entry Six HIDDEN",
        "author": u,
        "category": cat6,
    }
    e7 = {
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-01-01 15:20:34 -0500", datetime_format),
        "slug": "blog-entry-seven",
        "status": 1,
        "title": "Blog Entry Seven LIVE",
        "author": u,
        "category": cat7,
    }
    e8 = {
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-02-11 15:34:34 -0500", datetime_format),
        "slug": "blog-entry-eight",
        "status": 1,
        "title": "Blog Entry Eight LIVE",
        "author": u,
        "category": cat8,
    }
    e9 = {
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-02-11 15:34:34 -0500", datetime_format),
        "slug": "blog-entry-nine",
        "status": 1,
        "title": "Blog Entry Nine LIVE",
        "author": u,
        "category": cat9,
    }
    e10 = {
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-03-04 15:34:34 -0500", datetime_format),
        "slug": "blog-entry-ten",
        "status": 1,
        "title": "Blog Entry Ten LIVE",
        "author": u,
        "category": cat10,
    }
    e11 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-04-20 16:20:34 -0500", datetime_format),
        "slug": "blog-entry-eleven",
        "status": 1,
        "title": "Blog Entry Eleven LIVE",
        "author": u,
        "category": cat1,
    }
    e12 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-05-06 15:34:34 -0500", datetime_format),
        "slug": "blog-entry-twelve",
        "status": 1,
        "title": "Blog Entry Twelve LIVE",
        "author": u,
        "category": cat2,
    }
    e13 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-05-06 15:34:34 -0500", datetime_format),
        "slug": "blog-entry-thirteen",
        "status": 1,
        "title": "Blog Entry Thirteen LIVE",
        "author": u,
        "category": cat3,
    }
    e14 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2012-06-30 15:34:34 -0500", datetime_format),
        "slug": "blog-entry-fourteen",
        "status": 1,
        "title": "Blog Entry Fourteen LIVE",
        "author": u,
        "category": cat4,
    }
    e15 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2013-03-04 20:34:34 -0500", datetime_format),
        "slug": "blog-entry-fifteen",
        "status": 1,
        "title": "Blog Entry Fifteen LIVE",
        "author": u,
        "category": cat5,
    }
    e16 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2013-07-13 20:34:34 -0500", datetime_format),
        "slug": "blog-entry-sixteen",
        "status": 1,
        "title": "Blog Entry Sixteen LIVE",
        "author": u,
        "category": cat6,
    }
    e17 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2013-07-14 20:34:34 -0500", datetime_format),
        "slug": "blog-entry-seventeen",
        "status": 1,
        "title": "Blog Entry Seventeen LIVE",
        "author": u,
        "category": cat7,
    }
    e18 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2014-03-04 20:34:34 -0500", datetime_format),
        "slug": "blog-entry-eighteen",
        "status": 1,
        "title": "Blog Entry Eighteen LIVE",
        "author": u,
        "category": cat8,
    }
    e19 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2014-04-20 20:34:34 -0500", datetime_format),
        "slug": "blog-entry-nineteen",
        "status": 1,
        "title": "Blog Entry Nineteen LIVE",
        "author": u,
        "category": cat9,
    }
    e20 = {
        "excerpt": rikeripsum.generate_paragraph(5),
        "body": rikeripsum.generate_paragraph(100),
        "date_added": datetime.strptime("2014-10-10 20:34:34 -0500", datetime_format),
        "slug": "blog-entry-twenty",
        "status": 1,
        "title": "Blog Entry Twenty LIVE",
        "author": u,
        "category": cat10,
    }
    BlogEntry(**e1).save()
    BlogEntry(**e2).save()
    BlogEntry(**e3).save()
    BlogEntry(**e4).save()
    BlogEntry(**e5).save()
    BlogEntry(**e6).save()
    BlogEntry(**e7).save()
    BlogEntry(**e8).save()
    BlogEntry(**e9).save()
    BlogEntry(**e10).save()
    BlogEntry(**e11).save()
    BlogEntry(**e12).save()
    BlogEntry(**e13).save()
    BlogEntry(**e14).save()
    BlogEntry(**e15).save()
    BlogEntry(**e16).save()
    BlogEntry(**e17).save()
    BlogEntry(**e18).save()
    BlogEntry(**e19).save()
    BlogEntry(**e20).save()
    for _e in range(21, 100):
        BlogEntry(
            excerpt=rikeripsum.generate_paragraph(5),
            body=rikeripsum.generate_paragraph(100),
            date_added=datetime.now(TZ),
            slug=str(_e) * 10,
            status=1,
            title=str(_e) * 10,
            author=u,
            category=cat1,
        ).save()
    entry1 = BlogEntry.objects.get(pk=1)
    entry2 = BlogEntry.objects.get(pk=2)
    entry3 = BlogEntry.objects.get(pk=3)
    entry4 = BlogEntry.objects.get(pk=4)
    entry5 = BlogEntry.objects.get(pk=5)
    entry6 = BlogEntry.objects.get(pk=6)
    entry7 = BlogEntry.objects.get(pk=7)
    entry8 = BlogEntry.objects.get(pk=8)
    entry9 = BlogEntry.objects.get(pk=9)
    entry10 = BlogEntry.objects.get(pk=10)
    entry11 = BlogEntry.objects.get(pk=11)
    entry12 = BlogEntry.objects.get(pk=12)
    entry13 = BlogEntry.objects.get(pk=13)
    entry14 = BlogEntry.objects.get(pk=14)
    entry15 = BlogEntry.objects.get(pk=15)
    entry16 = BlogEntry.objects.get(pk=16)
    entry17 = BlogEntry.objects.get(pk=17)
    entry18 = BlogEntry.objects.get(pk=18)
    entry19 = BlogEntry.objects.get(pk=19)
    entry20 = BlogEntry.objects.get(pk=20)
    entry1.tags.add(tag1, tag2)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry1.id, tag=tag1
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry1.id, tag=tag2
    ).save()
    entry2.tags.add(tag3, tag4)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry2.id, tag=tag3
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry2.id, tag=tag4
    ).save()
    entry3.tags.add(tag5, tag6)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry3.id, tag=tag5
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry3.id, tag=tag6
    ).save()
    entry4.tags.add(tag7, tag8)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry4.id, tag=tag7
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry4.id, tag=tag8
    ).save()
    entry5.tags.add(tag9, tag10)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry5.id, tag=tag9
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry5.id, tag=tag10
    ).save()
    entry6.tags.add(tag11, tag12)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry6.id, tag=tag11
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry6.id, tag=tag12
    ).save()
    entry7.tags.add(tag13, tag14)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry7.id, tag=tag13
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry7.id, tag=tag14
    ).save()
    entry8.tags.add(tag15, tag16)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry8.id, tag=tag15
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry8.id, tag=tag16
    ).save()
    entry9.tags.add(tag17, tag18)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry9.id, tag=tag17
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry9.id, tag=tag18
    ).save()
    entry10.tags.add(tag19, tag20)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry10.id, tag=tag19
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry10.id, tag=tag20
    ).save()
    entry11.tags.add(tag21, tag22)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry11.id, tag=tag21
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry11.id, tag=tag22
    ).save()
    entry12.tags.add(tag23, tag24)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry12.id, tag=tag23
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry12.id, tag=tag24
    ).save()
    entry13.tags.add(tag25, tag26)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry13.id, tag=tag25
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry13.id, tag=tag26
    ).save()
    entry14.tags.add(tag27, tag28)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry14.id, tag=tag27
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry14.id, tag=tag28
    ).save()
    entry15.tags.add(tag29, tag30)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry15.id, tag=tag29
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry15.id, tag=tag30
    ).save()
    entry16.tags.add(tag31, tag32)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry16.id, tag=tag31
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry16.id, tag=tag32
    ).save()
    entry17.tags.add(tag33, tag34)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry17.id, tag=tag33
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry17.id, tag=tag34
    ).save()
    entry18.tags.add(tag35, tag36)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry18.id, tag=tag35
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry18.id, tag=tag36
    ).save()
    entry19.tags.add(tag37, tag38)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry19.id, tag=tag37
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry19.id, tag=tag38
    ).save()
    entry20.tags.add(tag39, tag40)
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry20.id, tag=tag39
    ).save()
    TaggedEntry(
        content_type_id=entry_content_type.id, object_id=entry20.id, tag=tag40
    ).save()
