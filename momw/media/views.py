from datetime import datetime
from django.conf import settings
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, reverse
from django.views.generic import DetailView, ListView
from utilz.cache import CacheCtl
from . import CHANGE_MEDIA_PERM, MEDIA_DATE_FIELD, MONTH_FORMAT
from .forms import MediaTrackForm
from .models import MediaTrack


class LiveAsciinemaView(CacheCtl, ListView):
    context_object_name = "live_asciinema"
    date_field = MEDIA_DATE_FIELD
    queryset = MediaTrack.live.all().filter(media_type=MediaTrack.ASCIINEMA)
    template_name = "live_asciinema.html"


class LiveAudioView(CacheCtl, ListView):
    context_object_name = "live_audio"
    date_field = MEDIA_DATE_FIELD
    queryset = MediaTrack.live.all().filter(media_type=MediaTrack.AUDIO)
    template_name = "live_audio.html"


class LiveMediaTracksView(CacheCtl, ListView):
    context_object_name = "all_media_tracks"
    date_field = MEDIA_DATE_FIELD
    queryset = MediaTrack.live.all()
    template_name = "live_media.html"


class LiveVideoView(CacheCtl, ListView):
    context_object_name = "live_video"
    date_field = MEDIA_DATE_FIELD
    queryset = MediaTrack.live.all().filter(media_type=MediaTrack.VIDEO)
    template_name = "live_video.html"


class MediaTrackDetailView(CacheCtl, DetailView):
    context_object_name = "media"
    model = MediaTrack
    month_format = MONTH_FORMAT
    queryset = MediaTrack.live.all()
    template_name = "media_detail.html"


def live_images(request):
    images = MediaTrack.live.all().filter(media_type=MediaTrack.IMAGE)
    return render(request, "live_images.html", {"images": images})


def create_mediatrack(request):
    """Create a new MediaTrack object."""
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    user = request.user
    if user.is_authenticated and user.has_perm(CHANGE_MEDIA_PERM):
        form = MediaTrackForm()
        if request.method == "POST":
            postdata = request.POST.copy()
            form = MediaTrackForm(postdata)
            if form.is_valid():
                mediatrack = form.instance
                mediatrack.author = user
                # Update the date and time if need be.
                new_date = postdata.get("date_added_0", None)
                new_time = postdata.get("date_added_1", None)
                if new_date and new_time:
                    new_datetime_string = " ".join(
                        (
                            new_date,
                            "".join(
                                (new_time, " ".join((":00", settings.TIME_ZONE_OFFSET)))
                            ),
                        )
                    )
                    new_datetime = datetime.strptime(
                        new_datetime_string, datetime_format
                    )
                    mediatrack.date_added = new_datetime
                form.save()
                # Redirect to the MediaTrack's edit page,
                # in case the slug was changed.
                return HttpResponseRedirect(
                    reverse("media:edit", kwargs={"slug": postdata.get("slug")})
                )
        else:  # if request.method == 'POST':
            pass
        return render(request, "media_create.html", {"form": form})
    else:  # if user.is_authenticated and user.has_perm(CHANGE_MEDIA_PERM):
        # Anonymous user
        raise Http404


def edit_mediatrack(request, slug):
    """Edit the MediaTrack with the slug 'slug'."""
    date_string = "%Y-%m-%d"
    time_string = "%H:%M"
    datetime_format = "%Y-%m-%d %H:%M:%S %z"
    mediatrack = get_object_or_404(MediaTrack, slug=slug)
    mediatrack_date_added = mediatrack.date_added.date().strftime(date_string)
    mediatrack_time_added = mediatrack.date_added.time().strftime(time_string)
    user = request.user
    if user.is_authenticated and user.has_perm(CHANGE_MEDIA_PERM):
        if request.method == "POST":
            # We are trying to post changes to an MediaTrack.
            postdata = request.POST.copy()
            form = MediaTrackForm(postdata, instance=mediatrack)
            if form.is_valid():
                form.save()
                # Update the date and time if need be.
                new_date = postdata.get("date_added_0", None)
                new_time = postdata.get("date_added_1", None)
                if new_date and new_time:
                    new_datetime_string = " ".join(
                        (
                            new_date,
                            "".join(
                                (new_time, " ".join((":00", settings.TIME_ZONE_OFFSET)))
                            ),
                        )
                    )
                    new_datetime = datetime.strptime(
                        new_datetime_string, datetime_format
                    )
                    mediatrack.date_added = new_datetime
                    mediatrack.save()
                # Redirect to the MediaTrack's edit page,
                # in case the slug was changed.
                return HttpResponseRedirect(
                    reverse("media:edit", kwargs={"slug": postdata.get("slug")})
                )
        else:  # if request.method == 'POST':
            # We are just looking at an MediaTrack
            form = MediaTrackForm(instance=mediatrack)
        return render(
            request,
            "media_edit.html",
            {
                "mediatrack": mediatrack,
                "mediatrack_date_added": mediatrack_date_added,
                "mediatrack_time_added": mediatrack_time_added,
                "form": form,
            },
        )
    else:  # if user.is_authenticated and user.has_perm(CHANGE_MEDIA_PERM):
        # Anonymous user
        raise Http404
