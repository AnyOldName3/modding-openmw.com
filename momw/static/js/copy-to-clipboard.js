/*
  momw - copy-to-clipboard.js
  Copyright (C) 2023  MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const copyOpenMWCfg = document.getElementById("copyOpenMWCfg");
const copySettingsCfg = document.getElementById("copySettingsCfg");

//THANKS: https://stackoverflow.com/a/30810322
function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        // console.log('Fallback: Copying text command was ' + msg);
    } catch (err) {
        // console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
}

function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
        fallbackCopyTextToClipboard(text);
        return;
    }
    navigator.clipboard.writeText(text).then(function () {
        // console.log('Async: Copying to clipboard was successful!');
    }, function (err) {
        // console.error('Async: Could not copy text: ', err);
    });
}

function AddCopyToClipboard() {

    if (copyOpenMWCfg) {
        copyOpenMWCfg.addEventListener("click", function () {
            copyTextToClipboard(document.getElementById("extra-cfg-o").textContent);
            copyOpenMWCfg.textContent = "Copied!";
        });
    }

    if (copySettingsCfg) {
        copySettingsCfg.addEventListener("click", function () {
            copyTextToClipboard(document.getElementById("extra-cfg-s").textContent);
            copySettingsCfg.textContent = "Copied!";
        });
    }
}

AddCopyToClipboard();
