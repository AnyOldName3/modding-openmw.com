/*
  momw - custom-cfg.js
  Copyright (C) 2023  MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const storageKeyInstalledMods = "installedMods";

function InstalledModsSetup() {
    const checkAll =  document.getElementById("check-all");
    const modDetailBtn = document.getElementById("moddetail-customcfg");
    const cfgGenInputs = document.getElementsByClassName("customcfg-input");

    function getInstalled() {
        return JSON.parse(localStorage.getItem(storageKeyInstalledMods));
    }

    function setInstalled(installd, slug, txt) {
        if (installd === null) {
            localStorage.setItem(storageKeyInstalledMods, JSON.stringify(new Array(slug)));
        } else {
            installd.push(slug);
            localStorage.setItem(storageKeyInstalledMods, JSON.stringify(installd));
        }
        if (txt !== null)
            modDetailBtn.textContent = txt;
    }

    function rmInstalled(installd, slug, txt, parentBigCheck) {
        installd.splice(installd.indexOf(slug), 1);
        localStorage.setItem(storageKeyInstalledMods, JSON.stringify(installd));
        if (txt !== null)
            modDetailBtn.textContent = txt;
        if (parentBigCheck !== null)
            parentBigCheck.checked = false;
    }

    function checksCheck() {
        let allChecked;
        for (let i = 0; i < cfgGenInputs.length > 0; i++) {
            let thisInput = cfgGenInputs[i];
            if (allChecked !== false)
                allChecked = thisInput.checked;
            if (allChecked === false)
                break;
        }
        if (allChecked === true) {
            checkAll.checked = true;
            // const parentCheckboxes = document.getElementsByClassName("customcfg-input-sublist");
            // if (parentCheckboxes)
            //     for (let i = 0; i < parentCheckboxes.length > 0; i++)
            //         parentCheckboxes[i].checked = true
        }
        // console.log("Do Checks Check Done!");
        // console.log(new Date());
    }

    function setup() {
        const parentCheckboxes = document.getElementsByClassName("customcfg-input-sublist");
        for (let i = 0; i < parentCheckboxes.length > 0; i++) {
            const childCheckboxes = document.getElementsByClassName(parentCheckboxes[i].id);
            parentCheckboxes[i].addEventListener("click", function() {
                for (let j = 0; j < childCheckboxes.length > 0; j++) {
                    childCheckboxes[j].checked = parentCheckboxes[i].checked;
                    if (parentCheckboxes[i].checked === true) {
                        setInstalled(getInstalled(), childCheckboxes[j].id, null);
                        // checksCheck();
                    } else {
                        rmInstalled(getInstalled(), childCheckboxes[j].getAttribute("data-slug"), null, document.getElementById(parentCheckboxes[i].classList[1]));
                        checkAll.checked = false;
                    }
                }
            });
        }

        for (let i = 0; i < cfgGenInputs.length > 0; i++) {
            let thisInput = cfgGenInputs[i];
            thisInput.addEventListener("click", function() {
                let slug = cfgGenInputs[i].id;
                let installed = JSON.parse(localStorage.getItem(storageKeyInstalledMods));
                if (installed) {
                    if (installed.includes(slug)) {
                        rmInstalled(getInstalled(), slug, null, document.getElementById(cfgGenInputs[i].classList[1]));
                        checkAll.checked = false;
                    } else {
                        setInstalled(getInstalled(), slug, null);
                        // checksCheck();
                    }
                } else {
                    setInstalled(null, slug, null);
                    // checksCheck();
                }
            });
        }

        let installed = getInstalled();
        for (let i = 0; i < cfgGenInputs.length > 0; i++)
            if (installed)
                if (installed.includes(cfgGenInputs[i].id))
                    cfgGenInputs[i].checked = true;

        // const parents = document.getElementsByClassName("customcfg-input-sublist");
        // for (let i = 0; i < parents.length> 0; i++) {
        //     let thisParent = parents[i];
        //     let allChecked;
        //     const childCheckboxes = document.getElementsByClassName(thisParent.id);
        //     for (let j = 0; j < childCheckboxes.length> 0; j++)
        //         allChecked = childCheckboxes[j].checked;
        //     thisParent.checked = allChecked;
        // }
        // console.log("Setup done!");
    }

    // Do stuff when the page loads
    if (checkAll)
        checkAll.addEventListener("click", function() {
            const parentCheckboxes = document.getElementsByClassName("customcfg-input-sublist");
            const checkInputs = document.getElementsByClassName("customcfg-input");

            for (let i = 0; i < checkInputs.length > 0; i++) {
                checkInputs[i].checked = this.checked;
                const slug = checkInputs[i].id;
                if (this.checked) {
                    setInstalled(getInstalled(), slug, null);
                } else {
                    rmInstalled(getInstalled(), slug, null, null);
                }
            }

            if (parentCheckboxes)
                for (let i = 0; i < parentCheckboxes.length> 0; i++)
                    parentCheckboxes[i].checked = this.checked;
        });

    // const P = new Promise(function(resolve) {
    //     resolve();
    // });
    // P.then(setup).then(checksCheck);

    setup();
    // checksCheck();
}

InstalledModsSetup();
