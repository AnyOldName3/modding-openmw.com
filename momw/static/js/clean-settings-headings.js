/*
  momw - clean-settings-headings.js
  Copyright (C) 2023  MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

function CleanSettingsHeadings() {
    let extraCfgScode = document.getElementById("extra-cfg-s");
    if (!extraCfgScode)
        return;
    const oldStr = extraCfgScode.textContent;
    let haveShadersHeading = false;
    let newStr = "";

    for (i=0; i < oldStr.length; i++) {
        let line = oldStr.split("\n")[i];
        if (line !== undefined) {
            if ((!haveShadersHeading) && (line.includes("[Shaders]"))) {
                newStr += line + "\n";
                haveShadersHeading = true;
            } else if (!line.includes("[Shaders]")) {
                if ((line === "enabled = true") || (!newStr.includes(line.trim())))
                    newStr += line + "\n";
            }
        }
    }
    extraCfgScode.textContent = newStr.replaceAll("[", "\n[").trim();
}

CleanSettingsHeadings();
