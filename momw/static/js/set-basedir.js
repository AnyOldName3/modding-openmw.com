/*
  momw - set-basedir.js
  Copyright (C) 2023  MOMW Authors

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

const storageKeyBaseDir = "baseDir";

function DownloadFolderScript() {
    const fileExtElem = document.getElementById("file-ext");
    if (!fileExtElem)
        return;
    const fileExt = fileExtElem.getAttribute("data-ext");
    const modList = document.getElementById("file-modlist").getAttribute("data-modlist");
    const scriptText = new Blob([document.getElementById("data-paths").textContent], {"type": "text/plain"});
    const dlLink = document.getElementById("dl-script");
    dlLink.href = URL.createObjectURL(scriptText);
    dlLink.download = `momw-folder-generator-${modList}.${fileExt}`;
    dlLink.click();
}

async function BaseDirSetup() {
    const defaultWindows = "C:\\games\\OpenMWMods";
    const defaultMacOs = "/Users/username/games/OpenMWMods";
    const defaultLinux = "/home/username/games/OpenMWMods";

    const currentBaseDir = localStorage.getItem(storageKeyBaseDir);
    const currentSpan = document.getElementById("current");
    const inputBox = document.getElementById("basedir");
    const setBtn = document.getElementById("set-basedir");
    const modDetailBlock = document.getElementById("data-paths");
    const cfgGeneratorBlock = document.getElementById("extra-cfg-o");

    const curValSpan = document.getElementById("baseDir");
    if (curValSpan)
        if (currentBaseDir) {
            curValSpan.textContent = currentBaseDir;
        } else {
            curValSpan.textContent = "None";
        }

    if ((currentBaseDir) && (currentSpan))
        currentSpan.textContent = currentBaseDir;

    if (inputBox)
        inputBox.addEventListener("keydown", function (e) {
            if (e.key === "Enter")
                setBaseDir(inputBox.value);
        });

    if (setBtn)
        setBtn.addEventListener("click", function () {
            setBaseDir(inputBox.value);
        });

    if (currentBaseDir)
        if (cfgGeneratorBlock) {
            let newText = "";
            for (let i = 0; i < cfgGeneratorBlock.textContent.split("\n").length; i++) {
                if (((cfgGeneratorBlock.textContent.split("\n")[i].includes("ModdingResources"))
                    && (cfgGeneratorBlock.textContent.split("\n")[i].includes("Morrowind"))
                    && (cfgGeneratorBlock.textContent.split("\n")[i].includes("Data Files")))) {
                    newText += cfgGeneratorBlock.textContent.split("\n")[i] + "\n";
                } else {
                    newText += cfgGeneratorBlock.textContent.split("\n")[i]
                        .replace(defaultLinux, currentBaseDir)
                        .replace(defaultMacOs, currentBaseDir)
                        .replace(defaultWindows, currentBaseDir) + "\n";
                }
            }
            cfgGeneratorBlock.textContent = newText.trim();

        } else if (modDetailBlock) {
            modDetailBlock.textContent = modDetailBlock.textContent
                .replaceAll(defaultLinux, currentBaseDir)
                .replaceAll(defaultMacOs, currentBaseDir)
                .replaceAll(defaultWindows, currentBaseDir);
        }

}

function setBaseDir(str) {
    const currentSpan = document.getElementById("current");
    if (str === "") {
        localStorage.removeItem(storageKeyBaseDir);
        currentSpan.textContent = "None";
    } else {
        localStorage.setItem(storageKeyBaseDir, str);
        currentSpan.textContent = str;
    }
}

async function main() {
    await BaseDirSetup();
    DownloadFolderScript();
}

main();
