import pytz

from django.conf import settings
from django.urls import reverse
from django.db import models
from django.utils.translation import gettext_lazy as _
from taggit.managers import TaggableManager
from taggit.models import TagBase, GenericTaggedItemBase
from utilz.managers import (
    EnabledTagManager,
    DraftThingManager,
    HiddenThingManager,
    LiveThingManager,
)
from .managers import (
    AltModsManager,
    AndroidSwitchManager,
    CompatFullyWorkingManager,
    CompatNotWorkingManager,
    CompatPartialWorkingManager,
    CompatUnknownManager,
    ExpandedVanillaManager,
    GraphicsOverhaulManager,
    OneDayModernizationManager,
    TotalOverhaulManager,
    IHeartVanillaManager,
    IHeartVanillaDCManager,
    StarwindModdedManager,
    UsedCategoriesManager,
    NoPluginsManager,
    WithPluginsManager,
    LatestModsManager,
    ListedModsJustGoodMorrowindManager,
    ListedModsExpandedVanillaManager,
    ListedModsGraphicsOverhaulManager,
    ListedModsIHeartVanillaManager,
    ListedModsIHeartVanillaDCManager,
    ListedModsOneDayModernizeManager,
    ListedModsTotalOverhaulManager,
    ListedModsStarwindModdedManager,
    TopLevelListsManager,
    ModPluginsManagerTotalOverhaul,
    ModPluginsManagerGraphicsOverhaul,
    ModPluginsManagerExpandedVanilla,
    ModPluginsManagerIHeartVanilla,
    ModPluginsManagerIHeartVanillaDC,
    ModPluginsManager1DayModernize,
    ModPluginsManagerStarwind,
    ModPluginsManagerJustGoodMorrowind,
    ModPluginsManagerContent,
    ModPluginsManagerBSA,
    ModPluginsManagerGroundcover,
    ModPluginsManagerNeedsCleaning,
    ExtraCfgManagerTotalOverhaulNoSettings,
    ExtraCfgManagerTotalOverhaulSettings,
    JustGoodMorrowindManager,
)

TZ = pytz.timezone(settings.TIME_ZONE)


# Mod author class


class Category(models.Model):
    objects = models.Manager()
    used = UsedCategoriesManager()
    title = models.CharField(max_length=250)
    slug = models.SlugField(
        help_text="Suggested value automatically generated from title.", unique=True
    )
    description = models.TextField()

    class Meta:
        db_table = "mod_categories"
        ordering = ["title"]
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.title

    @property
    def clean_name(self):
        return "".join(s for s in self.title if s.isalnum())

    @property
    def mod_count(self):
        return self.mod_set.count()

    def get_absolute_url(self):
        return reverse("mod_category_detail", kwargs={"slug": self.slug})


class Mod(models.Model):
    LIVE = 1
    DRAFT = 2
    HIDDEN = 3
    STATUS_CHOICES = ((LIVE, "Live"), (DRAFT, "Draft"), (HIDDEN, "Hidden"))
    FULLY_WORKING = 4
    PARTIAL_WORKING = 5
    NOT_WORKING = 6
    UNKNOWN = 7
    COMPAT_CHOICES = (
        (FULLY_WORKING, "Fully Working"),
        (PARTIAL_WORKING, "Partially Working"),
        (NOT_WORKING, "Not Working"),
        (UNKNOWN, "Unknown"),
    )
    objects = models.Manager()
    live = LiveThingManager()
    alts = AltModsManager()
    android_switch = AndroidSwitchManager()
    i_heart_vanilla = IHeartVanillaManager()
    i_heart_vanilla_dc = IHeartVanillaDCManager()
    graphics_overhaul = GraphicsOverhaulManager()
    one_day_modernize = OneDayModernizationManager()
    total_overhaul = TotalOverhaulManager()
    starwind_modded = StarwindModdedManager()
    just_good_morrowind = JustGoodMorrowindManager()
    draft = DraftThingManager()
    hidden = HiddenThingManager()
    expanded_vanilla = ExpandedVanillaManager()
    no_plugins = NoPluginsManager()
    with_plugins = WithPluginsManager()
    fully_working = CompatFullyWorkingManager()
    partial_working = CompatPartialWorkingManager()
    not_working = CompatNotWorkingManager()
    compat_unknown = CompatUnknownManager()
    latest = LatestModsManager()

    # added_by = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    alt_to = models.ManyToManyField("self", blank=True)
    author = models.CharField(help_text="Who created this mod?", max_length=250)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    date_added = models.DateTimeField()
    date_updated = models.DateTimeField()
    description = models.TextField(max_length=300)
    dl_url = models.URLField(help_text="A direct download link for the mod.")
    is_active = models.BooleanField(
        default=True, help_text="Is this mod still active, or is it obsolete/abandoned?"
    )
    name = models.CharField(max_length=250)
    abbrev_name = models.CharField(max_length=250, default="")
    isalpha_name = models.CharField(max_length=250, default="")
    special_to_spaces_name = models.CharField(max_length=250, default="")
    picture = models.URLField(help_text="URL of a picture for this mod.")
    slug = models.SlugField(unique=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=DRAFT)
    compat = models.IntegerField(choices=COMPAT_CHOICES, default=FULLY_WORKING)
    tags = TaggableManager(through="TaggedMod", blank=True)
    url = models.URLField(help_text="Download URL for this mod.")
    custom_folder = models.CharField(max_length=250, null=True)

    class Meta:
        db_table = "mods"
        ordering = ["pk"]
        verbose_name = _("Mod")
        verbose_name_plural = _("Mods")

    def __str__(self):
        return self.name

    @property
    def clean_name(self):
        if self.custom_folder:
            return self.custom_folder
        else:
            if self.slug == "graphic-herbalism-mwse-and-openmw-edition-project-":
                # Special handling because this was just *too* long..
                return "GH-AtlasPatches"
            else:
                return "".join(s for s in self.name if s.isalnum())

    @property
    def compat_string(self):
        if self.compat == 4:
            return self.COMPAT_CHOICES[self.FULLY_WORKING - len(self.COMPAT_CHOICES)][1]
        elif self.compat == 5:
            return self.COMPAT_CHOICES[self.PARTIAL_WORKING - len(self.COMPAT_CHOICES)][
                1
            ]
        elif self.compat == 6:
            return self.COMPAT_CHOICES[self.NOT_WORKING - len(self.COMPAT_CHOICES)][1]
        elif self.compat == 7:
            return self.COMPAT_CHOICES[self.UNKNOWN - len(self.COMPAT_CHOICES)][1]

    @property
    def compat_link(self):
        if self.compat == 4:
            return """<a href="{0}#fully-working">{1}</a>""".format(
                reverse("compatibility"),
                self.COMPAT_CHOICES[self.FULLY_WORKING - len(self.COMPAT_CHOICES)][1],
            )
        elif self.compat == 5:
            return """<a href="{0}#partial-working">{1}</a>""".format(
                reverse("compatibility"),
                self.COMPAT_CHOICES[self.PARTIAL_WORKING - len(self.COMPAT_CHOICES)][1],
            )
        elif self.compat == 6:
            return """<a href="{0}#not-working">{1}</a>""".format(
                reverse("compatibility"),
                self.COMPAT_CHOICES[self.NOT_WORKING - len(self.COMPAT_CHOICES)][1],
            )
        elif self.compat == 7:
            return """<a href="{0}#unknown">{1}</a>""".format(
                reverse("compatibility"),
                self.COMPAT_CHOICES[self.UNKNOWN - len(self.COMPAT_CHOICES)][1],
            )

    def dl_item(self):
        if self.dl_url and self.dl_url.startswith("/files/"):
            return self.dl_url.split("/files/")[1]
        else:
            return self.dl_url

    def get_absolute_url(self):
        return reverse("mod_detail", kwargs={"slug": self.slug})

    def get_status_string(self):
        return self.STATUS_CHOICES[self.status - 1][1]

    def _files(self, type):
        pass

    @property
    def in_lists(self):
        """
        Return a list of ModList objects that this mod is featured in.

        Empty list if none.
        """
        lists = []
        for lm in self.listedmod_set.all():
            if lm.modlist not in lists:
                lists.append(lm.modlist)
        return lists

    @property
    def get_moddir_linux(self):
        return self.get_moddir("/home/username/games/OpenMWMods/", "/")

    @property
    def get_moddir_macos(self):
        return self.get_moddir("/Users/username/games/OpenMWMods/", "/")

    @property
    def get_moddir_windows(self):
        return self.get_moddir("C:\\games\\OpenMWMods\\", "\\")

    def get_moddir(self, base, sep):
        if "/" in self.clean_name:
            return base + self.clean_name.replace("/", sep)
        else:
            return base + self.category.clean_name + sep + self.clean_name


class ModIssue(models.Model):
    existing_mod = models.ForeignKey(Mod, on_delete=models.CASCADE, null=True)
    new_mod = models.TextField(null=True)
    openmw_version = models.CharField(max_length=250)
    compat = models.IntegerField(choices=Mod.COMPAT_CHOICES, default=Mod.FULLY_WORKING)
    submitter_name = models.CharField(max_length=250)
    submitter_email = models.EmailField()
    notes = models.TextField(null=True)
    is_anon = models.BooleanField()
    is_public = models.BooleanField()

    class Meta:
        managed = False


class ModList(models.Model):
    LIVE = 1
    BETA = 2
    INACTIVE = 3
    STATUS_CHOICES = ((LIVE, "Live"), (BETA, "Beta"), (INACTIVE, "Inactive"))

    description = models.TextField(
        default="Mod List Description Goes Here!",
        help_text="The description and/or introduction text for this list.",
    )
    short_description = models.TextField(
        default="Mod List Short Description Goes Here!",
        help_text="A shorter version of the description, for table listings.",
    )
    objects = models.Manager()
    top_level_lists = TopLevelListsManager()

    slug = models.SlugField(unique=True)
    status = models.IntegerField(choices=STATUS_CHOICES, default=BETA)
    title = models.CharField(max_length=250)
    parent_list = models.ForeignKey("ModList", on_delete=models.PROTECT, null=True)
    linux_str = models.TextField(null=True)
    windows_str = models.TextField(null=True)

    # TODO: last updated, which is based on last add/removal/update?

    class Meta:
        db_table = "mod_lists"
        ordering = ["pk"]
        verbose_name = _("Mod List")
        verbose_name_plural = _("Mod Lists")

    def __str__(self):
        c = self.mod_count
        w = "mod"
        if c > 1:
            w += "s"
        return "{t} - {c} {w}".format(c=self.mod_count, t=self.title, w=w)

    def str_no_parent_title(self):
        c = self.mod_count
        w = "mod"
        if c > 1:
            w += "s"
        return "{t}: {c} {w}".format(
            c=self.mod_count,
            t=self.title.replace("{}: ".format(self.parent_list.title), ""),
            w=w,
        )

    @property
    def mod_count(self):
        if self.is_parent:
            count = 0
            for cl in self.modlist_set.all():
                count += cl.listedmod_set.count()
            return count

        else:
            return self.listedmod_set.count()

    @property
    def is_parent(self):
        return self.parent_list is None and self.modlist_set.count() > 0

    @property
    def is_sublist(self):
        return self.parent_list is not None

    def get_absolute_url(self):
        return reverse("mod-list-detail", kwargs={"slug": self.slug})


class SubList(models.Model):
    parent_list = models.ForeignKey(ModList, on_delete=models.PROTECT)
    modlist = models.ForeignKey(
        ModList, on_delete=models.PROTECT, related_name="sublist_modlist"
    )
    order_number = models.IntegerField()

    class Meta:
        db_table = "sublist_lists"
        ordering = ["pk"]
        verbose_name = _("Sublist Lists")
        verbose_name_plural = _("Sublist Lists")

    def __str__(self):
        return "{ls} #{num}: {mod}".format(
            ls=self.parent_list.title, num=self.order_number, mod=self.modlist.title
        )


class ListedMod(models.Model):
    mod = models.ForeignKey(Mod, on_delete=models.PROTECT)
    modlist = models.ForeignKey(ModList, on_delete=models.PROTECT)
    order_number = models.IntegerField(default=0)
    parent_order = models.IntegerField(default=0)
    parent_slug = models.SlugField(default="")

    objects = models.Manager()
    expanded_vanilla = ListedModsExpandedVanillaManager()
    graphics_overhaul = ListedModsGraphicsOverhaulManager()
    i_heart_vanilla = ListedModsIHeartVanillaManager()
    i_heart_vanilla_dc = ListedModsIHeartVanillaDCManager()
    total_overhaul = ListedModsTotalOverhaulManager()
    one_day_modernize = ListedModsOneDayModernizeManager()
    starwind_modded = ListedModsStarwindModdedManager()
    just_good_morrowind = ListedModsJustGoodMorrowindManager()

    class Meta:
        db_table = "listed_mods"
        ordering = ["pk"]
        verbose_name = _("Listed Mod")
        verbose_name_plural = _("Listed Mods")

    def __str__(self):
        return "{ls} #{num} - {mod}".format(
            ls=self.modlist.title, num=self.order_number, mod=self.mod
        )

    @property
    def get_slug(self):
        return self.parent_slug or self.modlist.slug

    @property
    def get_modlist_name(self):
        if self.modlist.is_sublist:
            return ModList.objects.get(slug=self.parent_slug).title

        else:
            return self.modlist.title

    @property
    def ordernum(self):
        return self.parent_order or self.order_number


class ModPlugin(models.Model):
    objects = models.Manager()
    total_overhaul = ModPluginsManagerTotalOverhaul()
    graphics_overhaul = ModPluginsManagerGraphicsOverhaul()
    expanded_vanilla = ModPluginsManagerExpandedVanilla()
    i_heart_vanilla = ModPluginsManagerIHeartVanilla()
    i_heart_vanilla_dc = ModPluginsManagerIHeartVanillaDC()
    oneday_modernize = ModPluginsManager1DayModernize()
    starwind = ModPluginsManagerStarwind()
    just_good_morrowind = ModPluginsManagerJustGoodMorrowind()
    content = ModPluginsManagerContent()
    bsa = ModPluginsManagerBSA()
    groundcover = ModPluginsManagerGroundcover()
    needscleaning = ModPluginsManagerNeedsCleaning()

    file_name = models.CharField(max_length=250)
    for_mod = models.ForeignKey(Mod, on_delete=models.PROTECT, related_name="plugins")
    order_number = models.IntegerField(unique=True)
    on_lists = models.ManyToManyField(ModList, blank=True)
    needs_cleaning = models.BooleanField()
    depends = models.ManyToManyField("ModPlugin", related_name="depends_on")
    conflicts = models.ManyToManyField("ModPlugin", related_name="conflicts_with")
    is_bsa = models.BooleanField()
    is_groundcover = models.BooleanField()

    class Meta:
        db_table = "mod_plugin"
        ordering = ["order_number"]
        verbose_name = _("Mod Plugin")
        verbose_name_plural = _("Mod Plugins")

    def __str__(self):
        return f"{self.order_number} {self.file_name}"

    @property
    def is_content(self):
        return not self.is_bsa and not self.is_groundcover

    @property
    def filename(self):
        if self.needs_cleaning:
            return "Clean_" + self.file_name
        return self.file_name


class DataPath(models.Model):
    objects = models.Manager()
    # ModPlugin managers are actually general enough to work here, so we use it.
    total_overhaul = ModPluginsManagerTotalOverhaul()
    graphics_overhaul = ModPluginsManagerGraphicsOverhaul()
    expanded_vanilla = ModPluginsManagerExpandedVanilla()
    i_heart_vanilla = ModPluginsManagerIHeartVanilla()
    i_heart_vanilla_dc = ModPluginsManagerIHeartVanillaDC()
    oneday_modernize = ModPluginsManager1DayModernize()
    starwind = ModPluginsManagerStarwind()
    just_good_morrowind = ModPluginsManagerJustGoodMorrowind()

    extra_dirs = models.JSONField(null=True)
    for_mod = models.ForeignKey(
        Mod, on_delete=models.PROTECT, related_name="data_paths"
    )
    order_number = models.IntegerField(unique=True)
    on_lists = models.ManyToManyField(ModList, blank=True)
    depends = models.ManyToManyField("DataPath", related_name="path_depends_on")
    conflicts = models.ManyToManyField("DataPath", related_name="path_conflicts_with")
    manual = models.BooleanField(default=False)

    class Meta:
        db_table = "data_path"
        ordering = ["order_number"]
        verbose_name = _("Data Path")
        verbose_name_plural = _("Data Paths")

    def __str__(self):
        return f"{self.order_number}"

    @property
    def windows(self):
        if self.extra_dirs:
            return "\\".join(
                (self.for_mod.get_moddir_windows, "\\".join(self.extra_dirs))
            )
        else:
            return self.for_mod.get_moddir_windows

    @property
    def linux(self):
        if self.extra_dirs:
            return "/".join((self.for_mod.get_moddir_linux, "/".join(self.extra_dirs)))
        else:
            return self.for_mod.get_moddir_linux

    @property
    def macos(self):
        if self.extra_dirs:
            return "/".join((self.for_mod.get_moddir_macos, "/".join(self.extra_dirs)))
        else:
            return self.for_mod.get_moddir_macos


class UsageNotes(models.Model):
    objects = models.Manager()
    total_overhaul = ModPluginsManagerTotalOverhaul()
    graphics_overhaul = ModPluginsManagerGraphicsOverhaul()
    expanded_vanilla = ModPluginsManagerExpandedVanilla()
    i_heart_vanilla = ModPluginsManagerIHeartVanilla()
    i_heart_vanilla_dc = ModPluginsManagerIHeartVanillaDC()
    oneday_modernize = ModPluginsManager1DayModernize()
    starwind = ModPluginsManagerStarwind()

    text = models.TextField(max_length=900)
    generic = models.TextField(max_length=900)
    for_mod = models.ForeignKey(Mod, on_delete=models.PROTECT)
    on_lists = models.ManyToManyField(ModList)

    class Meta:
        db_table = "usage_notes"
        ordering = ["pk"]
        verbose_name = _("Usage Note")
        verbose_name_plural = _("Usage Notes")

    def __str__(self):
        return f"Usage Note #{self.pk} for {self.for_mod}"


class ExtraCfg(models.Model):
    objects = models.Manager()
    total_overhaul = ModPluginsManagerTotalOverhaul()
    graphics_overhaul = ModPluginsManagerGraphicsOverhaul()
    expanded_vanilla = ModPluginsManagerExpandedVanilla()
    i_heart_vanilla = ModPluginsManagerIHeartVanilla()
    i_heart_vanilla_dc = ModPluginsManagerIHeartVanillaDC()
    oneday_modernize = ModPluginsManager1DayModernize()
    starwind = ModPluginsManagerStarwind()
    extra_cfg_nosettings = ExtraCfgManagerTotalOverhaulNoSettings()
    extra_cfg_settings = ExtraCfgManagerTotalOverhaulSettings()

    text = models.TextField(max_length=400)
    for_mods = models.ManyToManyField(Mod, related_name="extra_cfg")
    on_lists = models.ManyToManyField(ModList)
    in_settings = models.BooleanField(default=False)

    class Meta:
        db_table = "extra_cfg"
        ordering = ["pk"]
        verbose_name = _("Extra Config")
        verbose_name_plural = _("Extra Configs")

    def __str__(self):
        return f"Extra Config #{self.pk}"


class Tag(TagBase):
    objects = models.Manager()
    enabled = EnabledTagManager()
    is_enabled = models.BooleanField(default=True)
    description = models.TextField(max_length=300, default="")

    class Meta:
        db_table = "mod_tags"
        ordering = ["pk"]
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")

    @property
    def tagged_mods(self):
        return Mod.live.filter(tags__pk=self.pk)

    @property
    def tag_cloud_size(self):
        return self.total_uses * 0.75

    @property
    def total_uses(self):
        return self.momw_taggedmod_items.count()

    def get_absolute_url(self):
        return reverse("mod_tag_detail", kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        if not self.pk:
            self.name = self.name.strip(",")
        super().save(*args, **kwargs)


class TaggedMod(GenericTaggedItemBase):
    tag = models.ForeignKey(
        Tag, related_name="%(app_label)s_%(class)s_items", on_delete=models.PROTECT
    )

    class Meta:
        db_table = "tagged_mods"


class Changelog(models.Model):
    version = models.CharField(max_length=20)

    class Meta:
        db_table = "changelogs"
        verbose_name = _("Changelog")
        verbose_name_plural = _("Changelog")

    def __str__(self):
        return "Changelog: {}".format(self.version)


class ChangelogEntry(models.Model):
    ADDED = 1
    REMOVED = 2
    UPDATED = 3
    KIND_CHOICES = ((ADDED, "Added"), (REMOVED, "Removed"), (UPDATED, "Updated"))
    kind = models.IntegerField(choices=KIND_CHOICES, default=ADDED)
    modlists = models.ManyToManyField(ModList)
    text = models.TextField()
    changelog = models.ForeignKey(Changelog, on_delete=models.PROTECT)
    date = models.DateField()

    class Meta:
        db_table = "changelog_entries"
        ordering = ["pk"]
        verbose_name = _("Changelog Entry")
        verbose_name_plural = _("Changelog Entries")

    def __str__(self):
        return "{} {}: {}".format(
            self.KIND_CHOICES[self.kind - 1][1].upper(), self.date, self.text
        )

    @property
    def fulltext(self):
        return '<span class="bold"><span class="{}">{}</span> {}</span>: {}'.format(
            self.KIND_CHOICES[self.kind - 1][1].lower(),
            self.KIND_CHOICES[self.kind - 1][1].upper(),
            self.date,
            self.text,
        )

    @property
    def getkind(self):
        return self.KIND_CHOICES[self.kind - 1][1]
