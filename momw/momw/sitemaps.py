from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from chroniko.models import BlogEntry
from media.models import MediaTrack
from .models import Category, Mod, ModList, Tag


class BlogEntrySitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return BlogEntry.live.all()

    def lastmod(self, obj):
        return obj.date_added


class CategorySitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.8

    def items(self):
        return Category.objects.all()


class MediaSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.6

    def items(self):
        return MediaTrack.live.all()

    def lastmod(self, obj):
        return obj.date_added


class ModSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        return Mod.live.all()

    def lastmod(self, obj):
        return obj.date_added


class ModListSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        return ModList.objects.all()


class ModListStepsSitemap(Sitemap):
    changefreq = "weekly"
    priority = 1.0

    def items(self):
        list_urls = []

        for modlist in ModList.objects.all():
            for step in range(1, modlist.mod_count + 1):
                list_urls.append(
                    "/lists/{slug}/{step}/".format(slug=modlist.slug, step=step)
                )

        return list_urls

    def location(self, item):
        return item


class StaticPageSiteMap(Sitemap):
    changefreq = "weekly"
    priority = 0.7

    def items(self):
        return [
            "index",
            "about",
            "cfg_generator",
            "changelogs",
            "changelogs-website",
            "cookie",
            "faq",
            "mod_feed",
            "enable-in-cfg-generator-button",
            "guides-users",
            "guides-modders",
            "guides-developers",
            "getting_started",
            "gs-buy",
            "gs-install",
            "gs-settings",
            "gs-directories",
            "gs-tips",
            "guides",
            "guides-developers",
            "guides-modders",
            "guides-users",
            "load_order",
            "mod-lists",
            "mod_index",
            "mod_todo",
            "all_mods",
            "latest_mods",
            "mod_categories",
            "mod_tags",
            "add-a-list",
            "privacy",
            "resources",
            "site-versions",
            "tips",
            "tips-atlased-meshes",
            "tips-cleaning",
            "tips-creating-custom-groundcover",
            "tips-file-renames",
            "tips-ini-importer",
            "tips-merging-objects",
            "tips-navmeshtool",
            "tips-openmw-physics-fps",
            "tips-performance",
            "tips-portable-install",
            "tips-register-bsas",
            "tips-shiny-meshes",
            "tips-custom-shaders",
        ]

    def location(self, item):
        return reverse(item)


class TagSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.9

    def items(self):
        return Tag.objects.all()


sitemap_dict = {
    "mods": ModSitemap,
    "mod_lists": ModListSitemap,
    "mod_lists_steps": ModListStepsSitemap,
    "mod_tags": TagSitemap,
    "mod_categories": CategorySitemap,
    "static_pages": StaticPageSiteMap,
    "media": MediaSitemap,
    "blog": BlogEntrySitemap,
}
