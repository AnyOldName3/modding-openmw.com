from django.shortcuts import get_object_or_404, render
from ..forms import CfgGeneratorPresetForm
from ..models import DataPath, ExtraCfg, ListedMod, ModList, ModPlugin


parent_lists = {
    "one day modernize": True,
    "graphics overhaul": True,
    "total overhaul": True,
    "expanded vanilla": True,
}


def cfg_blank(request, t="cfg_generator.html"):
    return render(request, t, {})


def cfg_generator(request, preset, t="cfg_generator.html"):
    queried = False
    if preset:
        queried = True
    fallback_archives = []
    data_paths = []
    extra_cfg_settings = []
    extra_cfg_nosettings = []
    groundcover = []
    plugins = []
    used_mods = []
    used_mods_by_sublist = {}
    has_openmw_cfg = False
    has_settings_cfg = False
    preset_form = CfgGeneratorPresetForm(request.POST)
    used_select = False

    # A preset button was clicked.
    if preset_form.is_valid():
        if request.method == "POST":
            mlist = [*request.POST.dict().keys()][1:]
            if preset == "one day modernize":
                modlist = get_object_or_404(
                    ModList, slug="one-day-morrowind-modernization"
                )
            elif preset == "i heart vanilla dc":
                modlist = get_object_or_404(
                    ModList, slug="i-heart-vanilla-directors-cut"
                )
            else:
                modlist = get_object_or_404(ModList, slug=preset.replace(" ", "-"))
            mlist.insert(0, "morrowind")
            plugins = ModPlugin.objects.filter(
                is_bsa=False,
                is_groundcover=False,
                for_mod__slug__in=mlist,
                on_lists=modlist,
            )
            groundcover = ModPlugin.objects.filter(
                is_groundcover=True, for_mod__slug__in=mlist, on_lists=modlist
            )
            fallback_archives = ModPlugin.objects.filter(
                is_bsa=True, for_mod__slug__in=mlist, on_lists=modlist
            )
            data_paths = DataPath.objects.filter(
                for_mod__slug__in=mlist, on_lists=modlist
            )
            extra_cfg_nosettings = ExtraCfg.extra_cfg_nosettings.filter(
                for_mods__slug__in=mlist, on_lists=modlist
            )
            extra_cfg_settings = ExtraCfg.extra_cfg_settings.filter(
                for_mods__slug__in=mlist, on_lists=modlist
            ).order_by("text")

            used_mods_by_sublist = {}
            if modlist.is_parent:
                for sublist in modlist.modlist_set.all():
                    used_mods_by_sublist.update({sublist: []})
                    for m in ListedMod.objects.select_related("mod").filter(
                        modlist=sublist
                    ):
                        used_mods_by_sublist[sublist].append(m)
            else:
                used_mods = ListedMod.objects.filter(modlist__slug=modlist.slug)

            if extra_cfg_settings:
                has_settings_cfg = True
            if (
                extra_cfg_nosettings
                or plugins
                or groundcover
                or fallback_archives
                or data_paths
            ):
                has_openmw_cfg = True

        elif preset:
            modlist = get_object_or_404(ModList, slug=preset)

            has_openmw_cfg = True
            has_settings_cfg = True
            plugins = ModPlugin.objects.filter(
                is_bsa=False, is_groundcover=False, on_lists=modlist
            )
            groundcover = ModPlugin.objects.filter(
                is_groundcover=True, on_lists=modlist
            )
            fallback_archives = ModPlugin.objects.filter(is_bsa=True, on_lists=modlist)
            data_paths = DataPath.objects.filter(on_lists=modlist)
            extra_cfg_nosettings = ExtraCfg.extra_cfg_nosettings.filter(
                on_lists=modlist
            )
            extra_cfg_settings = ExtraCfg.extra_cfg_settings.filter(
                on_lists=modlist
            ).order_by("text")

            # TODO: if it's a list with sublists, send mods grouped by sublist
            used_mods_by_sublist = {}
            if modlist.is_parent:
                for sublist in modlist.modlist_set.all():
                    used_mods_by_sublist.update({sublist: []})
                    for m in ListedMod.objects.select_related("mod").filter(
                        modlist=sublist
                    ):
                        used_mods_by_sublist[sublist].append(m)
            else:
                used_mods = ListedMod.objects.filter(modlist__slug=modlist.slug)

    used_settings = []
    if extra_cfg_settings:
        for e in extra_cfg_settings:
            if e.text not in used_settings:
                used_settings.append(e.text)

    return render(
        request,
        t,
        {
            "used_select": used_select,
            "preset": preset,
            "preset_title": modlist.title,
            "fallback_archives": fallback_archives,
            "data_paths": data_paths,
            "extra_cfg_nosettings": extra_cfg_nosettings,
            "groundcover": groundcover,
            "plugins": plugins,
            "queried": queried,
            "used_mods": used_mods,
            "has_settings_cfg": has_settings_cfg,
            "has_openmw_cfg": has_openmw_cfg,
            "used_settings": used_settings,
            "used_mods_by_sublist": used_mods_by_sublist,
        },
    )
