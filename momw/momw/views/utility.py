from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.views.decorators.cache import cache_page


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def robots_txt(request):
    return HttpResponse(
        """User-agent: *
Disallow: /""",
        content_type="text/plain",
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def webmanifest(request):
    return HttpResponse(
        """{
    "background_color": "#000",
    "description": "A website dedicated to modding and modernizing Morrowind with OpenMW!",
    "display": "standalone",
    "icons": [
        {
            "sizes":"256x256",
            "src":"/openmw.png",
            "type":"image/png"
        }
    ],
    "name": "Modding-OpenMW.com",
    "start_url": "/"
}""",
        content_type="application/json",
    )


def legacy_url_redirect(request, new_slug, new_view="mod_detail", **kwargs):
    return redirect(new_view, slug=new_slug, **kwargs)


def static_view(request, template):
    return render(request, template)
