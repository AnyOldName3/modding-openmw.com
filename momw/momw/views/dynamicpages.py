import datetime
import pytz

from django.conf import settings
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from ..helpers import get_visitor_os, paginator_for_queryset
from ..models import (
    Category,
    Changelog,
    ChangelogEntry,
    ListedMod,
    Mod,
    ModList,
    ModPlugin,
    DataPath,
    ExtraCfg,
    UsageNotes,
    Tag,
)


CLEAN_EXCEPTIONS = ("Better Robes.ESP", "Quill of Feyfolken 2.0-OpenMW-Patch.omwaddon")


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def all_mods(request):
    mods = Mod.live.all().order_by("name")
    return render(request, "all_mods.html", {"mods": mods})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def all_plugins(request):
    return render(request, "all_plugins.html", {"plugins": ModPlugin.content.all()})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def all_fallback_archives(request):
    return render(
        request, "all_plugins.html", {"fallback_archives": ModPlugin.bsa.all()}
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def all_groundcover(request):
    return render(
        request, "all_plugins.html", {"groundcover": ModPlugin.groundcover.all()}
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def categories(request):
    categories = Category.used.all().order_by("title")
    return render(request, "categories.html", {"categories": categories})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def category_detail(request, slug):
    category = get_object_or_404(Category, slug=slug)
    cat_mods = category.mod_set.all().order_by("name")
    return render(
        request,
        "mod_category_detail.html",
        {"category": category, "cat_mods": cat_mods},
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def compat_unknown(request):
    mods = Mod.compat_unknown.all().order_by("name")
    return render(
        request,
        "compat_lists.html",
        {
            "mods": mods,
            "status": "of unknown compatibility",
            "title": "Compatibility: Unknown",
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def fully_working(request):
    mods = Mod.fully_working.all().order_by("name")
    return render(
        request,
        "compat_lists.html",
        {
            "mods": mods,
            "status": "known to be fully working with OpenMW",
            "title": "Compatibility: Fully Working",
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def example_mod_page(request):
    class Category:
        def __init__(self, name, slug):
            self.name = name
            self.slug = slug

        def __str__(self):
            return self.name

    class Mod:
        def __init__(
            self,
            name,
            author,
            description,
            category,
            is_active,
            compat_link,
            date_added,
            date_updated,
            dl_item,
            dl_url,
            tags,
            alt_to=None,
        ):
            self.name = name
            self.author = author
            self.description = description
            self.category = category
            self.is_active = is_active
            self.compat_link = compat_link
            self.alt_to = alt_to
            self.date_added = date_added
            self.date_updated = date_updated
            self.dl_item = dl_item
            self.dl_url = dl_url
            self.tags = tags

        def __str__(self):
            return self.name

    class ModPlugin:
        def __init__(self, file_name, is_bsa=False, is_groundcover=False, clean=False):
            self.file_name = file_name
            self.filename = file_name
            self.is_bsa = is_bsa
            self.is_groundcover = is_groundcover

    class DataPath:
        def __init__(self, path):
            self.path = path

        def __str__(self):
            return self.path

    class UsageNotes:
        def __init__(self, text, for_mod, on_lists):
            self.generic = text
            self.text = text
            self.for_mod = for_mod
            self.on_lists = on_lists

    cat1 = Category("Example Category", "example-category")
    tag1 = Category("Example Tag", "example-tag")

    mod2 = Mod(
        "Example Mod 2",
        "Change Me Author!",
        "Change me Description!",
        cat1,
        True,
        "This field describes if the mod is compatible with OpenMW or not (not if the mod has bugs or not)",
        "2023-09-02 11:57:00 -0500",
        "2023-09-03 11:57:00 -0500",
        "ModFileName.zip",
        "#ModFileName.zip",
        [tag1],
    )

    mod = Mod(
        "How to use Modding-OpenMW.com",
        "The mod author or authors.",
        "A description of this mod.",
        cat1,
        True,
        "This field describes if the mod is compatible with OpenMW or not (not if the mod has bugs or not)",
        "2023-09-02 11:55:00 -0500",
        "2023-09-03 11:55:00 -0500",
        "ModFileName.zip",
        "#ModFileName.zip",
        tag1,  # TODO: do tags right
        [mod2],  # TODO do alt_to right
    )

    usage_notes_text = """This field will contain any relevant usage notes for this mod, including but not limited to special install instructions."""

    return render(
        request,
        "mod_detail.html",
        {
            "example_mod_page": True,
            "lm_set_count": 1,
            "mod": mod,
            "mod_is_settings": False,
            "needs_cleaning": True,
            "plugins": [ModPlugin("ExamplePlugin.esp")],
            "groundcover": [ModPlugin("ExampleGroundcover.esp", is_groundcover=True)],
            "bsas": [ModPlugin("ExampleArchive.bsa", is_bsa=True)],
            "visitor_os": "windows",
            "usage_notes": [UsageNotes(usage_notes_text, mod, None)],
            "extra_cfg": [],
            "data_paths": [
                DataPath("C:\\games\\OpenMWMods\\Quests\\ExampleMod\\00 Path 0"),
                DataPath("C:\\games\\OpenMWMods\\Quests\\ExampleMod\\01 Path 1"),
            ],
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def gs_directories(request):
    paths = ""
    modlist = request.GET.get("modlist")
    os = request.GET.get("os")
    if modlist and os:
        ml = ModList.objects.get(slug=modlist)
        if ml.is_parent:
            for sublist in ModList.objects.filter(slug__startswith=ml.slug):
                if os == "linux" or os == "macos":
                    if sublist.linux_str:
                        paths += sublist.linux_str
                else:
                    if sublist.windows_str:
                        paths += sublist.windows_str
        else:
            if os == "linux" or os == "macos":
                paths = ml.linux_str
            else:
                paths += ml.windows_str

    return render(
        request,
        "gs_directories.html",
        {"paths": paths, "modlist": modlist or None, "os": os or None},
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def index(request):
    mod_lists = ModList.top_level_lists.all()
    to = mod_lists.get(slug="total-overhaul")
    ev = mod_lists.get(slug="expanded-vanilla")
    ihv = mod_lists.get(slug="i-heart-vanilla")
    return render(
        request,
        "index.html",
        {
            "hostname": settings.PROJECT_HOSTNAME,
            "to_count": to.mod_count,
            "ihv_count": ihv.mod_count,
            "ev_count": ev.mod_count,
            "mod_list_count": mod_lists.count(),
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def users_guide(request):
    mod_lists = ModList.top_level_lists.all()
    return render(request, "guides_users.html", {"mod_lists": mod_lists})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def latest_mods(request):
    latest_count = 100
    return render(
        request,
        "latest_mods.html",
        {"latest_count": latest_count, "mods": Mod.latest.all()[:latest_count]},
    )


# TODO: Make this view show mods that arent in lists perhaps
@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_alts(request):
    alt_mods = Mod.alts.all()
    return render(request, "mod_alts.html", {"mods": alt_mods})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_detail(request, slug):
    mod = get_object_or_404(Mod, slug=slug)
    needs_cleaning = False
    visitor_os = get_visitor_os(request)
    data_paths = []

    for p in DataPath.objects.filter(for_mod=mod).order_by("extra_dirs"):
        if visitor_os == "linux":
            data_paths.append(p.linux)
        elif visitor_os == "macos":
            data_paths.append(p.macos)
        elif visitor_os == "windows":
            data_paths.append(p.windows)
        else:
            data_paths.append(p.windows)

    plugins = ModPlugin.content.filter(for_mod=mod)
    for plugin in plugins:
        if plugin.needs_cleaning:
            needs_cleaning = True
            break

    usage_notes = []
    u = UsageNotes.objects.filter(for_mod=mod)
    for un in u:
        if un.generic:
            usage_notes.append(un)

    return render(
        request,
        "mod_detail.html",
        {
            "lm_set_count": mod.listedmod_set.count(),
            "mod": mod,
            "mod_is_settings": mod.category.slug == "settings-tweaks",
            "needs_cleaning": needs_cleaning,
            "plugins": plugins,
            "groundcover": ModPlugin.groundcover.filter(for_mod=mod),
            "bsas": ModPlugin.bsa.filter(for_mod=mod),
            "visitor_os": visitor_os,
            "usage_notes": usage_notes,
            "extra_cfg": ExtraCfg.objects.filter(for_mods__in=[mod]),
            "data_paths": data_paths,
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_list_detail(request, slug):
    # TODO: maybe select related on parent_list too?
    modlist = get_object_or_404(ModList, slug=slug)
    modlist_mod_count = modlist.mod_count
    modlist_is_parent = modlist.is_parent

    if modlist_is_parent:
        listed_mods = {}
        for sublist in modlist.modlist_set.all():
            listed_mods.update({sublist: []})
            # TODO: This is an expensive set of queries, how to reduce cost?
            for m in ListedMod.objects.select_related("mod").filter(modlist=sublist):
                listed_mods[sublist].append(m)

    else:
        listed_mods = ListedMod.objects.select_related("mod").filter(modlist=modlist)

    return render(
        request,
        "mod_list_detail.html",
        {
            "listed_mods": listed_mods,
            "mod_list": modlist,
            "modlist_mod_count": modlist_mod_count,
            "modlist_is_parent": modlist_is_parent,
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_list_changelog(request, slug):
    c = []
    ml = get_object_or_404(ModList, slug=slug)
    change_count = 0
    for changelog in Changelog.objects.all():
        entries = ChangelogEntry.objects.select_related("changelog").filter(
            modlists__in=[ml], changelog__version=changelog.version
        )
        c.append(
            {"count": len(entries), "version": changelog.version, "entries": entries}
        )
        change_count += len(entries)

    return render(
        request,
        "mod_list_changelog.html",
        {
            "c": c,
            "change_count": change_count,
            "mod_list_slug": ml.slug,
            "mod_list_title": ml.title,
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_list_final(request, slug):
    listed_mods: list = []
    modlist = get_object_or_404(ModList, slug=slug)
    if modlist.slug == "tes3mp-server":
        raise Http404
    to_clean: list = []

    if modlist.title.startswith("Total Overhaul"):
        listed_mods = Mod.total_overhaul.all()
    elif modlist.title.startswith("Graphics Overhaul"):
        listed_mods = Mod.graphics_overhaul.all()
    elif modlist.title.startswith("Expanded Vanilla"):
        listed_mods = Mod.expanded_vanilla.all()

    # Starwind mods generally don't get cleaned... do they!?

    elif modlist.title.startswith("One Day Morrowind Modernization"):
        listed_mods = Mod.one_day_modernize.all()

    elif modlist.title == "I Heart Vanilla":
        listed_mods = Mod.i_heart_vanilla.all()

    elif modlist.title == "I Heart Vanilla: Director's Cut":
        listed_mods = Mod.i_heart_vanilla_dc.all()

    for m in listed_mods:
        for p in m.plugins.all():
            if p.needs_cleaning:
                to_clean.append(p.file_name)

    return render(
        request, "mod_list_final.html", {"mod_list": modlist, "to_clean": to_clean}
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_list_step_detail(request, slug, step):
    mod_next = False
    mod_prev = False
    needs_cleaning = False

    # START Legacy URL support
    # TODO: I need a test for this... but I'm not sure how to test GET args...
    _step = request.GET.get("step", step)
    if _step and request.path.startswith("/mods/steps/"):
        # Handle more BS requests from BingBot
        try:
            step = int(_step)
        except ValueError:
            step = 1  # just setting this to one since I'm getting endlessly spammed about this forsaken path
        # Send requests like /mods/steps/?step=142 to /lists/total-overhaul/142/
        return redirect("mod-list-step-detail", slug=slug, step=step)
    # END Legacy URL support

    modlist = get_object_or_404(ModList, slug=slug)
    # Do this once to reduce queries!!!!
    modlist_mod_count = modlist.mod_count
    modlist_is_parent = modlist.is_parent

    if modlist_is_parent:
        # Handle bunk order numbers
        if _step > modlist_mod_count:
            _step = modlist_mod_count

        # Don't try to serve step numbers that are less than one
        if _step < 1:
            _step = 1

        sl = None
        # Get the sublist, for querying later
        step_count = 0
        for sublist in modlist.modlist_set.all():  # 58 queries
            step_count += sublist.mod_count
            if step_count >= step:
                sl = sublist
                break

        if not sl:
            # This is a nonexistent step number
            raise Http404

        # Use select_related() to reduce query counts.....
        lm = (
            ListedMod.objects.select_related("mod")
            .select_related("modlist")
            .get(parent_order=_step, modlist=sl)
        )
        lm_set_count = lm.mod.listedmod_set.count()
        lm_parent_order = lm.parent_order
        modlist_mods = (
            ListedMod.objects.select_related("mod").select_related("modlist")
            # TODO: This filter is another potential optimization point...
            .filter(modlist__in=modlist.modlist_set.all())
        )

        pag_range, pag_prev, pag_next = paginator_for_queryset(
            request, modlist_mods, _step
        )
        if lm_parent_order > 1:
            mod_prev = lm_parent_order - 1
        if lm_parent_order < modlist_mod_count:
            mod_next = lm_parent_order + 1
        if modlist_mod_count > 21:
            show_final = len(pag_range) <= 20
        else:
            show_final = _step >= (modlist_mod_count - 9)

    else:
        # This is a monolithic (old-style) list with no sublists.
        lm = get_object_or_404(ListedMod, modlist=modlist, order_number=_step)
        lm_set_count = lm.mod.listedmod_set.count()
        modlist_mods = ListedMod.objects.select_related("mod").filter(modlist=modlist)
        # Handle bunk order numbers
        if _step > modlist_mod_count:
            _step = modlist_mod_count
            lm = get_object_or_404(ListedMod, modlist=modlist, order_number=_step)

        pag_range, pag_prev, pag_next = paginator_for_queryset(
            request, modlist_mods, _step
        )

        if lm.order_number > 1:
            mod_prev = lm.order_number - 1

        if lm.order_number < modlist_mod_count:
            mod_next = lm.order_number + 1

        if modlist_mod_count > 21:
            show_final = len(pag_range) <= 20
        else:
            show_final = _step >= (modlist_mod_count - 9)

    lm_is_settings = lm.mod.category.slug == "settings-tweaks"
    if modlist.slug == "tes3mp-server":
        show_final = False

    mod = lm.mod
    plugins = ModPlugin.content.filter(for_mod=mod, on_lists=modlist)
    bsas = ModPlugin.bsa.filter(for_mod=mod, on_lists=modlist)
    groundcover = ModPlugin.groundcover.filter(for_mod=mod, on_lists=modlist)
    visitor_os = get_visitor_os(request)
    data_paths = []
    db_paths = DataPath.objects.filter(for_mod=mod, on_lists=modlist)

    if db_paths:
        for p in db_paths:
            if visitor_os == "linux":
                data_paths.append(p.linux)
            elif visitor_os == "macos":
                data_paths.append(p.macos)
            elif visitor_os == "windows":
                data_paths.append(p.windows)
            else:
                data_paths.append(p.windows)
    else:
        if visitor_os == "linux":
            data_paths.append(mod.get_moddir_linux)
        elif visitor_os == "macos":
            data_paths.append(mod.get_moddir_macos)
        elif visitor_os == "windows":
            data_paths.append(mod.get_moddir_windows)

    for plugin in plugins:
        if plugin.needs_cleaning:
            needs_cleaning = True
            break

    return render(
        request,
        "mod_detail.html",
        {
            "listed_mod": lm,
            "mod_is_settings": lm_is_settings,
            "lm_set_count": lm_set_count,
            "mod": mod,
            "mod_list": modlist,
            "mod_next": mod_next,
            "mod_prev": mod_prev,
            "modlist_is_parent": modlist_is_parent,
            "plugins": plugins,
            "needs_cleaning": needs_cleaning,
            "groundcover": groundcover,
            "bsas": bsas,
            "data_paths": data_paths,
            "extra_cfg": ExtraCfg.objects.filter(for_mods__in=[mod]),
            "usage_notes": UsageNotes.objects.filter(for_mod=mod, on_lists=modlist),
            "pag_next": pag_next,
            "pag_prev": pag_prev,
            "pag_range": pag_range,
            "show_final": show_final,
            "visitor_os": visitor_os,
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def mod_lists(request):
    return render(
        request,
        "mod_lists.html",
        {
            "mod_lists": ModList.objects.filter(parent_list=None),
            "wabbajack_lists": Mod.objects.filter(
                category=Category.objects.get(slug="wabbajack-lists")
            ),
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def no_plugin(request):
    no_plugin = Mod.no_plugins.all()
    return render(request, "no_plugin.html", {"mods": no_plugin})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def not_working(request):
    mods = Mod.not_working.all().order_by("name")
    return render(
        request,
        "compat_lists.html",
        {
            "mods": mods,
            "status": "known to not work with OpenMW",
            "title": "Compatibility: Not Working",
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def partial_working(request):
    mods = Mod.partial_working.all().order_by("name")
    return render(
        request,
        "compat_lists.html",
        {
            "mods": mods,
            "status": "known to be partially working with OpenMW, some tweaks may be needed",
            "title": "Compatibility: Partially Working",
        },
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def tag_detail(request, slug):
    tag = get_object_or_404(Tag, slug=slug)
    tag_mods = tag.tagged_mods.all().order_by("name")
    return render(request, "mod_tag_detail.html", {"tag": tag, "tag_mods": tag_mods})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def tags(request):
    tags = Tag.objects.all().order_by("name")
    return render(request, "tags.html", {"tags": tags})


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def tips_cleaning(request):
    plugin_names = []
    for p in ModPlugin.needscleaning.all():
        plugin_names.append(p.file_name)
    return render(
        request, "tips_cleaning_with_tes3cmd.html", {"plugin_names": plugin_names}
    )


@cache_page(settings.CACHE_MIDDLEWARE_SECONDS)
def with_plugin(request):
    with_plugin = Mod.with_plugins.all()
    return render(request, "with_plugin.html", {"mods": with_plugin})


def ip_address(request):
    xff = request.META.get("HTTP_X_FORWARDED_FOR")
    if xff:
        ip = xff.split(",")[0]
    else:
        ip = request.META.get("REMOTE_ADDR")
    return render(request, "ip.html", {"ip": ip})


@csrf_exempt
def rn(request):
    fmt = "%Y-%m-%d %H:%M:%S"
    tz = pytz.timezone("UTC")
    if request.method == "POST":
        t = request.POST.get("tz")
        if t:
            try:
                tz = pytz.timezone(t)
                print("valid timezone accepted")
            except pytz.UnknownTimeZoneError:
                print("bunk timezone rejected; using UTC")
    return HttpResponse(datetime.datetime.now(tz=tz).strftime(fmt))


def user_settings(request):
    return render(request, "settings.html", {"visitor_os": get_visitor_os(request)})
