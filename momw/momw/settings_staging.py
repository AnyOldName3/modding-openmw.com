from .settings_server import *  # NOQA

MOMW_VER = False
PROJECT_HOSTNAME = "staging.modding-openmw.com"
SITE_NAME = "staging.Modding-OpenMW.com"
USE_ROBOTS = True
SERVER_EMAIL = f"app@{SITE_NAME}"
