import sys

from momw.helpers import read_yaml_data
from momw.models import Mod, ModList, DataPath


def data_paths():
    """
    Read a YAML file for a list of mod names to assemble a global data path
    load order based on DataPathLoadOrder objects.
    """
    ihvdc = ModList.objects.get(slug="i-heart-vanilla-directors-cut")
    modlist_map = {
        "i-heart-vanilla": ModList.objects.get(slug="i-heart-vanilla"),
        "i-heart-vanilla-dc": ihvdc,
        "i-heart-vanilla-directors-cut": ihvdc,
        "one-day-morrowind-modernization": ModList.objects.get(
            slug="one-day-morrowind-modernization"
        ),
        "graphics-overhaul": ModList.objects.get(slug="graphics-overhaul"),
        "total-overhaul": ModList.objects.get(slug="total-overhaul"),
        "expanded-vanilla": ModList.objects.get(slug="expanded-vanilla"),
        "starwind-modded": ModList.objects.get(slug="starwind-modded"),
        "just-good-morrowind": ModList.objects.get(slug="just-good-morrowind"),
    }

    order_data = read_yaml_data("data-path-order")

    count = 1
    for d in order_data:
        try:
            mod = Mod.objects.get(name=d["for_mod"])

            dp = DataPath(
                **{
                    "for_mod": mod,
                    "order_number": count,
                }
            )

            if "extra_dirs" in d:
                dp.extra_dirs = d["extra_dirs"]

            if "manual" in d:
                dp.manual = d["manual"]

            dp.save()

            if "on_lists" in d:
                on_lists = []

                for slug in d["on_lists"]:
                    try:
                        on_lists.append(modlist_map[slug])
                    except KeyError:
                        print("ERROR: modlist for data path doesn't exist:", slug)
                        sys.exit(1)

                for l in on_lists:
                    dp.on_lists.add(l)

            count += 1

        except Mod.DoesNotExist:
            print("ERROR: mod for data path doesn't exist:", d["for_mod"])
            sys.exit(1)
