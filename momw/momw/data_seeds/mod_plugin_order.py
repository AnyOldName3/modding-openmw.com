import sys

from momw.helpers import read_yaml_data
from momw.models import Mod, ModList, ModPlugin


# TODO: handle this broken data case:
# - for_mod: Repopulated Morrowind
#   file_name: RepopulatedMorrowind_6thHouseObsidWeps.esp
#     - "total-overhaul"
#     - "expanded-vanilla"
# The array is supposed to be under "on_lists"


def process_conflicts_and_deps(data, quiet, testing):
    for d in data:
        if "depends" in d:
            if "for_mod" in d:
                try:
                    m1 = Mod.objects.get(name=d["for_mod"])
                except Mod.DoesNotExist:
                    if not quiet:
                        print(
                            f"ERROR: mod '{d['for_mod']}' for plugin '{d['file_name']}' doesn't exist"
                        )
            p = ModPlugin.objects.get(file_name=d["file_name"], for_mod=m1)
            depends = []
            for dep in d["depends"]:
                if "for_mod" in dep:
                    try:
                        m = Mod.objects.get(name=dep["for_mod"])
                    except Mod.DoesNotExist:
                        if not quiet:
                            print(
                                f"ERROR: dep mod '{dep['for_mod']}' for plugin '{d['file_name']}' doesn't exist"
                            )
                        if not testing:
                            sys.exit(1)

                try:
                    # TODO: need to get the Mod in advance (wrapped in a try/except) and then pass it
                    if "for_mod" in dep:
                        depends.append(
                            ModPlugin.objects.get(file_name=dep["file_name"], for_mod=m)
                        )

                    else:
                        depends.append(
                            ModPlugin.objects.get(file_name=dep["file_name"])
                        )
                except ModPlugin.DoesNotExist:
                    if not quiet:
                        print(
                            f"ERROR: dependency '{dep['file_name']}' for plugin '{p.file_name}' doesn't exist"
                        )
                    if not testing:
                        sys.exit(1)
                except ModPlugin.MultipleObjectsReturned:
                    if not quiet:
                        print(f"ERROR: multiple mods found for plugin '{p.file_name}'")
                        print(ModPlugin.objects.filter(file_name=dep["file_name"]))
                    if not testing:
                        sys.exit(1)

            if depends:
                for dep in depends:
                    p.depends.add(dep)

        if "conflicts" in d:
            conflicts = []
            for dep in d["conflicts"]:
                try:
                    if "for_mod" in dep:
                        conflicts.append(
                            ModPlugin.objects.get(
                                file_name=dep["file_name"], for_mod=dep["for_mod"]
                            )
                        )
                    else:
                        conflicts.append(
                            ModPlugin.objects.get(file_name=dep["file_name"])
                        )
                except ModPlugin.DoesNotExist:
                    if not quiet:
                        print(
                            f"ERROR: dependency '{dep['file_name']}' for plugin '{p.file_name}' doesn't exist"
                        )
                    if not testing:
                        sys.exit(1)

            if conflicts:
                for conflict in conflicts:
                    p.conflicts.add(conflict)


def process_plugin_data(data, bsa, groundcover, quiet, testing):
    ihvdc = ModList.objects.get(slug="i-heart-vanilla-directors-cut")
    modlist_map = {
        "i-heart-vanilla": ModList.objects.get(slug="i-heart-vanilla"),
        "i-heart-vanilla-dc": ihvdc,
        "i-heart-vanilla-directors-cut": ihvdc,
        "one-day-morrowind-modernization": ModList.objects.get(
            slug="one-day-morrowind-modernization"
        ),
        "graphics-overhaul": ModList.objects.get(slug="graphics-overhaul"),
        "total-overhaul": ModList.objects.get(slug="total-overhaul"),
        "expanded-vanilla": ModList.objects.get(slug="expanded-vanilla"),
        "starwind-modded": ModList.objects.get(slug="starwind-modded"),
        "just-good-morrowind": ModList.objects.get(slug="just-good-morrowind"),
    }

    count = ModPlugin.objects.count() + 1
    for d in data:
        new_plugin = {
            "file_name": d["file_name"],
            "order_number": count,
            "is_bsa": bsa,
            "is_groundcover": groundcover,
        }

        if "needs_cleaning" in d:
            new_plugin.update({"needs_cleaning": d["needs_cleaning"]})
        else:
            new_plugin.update({"needs_cleaning": False})

        if "for_mod" in d:
            try:
                new_plugin.update({"for_mod": Mod.objects.get(name=d["for_mod"])})
            except Mod.DoesNotExist:
                if not quiet:
                    print(
                        f"ERROR: mod '{d['for_mod']}' for plugin '{d['file_name']}' doesn't exist"
                    )
                if not testing:
                    sys.exit(1)

        p = ModPlugin(**new_plugin)
        p.save()

        if "on_lists" in d:
            on_lists = []
            for slug in d["on_lists"]:
                try:
                    on_lists.append(modlist_map[slug])
                except KeyError:
                    if not quiet:
                        print(
                            f"ERROR: modlist {slug} for plugin {d['file_name']} doesn't exist"
                        )
                    if not testing:
                        sys.exit(1)

            if on_lists:
                for ml in on_lists:
                    p.on_lists.add(ml)

        count += 1


def plugins_load_order(quiet=False, testing=False):
    """
    Read a YAML or TOML file for a list of mod names to assemble a global
    plugin load order.

    Plugins are assigned to their respective mod with an integer key that
    represents their positioning in the global load order.
    """
    plugin_data = read_yaml_data("plugin-order")
    process_plugin_data(plugin_data, False, False, quiet, testing)
    process_conflicts_and_deps(plugin_data, quiet, testing)

    process_plugin_data(read_yaml_data("bsa-order"), True, False, quiet, testing)
    process_plugin_data(
        read_yaml_data("groundcover-order"), False, True, quiet, testing
    )
