title = "One Day Morrowind Modernization"
description = """<div class="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/wwwaXcuaUOo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

<p>Modernize Morrowind with a reasonably short but effective list that not only ups the visual ante but also adds a fair amount of gameplay content. The primary goal of this list is to be less time-consuming than other lists while also offering a highly rewarding end result.</p>

<p>This list has an accompanying video walkthrough that covers the entire list (see above), featuring Rob of <a href="https://www.youtube.com/c/RobsRedHotSpot/videos">Rob's Red Hot Spot</a> and your neighborhood friendly website admin!</p>

<p>This list is also forward compatible with the <a href="/lists/total-overhaul/">Total Overhaul</a> modlist for those folks who want to take a bit of a deeper dive.</p>"""
short_description = "Modernize Morrowind with a reasonably short but effective list that not only ups the visual ante but also adds a fair amount of gameplay content."

[[sublists]]
title = "Patches"
description = "Alter some aspect of the game in order to fix a bug, or to be more consistent with the game itself. Patches usually work via a plugin."
mods = [
  "Patch for Purists",
  "Unofficial Morrowind Official Plugins Patched",
  "Expansion Delay",
  "Expansion Resource Conflicts",
]

[[sublists]]
title = "Modding Resources"
description = "Community asset repositories, used by many other mods."
mods = [
  "Tamriel Data",
  "Tamriel_Data Graveyard - Deprecations un-deprecated",
  "OAAB_Data",
]

[[sublists]]
title = "Meshes / Performance"
description = "Mesh replacers. Most of these correct mistakes in the originals, often resulting in performance increases, others will affect gameplay (Graphic Herbalism)."
mods = [
  "Morrowind Optimization Patch",
  "Graphic Herbalism - MWSE and OpenMW Edition",
  "Properly Smoothed Meshes",
  "Project Atlas",
  "RR Mod Series - Better Meshes",
]

[[sublists]]
title = "Land & Landmass Additions"
description = "Add large amounts of extra land, including quests, to the game."
mods = [
  "Tamriel Rebuilt",
]

[[sublists]]
title = "Texture Packs"
description = "These provide large amounts of textures, covering all assets in the game or close to it."
mods = [
  "Morrowind Enhanced Textures",
  "Normal Maps for Morrowind",
]

[[sublists]]
title = "Landscapes"
description = "High quality texture replacements for large amounts of the games landscapes."
mods = [
  "Tyddy's Landscape Retexture",
  "Landscape Retexture",
  "Apel's Asura Coast and Sheogorath Region Retexture",
]

[[sublists]]
title = "Architecture"
description = "These provide new meshes and textures for general architecture."
mods = [
  "Daedric Ruins - Arkitektora of Vvardenfell",
  "Imperial Towns Normal Mapped for OpenMW by Lysol",
  "Imperial Forts Normal Mapped for OpenMW",
  "Shacks Docks and Ships - Arkitektora of Vvardenfell",
  "Ghastly Glowyfence",
  "OAAB Dwemer Pavements",
  "Vivec Normal Mapped for OpenMW",
  "Hlaalu Normal Mapped for OpenMW",
  "Telvanni - Arkitektora of Vvardenfell",
  "Necrom - Arkitektora of Morrowind",
]

[[sublists]]
title = "Flora & Nature"
description = "Update the look of various flora and other natural objects via improved meshes and high quality textures."
mods = [
  "Lava - Mold - Mud Caverns Revamp",
  "Minor patches and fixes",
  "Epic Plants",
  "Apel's Fire Retexture Patched for OpenMW and Morrowind Rebirth",
  "I Lava Good Mesh Replacer",
  "Subtle Smoke",
  "Better Waterfalls",
  "Comberry Bush and Ingredient Replacer",
  "Pherim's Fire Fern - Plant and Ingredient",
  "Scummy Scum",
]

[[sublists]]
title = "Sky"
description = "Alter the looks and behavior of the sky, clouds, and weather."
mods = [
  "Skies .IV",
  "Oh God Snow for Skies.iv and OpenMW",
  "New Starfields",
]

[[sublists]]
title = "Lighting"
description = "Update the behavior and look of lighting in the game, going for a darker and more accurate look overall."
mods = [
  "Glow in the Dahrk",
]

[[sublists]]
title = "Clutter And Items"
description = "Updated assets for the very large number of clutter objects and items in the game."
mods = [
  "OpenMW Containers Animated",
]

[[sublists]]
title = "Bodies And Heads"
description = "Replace the vanilla bodies and heads, and their textures, for all races and genders."
mods = [
  "Westly's Pluginless Head Replacer Complete",
  "Westly's Head and Hair Replacer - Hair Fix",
  "Khajiit Head Pack",
  "Whiskers Patch for MMH Version - All NPCs and Base Replace",
  "Whiskers Patch for MMH Version - All NPCs and Base Replace Fixed Meshes",
  "New Beast Bodies - Clean Version",
  "Better Bodies 3.2 (Better Beasts)",
]

[[sublists]]
title = "Creature Visuals"
description = "Give the various creatures of the game new life with updated assets and more."
mods = [
  "4thUnknowns Creatures Morrowind Edition",
]

[[sublists]]
title = "VFX"
description = "Updated visual effects.  Greatly enhance many scenes with subtle but beautiful additions."
mods = [
  "Mistify",
  "Remiros' Minor Retextures - Mist Retexture",
  "The Dream is the Door",
  "Parasol Particles",
  "Magic VFX Retexture by Articus",
]

[[sublists]]
title = "Animations"
description = "Update the game's dated animations with improved assets."
mods = [
  "Simply Walking (Remastered)",
  "Weapon Sheathing",
]

[[sublists]]
title = "Audio"
description = "Give new sound to almost everything in the game."
mods = [
  "Quest Voice Greetings",
  "Same Low Price Fix",
  "MAO Spell Sounds",
  "Audiobooks of Morrowind",
]

[[sublists]]
title = "Groundcover"
description = "Add grass, reeds, small mushrooms, stones, and other groundcover to the game."
mods = [
  "Lush Synthesis",
  "Vurt's Groundcover for OpenMW",
  "Remiros' Groundcover",
]

[[sublists]]
title = "NPCs"
description = "Add more variety and (hopefully) immersion by updating the rather limited NPC scope of the base game."
mods = [
  "Repopulated Morrowind",
]

[[sublists]]
title = "Balance And Nerfing"
description = "These mods try to bring some balance through changes to the game mechanics and world."
mods = [
  "Speechcraft Rebalance (OpenMW)",
  "Fatigue and Speed and Carryweight Rebalance (OpenMW)",
  "Pickpocket Rebalance (OpenMW)",
]

[[sublists]]
title = "Gameplay"
description = "Update gameplay mechanics. From leveling to monster spawning, marksman to sneaking, this selection tries to update game mechanics to feel better or more enjoyable compared to the vanilla game."
mods = [
  "Wares Ultimate",
  "NCGDMW Lua Edition",
  "MBSP Uncapped (OpenMW Lua) - NCGD Compat",
  "Smart Ammo for OpenMW-Lua",
  "Zack's Lua Multimark Mod",
  "Early Transport to Mournhold",
]

[[sublists]]
title = "Distant Details"
description = "Mods that affect object and visuals in the distance."
mods = [
  "Dynamic Distant Buildings for OpenMW",
]

[[sublists]]
title = "Water"
description = "Modify the water or sea in some shape or form."
mods = [
  "Distant Seafloor for OpenMW",
]

[[sublists]]
title = "Mod Patches"
description = "Collections of small patches and other plugins for compatibility between various mods, or between various mods and OpenMW."
mods = [
  "MOMW Patches",
  "Mono's Minor Moddities",
  "Animated Morrowind and Weapon Sheathing patch for OpenMW",
]

[[sublists]]
title = "Camera"
description = "Mods that affect the camera in one way or another."
mods = [
  "Action Camera Swap",
  "MOMW Camera",
]

[[sublists]]
title = "Post Processing Shaders"
description = "Post processing shaders add a variety of nice visual effects."
mods = [
  "MOMW Post Processing Pack",
]

[[sublists]]
title = "User Interface"
description = "Give the game UI a needed facelift in various ways."
mods = [
  "Monochrome User Interface",
  "Gonzo's Splash Screens",
  "Alternative TrueType Fonts",
  "Big Icons",
  "Simple HUD for OpenMW (with compass or minimap)",
]

[[sublists]]
title = "Dev Build Only"
description = "Mods that require the latest OpenMW Developer Build."
mods = [
  "Remove Negative Lights for OpenMW",
  "Harvest Lights",
  "Signpost Fast Travel",
  "Weather Particle Occlusion",
]

[[sublists]]
title = "Settings Tweaks"
description = "Not actually mods, but changes to settings that are built into the OpenMW engine. Enhance performance and visual quality, as well as enable or disable many gameplay options (including things provided by MCP for vanilla Morrowind)."
mods = [
  "True Nights and Darkness",
  "Distant Land And Objects",
  "Groundcover And Stomping",
  "Shadows",
  "Water",
  "Third Person Camera Improvements",
  "Head Bobbing",
  "Gameplay Tweaks",
  "MOMW Gameplay",
  "Generate A Navmesh Cache",
]

[[sublists]]
title = "Merging"
description = "The final bits of work, to ensure long-term smooth sailing."
mods = [
  "Delta Plugin",
  "Groundcoverify",
  "waza_lightfixes",
]
