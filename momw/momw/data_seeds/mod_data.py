from momw.data_seeds.by_category import (
    mwdata as MWData,
    animation as Animation,
    architecture as Architecture,
    armor as Armor,
    audio as Audio,
    books as Books,
    camera as Camera,
    caves_and_dungeons as CavesAndDungeons,
    chargen as Chargen,
    cities_and_towns as CitiesAndTowns,
    clothing as Clothing,
    collections as Collections,
    combat as Combat,
    companions as Companions,
    consistency as Consistency,
    creatures as Creatures,
    cut_content as CutContent,
    decor as Decor,
    fixes as Fixes,
    flora as Flora,
    fonts as Fonts,
    gameplay as Gameplay,
    groundcover as Groundcover,
    guilds_and_factions as GuildsAndFactions,
    interiors as Interiors,
    landmasses as Landmasses,
    landscape as Landscape,
    leveling as Leveling,
    lighting as Lighting,
    magic_n_spells as MagicNSpells,
    mod_managers as ModManagers,
    modding_resources as ModdingResources,
    npcs as NPCs,
    objects_and_clutter as ObjectsAndClutter,
    original_games as OriginalGames,
    patches as Patches,
    performance as Performance,
    player_homes as PlayerHomes,
    post_processing_shaders as PostProcessingShaders,
    quests as Quests,
    randomizers as Randomizers,
    replacers as Replacers,
    settings_tweaks as SettingsTweaks,
    skies as Skies,
    shaders as Shaders,
    tes3mp as TES3MP,
    test as TestMods,
    texture_packs as TexturePacks,
    tools as Tools,
    total_conversions as TotalConversions,
    travel as Travel,
    trees as Trees,
    ui as UI,
    vfx as VFX,
    videos as Videos,
    wabbajack_lists as WabbajackLists,
    water as Water,
    weapons as Weapons,
    weather as Weather,
)


def mods(for_tests=False):
    """
    TODO
    """
    # Morrowind game data
    MWData.init()

    if for_tests:
        TestMods.init()
        return

    Animation.init()
    Architecture.init()
    Armor.init()
    Audio.init()
    Books.init()
    Camera.init()
    CavesAndDungeons.init()
    Chargen.init()
    CitiesAndTowns.init()
    Clothing.init()
    Collections.init()
    Combat.init()
    Companions.init()
    Consistency.init()
    Creatures.init()
    CutContent.init()
    Decor.init()
    Fixes.init()
    Flora.init()
    Fonts.init()
    Gameplay.init()
    Groundcover.init()
    GuildsAndFactions.init()
    Interiors.init()
    Landmasses.init()
    Landscape.init()
    Leveling.init()
    Lighting.init()
    MagicNSpells.init()
    ModManagers.init()
    ModdingResources.init()
    NPCs.init()
    ObjectsAndClutter.init()
    OriginalGames.init()
    Patches.init()
    Performance.init()
    PlayerHomes.init()
    PostProcessingShaders.init()
    Quests.init()
    Randomizers.init()
    Replacers.init()
    SettingsTweaks.init()
    Shaders.init()
    Skies.init()
    TES3MP.init()
    TexturePacks.init()
    Tools.init()
    TotalConversions.init()
    Travel.init()
    Trees.init()
    UI.init()
    VFX.init()
    Videos.init()
    WabbajackLists.init()
    Water.init()
    Weapons.init()
    Weather.init()
