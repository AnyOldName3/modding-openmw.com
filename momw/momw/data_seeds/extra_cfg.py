import sys

from momw.helpers import read_toml_data
from momw.models import Mod, ModList, ExtraCfg


def extra_cfg():
    """
    This function reads TOML-formatted data to create ExtraCfg entries in the database.
    """
    data = read_toml_data("extra-cfg")
    for n in data["extra_cfg"]:
        d = {"text": n["text"]}
        if "in_settings" in n:
            d.update({"in_settings": n["in_settings"]})

        ec = ExtraCfg(**d)
        ec.save()

        mods = Mod.objects.filter(name__in=n["for_mods"])
        if mods:
            for mod in mods:
                ec.for_mods.add(mod)

        if "on_lists" in n.keys():
            on_lists = []
            for slug in n["on_lists"]:
                try:
                    on_lists.append(ModList.objects.get(slug=slug))
                except ModList.DoesNotExist:
                    print("ERROR: mod list for extra cfg does not exist!")
                    sys.exit(1)
            if on_lists:
                for l in on_lists:
                    ec.on_lists.add(l)
