from momw.helpers import generate_mod, nexus_mod

original_games = "Original Games"


def init():
    for m in [
        {
            "name": "RWC (Robowind)",
            "author": '<a href="https://discord.gg/MUcVdFqueK">MAD Team</a>',
            "category": original_games,
            "date_added": "2023-09-02 09:00:00 -0500",
            "date_updated": "2023-09-08 17:12:00 -0500",
            "description": """
<p>At the moment, RWC (RoboWindConstruct) is an independent, non-profit project developed by a team of a couple of people, with the active support of OpenMW developers. In fact, this is a demo level of the game in the "Run and Shoot" genre, created and fully powered by the OpenMW 0.49 engine. The level is made in Sci-Fi, and is a corridor shooter, from the first person.</p>

<ul>
<li>It is recommended to play from the first person. Since not all mechanics work adequately from a third person.</li>
<li>If your map is not displayed correctly, save/load the game.</li>
</ul>

<iframe width="560" height="315" src="https://www.youtube.com/embed/6xK3IXAimgs?si=1cdC7AskS_1kUcqE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
<img class="pic" src="https://cdn.discordapp.com/attachments/1054317463073988668/1147490608181690368/openmw_2023-09-02_14-10-28-378.jpg" />
<img class="pic" src="https://cdn.discordapp.com/attachments/1054317463073988668/1147490608710164510/openmw_2023-09-02_14-02-51-517.jpg" />
<img class="pic" src="https://cdn.discordapp.com/attachments/1054317463073988668/1147490609754554449/openmw_2023-09-02_14-06-55-196.jpg" />
<img class="pic" src="https://cdn.discordapp.com/attachments/1054317463073988668/1147490610987679804/openmw_2023-09-02_14-08-59-580.jpg" />
<img class="pic" src="https://cdn.discordapp.com/attachments/1054317463073988668/1147490631711727616/openmw_2023-09-02_14-14-16-212.jpg" />
<img class="pic" src="https://cdn.discordapp.com/attachments/1054317463073988668/1147490632051478528/openmw_2023-09-02_14-11-29-129.jpg" />
""",
            "dl_url": "/files/RWC_Final.zip",
            "status": "live",
            "url": nexus_mod("53480"),
        }
    ]:
        generate_mod(**m)
