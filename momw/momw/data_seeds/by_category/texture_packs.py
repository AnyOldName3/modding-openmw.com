from momw.helpers import generate_mod, nexus_mod, nexus_user


texture_packs = "Texture Packs"

# Names of tags that are used in this file.
tag_atlased_textures = "Atlased Textures"
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_non_hd = "Non-HD"
tag_normal_maps = "Normal Maps"
tag_oaab_data = "OAAB Data"
tag_tamriel_data = "Tamriel Data"
tag_tes3mp_graphics = "TES3MP Graphics"
tag_vanilla_friendly = "Vanilla Friendly"

# Names of mods that are used as alts in this file.
morrowind_enhanced_textures = "Morrowind Enhanced Textures"
mw_gigapixel = "MWGigapixel"
pixelwind_v2 = "Pixelwind V2"
vanilla_remastered = "Vanilla Remastered"
vanillaplus = "Morrowind VanillaPlus textures"


def init():
    for m in [
        {
            "name": "OAAB Full Upscale",
            "author": nexus_user("65333", "StaticNation"),
            "category": texture_packs,
            "date_added": "2023-09-15 13:18:00 -0500",
            "description": "Full Upscale of OAAB HD Textures.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("53379"),
        },
        {
            "name": vanilla_remastered,
            "author": nexus_user("9265453", "sam642"),
            "category": texture_packs,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Vanilla Remastered includes almost every texture in game at 1K and now 4K resolution! All included textures have been normal mapped, specular, ambient occlusion, gloss and height mapped!!",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("45341"),
        },
        {
            "alt_to": [vanilla_remastered],
            "author": nexus_user("55309317", "Nico Wilhelm"),
            "category": texture_packs,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """<p>Morrowind VanillaPlus gives most terrain textures in the game a higher (4x) resolution and adds texture maps. </p>

<ul>
    <li>Over 130 re-made terrain and wall textures</li>
    <li>Over 660 normal, specular and ambient occlusion maps for terrain, environment and most frequent objects (rocks, buildings, etc)</li>
    <li>Normal and specular maps for all clothes</li>
</ul>""",
            "name": vanillaplus,
            "status": "live",
            "tags": [tag_vanilla_friendly, tag_tes3mp_graphics],
            "url": nexus_mod("45813"),
        },
        {
            "alt_to": [vanilla_remastered],
            "author": "DassiD Poodlesandwich Gigan",
            "category": texture_packs,
            "date_added": "2019-04-06 12:58:52 -0500",
            "date_updated": "2021-02-07 14:14:34 -0500",
            "description": """<p>Upscales every in-game texture with the help of machine learning. 100% vanilla-friendly.</p>""",
            "name": morrowind_enhanced_textures,
            "status": "live",
            "tags": [tag_vanilla_friendly, tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("46221"),
        },
        {
            "alt_to": [morrowind_enhanced_textures, vanilla_remastered],
            "author": nexus_user("50985026", "Levi Smith"),
            "category": texture_packs,
            "date_added": "2019-08-03 17:52:10 -0500",
            "description": """<p>[Levi Smith] was testing out A.I. Gigapixel and was impressed with the results so [Levi Smith] decided to share.</p>

<p>This is a simple drag and drop texture replacer.</p>

<p>No other mods/requirements needed.</p>

<p>Replaces all vanilla textures with a 4x upscale.</p>

<p>Does not replace UI or vfx like spells because [Levi Smith] didn't feel it was necessary.</p>""",
            "name": mw_gigapixel,
            "status": "live",
            "tags": [tag_vanilla_friendly, tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("46352"),
        },
        {
            "alt_to": [morrowind_enhanced_textures, vanilla_remastered, vanillaplus],
            "author": nexus_user("6344059", "DassiD"),
            "category": texture_packs,
            "date_added": "2019-04-06 13:05:40 -0500",
            "date_updated": "2020-11-14 14:48:07 -0500",
            "description": """<p>Vanilla-friendly normal maps for Morrowind.</p>""",
            "name": "Normal Maps for Morrowind",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_tes3mp_graphics],
            "url": nexus_mod("45336"),
        },
        {
            "alt_to": [morrowind_enhanced_textures, vanilla_remastered, vanillaplus],
            "author": nexus_user("137283", "Darknut"),
            "category": texture_packs,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "High quality texture replacement for almost all of the world textures in the game.",
            "name": "Darknut's World Textures",
            "slug": "darknuts-world-textures",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("45056"),
        },
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": texture_packs,
            "date_added": "2019-03-05 19:05:00 -0500",
            "description": "Only for using with Tamriel Rebuilt Project. New, Better. Detailed. Textures for Necrom, Saint City of the Dead Ones.",
            "name": "Necrom - Arkitektora of Morrowind",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics, tag_tamriel_data],
            "url": nexus_mod("46289"),
            "usage_notes": """Requires <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>, or else you won't have a Necrom to retexture.  Choose either the HQ or MQ version, and be sure to grab the optional meshes fix.""",
        },
        {
            "alt_to": [morrowind_enhanced_textures, vanilla_remastered, vanillaplus],
            "author": nexus_user("50985026", "Levi Smith"),
            "category": texture_packs,
            "date_added": "2018-06-30 21:00:00 -0500",
            "description": "It gives Morrowind a Pixel art style.  It covers all vanilla textures.",
            "name": pixelwind_v2,
            "status": "live",
            "tags": [tag_non_hd],
            "url": nexus_mod("45492"),
            "usage_notes": "Works best when loaded after all other textures.",
        },
        {
            "alt_to": [
                morrowind_enhanced_textures,
                pixelwind_v2,
                vanilla_remastered,
                vanillaplus,
            ],
            "author": nexus_user("4381248", "PeterBitt"),
            "category": texture_packs,
            "date_added": "2018-06-30 21:00:00 -0500",
            "description": "This is a texture replacer that gives Morrowind a watercolored look.  Quick and easy installation.  Covers the whole game.",
            "name": "Morrowind Watercolored v2",
            "status": "live",
            "tags": [tag_non_hd],
            "url": nexus_mod("43375"),
            "usage_notes": "Works best when loaded after all other textures.",
        },
        {
            "alt_to": [
                morrowind_enhanced_textures,
                mw_gigapixel,
                pixelwind_v2,
                vanilla_remastered,
                vanillaplus,
            ],
            "author": nexus_user("899234", "Remiros"),
            "category": texture_packs,
            "date_added": "2020-01-04 10:46:41 -0500",
            "description": "Intelligent Textures is a full texture pack of the game using carefully curated AI upscales that have been manually edited by hand, as well as completely custom textures in cases where upscaling failed. It uses ESRGAN combined with the excellent custom models created by the upscale community. All textures are exactly 4x the size of the vanilla textures.",
            "name": "Intelligent Textures",
            "status": "live",
            "tags": [
                tag_atlased_textures,
                tag_vanilla_friendly,
                tag_high_res,
                tag_tes3mp_graphics,
            ],
            "url": nexus_mod("47469"),
        },
        {
            "author": nexus_user("8298461", "Noiselarp7"),
            "category": texture_packs,
            "date_added": "2021-01-19 10:02:49 -0500",
            "description": "Normal Height Maps for Tamriel Rebuilt Terrain.",
            "name": "Tamriel Rebuilt Terrain Normal Height Maps for OpenMW",
            "status": "live",
            "tags": [
                tag_high_res,
                tag_normal_maps,
                tag_tes3mp_graphics,
                tag_tamriel_data,
            ],
            "url": nexus_mod("48676"),
        },
        {
            "name": "Normal maps for Morrowind - Tamriel Rebuilt - OAAB - Skyrim HotN - Stirk and other mods",
            "author": nexus_user("38215290", "Mrovkogon"),
            "category": texture_packs,
            "date_added": "2022-11-05 16:45:01 -0500",
            "description": "Normal and specular maps for Morrowind & expansions, Tamriel Rebuild, OAAB, Aesthesia Grass and other mods. See screenshots and description.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_oaab_data],
            "url": nexus_mod("50586"),
        },
        {
            "name": "Morrowind AIO normalmaps for OpenMW",
            "author": nexus_user("4012159", "greg079"),
            "category": texture_packs,
            "date_added": "2022-11-05 18:41:18 -0500",
            "description": "A set of normalmaps for the vanilla and add-on and DLC textures.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("48672"),
        },
        {
            "name": "Normal Maps for Everything",
            "author": nexus_user("2282408", "cane1314"),
            "category": texture_packs,
            "date_added": "2023-09-10 12:03:00 -0500",
            "description": "Normal Maps for Everything is a mod for Morrowind that enhances the game's visuals with high-quality normal map textures.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_manual_edit_needed],
            "url": nexus_mod("52567"),
        },
        {
            "name": "The Doors of Oblivion Full Upscale",
            "author": nexus_user("65333", "Team DetailNation"),
            "category": texture_packs,
            "date_added": "2023-10-29 17:09:00 -0500",
            "description": "Fixed and Fully Upscaled Assets for The Doors of Oblivion (by RealAshstaar) brought to you by the DetailNation team: DetailDevil & StaticNation",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("53351"),
        },
        {
            "name": "Sharper Normal Maps for Greymarch Dawn - Whispers of Jyggalag",
            "author": nexus_user("253024", "Eledin"),
            "category": texture_packs,
            "date_added": "2023-11-08 14:24:00 -0500",
            "description": "High quality, sharper alternative for the default Normal Maps of Greymarch Dawn - Whispers of Jyggalag.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("53697"),
        },
        {
            "name": "Tamriel Textures Reloaded",
            "author": "bigguy4u",
            "category": texture_packs,
            "date_added": "2022-07-22 21:00:00 -0500",
            "description": "All Tamriel Rebuilt textures upscaled for HD 2022 gameplay. Weapons, armor, creatures, buildings, landscape, people, everything. Large textures upscaled to 2k.",
            "tags": [tag_high_res, tag_tamriel_data],
            "url": nexus_mod("51464"),
        },
    ]:
        generate_mod(**m)
