from momw.helpers import generate_mod, nexus_mod, nexus_user, modhistory_archive_link


clothing = "Clothing"

# Names of tags that are used in this file.
tag_high_res = "High Res"
tag_normal_maps = "Normal Maps"
tag_manual_edit_needed = "Manual Edit Needed"

# Names of mods that are used as alts in this file.
unique_finery_replacer_ufr = "Unique Finery Replacer UFR"


def init():
    for m in [
        {
            # TODO: Also this? http://mw.modhistory.com/download--14998 (bc_shirt_exp_03_Fem.nif)
            "author": "Psychodog Studios",
            "category": clothing,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Better Clothes replaces most of the default clothing from the standard Morrowind game with new non-segmented versions designed to work with Better Bodies. This improves the visual appearance of most items of clothing that are available in stores, and worn by NPCs, and in your inventory.",
            "is_active": False,
            "name": "Better Clothes v1.1",
            "needs_cleaning": True,
            "slug": "better-clothes-v11",
            "status": "live",
            "tags": [tag_high_res],
            "url": modhistory_archive_link("21-14097"),
            "usage_notes": """Ignore the plugin from this and grab <a href="/mods/better-clothes-whos-there-fix/">Better Clothes "Who's There?" Fix</a> instead.""",
        },
        {
            "author": nexus_user("37580", "Plangkye"),
            "category": clothing,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Supplement to Better Clothes.",
            "name": "More Better Clothes Vol I - Vanilla",
            "slug": "more-better-clothes-vol-i-vanilla",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("36406"),
        },
        {
            "name": "Hirez Better Clothes",
            "author": "Saint_Jiub",
            "category": clothing,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Early work in progress - high resolution texture replacer for Better Clothes and More Better Clothes Vol. 1",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("45499"),
        },
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": clothing,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "High quality replacers for the goldolier helm.",
            "name": "New Gondolier Helm",
            "slug": "new-gondolier-helm",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_manual_edit_needed],
            "url": nexus_mod("42138"),
            "usage_notes": """OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.""",
        },
        {
            "author": nexus_user("610764", "Alaisiagae"),
            "category": clothing,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Gives most special and unique amulets, belts, rings, robes, shoes, and amulets new, unique models.",
            "name": unique_finery_replacer_ufr,
            "needs_cleaning": True,
            "slug": "unique-finery-replacer-ufr",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("25725"),
        },
        {
            "alt_to": [unique_finery_replacer_ufr],
            "author": "The Morrowind Modding Community",
            "category": clothing,
            "date_added": "2019-03-30 14:19:50 -0500",
            "date_updated": "2020-05-02 14:26:45 -0500",
            "description": "A compilation of mods that add unique looks to unique items that previously lacked them.",
            "name": "FM - Unique Items Compilation",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("46433"),
            "usage_notes": """OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.""",
        },
        {
            "author": nexus_user("6676263", "Moranar"),
            "category": clothing,
            "compat": "partially working",
            "date_added": "2020-01-12 19:19:50 -0500",
            "description": "This plugin replaces male meshes with more smooth versions (based on NioLiv's meshes) and adds the analogous female variant of the robe too. I tried to retain the original appearance as much as possible.",
            "name": "Better Robes",
            "needs_cleaning": True,
            "status": "live",
            "url": nexus_mod("42773"),
            "usage_notes": """<p>If you're following <a href="/lists/total-overhaul/">the Total Overhaul list</a>, skip the <code>UFR_v3dot2.esp</code> plugin as it will be provided by <a href="/mods/fm-unique-items-compilation/">FM - Unique Items Compilation</a>.</p>

<p>Skip the <code>Better Robes TR.esp</code> plugin, <a href="/mods/better-robes-updated-tamriel-rebuilt-patch/">a patched version</a> should be used instead (if you're following a list, it's up next).  This mod is listed as 'Partially Working' due to Tamriel Rebuilt robe issues, and that patch fixes them.</p>

<p>The <code>UFR_v3dot2.esp</code> plugin needs cleaning, however the main <code>Better Clothes.ESP</code> plugin does not; followers of <a href="/lists/total-overhaul/">the Total Overhaul list</a> don't need to clean anything.</p>""",
        },
        {
            "alt_to": [
                "Better Clothes v1.1",
                "More Better Clothes Vol I - Vanilla",
                "Hirez Better Clothes",
            ],
            "author": "Better Clothes team and other authors who provided fixes and further versions",
            "category": clothing,
            "date_added": "2020-11-14 12:47:50 -0500",
            "description": 'A collection of <a href="/mods/better-clothes-v11/">Better Clothes</a> and related fixes.',
            "name": "Better Clothes Complete",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("47549"),
        },
        {
            "name": "New Gondolier Helm Fixed for OpenMW",
            "alt_to": ["New Gondolier Helm"],
            "author": nexus_user("3281858", "Tyddy")
            + " and "
            + nexus_user("44023872", "CY4NN"),
            "category": clothing,
            "date_added": "2021-05-05 09:14:53 -0500",
            "description": "High quality replacers for the goldolier helm. No edits are needed; this version has been edited in advance by CY4NN so that it will work with OpenMW.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("49602"),
        },
        {
            "name": "Common Shoe Pack",
            "author": nexus_user("2929833", "RubberMan"),
            "category": clothing,
            "date_added": "2021-08-28 12:56:10 -0500",
            "description": "Replaces 5 of the most commonly used shoes in the game with new models.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("46815"),
        },
    ]:
        generate_mod(**m)
