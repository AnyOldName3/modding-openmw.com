from momw.helpers import generate_mod, nexus_mod, nexus_user


tag_dev_build_only = "Dev Build Only"
tag_openmw_lua = "OpenMW Lua"


randomizers = "Randomizers"


def init():
    mods = [
        {
            "name": "mtrPermanentQuestCorpsesRandomizer OpenMW omwaddon - Randomizer for Location of Permanent Corpses of Imperial Taxman Processus Vitellius and Skooma Dealer Ernil Omoran Associated with Quests",
            "author": nexus_user("88247468", "MTR"),
            "category": randomizers,
            "date_added": "2022-11-12 16:45:00 -0500",
            "description": "This mod randomizes placement of Processus Vitellius and Ernil Omoran bodies.",
            "status": "live",
            "url": nexus_mod("51375"),
        },
        {
            "name": "mtrDwemerPuzzleBoxRandomizer - Randomizer for Location of Dwemer Puzzle Box within Arkngthand associated with Main Quest",
            "author": nexus_user("88247468", "MTR"),
            "category": randomizers,
            "date_added": "2022-11-15 18:56:00 -0500",
            "description": "This mod randomizes placement of Dwemer Puzzle Box. Made in regular mwscript, so it works in OpenMW too!",
            "status": "live",
            "url": nexus_mod("49781"),
        },
        {
            "name": "Oh No Stolen Reports",
            "author": nexus_user("2750367", "johnnyhostile"),
            "category": randomizers,
            "date_added": "2023-11-08 13:12:00 -0500",
            "description": "Randomizes the location of Ajira's stolen reports in the related Mage's Guild quest.",
            "status": "live",
            "url": nexus_mod("53046"),
        },
        {
            "name": "Farvyn Oreyn's Position Randomizer",
            "author": "ezze",
            "category": randomizers,
            "date_added": "2023-11-08 13:34:00 -0500",
            "description": "Place Farvyn Oreyn, the fake hero, in a random location around Gar Mok.",
            "status": "live",
            "url": nexus_mod("53292"),
        },
        {
            "name": "Din's Position Randomizer",
            "author": "ezze",
            "category": randomizers,
            "date_added": "2023-11-08 13:42:00 -0500",
            "description": "Place Din, the sick legionary, in a random location around Gnisis.",
            "status": "live",
            "url": nexus_mod("53286"),
        },
        {
            "name": "Suran Dancer Randomized",
            "author": "ezze",
            "category": randomizers,
            "date_added": "2023-11-08 13:44:00 -0500",
            "description": "Each new day you get three randomly chosen dancers in the Desele's House of Earthly Delights in Suran.",
            "status": "live",
            "url": nexus_mod("53389"),
        },
        {
            "name": "Randomize Kagrenac's Tools",
            "author": nexus_user("73025388", "SuperHobbit"),
            "category": randomizers,
            "date_added": "2023-12-04 09:30:05 +0100",
            "description": "This mod randomly places all three of Kagrenac's Tools (Wraithguard, Keening, and Sunder) across 10 potential pre-defined key locations within Ghostfence instead of their normal static locations. There is also a simpler version of this mod that only randomizes Keening and Sunder's location for those who prefer something closer to lore-friendly.",
            "status": "live",
            "url": nexus_mod("47267"),
        },
        {
            "name": "Morrowind World Randomizer for OpenMW",
            "author": nexus_user("60333061", "Diject"),
            "category": randomizers,
            "date_added": "2024-01-11 19:00:00 -0500",
            "description": "A randomizer for items in inventories and containers, creature spawns, exterior static objects and more.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53454"),
        },
    ]

    for m in mods:
        generate_mod(**m)
