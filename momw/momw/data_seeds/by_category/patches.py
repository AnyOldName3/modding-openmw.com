from momw.helpers import generate_mod, nexus_mod, nexus_user, modhistory_archive_link


patches = "Patches"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_normal_maps = "Normal Maps"
tag_oaab_data = "OAAB Data"
tag_vanilla_friendly = "Vanilla Friendly"
tag_openmw_lua = "OpenMW Lua"
tag_gitlab_mods = "GitLab mods"
tag_tamriel_data = "Tamriel Data"

# Names of mods that are used as alts in this file.
dark_brotherhood_armor_replacer_expanded = "Dark Brotherhood Armor Replacer Expanded"
delayed_db_attack_v2 = "Delayed DB Attack V2"
illys_solsteim_rumour_fix_v10 = "Illy's Solsteim Rumour Fix v1.0"
official_plugins_fixes_v11 = "Official Plugins Fixes v1.1"
papill6n_various_graphics_things = "Papill6n various graphics things"
solstheim_rumor_fix_fixed = "Solstheim Rumor Fix - Fixed"
tel_fyr_amulet_fix = "Tel Fyr Amulet Fix"
tribunal_delayed = "Tribunal Delayed"


def init():
    for m in [
        {
            "name": "Patch for Purists",
            "author": nexus_user("36879320", "Half11"),
            "category": patches,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-06-25 08:59:14 -0500",
            "description": """<p>Patch for Purists strives to be an all-in-one patcher for everything in the The Elder Scrolls III: Morrowind Game of the Year Edition that is considerd a bug. I define a bug as: "an immersion breaking issue which was clearly not intended by the developers". This definition was the benchmark for creating this patch. Patch for Purists covers almost every aspect of the game. A complete overview of fixes can be found in the Patch Notes.</p>

<p>“What distinguishes the Patch for Purists from existing player made patches?,” you may ask. My purist approach kept me from introducing debatable and unnecessary changes: balance changes based on personal preferences, so-called improvements in gameplay, and adding of unnecessary stuff. These are changes which, according to me, do not belong in a bugfixing patch because they go beyond fixing unintentional immersion breaking issues. Unfortunately, these are the exact changes that plague most player made patches. These patches force purists to play an outdated version of the game and alter the experience of new players who are not aware of these unnecessary changes to the original game, as was intended by the developers.</p>""",
            "status": "live",
            "url": nexus_mod("45096"),
        },
        {
            "name": official_plugins_fixes_v11,
            "author": "michael163377",
            "category": patches,
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """<p>Fixes a few annoying issues in Bethesda's official plugins. You will no longer collide with dragonflies, Sirollus Saccus now sells Gold Armor instead of wearing it, the quests will be added to your Quests tab in your journal and dragonflies and the master index will glow.</p>

<p>The Helm of Tohan, Master Index and Siege at Firemoth plugins require Tribunal.</p>""",
            "is_active": False,
            "status": "live",
            "url": modhistory_archive_link("13-14291"),
        },
        {
            "name": "Unofficial Morrowind Official Plugins Patched",
            "alt_to": [official_plugins_fixes_v11],
            "author": nexus_user("16269634", "PikachunoTM"),
            "category": patches,
            "date_added": "2019-05-12 15:25:05 -0500",
            "description": """<p>An attempt to fix the many issues present in Bethesda's original Official Plugins. Includes fixes for all of the Official Plugins, and offers merged and compatibility options as well.</p>""",
            "status": "live",
            "url": nexus_mod("43931"),
        },
        {
            "name": "True Flame Script Fix",
            "author": "Amenophis",
            "category": patches,
            "date_added": "2019-07-19 17:55:18 -0500",
            "description": "The sword also functions as a torch when drawn. If you have hotkeyed another weapon and switch to that without sheathing Trueflame, the light it generates will remain. This mod fixes the bug.",
            "status": "live",
            "url": nexus_mod("43107") + "?tab=files",
            "usage_notes": """<span class='bold'>NOTE: This plugin is not needed if you are using <a href='/mods/patch-for-purists/'>Patch For Purists</a>!</span>  On the files page (linked above) do a find on page (Ctrl-f/Cmd-f) for 'True Flame Script Fix' - it's number 17 in the list of files.""",
        },
        {
            "author": "Vurt and Rytelier",
            "category": patches,
            "date_added": "2019-03-05 19:00:00 -0500",
            "description": "Vurt's Bitter Coast Trees II with optimizations and model fixes so their performance is much better and their bugs are fixed.",
            "name": "Vurt's Bitter Coast Trees II - Remastered and Optimized",
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly, tag_high_res],
            "url": nexus_mod("46418"),
            "usage_notes": """<p>In my own testing, this yielded me an extra 5 FPS (or more) in the Seyda Neen area.</p>""",
        },
        {
            "name": "Main Quest Enhancers Patch",
            "author": nexus_user("38047", "abot"),
            "category": patches,
            "date_added": "2019-03-04 18:00:00 -0500",
            "description": """<p>This replacer of TrainWiz's MQE_MainQuestEnhancers.esp was originally for personal use, but as no official patch by original author at least for scripted leveled list problem exists yet...</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "http://abitoftaste.altervista.org/morrowind/index.php?option=downloads&task=info&id=72&Itemid=50",
        },
        {
            "name": "Welcome to the Arena - OpenMW Patch",
            "author": nexus_user("33005805", "hamod9041"),
            "category": patches,
            "compat": "partially working",
            "date_added": "2018-08-05 21:00:00 -0500",
            "date_updated": "2020-08-01 09:14:23 -0500",
            "description": "This is a replacement esp to allow the welcome to the arena mod to work correctly with openmw.",
            "status": "live",
            "url": nexus_mod("45907"),
            "usage_notes": """The parent mod has updated to v6.7 and this patch is no longer compatible (it only supports v6.6).""",
        },
        {
            "author": "Hristos N. Triantafillou",
            "category": patches,
            "date_added": "2019-03-03 20:53:20 -0500",
            "description": """<p>I created this plugin so that <a href="/mods/multiple-teleport-marking-openmw-and-tamriel-rebui/">Multiple Teleport Marking (OpenMW and Tamriel Rebuilt)</a> could allow setting marks in cells from <a href="/mods/abandoned-flat-v2/">Abandoned Flat</a>.  As such, this plugin is useless if you don't use that mod too.</p>""",
            "dl_url": "/files/MultipleTeleportMarkingOpenMW-AbandonedFlat.zip",
            "name": "Multiple Teleport Marking For Abandoned Flat",
            "tags": [tag_game_expanding],
            "status": "live",
        },
        {
            "author": '<a href="https://wiki.openmw.org/index.php?title=User:Darklocq">Darklocq</a>',
            "category": patches,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-05-09 10:40:19 -0500",
            "description": "Repairs (non-OpenMW-specific) errors in Better Sounds 1.1 by by JEKA, TJ, and Colobos, which broke quests and other mods.",
            "dl_url": "/files/Better_Sounds_1.1_Patch_2.7z",
            "extra_cfg": """<p>The following needs to be set in your <code>[Sound]</code> section in the <code>settings.cfg</code> file:</p>

<pre><code>buffer cache max = 64</code></pre>""",
            "extra_cfg_raw": """#
# Better Sounds Patch:
# (This goes into settings.cfg under [Sound])
#
buffer cache max = 64""",
            "name": "Better Sounds 1.1 Patch 2",
            "slug": "better-sounds-11-patch2",
            "status": "live",
            "url": "",
            "usage_notes": """<p>See <a href=\'https://wiki.openmw.org/index.php?title=User:Darklocq/Mod_testing_notes#Better_Sounds\'>this wiki page</a> for more detailed notes about this patch.  The version linked to from the wiki contains a script error, the version I am hosting has been edited to fix the error (Open the original plugin with OpenMW-CS and select File >> Verify, which will open a pane that displays all warnings and errors.  The specific script error can be seen here).</p>

<p><span class="bold">NOTE TO NCGD USERS:</span> If you are following a list, then the list order will have you covered.  If you aren't following a list, please be sure to also use <a href="/mods/magicka-based-skill-progression-better-sounds-ncgd/">Magicka Based Skill Progression - Better Sounds - ncgdMW Compatible - Class-Spec Benefit Fix</a>, which fixes this problem:  This plugin conflicts with <a href="/mods/natural-character-growth-and-decay-mw/">Natural Character Growth and Decay - MW</a> and <a href="/mods/magicka-based-skill-progression-ncgdmw-compatility/">Magicka Based Skill Progression - ncgdMW Compatility Version</a> such that your character\'s Destruction skill will not level up.</p>""",
        },
        {
            "author": nexus_user("843673", "Hrnchamd"),
            "category": patches,
            "compat": "not working",
            "date_added": "2019-05-12 16:55:37 -0500",
            "description": """<p>Patches bugs in the Morrowind program, which cannot otherwise be fixed by editing scripts or data files. Cures many crash and save corruption problems.</p>""",
            "name": "Morrowind Code Patch",
            "status": "live",
            "url": nexus_mod("19510"),
            "usage_notes": """<p>This does not, and never will work with OpenMW.  It's listed here as a reference.  This is needed for vanilla Morrowind because it is closed source, therefore a patch is the only way to modify the engine.  With OpenMW, the entire engine is free software and can be modified; no "patches" are needed.</p>""",
        },
        {
            "name": "Book Rotate - Tamriel Rebuilt Patch",
            "author": nexus_user("78728", "Personman"),
            "category": patches,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "An updated patch for Book Rotate 5.3 and Tamriel Rebuilt 17.09.",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("45564"),
        },
        {
            "name": "OpenMW Fixes For Uvirith's Legacy",
            "author": "Xandaros",
            "category": patches,
            "date_added": "2020-01-19 12:59:09 -0500",
            "description": "Corrects some problems with Uvirith's Legacy and OpenMW.",
            "dl_url": "/files/UL_3.5_OpenMW_1.3_Add-on.7z",
            "status": "live",
            "url": "https://forum.openmw.org/viewtopic.php?p=65466#p65466",
        },
        {
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": patches,
            "date_added": "2020-05-02 22:16:42 -0500",
            "description": 'Patched meshes so that <a href="/mods/dark-molag-mar/">Dark Molag Mar</a>\'s windows glow with the rest of <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.',
            "name": "Glow in the Dahrk: Dark Molag Mar patch",
            "status": "live",
            "url": nexus_mod("45886"),
            "usage_notes": """If following the Total Overhaul list, you already have the archive for this.  It's numbered 06 in the main GitD archive, pick with or without sunrays as desired.""",
        },
        {
            "alt_to": [
                solstheim_rumor_fix_fixed,
                illys_solsteim_rumour_fix_v10,
                delayed_db_attack_v2,
                tribunal_delayed,
                dark_brotherhood_armor_replacer_expanded,
            ],
            "author": nexus_user("36879320", "Half11"),
            "category": patches,
            "date_added": "2020-05-05 08:21:40 -0500",
            "description": """This mod fixes Bethesda's overly enthusiastic expansion hooks by delaying the Dark Brotherhood attacks (for Tribunal) and limiting intrustive dialogue topics to a few NPCs (Bloodmoon).""",
            "name": "Expansion Delay",
            "status": "live",
            "url": nexus_mod("47588"),
        },
        {
            "alt_to": ["Yet Another Guard Diversity"],
            "name": "Compatibility patch for NX9's Guards and RR_Better_Telvanni_Cephlapod_Armor",
            "author": nexus_user("38585360", "Serraphi"),
            "category": patches,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Allows NX9's wonderful guards mod to work alongside the equally wonderful Telvanni Cephlapod armor by the RR team!",
            "needs_cleaning": True,
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("45163"),
            "usage_notes": "Contains a patched <code>NX9_Guards_Complete.ESP</code> plugin file that should be used instead of the one from that mod.",
        },
        {
            "name": "Welcome to the Arena v6.7 patched for OpenMW",
            "author": "eddie5",
            "category": patches,
            "date_added": "2020-09-24 18:18:10 -0500",
            "description": """This is a replacement esp for Kalamestari_69's WttA v6.7 with all the doubled calls to MessageBox condensed ("MessageBox MessageBox" -> "MessageBox"). It was good enough to get me all the way to grand champion, maybe it'll be useful to you too.""",
            "url": "https://forum.openmw.org/viewtopic.php?f=40&t=6957",
            "status": "live",
        },
        {
            "name": "Blank Glow Maps for Robe Overhaul",
            "author": nexus_user("1178178", "borok"),
            "category": patches,
            "date_added": "2021-01-31 08:35:10 -0500",
            "description": """Remove the bright purple glow maps from Robe Overhaul.""",
            "url": nexus_mod("43795"),
            "status": "live",
            "usage_notes": """Find <code>Blank Glow Maps for Robe Overhaul</code> on the files tab.""",
        },
        {
            "name": "Ports Of Vvardenfell - TR - TOTSP Patch",
            "author": nexus_user("77253", "alvazir"),
            "category": patches,
            "date_added": "2021-09-01 17:51:03 -0500",
            "description": """This handy patch by alvazir fixes several conflicts between <a href="/mods/ports-of-vvardenfell/">Ports Of Vvardenfell</a>, <a href="/mods/tamriel-rebuilt/">Tamriel Rebuilt</a>, and <a href="/mods/solstheim-tomb-of-the-snow-prince/">Solstheim Tomb of the Snow Prince</a>.""",
            "url": nexus_mod("48955"),
            "status": "live",
        },
        {
            "alt_to": ["Welcome to the Arena v6.7 patched for OpenMW"],
            "name": "Welcome to the Arena Patched for OpenMW",
            "author": nexus_user("449021", "JaceX"),
            "category": patches,
            "date_added": "2021-09-06 11:12:43 -0500",
            "description": """This patch fixes a significant number of scripting and mesh issues in Welcome to the Arena 6.7, improves how the AI fights, and removes various spell scripts that were breaking in OpenMW.""",
            "url": nexus_mod("50097"),
            "status": "live",
        },
        {
            "name": "MOMW Aesthesia Groundcover Reworked Patches",
            "author": "eddie5, Hristos N. Triantafillou",
            "category": patches,
            "date_added": "2021-09-12 09:37:25 -0500",
            "description": """These plugins provide correct groundcover for the altered landscape of the Total Overhaul or Graphics Overhaul mod lists. Generated with Yacoby's <a href="/mods/mw-mesh-generator/">MW Mesh Generator</a>.""",
            "dl_url": "/files/MOMWAesthesiaGroundcoverReworkedPatches.zip",
            "extra_cfg": """<p>Add this to your <code>openmw.cfg</code>:</p>

<pre><code>groundcover=momw_AesAdGrass_AC.omwaddon
groundcover=momw_AesAdGrass_AI.omwaddon
groundcover=momw_AesAdGrass_AL.omwaddon
groundcover=momw_AesAdGrass_BC.omwaddon
groundcover=momw_AesGrass_GL.omwaddon
groundcover=momw_AesAdGrass_WG.omwaddon
groundcover=mw_AesAdGrass_AC.omwaddon
groundcover=mw_AesAdGrass_AI.omwaddon
groundcover=mw_AesAdGrass_AL.omwaddon
groundcover=mw_AesAdGrass_BC.omwaddon
groundcover=mw_AesGrass_GL.omwaddon
groundcover=mw_AesAdGrass_WG.omwaddon</code></pre>

<p>Add this to your <code>settings.cfg</code>:</p>

<pre><code>[Groundcover]
enabled = true
density = 1.0
stomp mode = 2
stomp intensity = 2</code></pre>""",
            "extra_cfg_raw": """#
# MOMW Aesthesia Groundcover Reworked Patches
# (this goes into openmw.cfg)
#
groundcover=momw_AesAdGrass_AC.omwaddon
groundcover=momw_AesAdGrass_AI.omwaddon
groundcover=momw_AesAdGrass_AL.omwaddon
groundcover=momw_AesAdGrass_BC.omwaddon
groundcover=momw_AesGrass_GL.omwaddon
groundcover=momw_AesAdGrass_WG.omwaddon
groundcover=mw_AesAdGrass_AC.omwaddon
groundcover=mw_AesAdGrass_AI.omwaddon
groundcover=mw_AesAdGrass_AL.omwaddon
groundcover=mw_AesAdGrass_BC.omwaddon
groundcover=mw_AesGrass_GL.omwaddon
groundcover=mw_AesAdGrass_WG.omwaddon
#
# MOMW Aesthesia Groundcover Reworked Patches
# (this goes into settings.cfg)
#
[Groundcover]
enabled = true
density = 1.0
stomp mode = 2
stomp intensity = 2""",
            "is_active": False,
            "url": "",
            "usage_notes": """If you want to generate your own custom groundcover plugins, please check out <a href="/tips/creating-custom-groundcover/">the guide I wrote</a>.""",
            "status": "live",
        },
        {
            "name": "Ports Of Vvardenfell - Seyda Neen Gateway Compatibility Plugin",
            "author": "Hristos N. Triantafillou",
            "category": patches,
            "date_added": "2021-09-12 22:28:32 -0500",
            "description": """This plugin deletes some clutter to allow <a href="/mods/ports-of-vvardenfell-tr-totsp-patch/">alvazir's patched Ports Of Vvardenfell</a> to better coexist with <a href="/mods/seyda-neen-gateway-to-vvardenfell/">Seyda Neen - Gateway to Vvardenfell</a>.""",
            "dl_url": "/files/PortsOfVvardenfell_SeydaNeenGateway_Compat.zip",
            "url": "",
            "status": "live",
            "usage_notes": """<p>For Expanded Vanilla, enable the <code>PortsOfVvardenfell_SeydaNeenGateway_ExpandedVanilla_Compat.omwaddon</code> plugin from the download file.</p>

<p>For Graphics Overhaul and Total Overhaul, enable the <code>PortsOfVvardenfell_SeydaNeenGateway_Compat.omwaddon</code> plugin from the download file.</p>""",
        },
        {
            "name": "Ports Of Vvardenfell - TR - TOTSP - Ald'ruhn - Vivec City Patch",
            "author": nexus_user("77253", "alvazir") + ", various",
            "category": patches,
            "date_added": "2021-09-13 17:51:03 -0500",
            "description": """Alvazir's <a href="/mods/ports-of-vvardenfell-tr-totsp-patch/">patched version of the Ports Of Vvardenfell plugin</a>, patched yet again to enable compatibility with <a href="/mods/aldruhn/">Ald'ruhn</a>, <a href="/mods/solstheim-tomb-of-the-snow-prince/">Solstheim Tomb of the Snow Prince</a> (Total Overhaul only), and <a href="/mods/vivec-city/">Vivec City</a>.""",
            "dl_url": "/files/PortsOfVvardenfellTRTOTSPAldruhnVivecCityPatch.zip",
            "url": "",
            "status": "live",
            "usage_notes": """<p>If you're following the Total Overhaul list, download <a href="/files/PortsOfVvardenfellTRTOTSPAldruhnPatch.zip">this file</a>.</p>

<p>If you're following the Expanded Vanilla or Graphics Overhaul lists, download <a href="/files/PortsOfVvardenfellTRAldruhnVivecCityPatch.zip">this file</a>.</p>

<p>I created these plugins by running the following TES3CMD invocation on alvazir's plugins:</p>

<pre><code>$ tes3cmd delete --type land --id '\\(-2, 6\\)' "Ports Of Vvardenfell V1.6 - TR_Travels - TOTSP.esp"
$ tes3cmd delete --type cell --id '\\(3, -9\\)' "Ports Of Vvardenfell V1.6 - TR_Travels - TOTSP.esp"
$ tes3cmd delete --type land --id '\\(3, -9\\)' "Ports Of Vvardenfell V1.6 - TR_Travels - TOTSP.esp"
$ tes3cmd delete --type cell --id '\\(-3, 6\\)' "Ports Of Vvardenfell V1.6 - TR_Travels - TOTSP.esp"
$ tes3cmd delete --type land --id '\\(-3, 6\\)' "Ports Of Vvardenfell V1.6 - TR_Travels - TOTSP.esp"</code></pre>""",
        },
        {
            "name": "Yet Another Better Sounds Patch",
            "author": nexus_user("77253", "alvazir"),
            "category": patches,
            "date_added": "2021-09-23 18:01:31 -0500",
            "description": """This replacement patch fixes flaws of original 1.1 patch identified by collaborative effort and then a bit more. Alternative version included with more places to hear ambient tavern and town sounds.""",
            "status": "live",
            "url": nexus_mod("48955"),
            "usage_notes": """I recommend the "Tavern and Town Sounds Expansion" version.""",
        },
        {
            "name": "alvazir's Bloodmoon Rebalance - Patch for Purists patch",
            "author": nexus_user("77253", "alvazir"),
            "category": patches,
            "date_added": "2021-10-23 17:17:06 -0500",
            "date_updated": "2022-02-20 10:13:55 -0500",
            "description": """This patch adds PfP fixes to 17 creatures.""",
            "status": "live",
            "url": nexus_mod("48955"),
            "usage_notes": """This is no longer needed with recent versions of Bloodmoon Rebalance.""",
        },
        {
            "name": "Bethesda Official Plugins Naturalized",
            "alt_to": [
                official_plugins_fixes_v11,
                "Unofficial Morrowind Official Plugins Patched",
                "Smooth Master Index",
            ],
            "author": nexus_user("16269634", "PikachunoTM"),
            "category": patches,
            "date_added": "2022-11-05 10:32:07 -0500",
            "date_updated": "2023-07-09 11:16:00 -0500",
            "description": """Implementation of Bethesda's Official Plugins with some slightly improved implementation. Uses <a href="/mods/unofficial-morrowind-official-plugins-patched/">UMOPP</a> as a base.""",
            "status": "live",
            "url": nexus_mod("51107"),
            "usage_notes": """If you're following the <span class="bold">Expanded Vanilla</span> or <span class="bold">Total Overhaul</span> mod lists then skip the <code>Area Effect Arrows Naturalized.esp</code>, <code>Siege at Firemoth Naturalized.esp</code>, and <code>Clean_Adamantium Armor Naturalized.esp</code> plugins (they will be excluded for you when using <a href="/cfg">the CFG Generator</a>).""",
        },
        {
            "name": "Merch's Minor Mods: Blighted Animals Retextured Updated",
            "author": nexus_user("12988010", "Merch_Lis"),
            "category": patches,
            "date_added": "2022-11-09 09:49:00 -0500",
            "description": """Updates Blighted Animals Retextured to use some of PB's and Tyddy's newer models. Now includes the Cliffracer Replacer.""",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("51495"),
            "usage_notes": """<p>Requires <a href="/mods/blighted-animals-retextured/">Blighted Animals Retextured</a>.</p>

<p>Download the <code>Blighted Animals Retextured Updated</code> file for this.</p>""",
        },
        {
            "name": "Glass Glowset and Weapon Sheathing Patch",
            "author": "Avienus, Soldifire, and Darknut",
            "category": patches,
            "date_added": "2022-11-12 10:44:00 -0500",
            "date_updated": "2023-06-04 11:32:00 -0500",
            "description": "This plugin-less mod will restore the glow maps of Glass Glowset-modded weapons when sheathed with the Weapon Sheathing mod.",
            "status": "live",
            "url": nexus_mod("48602"),
        },
        {
            "name": "DA_Sobitur_TRIngred_Compat",
            "author": nexus_user("558771", "dagothagahnim") + " and eddie5",
            "category": patches,
            "date_added": "2022-11-12 14:34:00 -0500",
            "date_updated": "2023-01-16 10:13:00 -0500",
            "description": """A updated replacement for the <code>DA_Sobitur_TRIngred_Compat.ESP</code> plugin that comes with <a href="/mods/sobitur-facility/">Sobitur Facility</a> that works with the current version of Tamriel Rebuilt.""",
            "dl_url": "/files/DA_Sobitur_TRIngred_Compat.zip",
            "status": "live",
            "url": "",
        },
        {
            # TODO: Add to TO Patches
            "name": "Uvirith's Legacy - Missing NoM Files",
            "author": "NoM authors",
            "category": patches,
            "date_added": "2022-11-12 15:58:00 -0500",
            "description": """Files that are missing from the Uvirith's Legacy install archive but are used by the mod, originally from Necessities of Morrowind.""",
            "dl_url": "/files/UvirithsLegacyMissingNoMFiles.zip",
            "status": "live",
            "url": "",
        },
        {
            "name": "Alvazir's Various Patches",
            "author": nexus_user("77253", "alvazir"),
            "category": patches,
            "date_added": "2023-06-04 12:57:00 -0500",
            "description": """ Collection of compatibility patches, fixes, mod expansions and tiny mods.""",
            "url": nexus_mod("48955"),
            "status": "live",
        },
        {
            "name": "OAAB Shipwrecks and Justice for Khartag - merged plugin",
            "author": "ffann1998",
            "category": patches,
            "date_added": "2023-07-12 11:24:00 -0500",
            "date_updated": "2023-10-08 12:53:00 -0500",
            "description": """Plugin combining OAAB Shipwreck 1.7.3 and Justice for Khartag therefore resolving conflcts between them. Cleaned with tes3cmd.""",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("53044"),
        },
        {
            "name": "Mono's Minor Moddities",
            "author": nexus_user("2491712", "MonoAccipiter"),
            "category": patches,
            "date_added": "2023-09-25 12:17:00 -0500",
            "date_updated": "2023-11-08 12:30:00 -0500",
            "description": "Various minor mods patched together mostly as small improvements or patches to other popular mods.",
            "status": "live",
            "url": nexus_mod("53027"),
        },
        {
            "name": "Better Flames for Concept Art Palace (Vivec City) - OpenMW Version",
            "author": nexus_user("98746398", "Dyvalas"),
            "category": patches,
            "date_added": "2023-10-16 17:07:00 -0500",
            "description": "This mod is a very simple modification of the texture already provided by the OpenMWVersion of the Concept Art Palace original mod, improving its look.",
            "status": "live",
            "url": nexus_mod("52903"),
        },
        {
            "name": "Minor patches and fixes",
            "author": nexus_user("1808532", "Lord Zarcon"),
            "category": patches,
            "date_added": "2023-11-10 12:56:00 -0500",
            "description": "Fixes some incompatibilities between Lava - Mold - Mud Caverns Revamp by SVNR and the popular modder repositories of TR and OAAB.",
            "status": "live",
            "url": nexus_mod("53570"),
        },
        {
            "name": "Oh God Snow for Skies.iv and OpenMW",
            "author": nexus_user("3248311", "ward"),
            "category": patches,
            "date_added": "2023-11-10 13:11:00 -0500",
            "description": "The blizzard textures are blinding when using skies .iv and OpenMW. This is a very simple texture replacement for the snow, so it's not as dense.",
            "status": "live",
            "url": nexus_mod("50509"),
        },
        {
            "name": "Sigourn's Misc Mods and Patches",
            "author": "Sigourn and MMC",
            "category": patches,
            "date_added": "2023-11-10 13:36:00 -0500",
            "description": "Compilation of compatibility patches for a variety of mods, edited versions of mods, as well as other minor mods.",
            "status": "live",
            "url": nexus_mod("49232"),
        },
        {
            "name": "Animated Morrowind and Weapon Sheathing patch for OpenMW",
            "author": nexus_user("21574104", "Katya Karrel"),
            "category": patches,
            "date_added": "2023-11-10 13:57:00 -0500",
            "description": "Fixes invisible objects in the hands of Animated NPCs when used together with Weapon sheathing. Useful when you have any mod that uses AM as a resource.",
            "status": "live",
            "url": nexus_mod("53479"),
        },
        {
            "name": "V.I.P. - Vegetto's Important Patches",
            "author": nexus_user("4655319", "Vegetto88"),
            "category": patches,
            "date_added": "2023-11-17 14:02:30 +0100",
            "description": "Small but important things: UV fixes - Add-Ons - Normalmaps - Patches",
            "status": "live",
            "url": nexus_mod("51813"),
        },
        {
            "name": "MOMW Patches",
            "author": "MOMW Team + OpenMW Modding Community",
            "category": patches,
            "date_added": "2023-11-19 12:30:00 -0500",
            "date_updated": "2024-01-06 21:23:25 +0100",
            "description": """Patches made by the Modding-OpenMW.com and OpenMW modding communities.""",
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods, tag_oaab_data, tag_tamriel_data],
            "url": "https://modding-openmw.gitlab.io/momw-patches/",
        },
    ]:
        generate_mod(**m)
