from momw.helpers import generate_mod, nexus_mod, nexus_user


magic_n_spells = "Magic & Spells"

tag_openmw_lua = "OpenMW Lua"

# Names of mods that are used as alts in this file.


def init():
    for m in [
        {
            "alt_to": ["Multiple Teleport Marking (OpenMW and Tamriel Rebuilt)"],
            "name": "Zack's Lua Multimark Mod",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": magic_n_spells,
            "date_added": "2023-10-18 16:49:00 -0500",
            "description": """OpenMW 0.48 or newer required. Allows the player to have multiple marked locations. Very configurable, uses lua to allow marking of any location.""",
            "status": "live",
            "tags": [tag_openmw_lua],
            "url": nexus_mod("53260"),
        }
    ]:
        generate_mod(**m)
