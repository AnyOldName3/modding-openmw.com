from momw.helpers import generate_mod, nexus_mod, nexus_user

water = "Water"

# Names of tags that are used in this file.
tag_high_res = "High Res"
tag_tes3mp_graphics = "TES3MP Graphics"


def init():
    for m in [
        {
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": water,
            "date_added": "2020-06-24 18:19:10 -0500",
            "description": "New effects and textures for TES3 Morrowind waterfalls.",
            "name": "Better Waterfalls",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("45424"),
        },
        {
            "author": nexus_user("3919365", "multiple"),
            "category": water,
            "date_added": "2020-06-24 18:20:10 -0500",
            "description": 'Various modifications to waterfalls and water splashes. Born to fit <a href="/mods/better-waterfalls/">Better Waterfalls</a> mod.',
            "name": "Waterfalls Tweaks",
            "status": "live",
            "url": nexus_mod("46271"),
        },
        {
            "name": "Distant Seafloor for OpenMW",
            "author": "Hemaris",
            "category": water,
            "date_added": "2022-02-21 18:20:10 -0500",
            "date_updated": "2023-06-04 18:20:10 -0500",
            "description": "Extends the seafloor around Vvardenfell to hide the edge of the world in OpenMW. Compatible with everything.",
            "status": "live",
            "url": nexus_mod("50796"),
        },
        {
            "name": "SSTh Low Graphics Water Retex",
            "author": nexus_user("158665803", "SnakeSkullTh"),
            "category": water,
            "date_added": "2024-01-12 06:55:00 -0500",
            "description": "If you have a bad PC, plays the game on a smartphone, likes the nostalgic feeling of the original water, want to save some FPS, but cant get througth the ugly vanilla water, then this mod is for you!",
            "status": "live",
            "url": nexus_mod("53116"),
        },
    ]:
        generate_mod(**m)
