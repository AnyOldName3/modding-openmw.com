from momw.helpers import generate_mod, nexus_mod, nexus_user


combat = "Combat"

tag_openmw_lua = "OpenMW Lua"


def init():
    # print("START: {} ... ".format(leveling), end="")

    _mods = [
        {
            "alt_to": ["Marksman's Eye"],
            "name": "Solthas Combat Pack (OpenMW Lua)",
            "author": nexus_user("2497139", "Solthas"),
            "category": combat,
            "date_added": "2023-04-30 09:36:00 -0500",
            "description": """Get all my combat mods in one place, pre-configured to work together seamlessly. Charge attacks. Timed directional attacks. Dodge "rolls". Etc. Fully configurable.""",
            "status": "live",
            "tags": [tag_openmw_lua],
            "url": nexus_mod("52221"),
            "usage_notes": """<span class="bold">Requires OpenMW 0.48.0 or newer!</span>""",
        }
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
