from momw.helpers import generate_mod


post_processing_shaders = "Post Processing Shaders"
tag_gitlab_mods = "GitLab mods"


def init():
    mods = [
        {
            "name": "OMWFX Shaders",
            "author": "Christian Cann Schuldt Jensen ~ CeeJay.dk, Haasn, Hrnchamd, Knu, peachykeen, Phal, Vtastek, Wazabear",
            "category": post_processing_shaders,
            "date_added": "2022-11-13 13:38:00 -0500",
            "date_updated": "2023-05-26 17:51:00 -0500",
            "description": """A collection of post processing shaders.""",
            "is_featured": True,
            "status": "live",
            "tags": [tag_gitlab_mods],
            "url": "https://gitlab.com/vtastek/omwfx-shaders",
            "usage_notes": """<p><span class="bold">OpenMW 0.48 or newer is required!</span></p>

<p>How to use these:</p>

<ul>
  <li>Enable post processing if it is not already
    <ul>
      <li>Run OpenMW-Launcher</li>
      <li>Navigate to the Advanced >> Visuals >> Post Processing section</li>
      <li>Ensure that "Enable post processing" is checked</li>
    </ul>
  </li>
  <li>Click <a href="https://gitlab.com/vtastek/omwfx-shaders/-/archive/main/omwfx-shaders-main.zip">here</a> to download the mod as a zip file</li>
  <li>Extract the <code>omwfx-shaders-main.zip</code> file to a folder of your choosing and add a data path for it (see below for a reference)</li>
  <li>Run OpenMW, once in game press F2 to open the shaders configuration menu</li>
</ul>

<p>I recommend using these, in this order:</p>

<pre><code>ssao_hq
underwater_interior_effects
underwater_effects
godrays
wetworld
SSR
hdr</code></pre>""",
        },
        {
            "name": "Zesterer's Volumetric Cloud & Mist Mod for OpenMW",
            "author": "<a href='https://github.com/zesterer'>zesterer</a>",
            "category": post_processing_shaders,
            "date_added": "2022-11-13 13:49:00 -0500",
            "date_updated": "2023-07-15 11:51:00 -0500",
            "description": """A mod that adds raycasted volumetric clouds to Morrowind.""",
            "extra_cfg": """<p><span class="bold">OpenMW 0.48.0 or newer is required for this to work</span>. Additionally, the following needs to be added to your <code>[Shaders]</code> section in the <code>settings.cfg</code> file:</p>

<pre><code>force per pixel lighting = true</code></pre>""",
            "extra_cfg_raw": """#
# Zesterer's Volumetric Cloud & Mist Mod for OpenMW:
# (This goes into settings.cfg under [Shaders])
#
force per pixel lighting = true""",
            "is_featured": True,
            "status": "live",
            "url": "https://github.com/zesterer/openmw-volumetric-clouds#installing",
            "usage_notes": """<p><span class="bold">OpenMW 0.48 or newer is required!</span> Click <a href="https://github.com/zesterer/openmw-volumetric-clouds/archive/refs/heads/main.zip">here</a> to download the mod as a zip file. Be sure to check the README for this, including <a href="https://github.com/zesterer/openmw-volumetric-clouds#recommendations">the Recommendations section</a>.</p>

<p>Shaders are enabled and configured with the <code>F2</code> key while in game.</p>

<p>You will need to enter the F2 menu to disable this shader's "Interior Mist" feature as it can cause the screen to be black when spawning in an interior (<a href="https://gitlab.com/modding-openmw/modding-openmw.com/-/issues/140">related issue</a>).</p>

<p>If you're following a mod list: this shader's cloud and sky replacements need to be disabled (they don't fully interact with particles, causing a jarring visual effect. This is a limitation with OpenMW's current shaders system). In the settings menu for this shader, disable: <code>clouds_enabled</code>, <code>dynamic_clouds</code>, and <code>replace_skybox</code>.</p>

<p>If you're using the <code>godrays</code> shader from <a href="/mods/omwfx-shaders/">OMWFX Shaders</a>, then you'll want to disable the sun in this one (set <code>better_sun</code> to "off").</p>""",
        },
        {
            "name": "Bloom Linear",
            "author": "OpenMW Authors",
            "category": post_processing_shaders,
            "date_added": "2022-11-29 19:23:00 -0500",
            "description": """Bloom shader performing its calculations in (approximately) linear light.""",
            "is_featured": True,
            "status": "live",
            "url": "",
        },
        {
            "name": "MOMW Post Processing Pack",
            "author": "Dexter, Zesterer, wazabear, akortunov, wareya, various",
            "category": post_processing_shaders,
            "date_added": "2023-09-02 12:41:00 -0500",
            "description": """<p>A collection of post-processing shaders for OpenMW.</p>
<p>Includes: <a href="https://gitlab.com/akortunov/xe-shaders">XE-Shaders by akortunov and various others</a>, <a href="https://gitlab.com/vtastek/omwfx-shaders">OMWFX-Shaders by vtastek and various others</a>, <a href="https://github.com/zesterer/openmw-volumetric-clouds">OpenMW Volumetric Clouds by Zesterer</a>, and <a href="https://github.com/wareya/OpenMW-Shaders">OpenMW-Shaders by wareya</a>.</p>""",
            "status": "live",
            "tags": [tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/momw-post-processing-pack/",
        },
    ]

    for m in mods:
        generate_mod(**m)
