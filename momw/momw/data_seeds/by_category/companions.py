from momw.helpers import generate_mod, nexus_mod, nexus_user


companions = "Companions"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"

# Names of mods that are used as alts in this file.
# to_do = "TO DO"


def init():
    _mods = [
        {
            "author": "Seth",
            "category": companions,
            "date_added": "2020-07-21 18:45:35 -0500",
            "description": "This roleplayer-friendly mod adds the ability to recruit nearly any NPC as a companion with functional inventory sharing, as long as the proper conditions are met. OpenMW required.",
            "name": "Persuasive Speech (OpenMW)",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("47388"),
        },
        {
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": companions,
            "compat": "unknown",
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """<p>This mod adds a corgi companion to Morrowind made by me [Melchior Dahrk, not me the site author]. The corgi, named Dinky by request, is a fully functional companion. You may now enjoy countless hours exploring Vvardenfell and beyond with this lovable pet and friend.</p>""",
            "name": "Corgi Companion",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("43985"),
            "usage_notes": "Other companion mods work fine so this one should too.",
        },
        {
            "name": "Blademeister",
            "author": nexus_user("30459795", "superduple"),
            "category": companions,
            "date_added": "2023-09-11 13:02:00 -0500",
            "date_updated": "2024-01-06 11:50:47 +0100",
            "description": """Tucked away in the flooded back tunnels of Addamasartus is a talking, shapeshifting blade that hungers for the power it once held. It asks that you help it regain what was lost, and in exchange, it will be your most powerful ally. """,
            "status": "live",
            "url": nexus_mod("52740"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
