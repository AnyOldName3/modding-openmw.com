from momw.helpers import generate_mod, nexus_mod, nexus_user

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_interesting_addition = "Interesting Additions"
tag_high_res = "High Res"
tag_oaab_data = "OAAB Data"
tag_vanilla_friendly = "Vanilla Friendly"

trees = "Trees"


def init():
    for m in [
        {
            "author": nexus_user("41808", "vurt"),
            "category": trees,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "New trees for the Bitter Coast region. REQUIRES the .esp. Otherwise the tree fungi won't fit the new trunks.",
            "name": "Vurts Bitter Coast Trees II",
            "needs_cleaning": True,
            "slug": "vurts-bitter-coast-trees-ii",
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly, tag_high_res],
            "url": nexus_mod("37489"),
        },
        {
            "author": nexus_user("41808", "vurt"),
            "category": trees,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "New trees for the Grazelands region, inspired by trees from Africa.",
            "name": "Vurts Grazeland Trees II",
            "slug": "vurts-grazeland-trees-ii",
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly, tag_high_res],
            "url": nexus_mod("37038"),
        },
        {
            "name": "Vanilla-friendly West Gash Tree Replacer",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": trees,
            "date_added": "2019-02-28 20:55:00 -0500",
            "date_updated": "2022-11-07 13:39:00 -0500",
            "description": "A West Gash tree replacer for players who desire a look closer to the vanilla game for the region.",
            "status": "live",
            "tags": [
                tag_game_expanding,
                tag_vanilla_friendly,
                tag_high_res,
                tag_oaab_data,
            ],
            "url": nexus_mod("44173"),
        },
        {
            "author": nexus_user("899234", "Remiros"),
            "category": trees,
            "date_added": "2019-02-28 21:00:00 -0500",
            "description": "Replaces all of the trees in the Ascadian Isles region.",
            "name": "Remiros' Ascadian Isles Trees 2",
            "status": "live",
            "tags": [tag_game_expanding, tag_high_res],
            "url": nexus_mod("45779"),
            "usage_notes": """The author recommends the vanilla version, which has no plugin.  Feel free to try the Lush version and see how it works out, though it will have lower performance than the vanilla option.""",
        },
        {
            "author": nexus_user("41808", "vurt"),
            "category": trees,
            "date_added": "2019-02-28 21:42:00 -0500",
            "date_updated": "2020-11-22 10:38:40 -0500",
            "description": ".esp-less tree replacer for the Solstheim region.",
            "name": "Vurts Solstheim Tree Replacer II",
            "status": "live",
            "tags": [tag_game_expanding, tag_high_res],
            "url": nexus_mod("37856"),
        },
        {
            "author": nexus_user("4381248", "PeterBitt"),
            "category": trees,
            "date_added": "2019-03-27 18:15:00 -0500",
            "description": "This is a mesh and texture replacer for the Emperor Parasol mushroom trees.",
            "name": "Mushroom Tree Replacer",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("45350"),
            "usage_notes": """OpenMW does not yet support PBR, so you do not need those files.""",
        },
        {
            "author": nexus_user("66357466", "ezze"),
            "category": trees,
            "date_added": "2021-06-13 10:40:41 -0500",
            "description": "Transform the bleak landscapes of Vvardenfell in into lush, green forested areas.",
            "name": "Morrowind Forested",
            "status": "live",
            "tags": [tag_interesting_addition],
            "url": nexus_mod("48975"),
            "usage_notes": """<p>Download the main "Morrowind Forrested" file as well as any of the groundcover or other addon files as desired. There's also a "Lore-friendlier variant" which does not touch Ashlands, Red Mountain and Molag Amur (see the mod images for examples of this one).</p>""",
        },
        {
            "name": "SM Bitter Coast Tree Replacer",
            "author": nexus_user("3755459", "ShadowMimicry"),
            "alt_to": ["Vurts Bitter Coast Trees II"],
            "category": trees,
            "date_added": "2022-11-07 12:23:00 -0500",
            "description": """<p>This mod replaces the eight main trees on the Bitter Coast.</p>

<p>The total number and location of trees did not change. The mod comes without an esp file, so don't forget to make copies of the original files(meshes/f). It's just 8 new models, 8 new textures.</p>

<p>The geometry of the models is 95 percent the same as vanilla.</p>""",
            "status": "live",
            "url": nexus_mod("49883"),
        },
        {
            "name": "Melchior's Excellent Grazelands Acacia",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "alt_to": ["Vurts Grazeland Trees II"],
            "category": trees,
            "date_added": "2022-11-07 13:21:00 -0500",
            "description": "This mod is a pluginless replacer for the Grazelands trees which was inspired by the umbrella thorn acacia species.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51058"),
            "usage_notes": """You need the <code>00 Core</code> and <code>02 OAAB Saplings Patch</code> folders.""",
        },
    ]:
        generate_mod(**m)
