from momw.helpers import generate_mod, nexus_mod, nexus_user


leveling = "Leveling"

tag_openmw_lua = "OpenMW Lua"
tag_gitlab_mods = "GitLab mods"

# Names of mods that are used as alts in this file.
linoras_leveling_mod = "Linoras Leveling Mod"
madd_leveler = "Madd Leveler"
ncgd = "Natural Character Growth and Decay - MW"
talrivians_state_based_hp_mod = "Talrivian's State-Based HP Mod"


def init():
    for m in [
        {
            "alt_to": [talrivians_state_based_hp_mod],
            "author": nexus_user("22851484", "Greywander")
            + " (with edits by Atahualpa and johnnyhostile)",
            "category": leveling,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2021-07-20 08:25:57 -0500",
            "description": """<p>The new Natural Grow leveling mod, only for OpenMW.</p>

<ul>
    <li>Your attributes grow automatically as your skills increase.</li>
    <li>Your skills and attributes can grow past 100, but...</li>
    <li>It becomes increasingly difficult to increase your skills the farther past 100 you go.</li>
    <li>Your skills will also decay over time. (Optional)</li>
</ul>""",
            "dl_url": "/files/ncgdMW.zip",
            "is_featured": True,
            "name": ncgd,
            "slug": "natural-character-growth-and-decay-mw",
            "status": "live",
            "url": nexus_mod("44967"),
        },
        {
            "alt_to": [ncgd],
            "author": "Mad Mugsy and the Madd Leveler Team",
            "category": leveling,
            "compat": "partially working",
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """How many times have you started a new game of Morrowind, only to think to yourself, "Oh crap, I have to build up my skill X to go along with my skill Y so I can get a 5x multiplier for Stat Z." If you're like us and hate planning this stuff out and just want to play the darn game already, this mod is for you. What the Madd Leveler mod does is strip out any need to plan your character out in advance. Your statistics will increase automatically as you use your skills, and you won't need to worry about trying to obtain 5x multipliers for leveling up.This mod is also a skill and attribute uncapper, effectively removing the 100 point limit.""",
            "name": madd_leveler,
            "status": "live",
            "url": nexus_mod("45865"),
        },
        {
            "alt_to": [madd_leveler, ncgd],
            "author": nexus_user("1118220", "Linora"),
            "category": leveling,
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """<p>Changes 11 GMSTs to remove the need to stat-crunch in Morrowind.</p>

<p>Basically I altered 9 GMSTs that make it so if you gain 1 skill point or more in a skill governed by an attribute you always get a x5 when adding points to named attribute when leveling. This essentially removes the need to skill-watch.</p>

<p>To balance this I altered 2 more GMSTs, so as to make it so MISC and non-Minor/Major specialization skills are not as easy to level up, so one can't so easily just level up a MISC skill and get that +5 bonus from just doing that.</p>""",
            "is_active": False,
            "name": linoras_leveling_mod,
            "status": "live",
            "url": nexus_mod("23676"),
        },
        {
            "alt_to": [linoras_leveling_mod, madd_leveler, ncgd],
            "author": "Galsiah",
            "category": leveling,
            "compat": "partially working",
            "date_added": "2019-03-04 18:05:00 -0500",
            "description": """<p>GCD changes Morrowind's levelling system to make it seamless. Limits are removed from skills and attributes, and skill gain is slowed down exponentially to preserve game balance. Skills influence attributes, health and magicka directly. Characters remain diverse throughout the game, with strengths and weaknesses dependent on initial skill choices, skill increases and racial factors.</p>""",
            "is_active": False,
            "name": "Galsiah's Character Development",
            "status": "live",
            "url": "https://www.moddb.com/games/morrowind/addons/galsiahs-character-development",
        },
        {
            "alt_to": [
                linoras_leveling_mod,
                madd_leveler,
                ncgd,
                "Galsiah's Character Development",
            ],
            "name": "Meet thy Ancestor for Level Up",
            "author": "ezze",
            "category": leveling,
            "date_added": "2021-09-15 18:05:00 -0500",
            "description": """<p>This mod changes the way to level up so that is thematic and immersive.</p>

<p>The character has a "focus" a special amulet where his or her umbilical cord has been embedded in tough Alit teeth.</p>

<p>Using the focus and burning incense the character goes in a deep meditative state and meet wise spirits, custodians of knowledge, and learn from them.</p>""",
            "status": "live",
            "url": nexus_mod("50252"),
        },
        {
            "name": "NCGDMW Lua Edition",
            "author": nexus_user("22851484", "Greywander")
            + ", EvilEye, johnnyhostile, Mehdi Yousfi-Monod",
            "category": leveling,
            "date_added": "2022-02-06 16:32:51 -0500",
            "date_updated": "2022-03-26 15:52:06 -0500",
            "description": """<p>A "Natural Grow" leveling mod, where your attributes grow automatically as your skills increase and your skills will also decay over time (Optional).</p>

<p>This is a port of <a href="/mods/natural-character-growth-and-decay-mw/">Greywander's "Natural Character Growth and Decay - MW" mod</a> for the OpenMW-Lua API.</p>
<ul>
  <li>Your attributes grow automatically as your skills increase.</li>
  <li>Your skills will also decay over time. (Optional)</li>
  <li>Many options are configurable at any time via the mod settings menu, accessible in-game.</li>
  <li>Unlike the original MWScript version, your attributes and skills do not grow past 100! This is implemented by <a href="/mods/mbsp-uncapped-openmw-lua-ncgd-compat/">a companion mod</a>.</li>
  <li class="bold">See the included <code>docs</code> directory for more information.</li>
  <li><a href="https://gitlab.com/modding-openmw/ncgdmw-lua">Open source</a>!</li>
</ul>""",
            "is_featured": True,
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/ncgdmw-lua/",
        },
        {
            "alt_to": ["NCGDMW Lua Edition"],
            "name": "Phi's Carefree Leveling",
            "author": '<a href="https://github.com/phi-fell">Phi</a>',
            "category": leveling,
            "date_added": "2022-12-01 18:01:00 -0500",
            "description": """Optimal vanilla leveling for OpenMW 0.48.""",
            "status": "live",
            "tags": [tag_openmw_lua],
            "url": "https://github.com/phi-fell/carefree_leveling#phis-carefree-leveling",
        },
        {
            "name": "MBSP Uncapped (OpenMW Lua) - NCGD Compat",
            "author": nexus_user("6708039", "Phoenixwarrior7"),
            "category": leveling,
            "date_added": "2023-09-27 14:24:00 -0500",
            "description": "This mod is port/rebuild of Magicka Based Spell Progression in Lua, combined with a universal skill uncapper. Designed for use with (and requiring) NCGDMW Lua Edition.",
            "status": "live",
            "tags": [tag_openmw_lua],
            "url": nexus_mod("53064"),
        },
    ]:
        generate_mod(**m)
