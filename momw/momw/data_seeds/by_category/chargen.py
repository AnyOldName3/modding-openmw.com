from momw.helpers import generate_mod, nexus_mod, nexus_user


chargen = "Chargen"

tag_gitlab_mods = "GitLab mods"
tag_openmw_lua = "OpenMW Lua"
tag_dev_build_only = "Dev Build Only"
tag_tamriel_data = "Tamriel Data"


def init():
    for m in [
        {
            "author": nexus_user("19777744", "helswake"),
            "category": chargen,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Vanilla-friendly character generation that does away with all tutorial pop-ups and makes the process more expedient while keeping it interesting.",
            "is_featured": True,
            "name": "Better Chargen",
            "slug": "better-chargen",
            "status": "live",
            "url": nexus_mod("43995"),
        },
        {
            "name": "Chargen Revamped - Expanded Lands",
            "author": nexus_user("38286905", "Texafornian"),
            "category": chargen,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """A continuation and complete overhaul of Chargen Revamped, the classic "alternate start" mod, with support for Solstheim, Mainland Morrowind (Tamriel Rebuilt), Skyrim (Skyrim: Home of the Nords), and Cyrodiil (Province: Cyrodiil). Includes optional customization or randomization of starting attributes, skills, spells, items, companions, factions, and locations. Different versions are included for vanilla, TR, and TR+SHOTN+PC.""",
            "status": "live",
            "url": nexus_mod("44615"),
        },
        {
            "name": "NoPopUp Chargen",
            "author": "johnnyhostile",
            "category": chargen,
            "date_added": "2023-11-19 07:39:00 -0500",
            "description": """Removes all tutorial pop-ups from chargen. Works with OpenMW and Morrowind.exe/MGE/MWSE.""",
            "status": "live",
            "url": "https://modding-openmw.gitlab.io/nopopup-chargen/",
        },
        {
            "name": "Firewatch Character Creation",
            "author": nexus_user("790766", "Zackhasacat"),
            "category": chargen,
            "date_added": "2023-11-22 16:08:00 -0500",
            "description": """Moves the vanilla character creation to Firewatch. Requires latest Tamriel Rebuilt and OpenMW 0.49 dev.""",
            "status": "live",
            "tags": [tag_openmw_lua, tag_dev_build_only, tag_tamriel_data],
            "url": nexus_mod("45910"),
        },
    ]:
        generate_mod(**m)
