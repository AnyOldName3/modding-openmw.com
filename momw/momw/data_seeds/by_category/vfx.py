from momw.helpers import generate_mod, nexus_mod, nexus_user


vfx = "VFX"

# Names of tags that are used in this file.
tag_tamriel_data = "Tamriel Data"


def init():
    mods = [
        {
            "name": "Fireflies",
            "author": nexus_user("59284071", "RandomPal"),
            "category": vfx,
            "date_added": "2022-11-08 08:32:00 -0500",
            "description": "Adds fireflies around the Bitter Coast and the Ascadian Isles",
            "status": "live",
            "tags": [tag_tamriel_data],
            "url": nexus_mod("51443"),
        },
        {
            "name": "Cave Drips",
            "author": nexus_user("3241081", "R-Zero"),
            "category": vfx,
            "date_added": "2022-11-08 08:42:00 -0500",
            "description": "Adds a visual effect to all vanilla cave drip sound emitters.",
            "status": "live",
            "url": nexus_mod("43488"),
        },
        {
            "name": "Spells Reforged - Shield",
            "author": "Greatness7 - Kurpulio",
            "category": vfx,
            "date_added": "2022-11-12 17:50:00 -0500",
            "date_updated": "2023-05-21 12:25:00 -0500",
            "description": "Replaces Shield spell visual effect.",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("50905"),
            "usage_notes": """This mod is no longer active or available, see <a href="/mods/spells-reforged/">Spells Reforged</a> instead.""",
        },
        {
            "name": "Spells Reforged",
            "author": "Greatness7 - Kurpulio",
            "category": vfx,
            "date_added": "2023-05-21 12:26:00 -0500",
            "date_updated": "2023-07-12 11:36:00 -0500",
            "description": "Spell effect visual replacers.",
            "status": "live",
            "url": nexus_mod("52263"),
        },
        {
            "name": "Improved Propylon Particles",
            "author": nexus_user("15199204", "Markel"),
            "category": vfx,
            "date_added": "2023-10-31 00:53:00 -0500",
            "description": "Replaces the particles in Propylon chambers with cool magicky starfield ones.",
            "status": "live",
            "url": nexus_mod("44509"),
        },
    ]

    for m in mods:
        generate_mod(**m)
