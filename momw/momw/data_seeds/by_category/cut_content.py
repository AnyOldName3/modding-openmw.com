from momw.helpers import generate_mod, nexus_mod, nexus_user

cut_content = "Cut Content"


def init():
    _mods = [
        {
            "name": "Cutting Room Floor",
            "description": """This mod aims to be the one stop mod for all cut content in The Elder Scrolls III: Morrowind, and continues to evolve over time, including more content along the way. What this mod is restoring includes items, creatures, quests, NPC's, keys, locations, scripts, spells, sounds, dialogue and voice acting.""",
            "author": nexus_user("64610026", "CiceroTR"),
            "category": cut_content,
            "date_added": "2023-11-07 22:42:00 -0500",
            "status": "live",
            "url": nexus_mod("47307"),
        },
        {
            "name": "Artifacts Reinstated",
            "description": """Necromancer's Amulet and the Bloodworm Helm now have their own unique model. Alongside some adjustments to the stats for artifacts in game to match the ones they are described to have in the lore. Also now Eltonbrand has a unique appearance and Madstone of the Ahemussa is unique again due to a model swap on the Daedric Sanctuary Amulet.""",
            "author": nexus_user("64610026", "CiceroTR"),
            "category": cut_content,
            "date_added": "2023-11-07 23:08:00 -0500",
            "status": "live",
            "url": nexus_mod("48074"),
        },
    ]

    for m in _mods:
        generate_mod(**m)
