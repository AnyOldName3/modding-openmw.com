from momw.helpers import generate_mod, nexus_mod, nexus_user


caves_and_dungeons = "Caves and Dungeons"

tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_normal_maps = "Normal Maps"
tag_oaab_data = "OAAB Data"
tag_openmw_lua = "OpenMW Lua"
tag_tamriel_data = "Tamriel Data"


def init():
    mods = [
        {
            "name": "Caverns Bump Mapped",
            "slug": "caverns-bump-mapped",
            "author": nexus_user("1600249", "Lougian"),
            "category": caves_and_dungeons,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-11-22 12:55:41 -0500",
            "description": "Bump mapped cave textures by Lougian -- do not use the included meshes or else they will be awkwardly shiny.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("42412"),
            "usage_notes": """<p>OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.</p>

<p>Download both the main file as the optional file for Bloodmoon (see the suggested folders below).</p>""",
        },
        {
            "name": "Mines and Caverns",
            "author": nexus_user("4638419", "jsp25"),
            "category": caves_and_dungeons,
            "date_added": "2018-08-05 21:00:00 -0500",
            "date_updated": "2023-09-19 13:03:00 -0500",
            "description": """<p>This mod attempts to improve several vanilla dungeons aesthetically and/or expand them.</p>

<p>No vanilla models are replaced and the changes to exterior cells have been kept minimal for compatibility with other mods.</p>

<p>It features a day/night lightning sistem; the dungeons with cracked ceilings will have it's lightning changed at 18:00 (night) and at 6:00 (day). Some torches will lit/unlit at the same time.</p>""",
            "status": "live",
            "tags": [tag_game_expanding, tag_normal_maps],
            "url": nexus_mod("44893"),
        },
        {
            "name": "Andrano Ancestral Tomb Remastered",
            "author": "Greatness7 MattTheBagel VvardenfellTribes",
            "category": caves_and_dungeons,
            "date_added": "2021-09-06 13:01:30 -0500",
            "description": """An overhaul for the second main quest and its associated dungeon.""",
            "status": "live",
            "url": nexus_mod("44672"),
        },
        {
            "name": "Mamaea Awakened",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": caves_and_dungeons,
            "date_added": "2021-09-19 16:38:26 -0500",
            "date_updated": "2023-09-19 13:39:00 -0500",
            "description": """In vanilla Morrowind, Mamaea is a dull maze of caves with nothing remarkable about them aside from the spine of bone down the center of the cavern network. This mod aims to make Mamaea a more memorable visit for the player.""",
            "status": "live",
            "url": nexus_mod("46096"),
        },
        {
            "name": "Lava - Mold - Mud Caverns Revamp",
            "author": nexus_user("88610433", "SVNR"),
            "category": caves_and_dungeons,
            "date_added": "2021-10-03 12:39:55 -0500",
            "date_updated": "2022-11-08 17:24:00 -0500",
            "description": """High poly caverns with optional normal maps and sharp-edged meshes.""",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("50191"),
        },
        {
            "name": "Samarys Ancestral Tomb Expanded",
            "author": "ATL Team and PikachunoTM",
            "category": caves_and_dungeons,
            "date_added": "2021-08-22 16:58:49 -0500",
            "description": "An ancient burial lies in ruins outside of Seyda Neen – the few who know of the secrets it holds guard them closely, and the tomb lays all but forgotten.",
            "status": "live",
            "url": nexus_mod("45612"),
        },
        {
            "name": "Caverns Bump Mapped Fix",
            "author": nexus_user("70336838", "Necrolesian"),
            "category": caves_and_dungeons,
            "date_added": "2022-11-07 14:00:00 -0500",
            "description": "Fixed versions of two meshes in Lougian's Caverns Bump Mapped and Bloodmoon Caverns Bump Mapped.",
            "status": "live",
            "url": nexus_mod("47147"),
        },
        {
            "name": "New Ilunibi",
            "author": nexus_user("4139826", "seelof"),
            "category": caves_and_dungeons,
            "date_added": "2022-11-07 15:03:00 -0500",
            "date_updated": "2024-01-03 11:51:47 +0100",
            "description": "This mod rebuilds the mainquest dungeon Ilunibi from scratch to make it larger, more imposing and more interesting to explore. It also adds four new quests for you to discover and embark upon.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50393"),
        },
        {
            "name": "Of Eggs and Dwarves - Gnisis Eggmine and Bethamez Overhaul",
            "author": nexus_user("4139826", "seelof"),
            "category": caves_and_dungeons,
            "date_added": "2022-11-08 17:59:00 -0500",
            "description": "This mod rebuilds and expands the Gnisis Eggmine complex from scratch, which includes the Eggmine, the Lower Eggmine, the Underground Stream, and Bethamez. It also adds a quest, which asks the player to investigate the origins of the tremors that have been plaguing the eggmine ever since the Dwemer ruin was uncovered. Features actual earth quakes!",
            "status": "live",
            "tags": [tag_openmw_lua, tag_oaab_data],
            "url": nexus_mod("51171"),
            "usage_notes": """<span class="bold">Requires OpenMW 0.48 or newerand <a href="/mods/oaab_data/">OAAB_Data</a>!</span>""",
        },
        {
            "name": "Tales from the Ashlands - The Great Hive Baan Binif",
            "author": "The Hive Mind",
            "category": caves_and_dungeons,
            "date_added": "2022-11-08 18:16:00 -0500",
            "description": "Implementation of the official concept art drawing by Michael Kirkbride.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51419"),
            "usage_notes": """<p>Requires <a href="/mods/oaab_data/">OAAB_Data</a>.</p>""",
        },
        {
            "name": "Berandas Overhaul",
            "author": nexus_user("4139826", "seelof"),
            "category": caves_and_dungeons,
            "date_added": "2022-11-11 17:40:00 -0500",
            "date_updated": "2023-07-02 12:10:00 -0500",
            "description": "This mod overhauls and slightly expands the old Dunmer fortress Berandas near Gnisis. Daedra have claimed Berandas, but other than the few roaming Daedra themselves there is not much evidence of the corruption. With this mod, old daedric structures have risen from the deep to transform the fortress into a proper daedric stronghold.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("48236"),
            "usage_notes": """<p>Requires <a href="/mods/oaab_data/">OAAB_Data</a>.</p>

<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean "Berandas Overhaul.esp"

CLEANING: "Berandas Overhaul.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
Loaded cached Master: <DATADIR>/oaab_data.esm
 Cleaned duplicate object instance (in_strong_vaultdoor00 FRMR: 24479) from CELL: berandas, keep, top level
Output saved in: "Clean_Berandas Overhaul.esp"
Original unaltered: "Berandas Overhaul.esp"

Cleaning Stats for "Berandas Overhaul.esp":
       duplicate object instance:     1</code></pre>
</details>""",
        },
        {
            "name": "OAAB - Tombs and Towers",
            "author": nexus_user("899234", "Remiros"),
            "category": caves_and_dungeons,
            "date_added": "2022-11-11 17:53:00 -0500",
            "description": "A remake of tombs and velothi towers.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("49131"),
            "usage_notes": """<p><span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span>.</p>

<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean "OAAB - Tombs and Towers.ESP"

CLEANING: "OAAB - Tombs and Towers.ESP" ...
Loading Master: morrowind.esm
Loading Master: tribunal.esm
Loading Master: bloodmoon.esm
Loading Master: oaab_data.esm
 Cleaned duplicate object instance (in_velothismall_ndoor_01 FRMR: 437723) from CELL: redas ancestral tomb
 Cleaned duplicate object instance (in_velothismall_ndoor_01 FRMR: 322825) from CELL: tharys ancestral tomb
Output saved in: "Clean_OAAB - Tombs and Towers.ESP"
Original unaltered: "OAAB - Tombs and Towers.ESP"

Cleaning Stats for "OAAB - Tombs and Towers.ESP":
       duplicate object instance:     2</code></pre>
</details>""",
        },
        {
            "name": "The Corprusarium experience",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": caves_and_dungeons,
            "date_added": "2022-11-11 17:59:00 -0500",
            "description": "Overhaul of the corprusarium, making it the creepy place it was meant to be.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49738"),
            "usage_notes": """<p><span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span></p>

<p>If you're following a mod list, only the <code>00 - Core</code> folder is needed.</p>""",
        },
        {
            "name": "Better Dunmer Strongholds",
            "author": nexus_user("3346952", "Dallara1000"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-08 11:47:00 -0500",
            "description": "This little replacement makes the old Dunmer strongholds less empty and a little bit more dangerous.",
            "status": "live",
            "url": nexus_mod("49692"),
        },
        {
            "name": "Adanumuran Reclaimed",
            "author": nexus_user("1233897", "Danae123"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 13:30:00 -0500",
            "description": "Clear Adanumuran (North west shore of Lake Amaya) of the smugglers and Nix-Hounds to start your own smuggling operations.",
            "status": "live",
            "url": nexus_mod("43340"),
        },
        {
            "name": "Fadathram",
            "author": nexus_user("153434038", "sch2266"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 13:49:00 -0500",
            "description": "Overhaul of Fadathram Ancestral Tomb. Recommended level 14 and higher. As a rule of thumb, if you can take on two ascended sleepers you should be fine.",
            "status": "live",
            "url": nexus_mod("52871"),
        },
        {
            "name": "Bthuand Expanded",
            "author": nexus_user("4638419", "jsp25"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 14:00:00 -0500",
            "description": "It expands Bthuand with a big cave that serves as a cliff racer burrow; a few ashlanders take care of them in order to farm their eggs.",
            "status": "live",
            "url": nexus_mod("46055"),
        },
        {
            "name": "Andrethi Tomb Overhaul",
            "author": nexus_user("4638419", "jsp25"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 14:07:00 -0500",
            "description": "It is a small but nice mod that overhauls Andrethi Ancestral Tomb.",
            "status": "live",
            "url": nexus_mod("46217"),
        },
        {
            "name": "Vennin's Ulummusa Overhaul",
            "author": nexus_user("5279365", "stonedoughnut5") + " AKA Vennin",
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 14:21:00 -0500",
            "description": "An overhaul of the cave Ulummusa using OAAB_Data assets.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("52699"),
        },
        {
            "name": "Vennin's Pulk Overhaul",
            "author": nexus_user("5279365", "stonedoughnut5") + " AKA Vennin",
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 14:29:00 -0500",
            "description": "A remake of the cavern Pulk using OAAB assets while keeping the original cave design in mind.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("52616"),
        },
        {
            "name": "Shal Overgrown",
            "author": nexus_user("1233897", "Danae123"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 14:38:00 -0500",
            "description": "Travel to Shal and discover its secrets: the once small cave is now overgrown and even reaches into the nearby Daedric ruin Ashurnibibi.",
            "status": "live",
            "url": nexus_mod("47219"),
        },
        {
            "name": "Dungeon Details Abaesen-Pulu Egg Mine",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 14:44:00 -0500",
            "description": "Details Abaesen-Pulu Egg Mine with OAAB assets, the mine near Seyda Neen and the one you get Tetra the Pack Guar in.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53285"),
        },
        {
            "name": "OAAB - Hawia Egg Mine",
            "author": nexus_user("58363776", "Juidius Xentao"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 15:07:00 -0500",
            "description": "A remake of Hawia egg mine using OAAB assets.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51846"),
        },
        {
            "name": "Drethos Ancestral Tomb",
            "author": nexus_user("4139826", "seelof") + " and Melchior Dahrk",
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 15:14:00 -0500",
            "description": "This mod adds a new dungeon to the Bitter Coast Region: Drethos Ancestral Tomb. Ancient and forgotten, it hides a sad story about loss and grief. Will you find your way through root and rock to uncover both it and an old and forsaken artifact?",
            "status": "live",
            "url": nexus_mod("53481"),
        },
        {
            "name": "What Kwamas Forage - An Eggmine Mod",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-09-19 15:23:00 -0500",
            "description": "Remakes the eggmines using OAAB assets and adds some minor loot. Now, eggmines look unique and you have a reason to go into them.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53360"),
        },
        {
            "name": "Daedric Shrine Overhaul Exteriors Mehrunes Dagon and Molag Bal",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-10-29 17:29:00 -0500",
            "description": "Overhauls the exteriors of shrines to Molag Bal and Mehrunes Dagon, so the exteriors match better with interiors from my Daedric Shrine Overhaul interiors.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53250"),
        },
        {
            "name": "Daedric Shrine Overhaul Vaermina",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-10-29 17:34:00 -0500",
            "description": "Turns Kushtashpi into a shrine to Vaermina instead of Molag Bal. <span class='bold'>Requires <a href='/mods/the-doors-of-oblivion/'>The Doors of Oblivion</a></span>.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53242"),
        },
        {
            "name": "Daedric Shrine Overhaul Sheogorath",
            "author": nexus_user("70112108", "GlitterGear") + " and Ashstaar",
            "category": caves_and_dungeons,
            "date_added": "2023-10-29 17:34:00 -0500",
            "description": "Overhauls most of Sheogorath's shrine with assets from OAAB, TR, and The Doors of Oblivion. <span class='bold'>Requires <a href='/mods/the-doors-of-oblivion/'>The Doors of Oblivion</a></span>.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51890"),
        },
        {
            "name": "Daedric Shrine Overhaul Malacath",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-10-29 17:50:00 -0500",
            "description": "Overhauls most of Malacath's shrine with assets from OAAB, TR, and The Doors of Oblivion. <span class='bold'>Requires <a href='/mods/the-doors-of-oblivion/'>The Doors of Oblivion</a></span>.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51882"),
        },
        {
            "name": "Daedric Shrine Overhaul Molag Bal",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-10-29 17:55:00 -0500",
            "description": "Overhauls each of Molag Bal's shrine with assets from OAAB, TR, and The Doors of Oblivion. <span class='bold'>Requires <a href='/mods/the-doors-of-oblivion/'>The Doors of Oblivion</a></span>.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51632"),
        },
        {
            "name": "Daedric Shrine Overhaul Mehrunes Dagon",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": caves_and_dungeons,
            "date_added": "2023-10-29 18:00:00 -0500",
            "description": "Overhauls each of Mehrunes Dagon's shrine with assets from OAAB and The Doors of Oblivion. <span class='bold'>Requires <a href='/mods/the-doors-of-oblivion/'>The Doors of Oblivion</a></span>.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51607"),
        },
        {
            "name": "Face of the Hortator",
            "author": nexus_user("8347311", "Infragris"),
            "category": caves_and_dungeons,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Adds a large and varied dungeon for higher-level players in Molag Amur, close to Lake Nabia. Dungeon reward is the fabled Face of the Hortator, a lore friendly unenchanted Daedric artifact. ",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("42928"),
        },
    ]

    for m in mods:
        generate_mod(**m)
