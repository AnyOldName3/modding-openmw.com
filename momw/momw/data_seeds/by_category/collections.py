from momw.helpers import generate_mod, nexus_mod


collections = "Collections"


def init():
    # print("START: {} ... ".format(collections), end="")

    _mods = [
        {
            "author": "Trancemaster_1988",
            "category": collections,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """Explore the island of Vvardenfell as you never have before, with a world built anew to be both alien and familiar to the Morrowind you knew before. New adventures await, with new exciting areas to see and explore, and new artifacts just waiting to be discovered.""",
            "name": "Morrowind Rebirth",
            "status": "live",
            "url": nexus_mod("37795"),
            "usage_notes": """<p>Morrowind Rebirth is nice if you want a greatly enhanced game in a simple package.  'Requires Plugin' says 'no', but like other collections Rebirth includes many plugins.</p>""",
        },
        {
            "author": "Ornitocopter Team",
            "category": collections,
            "compat": "partially working",
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """'Morrowind Sounds and Graphics Overhaul' contains mods that will alter and enhance the visual graphics and the sounds of the game. The aim of this project is to make more people around the world appreciate this huge game by giving them a simple installation file which includes everything they will need to play Morrowind with gorgeous graphics and sounds. I’ve tried to keep Morrowind’s “feeling”; therefore, the mods/meshes/textures I chose are the ones that I chose are the ones that I think are most faithful to the original colours and environment. A benefit of this compilations is that players don’t need to go through the “download every single mod out there and try it” phase of customizing Morrowind. The pack is intended for people who would prefer not to hand-pick mods out of the thousands of mods available. Modding Morrowind can be overwhelming for some people, and they might want to start out with a compilation before they feel comfortable picking and choosing mods on their own.""",
            "is_active": False,
            "name": "Morrowind Sounds and Graphics Overhaul (MGSO)",
            "status": "live",
            "url": nexus_mod("36945"),
            "usage_notes": """As far as I know, this should work with OpenMW out of the box though there may be some tweaks that are needed.  'Requires Plugin' says 'no', but this is actually a collection of many mods including several plugins.  Given the nature of collections, you won't normally mess with what's inside and are on your own in terms of load order and tweaks.""",
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
