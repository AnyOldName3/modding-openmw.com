from momw.helpers import generate_mod, nexus_mod, nexus_user


animation = "Animation"


def init():
    # print("START: {} ... ".format(tools), end="")

    _mods = [
        {
            "name": "Almalexia's Cast for Beasts (OpenMW Exclusive)",
            "author": "Amenophis",
            "category": animation,
            "date_added": "2019-05-12 16:45:33 -0500",
            "description": "An OpenMW exclusive animation replacer that gives to the Beasts the Target and Self animations from Almalexia.",
            "extra_cfg": """<p>The following needs to be added to your <code>[Game]</code> section in the <code>settings.cfg</code> file:</p>

<pre><code>use additional anim sources = true</code></pre>""",
            "extra_cfg_raw": """#
# Almalexia's Cast for Beasts (OpenMW Exclusive):
# (This goes into settings.cfg under [Game])
#
use additional anim sources = true""",
            "status": "live",
            "url": nexus_mod("45853"),
        },
        {
            "author": nexus_user("29921295", "Artaios"),
            "category": animation,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This mod combines the Almalexia casting animations with Dirnae's running animations and fixes the notorious errors that were usually accompanied with them. It also enables the player to see the casting animations in first person.",
            "name": "Animation Compilation",
            "slug": "animation-compilation",
            "status": "live",
            "url": nexus_mod("43982"),
            "usage_notes": """Download version 0.40.0, the one from 2017.""",
        },
        {
            "name": "Simply Walking (Remastered)",
            "alt_to": ["Animation Compilation"],
            "author": "Dirane, PurplePrankster101",
            "category": animation,
            "date_added": "2022-11-07 10:18:00 -0500",
            "date_updated": "2023-07-02 11:33:00 -0500",
            "description": "Replaces only the walking animation.",
            "status": "live",
            "url": nexus_mod("49785"),
            "usage_notes": """Download the <code>Simply Walking Weapon Sheathing Edition</code> file, or any other as you prefer.""",
        },
        {
            "name": "ReAnimations - first-person animation pack",
            "author": nexus_user("11230608", "Max Yari"),
            "category": animation,
            "date_added": "2023-10-31 00:29:00 -0500",
            "description": "This mod attempts to fix all of the junkiness of first-person combat animations by either remaking some of the animations from scratch, altering vanilla ones, or using and adjusting MCAR animations.",
            "status": "live",
            "url": nexus_mod("52596"),
        },
        {
            "name": "MCAR",
            "author": nexus_user("88610433", "SVNR"),
            "category": animation,
            "date_added": "2024-01-24 18:39:13 +0100",
            "description": " Replaces 1st person animation. Inspired by BFG Division.",
            "status": "live",
            "url": nexus_mod("48628"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
