from momw.helpers import generate_mod, nexus_mod, nexus_user


player_homes = "Player Homes"

tag_game_expanding = "Game Expanding"
tag_normal_maps = "Normal Maps"
tag_gitlab_mods = "GitLab mods"
tag_oaab_data = "OAAB Data"
tag_tamriel_data = "Tamriel Data"
tag_dev_build_only = "Dev Build Only"
tag_openmw_lua = "OpenMW Lua"


def init():
    for m in [
        {
            "name": "Abandoned Flat V2",
            "author": '<a href="http://www.ghostwhowalks.net/">Ghost Who Walks</a>',
            "category": player_homes,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2023-06-17 12:19:00 -0500",
            "description": "The first player home mod I ever used, and my favorite to date.  There's more than meets the eye here!",
            "needs_cleaning": True,
            "status": "live",
            "tags": [tag_game_expanding, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/abandoned-flat/",
        },
        {
            "alt_to": ["Abandoned Flat V2"],
            "name": "Fargoth's Mountain Hut",
            "author": '<a href="https://baturin.org/">dmbaturin</a>',
            "category": player_homes,
            "date_added": "2019-05-14 18:39:31 -0500",
            "description": """<p>A deconstruction of the house mod genre. You know all those mods that provide you with an unusually nice house for free? This isn't one of them.</p>

<p>Your old friend Fargoth gives you a key to a house he bought from a shady real estate agent and never got to visit. The house is in the middle of nowhere on the slopes of the Red Mountain, between two leveled creature spawn points. It gets worse from there.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "https://baturin.org/misc/morrowind-mods/#fargoths-mountain-hut",
        },
        {
            "alt_to": ["Abandoned Flat V2", "Fargoth's Mountain Hut"],
            "author": "TheDrunkenMudcrab and MementoMoritius",
            "category": player_homes,
            "date_added": "2018-04-13 21:00:00 -0500",
            "description": "This mod adds a lore-friendly Velothi home to the Grazelands Region southwest of the Zainab Camp.  Inlcudes some goodies like a teleport ring and more.",
            "name": "Nil-Ruhn Velothi-Style house",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("44627"),
        },
        {
            "alt_to": [
                "Abandoned Flat V2",
                "Fargoth's Mountain Hut",
                "Nil-Ruhn Velothi-Style house",
            ],
            "author": nexus_user("60676311", "UnderSunAndSky"),
            "category": player_homes,
            "date_added": "2020-06-20 09:30:12 -0500",
            "date_updated": "2021-11-11 21:30:12 -0500",
            "description": """Adds a well furnished house to one of 12 locations, plus a single Deluxe house.""",
            "name": "Nerevars House Mod",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("46138"),
        },
        {
            "name": "Fabulous Hlaalo Manor",
            "author": nexus_user("55823457", "TheNocturus"),
            "category": player_homes,
            "date_added": "2022-11-12 16:32:00 -0500",
            "description": "The BEST early house just got better!",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51438"),
        },
        {
            "name": "Draggle-Tail Shack",
            "author": nexus_user("21574104", "Katya Karrel"),
            "category": player_homes,
            "date_added": "2023-10-19 15:01:00 -0500",
            "description": "Help a dunmer to get some money for his travels by buying his shack in the Bitter Coast. But why is he in such a hurry to get rid of it? Search for him on the road between Seyda Neen and Ebonheart.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("53305"),
        },
        {
            "name": "The Archmagister's Abode",
            "author": nexus_user("114986408", "Kalinter"),
            "category": player_homes,
            "date_added": "2023-11-08 11:56:00 -0500",
            "description": "Adds a new manor turret and overlook platform/skyport to the top of Tel Uvirith that grow automatically after becoming the Archmagister of Great House Telvanni.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53376"),
        },
        {
            "name": "Uvirith's Legacy",
            "author": '<a href="https://stuporstar.sarahdimento.com/">Stuporstar</a>',
            "category": player_homes,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2022-11-12 15:48:00 -0500",
            "description": """<p>This mod turns Tel Uvirith into a fitting home for a mage lord and a stunning symbol of the Archmagister's power. This third generation Tel Uvirith mod, based on Uvirith Inside, does what Uvirith Awakened was trying to do before the project fell into limbo.</p>

<!-- <p>For more information, along with FAQ, troubleshooting, and full walkthrough, visit the Uvirith's Legacy website: <a href="http://stuporstar.sarahdimento.com">http://stuporstar.sarahdimento.com/</a>.</p> -->""",
            "status": "live",
            "tags": [tag_game_expanding, tag_normal_maps],
            "url": "https://www.dropbox.com/s/sqxbej0nef7bg9e/Uviriths_Legacy_3.53.7z?dl=0",
        },
        {
            "author": nexus_user("40752190", "rotat"),
            "category": player_homes,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """<p>UL_3.5_TR Add-on pre-patched for TR 1807.  It's somewhat hidden on the files page, so I'm directly linking to that.</p>""",
            "name": "Uvirith's Legacy 3.5 TR add-on 1807",
            "status": "live",
            "url": nexus_mod("44656") + "?tab=files",
        },
        {
            "name": "Building Up Uvirith's Legacy",
            "author": "Acheron & Artimis Fowl",
            "category": player_homes,
            "date_added": "2018-04-13 21:00:00 -0500",
            "description": """<p>This mod is an attempt to patch together two of the most popular Tel Uvirth Stronghold mods, Uvirith's Legacy(UL) by StuporStar, and Building up Uvirith's grave(BuUG) by Mike "Acheron".</p>""",
            "dl_url": "/files/Building_Up_Uviriths_Legacy.7z",
            "is_active": False,
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "",
        },
        {
            "name": "Uvirith's Legacy 3.5 TR add-on 2101",
            "author": nexus_user("40752190", "rotat") + ", eddie5",
            "category": player_homes,
            "date_added": "2021-11-08 18:07:17 -0500",
            "description": """UL_3.5_TR Add-on patched for TR 2101.""",
            "dl_url": "/files/UL_3.5_TR_21.01_Add-on.zip",
            "status": "live",
            "url": "",
        },
        {
            "name": "Mages Guild Stronghold - Nchagalelft",
            "author": nexus_user("70112108", "Glittergear"),
            "category": player_homes,
            "date_added": "2023-11-10 19:40:00 -0500",
            "description": """A Mages Guild stronghold that is a customizable Dwemer ruin. Dozens of combinations are possible!""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53342"),
        },
        {
            "name": "Rather Nice Factor's Estate",
            "author": nexus_user("54268", "Atrayonis"),
            "category": player_homes,
            "date_added": "2023-11-10 19:49:00 -0500",
            "description": """This is an overhaul of the Factor's Estate, the player home which can be acquired by finishing the East Empire Company questline in the Solstheim expansion.""",
            "status": "live",
            "tags": [tag_tamriel_data],
            "url": nexus_mod("47933"),
        },
        {
            "name": "Abandoned Flat Containers",
            "author": "johnnyhostile",
            "category": player_homes,
            "date_added": "2023-11-29 11:47:00 -0500",
            "description": "An add-on for the Abandoned Flat player home mod that expandes a special storage cell to have containers with massive storage capacity. This interior cell has been totally expanded and revamped with assets from OAAB_Data and Tamriel_Data and a new boss fight. An additional feature adds Lua-powered inventory helper cubes that deposit items of certain types into designated containers.",
            "status": "live",
            "tags": [
                tag_gitlab_mods,
                tag_openmw_lua,
                tag_dev_build_only,
                tag_oaab_data,
                tag_tamriel_data,
            ],
            "url": "https://modding-openmw.gitlab.io/abandoned-flat-containers/",
        },
    ]:
        generate_mod(**m)
