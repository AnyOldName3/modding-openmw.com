from momw.helpers import (
    generate_mod,
    modhistory_archive_link,
    nexus_mod,
    nexus_user,
)

architecture = "Architecture"

# Names of tags that are used in this file.
tag_atlased_textures = "Atlased Textures"
tag_game_expanding = "Game Expanding"
tag_has_a_bsa = "Has a BSA"
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_normal_maps = "Normal Maps"
tag_specular_maps = "Specular Maps"
tag_tes3mp_graphics = "TES3MP Graphics"
tag_vanilla_friendly = "Vanilla Friendly"
tag_oaab_data = "OAAB Data"
tag_dev_build_only = "Dev Build Only"
tag_openmw_lua = "OpenMW Lua"
tag_gitlab_mods = "GitLab mods"
tag_tamriel_data = "Tamriel Data"

# Names of mods that are used as alts in this file.
dwemer_ruins_retexture = "Dwemer Ruins Retexture"
hlaalu_arkitektora_vol_2 = "Hlaalu - Arkitektora Vol.2"
imperial_forts_normal_mapped_for_openmw = "Imperial Forts Normal Mapped for OpenMW"
imperial_towns_normal_mapped_for_openmw_by_lysol = (
    "Imperial Towns Normal Mapped for OpenMW by Lysol"
)
redoran_arkitektora_vol_2 = "Redoran - Arkitektora Vol.2"
road_marker_retextured = "Road Marker Retextured"
telvanni_arkitektora_of_vvardenfell = "Telvanni - Arkitektora of Vvardenfell"
telvanni_retexture = "Telvanni Retexture"
vivec_and_velothi_arkitektora_vol_2 = "Vivec and Velothi - Arkitektora Vol.2"
windows_glow_v22 = "Windows Glow v. 2.2"
windows_glow_expansion = "Windows Glow Expansion"
windows_glow_tribunal = "Windows Glow - Tribunal"
windows_glow_bloodmoon = "Windows Glow - Bloodmoon (now with Raven Rock)"


def init():
    # # print("START: Architecture ... ", end="")

    _mods = [
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2019-04-06 16:19:10 -0500",
            "description": "Vanilla gamma and style HQ textures for Daedric Shrines.",
            "name": "Daedric Ruins - Arkitektora of Vvardenfell",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("43486"),
            "usage_notes": """Pick the HQ or MQ package, the bump map patch is not needed.""",
        },
        {
            "name": redoran_arkitektora_vol_2,
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2019-04-06 16:21:10 -0500",
            "description": "New. Better. Detailed. Richer. Closer to vanilla.  Textures of Great House Redoran and Ald Skar.",
            "status": "live",
            "tags": [
                tag_high_res,
                tag_atlased_textures,
                tag_vanilla_friendly,
                tag_tes3mp_graphics,
            ],
            "url": nexus_mod("46235"),
            "usage_notes": """<p>Download either the HQ or MQ (or UHQ if available) version of:</p>
<ul>
    <li>The main "Buildings" download</li>
    <li>The optional ATLAS version</li>
    <li>The optional Tamriel Rebuilt files</li>
</ul>""",
        },
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2019-04-07 18:59:15 -0500",
            "description": "New. Better. Detailed. Richer. Closer to vanilla.  Textures of Great House Hlaalu.",
            "name": hlaalu_arkitektora_vol_2,
            "status": "live",
            "tags": [
                tag_high_res,
                tag_atlased_textures,
                tag_vanilla_friendly,
                tag_tes3mp_graphics,
            ],
            "url": nexus_mod("46246"),
            "usage_notes": """Choose either the HQ or MQ version of the ATLAS version.""",
        },
        {
            "name": vivec_and_velothi_arkitektora_vol_2,
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2019-04-07 18:59:15 -0500",
            "description": "New. Better. Detailed. Richer. Closer to vanilla.  Textures of Vivec City and Velothi Architecture.",
            "status": "live",
            "tags": [
                tag_high_res,
                tag_vanilla_friendly,
                tag_tes3mp_graphics,
            ],
            "url": nexus_mod("46266"),
            "usage_notes": """Choose either the HQ or MQ version of the ATLAS version.""",
        },
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2019-04-06 16:21:10 -0500",
            "description": "Vanilla gamma and style HQ Telvanni and mushrooms retexture.",
            "name": telvanni_arkitektora_of_vvardenfell,
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("43530"),
            "usage_notes": """Choose either the HQ or MQ version.  The bump map files are not needed.""",
        },
        {
            "author": nexus_user("582122", "Apel"),
            "category": architecture,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Texture and mesh replacer for the exterior and interior of the Imperial lighthouse in Seyda Neen.",
            "name": "Apel's Lighthouse Retexture",
            "slug": "apels-lighthouse-retexture",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_manual_edit_needed],
            "url": nexus_mod("42532"),
            "usage_notes": 'OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.',
        },
        {
            "author": nexus_user("34241390", "Lysol"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-07-04 13:21:49 -0500",
            "description": "A high resolution retexture of the Imperial town architechture with normal maps and parallax maps. The texture pack aims to stay true to the original color palette. Works only with OpenMW 0.40 and newer.",
            "name": imperial_towns_normal_mapped_for_openmw_by_lysol,
            "slug": "imperial-towns-normal-mapped-for-openmw-by-lysol",
            "status": "live",
            "tags": [
                tag_high_res,
                tag_normal_maps,
                tag_specular_maps,
                tag_tes3mp_graphics,
            ],
            "url": nexus_mod("44536"),
        },
        {
            "author": nexus_user("34241390", "Lysol"),
            "category": architecture,
            "date_added": "2019-02-28 18:00:00 -0500",
            "date_updated": "2020-07-04 13:33:05 -0500",
            "description": """<p>A high resolution (2K) retexture of the Imperial forts with normal maps and parallax maps. The texture pack aims to stay true to the original color palette. Works only with OpenMW 0.40 and newer. Latest version is always recommended.</p>""",
            "name": imperial_forts_normal_mapped_for_openmw,
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_tes3mp_graphics],
            "url": nexus_mod("46413"),
        },
        {
            "alt_to": [imperial_forts_normal_mapped_for_openmw],
            "author": nexus_user("159722", "Mikeandike"),
            "category": architecture,
            "date_added": "2019-07-20 16:49:49 -0500",
            "description": """<p>A simple mod and efficient mod that retextures all Imperial Forts -- Interiors and Exteriors. Includes an extra .esp file which adds extra arrow slits to the smoother texture patches on fort structures.</p>""",
            "name": "Imperial Forts Retexture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43083"),
        },
        {
            "alt_to": [
                imperial_forts_normal_mapped_for_openmw,
                imperial_towns_normal_mapped_for_openmw_by_lysol,
            ],
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This mod contains full retexture for all imperial-style architecture on the island - Imperials' and Nords' Houses, Caldera Castle, Legions' Forts and Seyda Nin [sic] Lighthouse.",
            "name": "Imperial Houses and Forts Retexture - Ordo Arkitektora",
            "slug": "imperial-houses-and-forts-retexture-ordo-arkitekto",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43940"),
        },
        {
            "name": telvanni_retexture,
            "alt_to": [telvanni_arkitektora_of_vvardenfell],
            "author": nexus_user("1600249", "Lougian"),
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This mod retexture the Telvanni architecture with 2k & 1k textures. There is also an optional file that changes Sadrith Mora by adding windows, more roots, rocks, mushroom and some hanging vines (NOT recommended). This mod is purely about visual changes. So no new npcs, buildings or anything like that. An optionnal file for smoothed podplants with or without glowmap is available too.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("45516"),
        },
        {
            "alt_to": [telvanni_retexture, telvanni_arkitektora_of_vvardenfell],
            "author": "Biont ",
            "category": architecture,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This replaces the Telvanni architecture, roots and mushrooms with high-resolution meshes and textures with bump-mapping. Some textures also feature a subtle glow effect at night.",
            "name": "Telvanni Bump Maps",
            "slug": "telvanni-bump-maps",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("42431"),
            "usage_notes": "Grab the interior package as well, and do not use the included meshes or else you will end up with shiny textures.",
        },
        {
            "author": nexus_user("2447543", "Tommi2010"),
            "category": architecture,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Replaces Telvanni meshes with those from Morroblivion.  Replaces 32 Models. Very High Polys.",
            "name": "Improved Telvanni Architecture",
            "slug": "improved-telvanni-architecture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43241"),
            "usage_notes": """<p>After extensive playtesting with this one, I've decided that I can't recommend it for a few reasons:</p>
<ul>
    <li>Parts of Port Telvannis from Tamriel Rebuilt are unpassable, due to some of the root meshes being way bigger than vanilla.</li>
    <li>Performance in areas like Sadrith Mora, Port Telvannis, or any other decent-sized area with these meshes is utterly abysmal.</li>
</ul>
<p>All that aside, they do look really cool, but use it at your own risk!</p>""",
        },
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-06-27 15:43:07 -0500",
            "description": "This retexture contains new textures for wooden shacks, docks and ships (include rowboats and gondolas) and also some metal textures.",
            "name": "Shacks Docks and Ships - Arkitektora of Vvardenfell",
            "slug": "shacks-docks-and-ships-arkitektora-of-vvardenfell",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics, tag_manual_edit_needed],
            "url": nexus_mod("43520"),
            "usage_notes": """<p>Delete the following files to avoid conflicts with other mods that provide wood textures:</p>

<pre><code>Textures/Tx_wood_brown_shelf_corner.dds
Textures/Tx_wood_brown_shelf_corner_01.dds
Textures/Tx_wood_brown_shelf.dds</code></pre>""",
        },
        {
            "alt_to": [vivec_and_velothi_arkitektora_vol_2],
            "author": nexus_user("34241390", "Lysol"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-07-04 13:33:20 -0500",
            "description": "Replaces most textures in Vivec, Velothi and Molag Mar except for a few, by another community great -- Lysol!",
            "name": "Vivec Normal Mapped for OpenMW",
            "slug": "vivec-normal-mapped-for-openmw",
            "status": "live",
            "tags": [
                tag_high_res,
                tag_normal_maps,
                tag_specular_maps,
                tag_tes3mp_graphics,
            ],
            "url": nexus_mod("44259"),
            "usage_notes": """<p>If you're also using <a href="/mods/project-atlas/">Project Atlas</a> then these files need to be deleted from that mod to avoid a conflict (and broken textures in this mod):</p>

<pre><code>00 Core/meshes/x/ex_velothi_entrance_02.nif
00 Core/meshes/x/ex_velothi_temple_01.nif
00 Core/meshes/x/ex_velothi_temple_02.nif
00 Core/meshes/x/ex_velothi_tower_01.nif
00 Core/meshes/x/ex_velothi_window_01.nif
00 Core/meshes/x/ex_vivec_h_01.nif
00 Core/meshes/x/ex_vivec_h_02.nif
00 Core/meshes/x/ex_vivec_h_03.nif
00 Core/meshes/x/ex_vivec_h_04.nif
00 Core/meshes/x/ex_vivec_h_05.nif
00 Core/meshes/x/ex_vivec_h_06.nif
00 Core/meshes/x/ex_vivec_h_07.nif
00 Core/meshes/x/ex_vivec_h_08.nif
00 Core/meshes/x/ex_vivec_h_09.nif
00 Core/meshes/x/ex_vivec_h_10.nif
00 Core/meshes/x/ex_vivec_h_11.nif
00 Core/meshes/x/ex_vivec_h_12.nif
00 Core/meshes/x/ex_vivec_h_13.nif
00 Core/meshes/x/ex_vivec_h_14.nif
00 Core/meshes/x/ex_vivec_h_15.nif
00 Core/meshes/x/ex_vivec_h_16.nif
00 Core/meshes/x/ex_vivec_h_17.nif
00 Core/meshes/x/ex_vivec_w_01.nif
00 Core/meshes/x/ex_vivec_w_02.nif
00 Core/meshes/x/ex_vivec_w_03.nif
00 Core/meshes/x/ex_vivec_w_c_01.nif
00 Core/meshes/x/ex_vivec_w_e_01.nif
00 Core/meshes/x/ex_vivec_w_g_01.nif</code></pre>

<p>As well as these if you're using the Project Atlas patches for <a href="/mods/glow-in-the-dahrk/">Glow In The Dahrk</a>:</p>

<pre><code>10 Glow in the Dahrk Patch/meshes/x/ex_velothi_temple_01.nif
10 Glow in the Dahrk Patch/meshes/x/ex_velothi_tower_01.nif
10 Glow in the Dahrk Patch/meshes/x/ex_velothi_window_01.nif</code></pre>""",
        },
        {
            "alt_to": [
                vivec_and_velothi_arkitektora_vol_2,
                "Vivec Normal Mapped for OpenMW",
            ],
            "author": nexus_user("159722", "Mikeandike"),
            "category": architecture,
            "date_added": "2019-07-20 16:44:20 -0500",
            "description": "A simple and efficient mod that retextures all Velothi architecture. This covers everything from Vivec City to Velothi Domes and Ancestral tombs. Theres a lot of fine details to be seen like daedric engraving on railings and doors to custom Lithograph artwork drawn specifically for this mod by [Mikeandike's] brother, Josh.",
            "name": "Vivec and Velothi Retexture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43091"),
        },
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Vanilla-friendly but with a much higher quality look.",
            "name": "Sewers - Arkitektora of Vvardenfell",
            "slug": "sewers-Arkitektora-of-vvardenfell",
            "status": "live",
            "tags": [tag_high_res, tag_vanilla_friendly, tag_tes3mp_graphics],
            "url": nexus_mod("43144"),
        },
        {
            "alt_to": [hlaalu_arkitektora_vol_2],
            "author": nexus_user("34241390", "Lysol"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-07-04 13:33:30 -0500",
            "description": "This is a vanilla friendly high resolution texture pack of Hlaalu architechture, featuring normal maps and parallax maps.",
            "name": "Hlaalu Normal Mapped for OpenMW",
            "slug": "hlaalu-normal-mapped-for-openmw",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_tes3mp_graphics],
            "url": nexus_mod("44297"),
            "usage_notes": """<p>If you're also using <a href="/mods/project-atlas/">Project Atlas</a> then these files need to be deleted from that mod to avoid a conflict (and broken textures in this mod):</p>

<pre><code>00 Core/meshes/x/Ex_hlaalu_wall_curve_01.nif
00 Core/meshes/x/Ex_hlaalu_wall_end_01.nif
00 Core/meshes/x/Ex_hlaalu_wall_gate_01.nif
00 Core/meshes/x/Ex_hlaalu_wall_up_01.nif
00 Core/meshes/x/Ex_hlaalu_wall_up_02.nif
00 Core/meshes/x/ex_hlaalu_b_01.nif
00 Core/meshes/x/ex_hlaalu_b_02.nif
00 Core/meshes/x/ex_hlaalu_b_03.nif
00 Core/meshes/x/ex_hlaalu_b_04.nif
00 Core/meshes/x/ex_hlaalu_b_05.nif
00 Core/meshes/x/ex_hlaalu_b_06.nif
00 Core/meshes/x/ex_hlaalu_b_07.nif
00 Core/meshes/x/ex_hlaalu_b_08.nif
00 Core/meshes/x/ex_hlaalu_b_09.nif
00 Core/meshes/x/ex_hlaalu_b_10.nif
00 Core/meshes/x/ex_hlaalu_b_11.nif
00 Core/meshes/x/ex_hlaalu_b_12.nif
00 Core/meshes/x/ex_hlaalu_b_13.nif
00 Core/meshes/x/ex_hlaalu_b_14.nif
00 Core/meshes/x/ex_hlaalu_b_15.nif
00 Core/meshes/x/ex_hlaalu_b_16.nif
00 Core/meshes/x/ex_hlaalu_b_17.nif
00 Core/meshes/x/ex_hlaalu_b_18.nif
00 Core/meshes/x/ex_hlaalu_b_20.nif
00 Core/meshes/x/ex_hlaalu_b_21.nif
00 Core/meshes/x/ex_hlaalu_b_22.nif
00 Core/meshes/x/ex_hlaalu_b_23.nif
00 Core/meshes/x/ex_hlaalu_b_24.nif
00 Core/meshes/x/ex_hlaalu_b_25.nif
00 Core/meshes/x/ex_hlaalu_b_26.nif
00 Core/meshes/x/ex_hlaalu_b_27.nif
00 Core/meshes/x/ex_hlaalu_b_28.nif
00 Core/meshes/x/ex_hlaalu_bal_01.nif
00 Core/meshes/x/ex_hlaalu_bal_02.nif
00 Core/meshes/x/ex_hlaalu_bridge_01.nif
00 Core/meshes/x/ex_hlaalu_bridge_04.nif
00 Core/meshes/x/ex_hlaalu_bridge_05.nif
00 Core/meshes/x/ex_hlaalu_bridge_06.nif
00 Core/meshes/x/ex_hlaalu_bridge_07.nif
00 Core/meshes/x/ex_hlaalu_buttress_03.nif
00 Core/meshes/x/ex_hlaalu_buttress_04.nif
00 Core/meshes/x/ex_hlaalu_buttress_05.nif
00 Core/meshes/x/ex_hlaalu_dsteps_01.nif
00 Core/meshes/x/ex_hlaalu_dsteps_03.nif
00 Core/meshes/x/ex_hlaalu_fb_01.nif
00 Core/meshes/x/ex_hlaalu_pole_01.nif
00 Core/meshes/x/ex_hlaalu_steps_03.nif
00 Core/meshes/x/ex_hlaalu_steps_04.nif
00 Core/meshes/x/ex_hlaalu_steps_05.nif
00 Core/meshes/x/ex_hlaalu_steps_06.nif
00 Core/meshes/x/ex_hlaalu_steps_07.nif
00 Core/meshes/x/ex_hlaalu_striderport_01.nif
00 Core/meshes/x/ex_hlaalu_wall_01.nif
00 Core/meshes/x/ex_hlaalu_wall_02.nif</code></pre>

<p>As well as this one if you're using the Project Atlas patches for <a href="/mods/glow-in-the-dahrk/">Glow In The Dahrk</a>:</p>

<pre><code>10 Glow in the Dahrk Patch/meshes/x/Ex_hlaalu_win_01.nif</code></pre>""",
        },
        {
            "alt_to": [redoran_arkitektora_vol_2],
            "author": "Colt17",
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "High definition architecture meshes - only download HAM Skar (2048/4096).",
            "is_active": False,
            "name": "HD Architecture Meshes",
            "slug": "hd-architecture-meshes",
            "status": "live",
            "tags": [tag_high_res],
            "url": modhistory_archive_link("55-6854"),
        },
        {
            "author": nexus_user("899234", "Remiros"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-09-24 17:17:15 -0500",
            "description": "This mod completely replaces the textures for the Dunmer Strongholds.",
            "is_active": False,
            "name": "Stronghold Retexture",
            "slug": "stronghold-retexture",
            "status": "live",
            # "tags": [tag_high_res, tag_tes3mp_graphics],
            # "url": nexus_mod("43948"),
            "usage_notes": """<p><span class="bold">This mod has been removed from Nexus Mods and can no longer be downloaded!</span> This page is kept in place for historical reasons.</p>

<p>Please see <a href="/mods/aesthesia-stronghold-textures/">Aesthesia - Stronghold textures</a> for a modern alternative.</p>""",
        },
        {
            "author": "Max a.k.a. ~NOOBDY~",
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Makes windows glow at night time and other immersive effects.",
            "is_active": False,
            "name": windows_glow_v22,
            "needs_cleaning": True,
            "slug": "windows-glow-v-22",
            "status": "live",
            "tags": [tag_vanilla_friendly],
            "url": modhistory_archive_link("-442"),
        },
        {
            "author": nexus_user("841275", "Colt17"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Adds glowing windoes to Tribunal expansion areas.",
            "name": windows_glow_tribunal,
            "needs_cleaning": True,
            "slug": "windows-glow-tribunal",
            "status": "live",
            "tags": [tag_vanilla_friendly, tag_has_a_bsa],
            "url": nexus_mod("24561"),
            "usage_notes": 'This mod includes a <code>.bsa</code> file which needs to be registered.  Please see <a href="/tips/register-bsas/">the register BSAs tips page</a> for more information.',
        },
        {
            "author": "Colt17",
            "category": architecture,
            "date_added": "2019-02-25 21:00:00 -0500",
            "description": """<p>This mod makes every window on Solstheim glow. Exterior ones glow at night, and interior ones glow at day.</p>""",
            "name": windows_glow_bloodmoon,
            "needs_cleaning": True,
            "status": "live",
            "tags": [tag_vanilla_friendly, tag_has_a_bsa],
            "url": modhistory_archive_link("56-6231"),
        },
        {
            "author": nexus_user("38047", "abot"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Replaces the windows glow with a scripted on, also adds them to many Tamriel Rebuilt areas.",
            "name": windows_glow_expansion,
            "slug": "windows-glow-expansion",
            "status": "live",
            "tags": [tag_vanilla_friendly],
            "url": nexus_mod("42271"),
        },
        {
            "author": nexus_user("31611", "AnOldFriend"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": 'Replaces the textures for the "welcome" obelisk.',
            "name": road_marker_retextured,
            "slug": "road-marker-retextured",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("28311"),
            "usage_notes": """The provided loose files should go into a folder called <code>textures</code>.""",
        },
        {
            "name": "Signposts Retextured",
            "author": nexus_user("4381248", "PeterBitt"),
            "category": architecture,
            "date_added": "2019-03-01 18:00:00 -0500",
            "date_updated": "2023-06-24 12:50:00 -0500",
            "description": """<p>This makes the signs in Vvardenfell readable with vanilla friendly textures. No sign looks the same. Colors, font and resolution blend in with original MW graphics.</p>""",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("42126"),
            "usage_notes": """<p>Choose either the <code>1.2</code> or the <code>2.0</code> version.  Or try them both (one at a time, of course).</p>

<p>Use <a href="https://modding-openmw.com/files/PB_SignpostsRetexturedTR.zip">this version of the TR plugin</a> instead of the one from Nexus mods, but you still need to download the approptiate optional file for the TR meshes and textures.</p>""",
        },
        {
            "author": nexus_user("44102", "Vrakyas"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This is a textures replacer for the road markers that mark the entrance to a town - normally that brownish obelisk.",
            "name": "Hires Town Road Markers",
            "slug": "hires-town-road-markers",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("44102"),
        },
        {
            "name": "High-res Skaal Retex",
            "slug": "high-res-skaal-retex",
            "author": nexus_user("1067783", "LazyGhost"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-08-21 21:00:00 -0500",
            "description": "Retextures all bloodmoon buildings with high-res 2048x2048 textures, with an optional package for Raven Rock textures.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("25650"),
        },
        {
            "name": "RR Mod Series - Holamayan Monastery Replacer",
            "author": "Morrowind Community and Resdayn Revival Team",
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2020-01-18 13:54:15 -0500",
            "description": "Holamayan Monastery replacer, based on Bethesda Softworks original concept-art.",
            "status": "live",
            "tags": [tag_high_res, tag_vanilla_friendly],
            "url": nexus_mod("43524"),
            "usage_notes": """Select either the English of the Russian language plugin.  Also use the <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a> patch, unless for some reason you aren't using that.""",
        },
        {
            "name": "RR Mod Series - Morrowind Statues Replacer",
            "author": "Morrowind Community and Resdayn Revival Team",
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2021-09-16 19:16:25 -0500",
            "description": "Pluginless replacer for different Morrowind statues",
            "status": "live",
            "tags": [tag_high_res, tag_vanilla_friendly],
            "url": nexus_mod("43348"),
            "usage_notes": """The Marble option for Azura conflicts with <a href="/mods/rr-mod-series-holamayan-monastery-replacer/">RR Mod Series - Holamayan Monastery Replacer</a> (whichever one loads first has messed up textures) so use the Stone version. Choose the English or Russian option for the coda fix (<code>03</code> or <code>04</code>) as desired.""",
        },
        {
            "name": "RR Mod Series - Telvanni Lighthouse Tel Branora",
            "author": "Morrowind Community and Resdayn Revival Team",
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This mod adds a Telvanni style Lighthouse to Tel Branora.  Gives a really great view of TR lands (if you use that mod).",
            "status": "live",
            "url": nexus_mod("42664"),
        },
        {
            "name": "RR Mod Series - Telvanni Lighthouse Tel Vos",
            "author": "Resdayn Revival Team",
            "category": architecture,
            "compat": "fully working",
            "date_added": "2019-02-25 21:20:00 -0500",
            "date_updated": "2020-01-18 14:29:20 -0500",
            "description": """<p>This mod adds a Telvanni style Lighthouse not far from Tel Vos and Tel Mora.</p>""",
            "status": "live",
            "url": nexus_mod("42744"),
            "usage_notes": """Choose either the English or the Russian plugin, as well as the main patch files and the GITD patch.""",
        },
        {
            "name": "Redoran Bump mapped",
            "alt_to": [redoran_arkitektora_vol_2],
            "author": nexus_user("1600249", "Lougian"),
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2020-07-04 21:23:39 -0500",
            "description": "Bump mapped retexture of Redoran architecture.",
            "status": "live",
            "tags": [tag_high_res, tag_manual_edit_needed],
            "url": nexus_mod("42406"),
            "usage_notes": """OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.""",
        },
        {
            "name": "Skar Mega Textures",
            "author": "Bardon",
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Emperor crab model by Colt17 in 16K resolution",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("45310"),
        },
        {
            "name": "Tower of Vos",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": architecture,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This mod makes the Tower of Vos (Tel Vos) much, much taller than before. Now it dominates the Grazelands, overlooking the region.",
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly],
            "url": nexus_mod("43527"),
        },
        {
            "name": "Stav's Boxes - Gnisis Minaret",
            "author": nexus_user("7865047", "Stavroguin"),
            "category": architecture,
            "compat": "partially working",
            "date_added": "2019-07-20 11:15:18 -0500",
            "date_updated": "2020-01-18 14:44:52 -0500",
            "description": "This small mod add tower to the Gnisis temple that we can see on artworks, as well as priest that will speak of Vivec from 6am to 8am and then return to his room.",
            "needs_cleaning": True,
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly, tag_manual_edit_needed],
            "url": nexus_mod("43237"),
            "usage_notes": """<p>Manual steps are needed to avoid a missing trapdoor mesh in the tower:</p>

<ol>
  <li>Extract the zip as you normally would</li>
  <li>In the <code>Meshes</code> folder, create another new folder called <code>ghns</code></li>
  <li>In the <code>ghns</code> folder, create another new folder called <code>d</code>, open that in a new window</li>
  <li>Back in the <code>Meshes</code> folder, navigate to <code>Stav/gnss/d</code> and copy the <code>trapdoor.nif</code> file into the other window you opened for the <code>d</code> folder (from <code>Meshes/Stav/gnss/d</code> into <code>Meshes/ghns/d</code>)</li>
  <li>Rename this copied <code>trapdoor.nif</code> to <code>in_v_s_trapdoor_03.nif</code></li>
</ol>""",
        },
        {
            "author": nexus_user("159722", "Mikeandike"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This is a small texture pack that modifies the look and feeling of all Dwemer ruins. It draws inspiration to how Dwemer Ruins are approached in ESO and Skyrim without feeling all generic fantasy Dwarf (The excessive use of stone). It should be compatible with most Dwemer ruin mods unless they add their own unique meshes and textures.",
            "name": dwemer_ruins_retexture,
            "slug": "dwemer-ruins-retexture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43915"),
        },
        {
            "author": nexus_user("4272713", "Champion of Hircine"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Replaces models in the Dwemer set. All meshes are made from scratch. Most of the models were not replaced by MGSO. The ones that were had ridiculously high poly counts. For these models I reduced the poly count by about 80% and they look essentially the same.",
            "name": "Dwemer Mesh Improvement",
            "slug": "dwemer-mesh-improvement",
            "status": "live",
            "url": nexus_mod("43101"),
        },
        {
            "author": nexus_user("4572722", "darkfri"),
            "category": architecture,
            "date_added": "2022-04-22 21:00:00 -0500",
            "description": "Revamping of some meshes of Dwemer Mesh Improvement to look closer to vanilla.",
            "name": "Dwemer Mesh Improvements Revamped",
            "url": nexus_mod("51525"),
        },
        {
            "alt_to": [dwemer_ruins_retexture],
            "author": nexus_user("3281858", "Tyddy"),
            "category": architecture,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Full retexture of dwemeri set - architecture, armor and weapon, robots, including content from Tribunal Expansion, in lore-friendly vanilla-like style.",
            "name": "Full Dwemer Retexture",
            "slug": "full-dwemer-retexture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("44264"),
            "usage_notes": """Choose between the Full MQ or HQ versions.""",
        },
        {
            "name": "Red Vos",
            "author": nexus_user("3281858", "Tyddy")
            + " and "
            + nexus_user("962116", "Melchior Dahrk"),
            "category": architecture,
            "date_added": "2020-05-02 16:46:37 -0500",
            "description": "Gives the farming village of Vos a unique, reddish texture to complement the region. This changes both the interior and exterior areas of the village.",
            "status": "live",
            "url": nexus_mod("44729"),
            "usage_notes": """Be sure to use the patch for <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>.""",
        },
        {
            "author": nexus_user("999194", "Rattfink333"),
            "category": architecture,
            "date_added": "2020-05-02 17:40:07 -0500",
            "description": "Redone Molag Mar with textures from anoldfriend to make it unique from Vivec.",
            "name": "Dark Molag Mar",
            "status": "live",
            "url": nexus_mod("32101"),
        },
        {
            "name": "White Suran 2 - MD Edition",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": architecture,
            "date_added": "2020-05-02 22:16:53 -0500",
            "description": "Gives Suran white textures to make it stand out among Hlaalu architechture.",
            "status": "live",
            "url": nexus_mod("44153"),
            "usage_notes": """Followers of the <span class="bold">Total Overhaul</span>, <span class="bold">Graphics Overhaul</span>, and <span class="bold">Expanded Vanilla</span> lists only need to use the folders that are shown in the data paths below. Other setups may require other paths, consult the mod's documentation for more information.""",
        },
        {
            "author": nexus_user("4272713", "Champion of Hircine"),
            "category": architecture,
            "date_added": "2020-05-09 07:16:06 -0500",
            "description": "Replaces 43 models in total. Models are mostly 1k - 2.5k polys, a few are about 5k but it doesn't seem to be enough to effect framerate. It may depend on your computer though.",
            "name": "Telvanni Mesh Improvement",
            "status": "live",
            "url": nexus_mod("42343"),
        },
        {
            "author": nexus_user("6304834", "Rytelier"),
            "category": architecture,
            "date_added": "2020-06-23 20:42:13 -0500",
            "description": "Vanilla friendly textures for strongholds. 2k resolution, handmade, friendly to game aesthetics.",
            "name": "Aesthesia - Stronghold textures",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("46445"),
        },
        {
            "name": "RR Mod Series - Ghostgate Fortress",
            "author": "Resdayn Revival Team",
            "category": architecture,
            "compat": "fully working",
            "date_added": "2020-07-09 15:45:45 -0500",
            "date_updated": "2021-01-28 13:13:21 -0500",
            "description": """New models and textures for Ghostgate and Ghostfence pylons based on Morrowind concept art.""",
            "status": "live",
            "url": nexus_mod("45822"),
            "usage_notes": """<p>Choose either the English or the Russian plugin, as well as the main patch files and the GITD patch.</p>

<p>Note that this mod will cause floating grass if you use groundcover mods. I've created custom plugins for mod list users, you can find that later on in the list.</p>""",
        },
        {
            "author": nexus_user("307155", "Zobator"),
            "category": architecture,
            "date_added": "2021-02-14 08:47:33 -0500",
            "description": "Replaces the textures from White Suran 2 - MD edition with high quality textures.",
            "name": "Arkitektora White Suran",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": nexus_mod("45101"),
        },
        {
            "name": "Concept Art Palace (Vivec City)",
            "author": "NobuRed and NMTeam",
            "category": architecture,
            "date_added": "2021-04-18 10:21:21 -0500",
            "date_updated": "2022-01-25 10:39:28 -0500",
            "description": "The Concept Art Palace mod is an attempt to bring the Vivec palace closer to the original Kirkbride's concept art.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("48136"),
            "usage_notes": """<p class="bold">This requires <a href="/mods/oaab_data/">OAAB_Data</a> and <a href="/mods/velothi-wall-art/">Velothi Wall Art</a></p>

<p>Download the main file as well as the optional "Dome with huge fire OpenMW version" file. Use the <code>00 - Core Files</code> and <code>05 - GitD - patch</code> directories from the main file, and <code>00 Core</code> from the optional file. You'll need to enable the plugin from the optional file, as well.</p>""",
        },
        {
            "name": "Nords shut your windows",
            "author": nexus_user("4655319", "Vegetto"),
            "category": architecture,
            "date_added": "2021-08-29 12:01:50 -0500",
            "description": "Nord's windows have a wooden shutter, open in the day and closed at night.",
            "status": "live",
            "url": nexus_mod("50087"),
            "usage_notes": """<p>Mod list instructions:</p>

<ul>
  <li><span class="bold">Expanded Vanilla</span>, <span class="bold">I Heart Vanilla</span>, and <span class="bold">I Heart Vanilla: Director's Cut</span>: Use the <code>00 Core</code> and <code>03 Vanilla style sunrays</code> directories</li>
  <li><span class="bold">Graphics Overhaul</span> and <span class="bold">Total Overhaul</span>: Use the <code>00 Core</code> and <code>02 Interior sunrays</code> directories</li>
</ul>""",
        },
        {
            "name": "Magical lights for Telvanni",
            "author": nexus_user("59284071", "RandomPal"),
            "category": architecture,
            "date_added": "2021-08-29 12:12:24 -0500",
            "date_updated": "2023-01-16 12:18:24 -0500",
            "description": "Adds some unique, Telvanni style light sources to various Telvanni settlements",
            "status": "live",
            "url": nexus_mod("49453"),
            "tags": [tag_oaab_data],
            "usage_notes": """Go with the OAAB version (unless you know you don't want that one).""",
        },
        {
            "name": "OAAB Dwemer Pavements",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": architecture,
            "date_added": "2021-09-12 12:16:10 -0500",
            "description": """Many of the dwemer ruins use unfitting cobblestone textures in their exteriors. This mod replaces the cobblestone textures outside of all Vvardenfell-based dwemer ruins with a new texture. In addition to the texture swap, it also uses a "road edge" mesh which helps blend this new pavement into the ruins and the surrounding landscape.""",
            "status": "live",
            "url": nexus_mod("50237"),
            "tags": [tag_oaab_data],
            "usage_notes": """<span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span>. Note that the <a href="/mods/trackless-grazeland/">Trackless Grazeland</a> compatibility plugin has special load order requirements. See the mod's included readme or this website's <a href="/cfg-generator/">CFG Generator</a> for specific instructions.""",
        },
        {
            "name": "Daedric Ruins (Half-Revamped)",
            "author": nexus_user("88610433", "SVNR"),
            "category": architecture,
            "date_added": "2022-11-06 10:11:09 -0500",
            "description": "Very high quality meshes and normal maps for Daedric architecture. Incomplete, but beautiful.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("51674"),
            "usage_notes": """<p>Download the <code>DRHR</code> and <code>A handful of exterior meshes</code>. Create a folder called <code>exterior</code> in the base mod data path and put the contents of <code>A handful of exterior meshes</code> inside it.</p>

<p>If you're following a mod list, you'll use the <code>01 - Caverns Revamp patch</code> folder a bit later on so feel free to extract it now as well.</p>""",
        },
        {
            "author": nexus_user("21646464", "DaggerfallTeam"),
            "category": architecture,
            "date_added": "2022-11-06 10:45:17 -0500",
            "description": "Normal Maps for Rytelier's - Aesthesia - Stronghold textures",
            "name": "Aesthesia - Stronghold Normal Maps",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("51012"),
        },
        {
            "name": "Morrowind Interiors Project",
            "author": nexus_user("54525107", "GrumblingVomit"),
            "category": architecture,
            "date_added": "2023-09-17 10:32:17 -0500",
            "description": "Adds exteriors to all interior cells with windows. So far, Molag Mar, Caldera, Pelagiad, and Gnisis.",
            "status": "live",
            "url": nexus_mod("52237"),
        },
        {
            "name": "Dwemer Ruins Normal Mapped",
            "author": nexus_user("2314089", "FloorBelow"),
            "category": architecture,
            "date_added": "2023-11-07 23:39:00 -0500",
            "description": "A set of custom built normal maps for the Dwemer Ruins of Morrowind. Enhances lighting and depth of textures.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("43290"),
        },
        {
            "name": "Dwemer Lightning Rods",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": architecture,
            "date_added": "2023-11-08 00:18:00 -0500",
            "description": "With this mod, if you visit a dwemer ruin during a thunderstorm, you'll be greeted by blinding flashes as lightning strikes the steamstack lightning rods from the vanilla game. But be careful not to get too close lest you be shocked!",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50236"),
        },
        {
            "name": "Dwemer Lightning Rods for OpenMW-Lua",
            "author": "johnnyhostile, Melchior Dahrk, ZackHasACat, and ezze",
            "category": architecture,
            "date_added": "2023-11-08 00:18:00 -0500",
            "description": "Automatically replace vanilla Dwemer steam stacks with OAAB ones that use the lightning effect. Compatible with everything.",
            "status": "live",
            "tags": [
                tag_oaab_data,
                tag_dev_build_only,
                tag_openmw_lua,
                tag_gitlab_mods,
            ],
            "url": "https://modding-openmw.gitlab.io/dwemer-lightning-rods-for-openmw-lua/",
        },
        {
            "name": "Hlaalu - Arkitektora Vol.2 Normal Maps",
            "author": nexus_user("21646464", "DaggerfallTeam"),
            "category": architecture,
            "date_added": "2023-12-12 14:40:51 +0100",
            "description": "Provides Normal Maps for Hlaalu - Arkitektora Vol.2",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("46400"),
        },
        {
            "name": "Vivec and Velothi - Arkitektora Vol.2 Normal Maps",
            "author": nexus_user("21646464", "DaggerfallTeam"),
            "category": architecture,
            "date_added": "2023-12-12 14:50:51 +0100",
            "description": "Provides Normal Maps for Vivec and Velothi - Arkitektora Vol.2",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("46398"),
        },
        {
            "name": "Redoran - Arkitektora Vol.2 Normal Maps",
            "author": nexus_user("21646464", "DaggerfallTeam"),
            "category": architecture,
            "date_added": "2021-01-31 08:48:34 -0500",
            "description": " Adds Normal Maps to Redoran - Arkitektora Vol.2",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_tes3mp_graphics],
            "url": nexus_mod("47523"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # # print("Done!")
