from momw.helpers import generate_mod, nexus_mod, nexus_user


mod_res = "Modding Resources"
tag_oaab_data = "OAAB Data"
tag_has_a_bsa = "Has a BSA"
tag_high_res = "High Res"
tag_non_hd = "Non-HD"
tag_tamriel_data = "Tamriel Data"


def init():
    _mods = [
        {
            "name": "Tamriel Data",
            "author": '<a href="http://www.tamriel-rebuilt.org/">The Tamriel Rebuilt Team</a>',
            "category": mod_res,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2023-12-24 15:26:47 +0100",
            "description": "Adds game data files required by mods such as Tamriel Rebuilt, Province Cyrodiil, and Skyrim Home of the Nords.",
            "status": "live",
            "tags": [tag_high_res, tag_non_hd, tag_tamriel_data],
            "url": "http://www.tamriel-rebuilt.org/downloads/resources",
        },
        {
            "name": "Tamriel_Data Graveyard - Deprecations un-deprecated",
            "author": nexus_user("37737490", "Cross-Project Coordination"),
            "category": mod_res,
            "tags": [tag_high_res, tag_non_hd, tag_tamriel_data],
            "date_added": "2022-11-30 17:12:00 -0500",
            "description": "Reverts Tamriel Data deprecations to help maintain compatibility.",
            "status": "live",
            "url": nexus_mod("52000"),
        },
        {
            "name": "OAAB_Data",
            "author": "OAAB_Data Team",
            "category": mod_res,
            "compat": "partially working",
            "date_added": "2021-04-16 23:08:36 -0500",
            "date_updated": "2022-11-12 09:54:00 -0500",
            "description": "OAAB_Data is free-to-use asset repository for the Morrowind Community.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("49042"),
            "usage_notes": """<p>The mod description on Nexus Mods explains some known issues with this and OpenMW, but the majority of what OAAB_Data provides works fine.</p>

<p>Download the <code>OAAB_Data</code> file and the <code>OAAB_Data (HD)</code> file. Extract the <code>00 Core</code> and <code>01 Epic Plants Patch</code> folders from the main file. Create a folder called <code>HD</code> in the base mod data path and put the contents of the HD file into it.</p>

<p>If you're following a mod list, <code>02 SM_Bitter Coast Trees Patch</code> and <code>05 Glass Glowset Patch</code> will be used later on but feel free to extract them now.</p>

<p>Check out the OAAB_Data dev blog <a href="https://github.com/Of-Ash-And-Blight/OAAB-Data/discussions">here</a>!</p>""",
        },
        {
            "name": "Merch's Minor Mods-HD OAAB_Data",
            "author": nexus_user("12988010", "MerchLis"),
            "category": mod_res,
            "date_added": "2022-07-22 21:00:00 -0500",
            "description": "Upscaled OAAB_Data textures, except those which were already HD, and particles.",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51495"),
        },
    ]

    for m in _mods:
        generate_mod(**m)
