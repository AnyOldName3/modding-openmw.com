from momw.helpers import generate_mod, nexus_mod, nexus_user


landmasses = "Landmasses"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_grass = "Grass"
tag_tamriel_data = "Tamriel Data"


def init():
    for m in [
        {
            "name": "Tamriel Rebuilt",
            "slug": "tamriel-rebuilt",
            "author": '<a href="http://www.tamriel-rebuilt.org/">The Tamriel Rebuilt Team</a>',
            "category": landmasses,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2023-11-01 16:40:00 -0500",
            "description": "This is the product of a project that aims to fill in the mainland of Morrowind. At this time much of the additional lands feature NPCs, quests, guilds, and factions.",
            "status": "live",
            "tags": [tag_game_expanding, tag_tamriel_data],
            "url": "http://www.tamriel-rebuilt.org/downloads/main-release",
        },
        {
            "name": "Province Cyrodiil",
            "slug": "province-cyrodiil",
            "author": nexus_user("44324482", "Project Tamriel Team"),
            "category": landmasses,
            "date_added": "2021-02-12 21:28:33 -0500",
            "description": "Adds the island of Stirk, filled with style, items, and architecture that's reminiscent of that seen in Oblivion.",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_tamriel_data],
            "url": nexus_mod("44922"),
        },
        {
            "name": "Skyrim: Home Of The Nords",
            "author": nexus_user("44324482", "Project Tamriel Team"),
            "category": landmasses,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2022-11-25 22:05:00 -0500",
            "description": "A massive project that aims to bring the province of Skyrim to Morrowind.",
            "slug": "skyrim-home-of-the-nords",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_tamriel_data],
            "url": nexus_mod("44921"),
        },
        {
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": landmasses,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "A huge series of islands off the Eastern coast of the lands added by Tamriel Rebuilt, it's still in alpha state but there's still a lot of things to see.",
            "name": "Lyithdonea - The Azurian Isles",
            "needs_cleaning": True,
            "slug": "lyithdonea-the-azurian-isles",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "https://github.com/MelchiorDahrk/Lyithdonea",
        },
        {
            "name": "Wyrmhaven",
            "author": nexus_user("20590", "Neoptolemus"),
            "category": landmasses,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2023-10-29 15:06:00 -0500",
            "description": "Wyrmhaven is a tiny island far to the west of Solstheim, claimed at various times by a clan of Chimer fleeing the wars with the Dwarves, a fire-breathing dragon, a band of Nordic exiles, the Breton Kingdom of Farrun, and the Septim Empire.",
            "status": "live",
            "tags": [tag_game_expanding, tag_tamriel_data],
            "url": nexus_mod("42933"),
        },
        {
            "name": "Solstheim Tomb of the Snow Prince",
            "author": "TOTSP Team",
            "category": landmasses,
            "date_added": "2020-06-22 21:06:24 -0500",
            "date_updated": "2023-07-15 11:41:00 -0500",
            "description": """TOTSP goal is to improve the graphical fidelity, environmental design, and gameplay of the Bloodmoon DLC's to a level that is consistent with modern Tes3 mod projects, such as Tamriel Rebuilt, Project Tamriel, and Heart of the Velothi.""",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_tamriel_data],
            "url": nexus_mod("46810"),
            "usage_notes": """<p>If following the <a href="/lists/total-overhaul/">Total Overhaul</a> list, then you want to use the contents of the <code>Solstheim Tomb of the Snow Prince</code> file.</p>

<p>If following the <a href="/lists/graphics-overhaul/">Graphics Overhaul</a> list, then you want to use the contents of the <code>Solstheim Graphical Replacer</code> file.</p>

<p>If following the <a href="/lists/expanded-vanilla/">Expanded Vanilla</a>, <a href="/lists/graphics-overhaul/">Graphics Overhaul</a>, or <a href="/lists/total-overhaul/">Total Overhaul</a> lists, then you can skip the <code>014 Hide-Like Animal Pelts</code> folder path.</p>

<p>The groundcover plugin can be found <a class="bold" href="/files/RemirosGroundcoverTOTSPPluginFixed.zip">here</a>. Extract this plugin into the <code>000 Core</code> folder.</p>

<p>Note that if you're following any mod list: the groundcover textures aren't added until much later, so keep that in mind if you test and see the yellow exclamation marks or similar.</p>""",
        },
    ]:
        generate_mod(**m)
