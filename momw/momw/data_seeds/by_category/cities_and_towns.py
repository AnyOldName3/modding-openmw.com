from momw.helpers import generate_mod, nexus_mod, nexus_user


cities_and_towns = "Cities/Towns"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_normal_maps = "Normal Maps"
tag_vanilla_friendly = "Vanilla Friendly"
tag_oaab_data = "OAAB Data"
tag_tamriel_data = "Tamriel Data"
tag_grass = "Grass"
tag_manual_edit_needed = "Manual Edit Needed"

# Names of mods that are used as alts in this file.
epic_balmora = "Epic Balmora"
kilcundas_balmora = "Kilcunda's Balmora"
sadrith_mora_expanded = "Sadrith Mora Expanded"


def init():
    for m in [
        {
            "author": nexus_user("66088", "TRJTA"),
            "category": cities_and_towns,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Adds buildings and trees, improves tree models and gardens, and much much more.",
            "name": "Mournhold Overhaul v21",
            "needs_cleaning": True,
            "slug": "mournhold-overhaul-v21",
            "status": "live",
            "tags": [tag_game_expanding, tag_high_res],
            "url": nexus_mod("25147"),
            "usage_notes": """<p>It's not necessary activate the <code>Mournhold Overhaul v2.0 - WG + NPC's.esp</code> file in your load order.  Doing so may have an adverse impact on performance in Mournhold, as great as it might look.</p>

    <p>If you choose to use Windows Glow (<span class="bold">NOT</span> <a href="/mods/glow-in-the-dahrk/">Glow in the Dahrk</a>), replace the <code>Windows Glow - Tribunal Eng.esp</code> you already have with the one provided by this (or you will have two plugins of the same name in your launcher).</p>""",
        },
        {
            "author": nexus_user("4465922", "Fulgore"),
            "category": cities_and_towns,
            "date_added": "2019-12-27 18:54:43 -0500",
            "description": "Get lost within the depths of Balmora's Underworld. Walk through the sewers: let the Thousand Lanterns Market take your breath away and explore the abandoned canals. Witness criminal factions fighting against each other and common folks going on about their daily lives. Discover the access to the massive cave system that lies deep below Balmora.",
            "name": "Balmora Underworld",
            "needs_cleaning": True,
            "status": "live",
            "url": nexus_mod("42448"),
            "usage_notes": """<p>Be sure to also grab <a href="/mods/balmora-underworld-nav-point-fix/">Balmora Underworld Nav Point Fix</a> (list readers: it's next).</p>

<p>The author notes that some cells have quite a high object count, this might affect your game's FPS.  This mod has also been noted to have problems with the Dwemer elevetors when <code>OPENMW_PHYSICS_FPS</code> is set to 15.</p>""",
        },
        {
            "author": nexus_user("628185", "Kilcunda"),
            "category": cities_and_towns,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Gives Balmora a fresh look by adding more to it but without going too overboard.",
            "name": kilcundas_balmora,
            "needs_cleaning": True,
            "slug": "kilcundas-balmora",
            "status": "live",
            "url": nexus_mod("44149"),
            "usage_notes": "This should be loaded before Ports of Vvardenfell, both data and plugin, but there will still be a conflict near the strider docks.  This mod does cause performance in Balmora to drop a bit.",
        },
        {
            "name": "Dramatic Vivec",
            "author": "LondonRook",
            "category": cities_and_towns,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Updates the largest city in Vvardenfell by completely overhauling the lighting system, and adding features like flowerbeds, signs, and monuments.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43385"),
            "usage_notes": """This mod has a dramatic impact on performance at the moment.""",
        },
        {
            "name": sadrith_mora_expanded,
            "author": nexus_user("4238746", "Hlaalu66"),
            "category": cities_and_towns,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "A mod that expands the city of Sadrith Mora with different aditions including an upper district in the vacant area behind Tel Naga. I prefer the non-Epic version.",
            "dl_url": "/files/SadrithMoraExpandedTR-Cleaned.zip",
            "needs_cleaning": True,
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly],
            "url": nexus_mod("44113"),
            "usage_notes": """<p>Comes with a TR-compatible add-on, use that if need be (please see <a href='/tips/tr-patcher/'>this page</a> for more information).  A patched <code>.esp</code> file can be found at the "Download Link" below.  This has already been cleaned.</p>""",
        },
        {
            "name": "Epic Ald Ruhn",
            "author": nexus_user("159722", "Mikeandike"),
            "category": cities_and_towns,
            "date_added": "2019-07-20 15:52:54 -0500",
            "description": "Epic Ald'Ruhn is the final major installment in [Mikeandike's] epic city project. Like the others (see below) it does something unconventional; it looks at how future Bethesda games have crafted cities and tries to bring those teachings back to Morrowind. How it's done is by creating the illusion of a bigger city through the establishment of architecture surrounding vanilla buildings you interact with.",
            "status": "live",
            "url": nexus_mod("43072"),
        },
        {
            "name": epic_balmora,
            "alt_to": [kilcundas_balmora],
            "author": nexus_user("159722", "Mikeandike"),
            "category": cities_and_towns,
            "date_added": "2019-07-20 16:06:01 -0500",
            "description": "Epic Balmora does something unconventional; it looks at how future Bethesda games have crafted cities and tries to bring those teachings back to Morrowind. How it's done is by creating the illusion of a bigger city through the establishment of architecture surrounding vanilla buildings you interact with.",
            "status": "live",
            "url": nexus_mod("43014"),
        },
        {
            "name": "Epic Dagon Fel",
            "author": nexus_user("159722", "Mikeandike"),
            "category": cities_and_towns,
            "date_added": "2019-07-20 15:40:45 -0500",
            "description": "Epic Dagon Fel is the fourth installment in [Mikeandike's] Epic City Mods. It litters the slopes and edges of Dagon Fel with Dwemer Ruins, creating the illusion of a bigger city. There are also some shacks that have been added to town. Ontop of this, I've also added a mini dungeon that you can call your home. To get the details on that, talk to some villagers in town.",
            "status": "live",
            "url": nexus_mod("43939"),
        },
        {
            "name": "Epic Sadrith Mora",
            "alt_to": [sadrith_mora_expanded],
            "author": nexus_user("159722", "Mikeandike"),
            "category": cities_and_towns,
            "date_added": "2019-07-20 16:02:07 -0500",
            "description": "Epic Sadrith Mora does something unconventional; it looks at how future Bethesda games have crafted cities and tries to bring those teachings back to Morrowind. How it's done is by creating the illusion of a bigger city through the establishment of architecture surrounding vanilla buildings you interact with.",
            "status": "live",
            "url": nexus_mod("43057"),
        },
        {
            "name": "City of Balmora - Hlaalu Expansion",
            "alt_to": [epic_balmora, kilcundas_balmora],
            "author": "Praedator - " + nexus_user("2441003", "EnvyDeveloper"),
            "category": cities_and_towns,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": "Not your typical expansion of Balmora - City of Balmora also adds 3 new factions with their own headquarters, several side quests and many caves to explore. You may even decide the future of Vvardenfell for decades ahead...",
            "status": "live",
            "url": nexus_mod("45324"),
            "usage_notes": """<p>This mod adds a very large amount of content, so depending on how many mesh and texture replacers you use it may have a noticeable impact on performance in Balmora and the general vicinity.  Using <a href="/tips/openmw-physics-fps/"><code>OPENMW_PHYSICS_FPS</code></a> may help.</p>""",
        },
        {
            "name": "Caldera Mine Expanded",
            "slug": "caldera-mine-expanded",
            "author": "{0}, {1}, and {2}".format(
                nexus_user("2933231", "TheDrunkenMudcrab"),
                nexus_user("962116", "MelchiorDahrk"),
                nexus_user("64030", "Greatness7"),
            ),
            "category": cities_and_towns,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2023-06-19 13:10:00 -0500",
            "description": """<p>From the Nexus description:</p><p>Caldera Mine Expanded is a mod that enhances the Caldera Mine area, making it seem as important and high-priority as quests and dialogue made it out to be. The vanilla location is dead, lifeless, and doesn't seem like one of the largest workplaces in all of Vvardenfell. Caldera Mine was impressive in vanilla Morrowind, but it didn't quite live up to the lore around it. This mod aims to fix that.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("45194"),
            "usage_notes": """<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean DD_Caldera_Expansion.esp

CLEANING: "DD_Caldera_Expansion.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
 Cleaned redundant AMBI,WHGT from CELL: ald-ruhn, llethri private quarters
 Cleaned redundant AMBI,WHGT from CELL: ashanammu
 Cleaned redundant WHGT from CELL: caldera mine
 Cleaned redundant AMBI,WHGT from CELL: caldera, mining bunkhouse
 Cleaned redundant AMBI,WHGT from CELL: caldera, mining company office
 Cleaned redundant AMBI,WHGT from CELL: caldera, mining guard tower
 Cleaned redundant AMBI,WHGT from CELL: uveran ancestral tomb
Output saved in: "Clean_DD_Caldera_Expansion.esp"
Original unaltered: "DD_Caldera_Expansion.esp"

Cleaning Stats for "DD_Caldera_Expansion.esp":
             redundant CELL.AMBI:     6
             redundant CELL.WHGT:     7</code></pre>
</details>""",
        },
        {
            "author": nexus_user("44525932", "Mordaxis"),
            "category": cities_and_towns,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2020-11-22 13:22:59 -0500",
            "description": "A relatively small, lore-friendly expansion to the interior of the Legion fort in Sadrith Mora, Wolverine Hall.",
            "name": "Wolverine Hall Interior Expansion",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("44965"),
            "usage_notes": """<p>A patched version of the <code>Wolverine Hall Interior Expanded.esp</code> plugin can be found <a href="/files/Wolverine Hall Interior Expansion - Fixed-47700-1-2-1-1592548468.7z">here</a>, be sure to use that instead of the one from Nexus. Credit for <a href="https://www.nexusmods.com/morrowind/mods/47700">the plugin<a> goes to <a href="https://www.nexusmods.com/morrowind/users/57788911">qwertyquit</a>.</p>""",
        },
        {
            "author": nexus_user("2441003", "EnvyDeveloper"),
            "category": cities_and_towns,
            "date_added": "2020-06-25 18:00:00 -0500",
            "description": """A small mod which adds distant buildings to Mournhold. So you can see the temple from almost any part of the city.""",
            "name": "Distant Mournhold",
            "status": "live",
            "url": nexus_mod("43255"),
            "usage_notes": """Enable the plugin to get the distant buildings, no other extra steps are required.""",
        },
        {
            "name": "Velothi Wall Art",
            "author": "Ashstaar",
            "category": cities_and_towns,
            "date_added": "2021-04-18 10:26:43 -0500",
            "description": """Adds dunmer concept art around vvardenfell on the velothi buildings and temples in various locations as can be seen in some of bethesda's own concept art for Morrowind. Mostly in vivec city plaza's but also in a few obscure places as well.""",
            "status": "live",
            "url": nexus_mod("46454"),
            "usage_notes": """If you're following a mod list then skip the plugin that this one comes with (one that's patched for compatibility will be used later on).""",
        },
        {
            "name": "OAAB Tel Mora",
            "author": nexus_user("962116", "MelchiorDahrk"),
            "category": cities_and_towns,
            "date_added": "2021-08-25 22:26:09 -0500",
            "description": """OAAB Tel Mora enhances the city of Tel Mora by adding new buildings, new quests, detailing the landscape, and more. It accomplishes all this while staying lore friendly and without (in my opinion) going over the top by making the city too big.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("46177"),
        },
        {
            "name": "Seyda Neen - Gateway to Vvardenfell",
            "author": nexus_user("59284071", "RandomPal"),
            "category": cities_and_towns,
            "date_added": "2021-08-25 22:36:58 -0500",
            "description": """Vanilla friendly overhaul of Seyda Neen.""",
            "status": "live",
            "url": nexus_mod("48781"),
            "usage_notes": """<p>Mod list instructions:</p>
<ul>
            <li>For "Total Overhaul": download the "Seyda Neen - Gateway" file, enable the <code>Seyda_Neen_Gateway.ESP</code> plugin it comes with. I provide a compatible grass plugin later on in the list.</li>
            <li>For "Expanded Vanilla": download the "Seyda Neen - Gateway - Vanilla Lighthouse but taller" file. You need the <code>00 Core</code> directory, enable the <code>Seyda_Neen_Gateway_VanillaLighthouse_taller.ESP</code> it comes with.</li>
</ul>""",
        },
        {
            "alt_to": ["Dramatic Vivec"],
            "name": "Vivec City",
            "author": "Nwahs and Mushrooms Team",
            "category": cities_and_towns,
            "date_added": "2021-08-27 23:25:10 -0500",
            "description": """More details on the exteriors of the City of God. Port at Hlaalu. Extended Port of silt striders. Animated traders. Each quarter is now unique.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50111"),
            "usage_notes": """<p><span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span>.</p>""",
        },
        {
            "name": "Hla Oad",
            "author": "Nwahs and Mushrooms Team",
            "category": cities_and_towns,
            "date_added": "2021-09-04 17:16:56 -0500",
            "description": """Detailing Hla Oad to make this village more habitable. More defined boundaries. Fences!""",
            "needs_cleaning": True,
            #
            # tes3cmd clean "Hla Oad.esp"
            #
            # CLEANING: "Hla Oad.esp" ...
            # Loaded cached Master: <DATADIR>/morrowind.esm
            # Loaded cached Master: <DATADIR>/tribunal.esm
            # Loaded cached Master: <DATADIR>/bloodmoon.esm
            #  Cleaned duplicate object instance (ex_de_docks_center FRMR: 67834) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (terrain_rock_bc_15 FRMR: 255236) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (terrain_rock_bc_16 FRMR: 255237) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (flora_bc_tree_03 FRMR: 255238) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (terrain_rock_bc_15 FRMR: 255254) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (flora_bc_grass_01 FRMR: 255255) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (flora_bc_tree_02 FRMR: 255265) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (terrain_rock_bc_18 FRMR: 255277) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (flora_bc_grass_01 FRMR: 255278) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (flora_bc_tree_08 FRMR: 255327) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (furn_de_signpost_02 FRMR: 363322) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (Act_banner_Hla_Oad FRMR: 363323) from CELL: hla oad (-6, -5)
            #  Cleaned duplicate object instance (com_chest_02_hhbt FRMR: 374426) from CELL: hla oad (-6, -5)
            #  Cleaned junk-CELL: bitter coast region (-6, -6)
            #  Cleaned duplicate object instance (in_mudcave_15 FRMR: 113637) from CELL: hla oad, fatleg's drop off
            #  Cleaned duplicate object instance (in_cavern_beam10 FRMR: 113674) from CELL: hla oad, fatleg's drop off
            #  Cleaned duplicate object instance (in_mudboulder02 FRMR: 118765) from CELL: hla oad, fatleg's drop off
            #  Cleaned duplicate object instance (in_mudboulder05 FRMR: 118766) from CELL: hla oad, fatleg's drop off
            #  Cleaned redundant WHGT from CELL: hla oad, fatleg's drop off
            # Output saved in: "Clean_Hla Oad.esp"
            # Original unaltered: "Hla Oad.esp"
            #
            # Cleaning Stats for "Hla Oad.esp":
            #        duplicate object instance:    17
            #                        junk-CELL:     1
            #              redundant CELL.WHGT:     1
            #
            "status": "live",
            "url": nexus_mod("49035"),
        },
        {
            "alt_to": ["Epic Ald Ruhn"],
            "name": "Ald'ruhn",
            "author": "Nwahs and Mushrooms Team",
            "category": cities_and_towns,
            "date_added": "2021-09-11 23:41:17 -0500",
            "description": """Detailed Ald'ruhn. More logical port of silt strider. Reworked Temple from Zobator. Animations for NPC.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50120"),
            "usage_notes": """<p>Requires <a href="/mods/oaab_data/">OAAB_Data</a>.</p>""",
        },
        {
            "name": "Caldera",
            "author": "Nwahs and Mushrooms Team",
            "category": cities_and_towns,
            "date_added": "2021-10-21 20:24:48 -0500",
            "description": """Detailing Caldera.""",
            "needs_cleaning": True,
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50314"),
            "usage_notes": """<p>Requires <a href="/mods/oaab_data/">OAAB_Data</a>.</p>

<details>
<summary>
<p>The <code>Caldera.esp</code> plugin is dirty (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean Caldera.esp

CLEANING: "Caldera.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
Loaded cached Master: <DATADIR>/oaab_data.esm
 Cleaned junk-CELL: azura's coast region (18, 5)
 Cleaned duplicate object instance (ex_nord_house_02 FRMR: 132669) from CELL: caldera (-2, 2)
 Cleaned duplicate object instance (ex_nord_house_04 FRMR: 132700) from CELL: caldera (-2, 2)
Output saved in: "Clean_Caldera.esp"
Original unaltered: "Caldera.esp"

Cleaning Stats for "Caldera.esp":
       duplicate object instance:     2
                       junk-CELL:     1</code></pre>
</details>""",
        },
        {
            "name": "Beautiful Cities of Morrowind",
            "author": "RandomPal and the Morrowind Modding Community",
            "category": cities_and_towns,
            "date_added": "2022-07-16 2:45:30 -0500",
            "date_updated": "2024-01-06 11:51:47 +0100",
            "description": """Overhaul of almost every settlement of Morrowind, inspired by concept art and aimed at a vanilla+ feeling.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49231"),
        },
        {
            "name": "Guar Stables of Vivec",
            "author": nexus_user("59284071", "RandomPal"),
            "category": cities_and_towns,
            "date_added": "2022-11-11 17:10:00 -0500",
            "description": """Adds guar stables to Vivec.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("50118"),
        },
        {
            "name": "Immersive Mournhold",
            "author": "Various",
            "category": cities_and_towns,
            "date_added": "2022-11-11 17:16:00 -0500",
            "description": """A Mournhold Overhaul based on the works of several prior mods.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49353"),
        },
        {
            "name": "Imperial Towns Revamp",
            "author": nexus_user("88610433", "SVNR"),
            "category": cities_and_towns,
            "date_added": "2022-11-13 10:41:00 -0500",
            "description": """A high poly version of the meshes.""",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("49735"),
        },
        {
            "name": "New Gnaar Mok",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": cities_and_towns,
            "date_added": "2022-11-19 12:23:00 -0500",
            "description": """Expands the city of Gnaar Mok by adding in clutter, more NPCs, more buildings and among other things a new miscellaneous quest.""",
            "status": "live",
            "url": nexus_mod("22029"),
        },
        {
            "name": "Shipyards of Vvardenfell (Sadrith Mora - Seyda Neen - Gnaar Mok)",
            "author": "Sexy Slippery Sloads",
            "category": cities_and_towns,
            "date_added": "2022-11-19 15:08:00 -0500",
            "description": """Morrowind is full of ships, boats and skiffs but who built them and where? Our mod seeks to answer these questions once and for all by adding shipyards to towns by the sea. The mod adds shipyards to Sadrith Mora, Seyda Neen and Gnaar Mok with four engaging quests in total.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51928"),
        },
        {
            "name": "Baar Dau - Ministry of Truth",
            "author": "Nwahs and Mushrooms Team",
            "category": cities_and_towns,
            "date_added": "2023-09-12 11:16:00 -0500",
            "description": """New model of the moon. Redesigned interiors. Animated NPC.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("50877"),
        },
        {
            "name": "Nordic Dagon Fel",
            "author": nexus_user("59284071", "RandomPal"),
            "category": cities_and_towns,
            "date_added": "2023-09-12 12:19:00 -0500",
            "description": """Subtle changes to make the Nordic village of Dagon Fel...Nordic.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49603"),
        },
        {
            "name": "Redaynia Restored",
            "author": nexus_user("3241081", "Reizeron"),
            "category": cities_and_towns,
            "date_added": "2023-09-12 23:00:00 -0500",
            "description": """Adds the missing village of Ald Redaynia.""",
            "status": "live",
            "url": nexus_mod("47646"),
        },
        {
            "name": "The Wolverine Hall",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": cities_and_towns,
            "date_added": "2023-09-12 23:11:00 -0500",
            "description": """Overhaul of the Wolverine Hall, turning it into a proper hall.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_manual_edit_needed],
            "url": nexus_mod("52199"),
        },
        {
            "name": "Caldera Governors Manor Redone",
            "author": nexus_user("1729697", "Markond"),
            "category": cities_and_towns,
            "date_added": "2023-09-12 23:34:00 -0500",
            "description": """Rebuilds the interior and exterior of the Caldera governors manor to be more imposing.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53220"),
        },
        {
            "name": "Hanging Gardens of Suran",
            "author": nexus_user("4462276", "mwgek"),
            "category": cities_and_towns,
            "date_added": "2023-09-12 23:46:00 -0500",
            "description": """A breathtaking mod that introduces captivating vertical gardens to the heart of the bustling city of Suran!""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53316"),
        },
        {
            "name": "Sadrith Mora BCOM Plus Module",
            "author": nexus_user("5708545", "DetailDevil"),
            "category": cities_and_towns,
            "date_added": "2023-09-19 11:36:00 -0500",
            "description": """This is not BCOM but a BCOM extensions module. It includes a BAIN installer where you can choose from 9 mods (or even take them all) that have been made both BCOM compatible and compatible with each other.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53465"),
        },
        {
            "name": "Dagon Fel Lighthouse",
            "author": nexus_user("4462276", "MwGek"),
            "category": cities_and_towns,
            "date_added": "2023-10-19 13:58:00 -0500",
            "description": """A nord lighthouse has been added to the north-west of Dagon fel. Now ships from across Morrowind and Skyrim can safely find their way into the Dagon Fel bay. A Nord named Vigunn is the local lighthouse keeper.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52291"),
        },
        {
            "name": "Planters for Ajira",
            "author": nexus_user("58363776", "Juidius"),
            "category": cities_and_towns,
            "date_added": "2023-10-19 14:14:00 -0500",
            "description": """This simple mod will put planters in the Balmora mages guild that will be filled with the plants you bring to Ajira. Help Ajira gather plants to make the guild a more beautiful place!""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("53489"),
        },
        {
            "name": "Bell Towers of Vvardenfell",
            "author": nexus_user("1729697", "Markond"),
            "category": cities_and_towns,
            "date_added": "2023-11-08 11:32:00 -0500",
            "description": """Adds bell towers to several settlements that ring at dawn, midday, and dusk.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52523"),
        },
        {
            "name": "Bell Towers of Vvardenfell Directional Sound for OpenMW",
            "author": nexus_user("5708545", "DetailDevil") + " and Markond",
            "category": cities_and_towns,
            "date_added": "2023-11-08 11:33:00 -0500",
            "description": """The sound of the Bell Towers of Morrowind is now coming from the bells. In the original mod the sound comes from the player.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53037"),
        },
        {
            "name": "Concept Arts plantations",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": cities_and_towns,
            "date_added": "2021-08-21 09:19:03 -0500",
            "description": """A concept arts-based overhaul of the Arvel and Dren plantations.""",
            "status": "live",
            "tags": [tag_grass, tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("50020"),
        },
        {
            "name": "OAAB Grazelands",
            "alt_to": ["Tower of Vos"],
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": cities_and_towns,
            "compat": "partially working",
            "date_added": "2021-09-11 23:24:54 -0500",
            "description": """This mod is an overhaul of Vos and Tel Vos, the land surrounding them, the two nearby egg mines, covering a large portion of the northern Grazelands. Currently, the mod does not cover the entire Grazelands region, but additional work is planned in future updates. The mod includes 16+ new quests with approximately 18,000 words of dialogue.""",
            "status": "live",
            "tags": [tag_grass, tag_oaab_data],
            "url": nexus_mod("49075"),
        },
        {
            "name": "Maar Gan - Town of Pilgrimage",
            "author": nexus_user("59284071", "RandomPal"),
            "category": cities_and_towns,
            "date_added": "2024-01-03 18:00:00 -0500",
            "description": """Overhaul of Maar Gan to make it more of a distinct and beautiful settlement.""",
            "status": "live",
            "tags": [tag_tamriel_data, tag_oaab_data],
            "url": nexus_mod("53919"),
        },
    ]:
        generate_mod(**m)
