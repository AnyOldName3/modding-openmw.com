from momw.helpers import generate_mod, nexus_mod, nexus_user


landscape = "Landscape"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_normal_maps = "Normal Maps"
tag_tes3mp_graphics = "TES3MP Graphics"
tag_vanilla_friendly = "Vanilla Friendly"
tag_oaab_data = "OAAB Data"
tag_tamriel_data = "Tamriel Data"

# Names of mods that are used as alts in this file.
tyddys_landscape_retexture = "Tyddy's Landscape Retexture"


def init():
    # print("START: {} ... ".format(flora_and_landscape), end="")

    _mods = [
        {
            "name": tyddys_landscape_retexture,
            "author": nexus_user("3281858", "Tyddy"),
            "category": landscape,
            "date_added": "2019-04-06 15:45:35 -0500",
            "description": "Retexture Vvardenfell and Solstheim in the original style",
            "slug": "tyddys-landscape-retexture",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": "http://www.fullrest.ru/files/tyd_landscape-texture_compilation_1/files",
        },
        {
            "name": "Taddeus On the Rocks with normal maps for OpenMW",
            "author": "Taddeus with edits by Lysol",
            "category": landscape,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Taddeus' excellent On the Rocks, fixed up for OpenMW by the great Lysol!",
            "needs_cleaning": True,
            "slug": "taddeus-on-the-rocks-with-normal-maps-openmw",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("45140"),
            "usage_notes": """This is known to conflict with Tamriel Rebuilt in various areas.""",
        },
        {
            "name": "Apel's Asura Coast and Sheogorath Region Retexture",
            "author": nexus_user("582122", "Apel"),
            "category": landscape,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-01-17 21:00:00 -0500",
            "description": "This mode is retexture and mesh replacer for Asura Coast & Sheogorath Regions.",
            "slug": "apels-asura-coast-and-sheogorath-region-retexture",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("42573"),
            "usage_notes": """<p>If you are following a mod list then all you need are the textures (you don't need the plugin).</p>

<p>OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.</p>""",
        },
        {
            "name": "Landscape Retexture",
            "slug": "landscape-retexture",
            "alt_to": [tyddys_landscape_retexture],
            "author": nexus_user("1600249", "Lougian"),
            "category": landscape,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2021-04-26 21:00:00 -0500",
            "description": "This mod retextures the ground of all regions, except for the rock textures. Now includes normal and ",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("42575"),
            "usage_notes": """<p>Download one of the <span class="bold">1K</span> or <span class="bold">2K</span> main files as well as the <span class="bold">OpenMW Normal map and Parallax patch</span> file. Rename the folder from the normal map/paralax file from <code>Landscape retexture Normal map & parallax</code> to <code>Landscape retexture Normal map parallax</code>; OpenMW will chop off the <code>&</code> in the cfg file if you don't, causing them to not be used.</p>

<p>If you aren't already using <a href="/mods/solstheim-tomb-of-the-snow-prince/">Solstheim Tomb of the Snow Prince</a>, also grab the Bloodmoon Landscape Retexture too.</p>

<p>Note that <a href="/mods/tyddys-landscape-retexture/">Tyddy's Landscape Retexture</a> is listed as an alternative to this, but it can compliment this one (providing many textures this does not).</p>""",
        },
        {
            "name": "Lougian's Landscape Retexture Normal Mapped",
            "author": nexus_user("1025257", "DontReadThis"),
            "category": landscape,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Normal mapped add on for Lougian's Landscape Retexxture - be sure to also grab the optional paralax maps.",
            "is_active": False,
            "slug": "lougians-landscape-retexture-normal-mapped",
            "status": "live",
            "url": nexus_mod("45178"),
            "usage_notes": """This mod is made obsolete by normal and specular maps that were made by Lougian, they are available on the download page of Landscape Retexture.""",
        },
        {
            "name": "Balmora Road Normal Maps",
            "author": "<a href='https://forum.openmw.org/memberlist.php?mode=viewprofile&u=3269&sid=d217514e6637f1b2f5cb8108125786cd'>Uradamus</a>",
            "category": landscape,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Excellent, vanilla-friendly normal maps of the road stones in Balmora.  This texture also shows up in parts of Tamriel Rebuilt so be in the lookout.",
            "slug": "balmora-road-normal-maps",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": "https://forum.openmw.org/viewtopic.php?t=3659",
        },
        {
            "name": "Apel's Fire Retexture",
            "author": nexus_user("582122", "Apel"),
            # TODO relocate this mod to an appropriate category (replacers?)
            "category": landscape,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2021-08-15 10:11:55 -0500",
            "description": "High quality fire texture replacer.",
            "slug": "apels-fire-retexture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("42554"),
            "usage_notes": """The textures from this mod have alpha issues, see the alternative listed above for a fully working version of this.""",
        },
        {
            "name": "Vurt's Lava and Smoke",
            "author": nexus_user("41808", "Vurt"),
            "category": landscape,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2022-11-07 10:54:00 -0500",
            "description": "4 Hi-res lava retextures to choose from and new smoke textures.",
            "status": "live",
            "tags": [tag_high_res, tag_vanilla_friendly],
            "url": nexus_mod("28519"),
            "usage_notes": """If following any mod list (or if trying to use with <a href="/mods/i-lava-good-mesh-replacer/">I Lava Good</a>) extract the <code>/4/Textures</code> folder from the archive into the base mod data path. The meshes are not needed.""",
        },
        {
            "name": "Mountainous Red Mountain",
            "author": nexus_user("28350", "Piratelord"),
            "category": landscape,
            "date_added": "2019-07-20 11:54:58 -0500",
            "description": "Increases the height of Red Mountain to almost 1.6 times original height and gives the landscape a more spikey volcanic fantasy look.",
            "needs_cleaning": True,
            "status": "live",
            "url": nexus_mod("42125"),
            "usage_notes": """See the mod's download page for a full description, as well as a rundown of known compatibility tweaks.  This mod makes extensive landscape changes, and will conflict with many mods as a result.  Testing is advised when mixing mods with MRM!""",
        },
        {
            "name": "Mistify",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": landscape,
            "date_added": "2021-04-10 07:17:03 -0500",
            "date_updated": "2023-05-13 11:30:27 -0500",
            "description": "This mod enhances the ambiance of the Bitter Coast by adding a new mist effect throughout region which appears during the night and certain weather conditions. It will burn off in the morning sun. The effect has been optimized to minimize performance impact. The mod also includes an optional replacer for the vanilla effect.",
            "status": "live",
            "url": nexus_mod("48112"),
        },
        {
            "name": "Apel's Fire Retexture Patched for OpenMW and Morrowind Rebirth",
            "author": nexus_user("449021", "JaceX"),
            # TODO relocate this mod to an appropriate category (replacers?)
            "category": landscape,
            "date_added": "2021-08-15 09:17:59 -0500",
            "description": "This patch fixes alpha issues that occur with Apel's fires in OpenMW and adds the enhanced fire textures to Morrowind Rebirth meshes.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("50092"),
        },
        {
            "name": "Subtle Smoke",
            "author": nexus_user("71679553", "wazabear"),
            # TODO relocate this mod to an appropriate category (replacers?)
            "category": landscape,
            "date_added": "2021-08-15 17:41:14 -0500",
            "description": "Makes it so many smoke effects are much more laid back and easier on the eyes.",
            "status": "live",
            "url": nexus_mod("47341"),
        },
        {
            "name": "I Lava Good Mesh Replacer",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": landscape,
            "date_added": "2021-08-15 18:10:44 -0500",
            "date_updated": "2023-05-27 11:33:00 -0500",
            "description": "This mod replaces all the lava meshes in the vanilla game. Removes alpha blending from lava meshes to eliminate flickering with effects like steam. Synchronizes tiled lava effects to reduce the occurrence of seams found in large lava pools. Adds performance friendly spark particle effects to lava pools. Texture replacer compatible.",
            "status": "live",
            "url": nexus_mod("49605"),
            "usage_notes": """<p>If you're following the Graphics Overhaul or Total Overhaul mod lists, use the <code>01 Vurt's Lava Patch</code> and <code>02 Tamriel_Data Patch</code> folder paths.</p>

<p>If you're following the Expanded Vanilla mod list, use the <code>00 Core</code> folder path.</p>""",
        },
        {
            "name": "OAAB - Foyada Mamaea",
            "author": nexus_user("899234", "Remiros"),
            "category": landscape,
            "date_added": "2021-09-06 14:20:44 -0500",
            "date_updated": "2022-11-07 14:33:00 -0500",
            "description": """This mod overhauls the entire Foyada Mamaea up until Ghostgate making the area much more interesting and varied. Most changes are visual in nature.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("46424"),
            "usage_notes": """<p><span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span></p>

<details>
<summary>
<p>The plugin is dirty (click to expand/collapse), <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> and you can skip if you're doing a mod list; a replacement plugin will be provided by <a href="/mods/beautiful-cities-of-morrowind/">BCOM</a>):</p>
</summary>

<pre><code>$ tes3cmd clean "OAAB - Foyada Mamaea.ESP"

CLEANING: "OAAB - Foyada Mamaea.ESP" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
Loaded cached Master: <DATADIR>/oaab_data.esm
 Cleaned duplicate object instance (volcano_steam FRMR: 359001) from CELL: molag mar region (2, -1)
 Cleaned junk-CELL: ashlands region (1, 0)
 Cleaned junk-CELL: bitter coast region (-4, -6)
Output saved in: "Clean_OAAB - Foyada Mamaea.ESP"
Original unaltered: "OAAB - Foyada Mamaea.ESP"

Cleaning Stats for "OAAB - Foyada Mamaea.ESP":
       duplicate object instance:     1
                       junk-CELL:     2</code></pre>
</details>""",
        },
        {
            "name": "Trackless Grazeland",
            "author": nexus_user("3241081", "R-Zero"),
            "category": landscape,
            "date_added": "2021-09-08 08:25:22 -0500",
            "description": """Removes the roads and paths from the Grazelands region to make it consistent with in-game descriptions of it having \"no roads or tracks\".""",
            "status": "live",
            "url": nexus_mod("44194"),
        },
        {
            "name": "OAAB - The Ashen Divide",
            "author": nexus_user("899234", "Remiros"),
            "category": landscape,
            "date_added": "2021-09-13 08:21:22 -0500",
            "description": """OAAB - The Ashen Divide is a mini OAAB release and a remake/adaptation of the original The Ashen Divide mod. It transforms the Ashlands region between Seyda Neen and Balmora into a unique region that merges the biomes of the Bitter Coast and the Ashlands utilizing unique assets. It also overhauls the bandit lair of Zainsipilu.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("49047"),
            "usage_notes": """<p><span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span>.</p>

<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean "OAAB - The Ashen Divide.ESP"

CLEANING: "OAAB - The Ashen Divide.ESP" ...
Loading Master: morrowind.esm
Loading Master: tribunal.esm
Loading Master: bloodmoon.esm
Loading Master: oaab_data.esm
 Cleaned junk-CELL: ascadian isles region (-4, -5)
 Cleaned junk-CELL: bitter coast region (-6, -6)
 Cleaned redundant WHGT from CELL: zainsipilu
Output saved in: "Clean_OAAB - The Ashen Divide.ESP"
Original unaltered: "OAAB - The Ashen Divide.ESP"

Cleaning Stats for "OAAB - The Ashen Divide.ESP":
                       junk-CELL:     2
             redundant CELL.WHGT:     1</code></pre>
</details>""",
        },
        {
            "name": "OAAB Shipwrecks",
            "author": nexus_user("23131859", "Corsair83"),
            "category": landscape,
            "date_added": "2022-11-09 08:20:00 -0500",
            "description": """A complete overhaul of every vanilla shipwreck.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51364"),
            "usage_notes": """<p>If you're using a mod list:</p>

<ul>
    <li>Extract the <code>00 Core</code> folder but you won't need the plugin inside (a replacement that's been edited for compatibility will be given by another mod later on).</li>
    <li>Extract the <code>01 Patch - Uncharted Artifacts</code> folder as well, but that won't be used until <a href="/mods/oaab-shipwrecks-patches-uncharted-artifacts/">a bit later</a>.</li>
    <li>Extract and use the <code>02 Patch - TR Patch</code> folder  and its contents as normal, no special instructions for this one.
</ul>

<details>
<summary>
<p>The <code>OAAB - Shipwrecks - TR Patch.ESP</code> plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>CLEANING: "OAAB - Shipwrecks - TR Patch.ESP" ...
Loading Master: tamriel_data.esm
Loading Master: bloodmoon.esm
Loading Master: tribunal.esm
Loading Master: morrowind.esm
Loading Master: tr_mainland.esm
Loading Master: oaab_data.esm
 Cleaned junk-CELL: ascadian isles region (4, -15)
 Cleaned junk-CELL: ascadian bluffs region (4, -16)
 Cleaned junk-CELL: ascadian bluffs region (3, -16)
Output saved in: "Clean_OAAB - Shipwrecks - TR Patch.ESP"
Original unaltered: "OAAB - Shipwrecks - TR Patch.ESP"

Cleaning Stats for "OAAB - Shipwrecks - TR Patch.ESP":
                       junk-CELL:     3</code></pre>
</details>""",
        },
        {
            "name": "Justice for Khartag (J.F.K.)",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": landscape,
            "date_added": "2022-11-09 08:43:00 -0500",
            "date_updated": "2023-10-08 12:59:00 -0500",
            "description": """Changes Khartag Point making it the high peak described in the in-game dialogues.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49832"),
            "usage_notes": """<p><span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span></p>

<p><span class="bold">Mod list users</span>: download the <code>Justice for Khartag</code> file, but you will not use the plugin provided within. A compatibility version will be used in a later step.</p>""",
        },
        {
            "name": "Holamayan Island",
            "author": "Nwahs and Mushrooms Team",
            "category": landscape,
            "date_added": "2022-11-11 16:51:00 -0500",
            "description": """A simple mod that adds a few details to the monastery island.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50996"),
            "usage_notes": """<span class="bold">Requires <a href="/mods/oaab_data/">OAAB_Data</a></span>""",
        },
        {
            "name": "The Grove of Ben'Abi",
            "author": nexus_user("899234", "Remiros"),
            "category": landscape,
            "date_added": "2022-11-19 21:03:00 -0500",
            "description": """Small expansion to the mushroom grove near Dagon Fel.""",
            "status": "live",
            "url": nexus_mod("46137"),
        },
        {
            "name": "Concept Art Molag Amur Region - The Great Scathes",
            "author": nexus_user("59284071", "RandomPal"),
            "category": landscape,
            "date_added": "2023-09-28 10:55:00 -0500",
            "description": "Overhaul of the Molag Amur Region. This mod introduces actual sulfur ponds and transforms Lake Nabia into a large sulfur lake.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52971"),
        },
        {
            "name": "The Mountain of Fear",
            "author": nexus_user("114986408", "Kalinter"),
            "category": landscape,
            "date_added": "2023-09-28 11:17:00 -0500",
            "description": "This mod touches up Mount Kand, to make it slightly more worthy of its secondary name: 'Mountain of Fear'.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52859"),
        },
        {
            "name": "Bal'laku - The Lonely Towers",
            "author": nexus_user("114986408", "Kalinter"),
            "category": landscape,
            "date_added": "2023-09-28 12:47:00 -0500",
            "description": "This mod adds two towers to the northeastern ashlands, using resources from 'The Hive' - an unfinished beta from the Resdayn Revival Team.",
            "status": "live",
            "url": nexus_mod("51060"),
        },
        {
            "name": "Little Landscapes - Bitter Coast Waterway",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-09-28 12:57:00 -0500",
            "description": "Visual overhaul of the coastline between Seyda Neen and the shipwreck at the mouth of the Odai River.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53262"),
        },
        {
            "name": "Little Landscapes - Path to Pelagiad",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-09-28 12:57:00 -0500",
            "description": "Visual overhaul of the path from Addamasartus to just outside Pelagiad.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53281"),
        },
        {
            "name": "Little Landscapes - Vivec Islands",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-09-28 13:18:00 -0500",
            "description": "Visual overhaul of the sandy islands near Vivec.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53276"),
        },
        {
            "name": "Compatible Odai River Upper Overhaul",
            "author": nexus_user("139627708", "CoffeeBeard0"),
            "category": landscape,
            "date_added": "2023-09-28 13:23:00 -0500",
            "description": "A modified version of GlitterGears Upper Odai Overhaul, makes changes to the area between that tiny island outside Balmora to the rickety wooden bridge that crosses the Odai River.",
            "status": "live",
            "url": nexus_mod("53323"),
        },
        {
            "name": "Little Landscapes - Path to Vivec Lighthouse",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-09-28 13:41:00 -0500",
            "description": "Visual overhaul of the walk to/from the Lighthouse and Ebonheart.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53352"),
        },
        {
            "name": "Vivec Lighthouse",
            "author": nexus_user("4462276", "mwgek"),
            "category": landscape,
            "date_added": "2023-09-28 13:58:00 -0500",
            "description": "This mod adds a Velothi lighthouse to the peninsula south of Ebonheart.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52019"),
        },
        {
            "name": "Hidden Sea Loot",
            "author": nexus_user("109130978", "XeroFoxx1"),
            "category": landscape,
            "date_added": "2023-09-28 14:08:00 -0500",
            "description": "Adds over 500 hidden containers for you to find in the sea for every region around the island of Vvardenfell. Each contains random items that change for EVERY playthrough. Randomization is weighted for balanced distribution.",
            "status": "live",
            "url": nexus_mod("52296"),
        },
        {
            "name": "Telvanni Sea Beacons",
            "author": nexus_user("1729697", "Markond"),
            "category": landscape,
            "date_added": "2023-10-16 17:51:00 -0500",
            "description": "A series of navigation beacons along the length of the Azuras Coast.",
            "status": "live",
            "url": nexus_mod("50957"),
        },
        {
            "name": "Marbled Zafirbel Bay",
            "author": nexus_user("1808532", "Lord Zarcon"),
            "category": landscape,
            "date_added": "2023-10-16 18:05:00 -0500",
            "description": "Gives the central most part of Azura's Coast a distinct appearance.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53126"),
        },
        {
            "name": "Gates of Ascadia",
            "author": nexus_user("114986408", "Kalinter"),
            "category": landscape,
            "date_added": "2023-10-20 12:53:00 -0500",
            "description": "This mod serves as a companion mod to The Great Seawall of Vivec. The Gates of Ascadia fortifies the land routes to Vivec, with several Velothi gates and walls controlling access to the city. It also replaces several wooden bridges in the Ascadian Isles with more imposing Velothi style bridges.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53547"),
        },
        {
            "name": "The Great Seawall of Vivec",
            "author": nexus_user("114986408", "Kalinter"),
            "category": landscape,
            "date_added": "2023-11-13 17:28:03 +0100",
            "description": "The Great Seawall stands south of Vivec, serving as a fortification to repel naval assaults against the city, and as a chokepoint to control maritime traffic to and from Ebonheart and Vivec. It offers splendid views of both the Ascadian Isles and the Inner Sea.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53544"),
        },
        {
            "name": "The Grove of Ben'Abi Enhanced",
            "author": "Remiros and " + nexus_user("6673285", "AbbaddoN33"),
            "category": landscape,
            "date_added": "2023-10-31 14:02:00 -0500",
            "description": "This mod provides some small fixes to The Grove of Ben'Abi by Remiros. The Grove of Ben'Abi is an overhaul to the memorable mushroom grove near Dagon Fel. It adds a lot of detail as well as a background story to this area.",
            "status": "live",
            "url": nexus_mod("53529"),
        },
        {
            "name": "Little Landscapes - Seyda Neen Swamp Pools",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-11-06 12:30:53 +0100",
            "description": " Overhauls those swamp pools outside Seyda Neen near the tax collector.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53335"),
        },
        {
            "name": "Little Landscapes - Nix Hound Hunting Grounds",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-11-07 18:21:57 +0100",
            "description": " Overhauls the area where the quest Separated by Nixhounds takes place. ",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53333"),
        },
        {
            "name": "Little Landscape - Path to Balmora",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-11-07 18:28:57 +0100",
            "description": "Overhauls the Ascadian Isles part from Pelagiad to Balmora.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53343"),
        },
        {
            "name": "Little Landscape - Foyada of Sharp Teeth",
            "author": nexus_user("70112108", "GlitterGear"),
            "category": landscape,
            "date_added": "2023-11-07 18:28:57 +0100",
            "description": "Overhauls the northernmost part of Foyada Bani-Dad, near the Derelict Shipwreck near Khuul.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53355"),
        },
        {
            "name": "The Ebonheart Lighthouse",
            "author": nexus_user("70112108", "mwgek"),
            "category": landscape,
            "date_added": "2023-11-18 14:55:39 +0100",
            "description": "This carefully crafted mod introduces an Imperial lighthouse that stands tall and proud in the heart of Ebonheart's harbor.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53417"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
