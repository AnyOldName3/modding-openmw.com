from momw.helpers import generate_mod, nexus_mod, nexus_user, modhistory_archive_link


skies = "Skies"

tag_high_res = "High Res"


def init():
    # print("START: {} ... ".format(skies), end="")

    _mods = [
        {
            "author": nexus_user("3281858", "Tyddy"),
            "category": skies,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Simple retexture, which make Masser and Secunda dying planets, where is no more place for life.",
            "name": "Dying Worlds - moons retexture",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43023"),
        },
        {
            "author": nexus_user("15199204", "starwarsgal9875"),
            "category": skies,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-06-26 12:57:52 -0500",
            "description": """<p>Comprehensive Overhaul for the weather in Morrowind, Solsthiem, and beyond.</p><p>The README mentions that tweaks need to be made to cloud speed (or else they move *really* fast!), see below for those....</p>""",
            "extra_cfg": """<p>Below are the values that need to be changed and their default settings:</p>

<pre><code>fallback=Weather_Clear_Cloud_Speed,1.25
fallback=Weather_Cloudy_Cloud_Speed,2
fallback=Weather_Foggy_Cloud_Speed,1.25
fallback=Weather_Thunderstorm_Cloud_Speed,3
fallback=Weather_Rain_Cloud_Speed,2
fallback=Weather_Overcast_Cloud_Speed,1.5
fallback=Weather_Ashstorm_Cloud_Speed,7
fallback=Weather_Blight_Cloud_Speed,9
fallback=Weather_Snow_Cloud_Speed,1.5
fallback=Weather_Blizzard_Cloud_Speed,7.5</code></pre>

<p>And the edited values are below:</p>

<pre><code>fallback=Weather_Clear_Cloud_Speed,0.25
fallback=Weather_Cloudy_Cloud_Speed,0.4
fallback=Weather_Foggy_Cloud_Speed,0.25
fallback=Weather_Thunderstorm_Cloud_Speed,0.6
fallback=Weather_Rain_Cloud_Speed,0.4
fallback=Weather_Overcast_Cloud_Speed,0.3
fallback=Weather_Ashstorm_Cloud_Speed,1.4
fallback=Weather_Blight_Cloud_Speed,1.8
fallback=Weather_Snow_Cloud_Speed,0.3
fallback=Weather_Blizzard_Cloud_Speed,1.5</code></pre>

<p>You can paste these just after the last <code>fallback=</code> line in your <code>openmw.cfg</code> file.</p>""",
            "extra_cfg_raw": """#
# Skies .IV
# (This goes into openmw.cfg)
#
fallback=Weather_Clear_Cloud_Speed,0.25
fallback=Weather_Cloudy_Cloud_Speed,0.4
fallback=Weather_Foggy_Cloud_Speed,0.25
fallback=Weather_Thunderstorm_Cloud_Speed,0.6
fallback=Weather_Rain_Cloud_Speed,0.4
fallback=Weather_Overcast_Cloud_Speed,0.3
fallback=Weather_Ashstorm_Cloud_Speed,1.4
fallback=Weather_Blight_Cloud_Speed,1.8
fallback=Weather_Snow_Cloud_Speed,0.3
fallback=Weather_Blizzard_Cloud_Speed,1.5""",
            "name": "Skies .IV",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43311"),
            "usage_notes": """<p>Use the contents of the <code>Skies - .IV</code> folder in the zip file. <span class="bold">The <code>Particles/meshes/raindrop.nif</code> and <code>Particles/textures/tx_raindrop_01.dds</code> files must be deleted, they are not compatible with OpenMW at this time</span>. Read the included Readme.txt file for information on the available installation choices.</p>""",
        },
        {
            "author": nexus_user("41808", "Vurt"),
            "category": skies,
            "compat": "unknown",
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Hi-res Skies and weather Pack for use with the Skies 3 mod.",
            "extra_cfg": """<p>These values are adapted from the readme file included in the mod (changed into OpenMW format via the INI-Importer).  Paste these values at the end of the fallbacks section in your <code>openmw.cfg</code> (that is, after the last line that says <code>fallback=</code>):</p>
<pre><code>fallback=Weather_Clear_Cloud_Texture,Tx_Sky_Clear.tga
fallback=Weather_Clear_Clouds_Maximum_Percent,1.0
fallback=Weather_Clear_Transition_Delta,.015
fallback=Weather_Clear_Sky_Sunrise_Color,100,140,205
fallback=Weather_Clear_Sky_Day_Color,115,156,220
fallback=Weather_Clear_Sky_Sunset_Color,196,141,170
fallback=Weather_Clear_Sky_Night_Color,000,000,010
fallback=Weather_Clear_Fog_Sunrise_Color,187,100,023
fallback=Weather_Clear_Fog_Day_Color,187,222,255
fallback=Weather_Clear_Fog_Sunset_Color,181,095,019
fallback=Weather_Clear_Fog_Night_Color,015,049,069
fallback=Weather_Clear_Ambient_Sunrise_Color,095,090,073
fallback=Weather_Clear_Ambient_Day_Color,184,178,117
fallback=Weather_Clear_Ambient_Sunset_Color,051,038,006
fallback=Weather_Clear_Ambient_Night_Color,030,030,050
fallback=Weather_Clear_Sun_Sunrise_Color,179,119,023
fallback=Weather_Clear_Sun_Day_Color,220,198,133
fallback=Weather_Clear_Sun_Sunset_Color,092,052,022
fallback=Weather_Clear_Sun_Night_Color,008,008,019
fallback=Weather_Clear_Sun_Disc_Sunset_Color,220,190,010
fallback=Weather_Clear_Land_Fog_Day_Depth,.34
fallback=Weather_Clear_Land_Fog_Night_Depth,.34
fallback=Weather_Clear_Wind_Speed,.1
fallback=Weather_Clear_Cloud_Speed,.50
fallback=Weather_Clear_Glare_View,1
fallback=Weather_Clear_Ambient_Loop_Sound_ID,None
fallback=Weather_Cloudy_Cloud_Texture,Tx_Sky_Cloudy.tga
fallback=Weather_Cloudy_Clouds_Maximum_Percent,1.0
fallback=Weather_Cloudy_Transition_Delta,.015
fallback=Weather_Cloudy_Sky_Sunrise_Color,049,078,120
fallback=Weather_Cloudy_Sky_Day_Color,059,088,130
fallback=Weather_Cloudy_Sky_Sunset_Color,102,073,089
fallback=Weather_Cloudy_Sky_Night_Color,005,005,009
fallback=Weather_Cloudy_Fog_Sunrise_Color,133,122,082
fallback=Weather_Cloudy_Fog_Day_Color,250,250,250
fallback=Weather_Cloudy_Fog_Sunset_Color,255,154,105
fallback=Weather_Cloudy_Fog_Night_Color,025,059,079
fallback=Weather_Cloudy_Ambient_Sunrise_Color,081,078,071
fallback=Weather_Cloudy_Ambient_Day_Color,245,255,255
fallback=Weather_Cloudy_Ambient_Sunset_Color,070,066,045
fallback=Weather_Cloudy_Ambient_Night_Color,023,026,029
fallback=Weather_Cloudy_Sun_Sunrise_Color,164,147,089
fallback=Weather_Cloudy_Sun_Day_Color,170,235,235
fallback=Weather_Cloudy_Sun_Sunset_Color,026,014,022
fallback=Weather_Cloudy_Sun_Night_Color,039,046,061
fallback=Weather_Cloudy_Sun_Disc_Sunset_Color,255,189,190
fallback=Weather_Cloudy_Land_Fog_Day_Depth,.72
fallback=Weather_Cloudy_Land_Fog_Night_Depth,.72
fallback=Weather_Cloudy_Wind_Speed,.2
fallback=Weather_Cloudy_Cloud_Speed,1
fallback=Weather_Cloudy_Glare_View,1
fallback=Weather_Cloudy_Ambient_Loop_Sound_ID,None
fallback=Weather_Foggy_Cloud_Texture,Tx_Sky_Foggy.tga
fallback=Weather_Foggy_Clouds_Maximum_Percent,1.0
fallback=Weather_Foggy_Transition_Delta,.015
fallback=Weather_Foggy_Sky_Sunrise_Color,150,150,150
fallback=Weather_Foggy_Sky_Day_Color,120,120,120
fallback=Weather_Foggy_Sky_Sunset_Color,030,030,030
fallback=Weather_Foggy_Sky_Night_Color,020,020,020
fallback=Weather_Foggy_Fog_Sunrise_Color,100,75,070
fallback=Weather_Foggy_Fog_Day_Color,190,190,190
fallback=Weather_Foggy_Fog_Sunset_Color,058,057,052
fallback=Weather_Foggy_Fog_Night_Color,023,023,033
fallback=Weather_Foggy_Ambient_Sunrise_Color,020,022,024
fallback=Weather_Foggy_Ambient_Day_Color,089,090,092
fallback=Weather_Foggy_Ambient_Sunset_Color,041,035,035
fallback=Weather_Foggy_Ambient_Night_Color,022,023,028
fallback=Weather_Foggy_Sun_Sunrise_Color,115,110,087
fallback=Weather_Foggy_Sun_Day_Color,111,131,141
fallback=Weather_Foggy_Sun_Sunset_Color,102,084,078
fallback=Weather_Foggy_Sun_Night_Color,008,006,006
fallback=Weather_Foggy_Sun_Disc_Sunset_Color,223,223,223
fallback=Weather_Foggy_Land_Fog_Day_Depth,1.0
fallback=Weather_Foggy_Land_Fog_Night_Depth,1.9
fallback=Weather_Foggy_Wind_Speed,0
fallback=Weather_Foggy_Cloud_Speed,.25
fallback=Weather_Foggy_Glare_View,0.15
fallback=Weather_Foggy_Ambient_Loop_Sound_ID,None
fallback=Weather_Thunderstorm_Cloud_Texture,Tx_Sky_Thunder.tga
fallback=Weather_Thunderstorm_Clouds_Maximum_Percent,0.66
fallback=Weather_Thunderstorm_Transition_Delta,.030
fallback=Weather_Thunderstorm_Sky_Sunrise_Color,024,024,024
fallback=Weather_Thunderstorm_Sky_Day_Color,016,016,017
fallback=Weather_Thunderstorm_Sky_Sunset_Color,022,021,021
fallback=Weather_Thunderstorm_Sky_Night_Color,011,011,014
fallback=Weather_Thunderstorm_Fog_Sunrise_Color,066,061,050
fallback=Weather_Thunderstorm_Fog_Day_Color,018,021,025
fallback=Weather_Thunderstorm_Fog_Sunset_Color,033,033,033
fallback=Weather_Thunderstorm_Fog_Night_Color,024,030,030
fallback=Weather_Thunderstorm_Ambient_Sunrise_Color,018,018,018
fallback=Weather_Thunderstorm_Ambient_Day_Color,050,050,060
fallback=Weather_Thunderstorm_Ambient_Sunset_Color,020,020,020
fallback=Weather_Thunderstorm_Ambient_Night_Color,010,010,014
fallback=Weather_Thunderstorm_Sun_Sunrise_Color,091,099,122
fallback=Weather_Thunderstorm_Sun_Day_Color,045,045,80
fallback=Weather_Thunderstorm_Sun_Sunset_Color,016,025,050
fallback=Weather_Thunderstorm_Sun_Night_Color,001,001,015
fallback=Weather_Thunderstorm_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Thunderstorm_Land_Fog_Day_Depth,1
fallback=Weather_Thunderstorm_Land_Fog_Night_Depth,1.15
fallback=Weather_Thunderstorm_Wind_Speed,.5
fallback=Weather_Thunderstorm_Cloud_Speed,3
fallback=Weather_Thunderstorm_Glare_View,0
fallback=Weather_Thunderstorm_Rain_Loop_Sound_ID,rain heavy
fallback=Weather_Thunderstorm_Using_Precip,1
fallback=Weather_Thunderstorm_Rain_Diameter,600
fallback=Weather_Thunderstorm_Rain_Height_Min,200
fallback=Weather_Thunderstorm_Rain_Height_Max,700
fallback=Weather_Thunderstorm_Rain_Threshold,0.6
fallback=Weather_Thunderstorm_Max_Raindrops,650
fallback=Weather_Thunderstorm_Rain_Entrance_Speed,5
fallback=Weather_Thunderstorm_Ambient_Loop_Sound_ID,None
fallback=Weather_Thunderstorm_Flash_Decrement,4
fallback=Weather_Rain_Cloud_Texture,Tx_Sky_Rainy.tga
fallback=Weather_Rain_Clouds_Maximum_Percent,0.66
fallback=Weather_Rain_Transition_Delta,.015
fallback=Weather_Rain_Sky_Sunrise_Color,071,074,075
fallback=Weather_Rain_Sky_Day_Color,083,083,083
fallback=Weather_Rain_Sky_Sunset_Color,033,033,033
fallback=Weather_Rain_Sky_Night_Color,010,010,013
fallback=Weather_Rain_Fog_Sunrise_Color,077,077,077
fallback=Weather_Rain_Fog_Day_Color,060,060,060
fallback=Weather_Rain_Fog_Sunset_Color,098,062,013
fallback=Weather_Rain_Fog_Night_Color,015,015,015
fallback=Weather_Rain_Ambient_Sunrise_Color,037,044,051
fallback=Weather_Rain_Ambient_Day_Color,040,040,052
fallback=Weather_Rain_Ambient_Sunset_Color,030,030,035
fallback=Weather_Rain_Ambient_Night_Color,016,017,020
fallback=Weather_Rain_Sun_Sunrise_Color,128,119,097
fallback=Weather_Rain_Sun_Day_Color,110,110,115
fallback=Weather_Rain_Sun_Sunset_Color,060,070,090
fallback=Weather_Rain_Sun_Night_Color,001,001,004
fallback=Weather_Rain_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Rain_Land_Fog_Day_Depth,.8
fallback=Weather_Rain_Land_Fog_Night_Depth,.8
fallback=Weather_Rain_Wind_Speed,.3
fallback=Weather_Rain_Cloud_Speed,2
fallback=Weather_Rain_Glare_View,0
fallback=Weather_Rain_Rain_Loop_Sound_ID,Rain
fallback=Weather_Rain_Using_Precip,1
fallback=Weather_Rain_Rain_Diameter,600
fallback=Weather_Rain_Rain_Height_Min,200
fallback=Weather_Rain_Rain_Height_Max,700
fallback=Weather_Rain_Rain_Threshold,0.6
fallback=Weather_Rain_Rain_Entrance_Speed,7
fallback=Weather_Rain_Ambient_Loop_Sound_ID,None
fallback=Weather_Rain_Max_Raindrops,2000
fallback=Weather_Overcast_Cloud_Texture,Tx_Sky_Overcast.tga
fallback=Weather_Overcast_Clouds_Maximum_Percent,1.0
fallback=Weather_Overcast_Transition_Delta,.015
fallback=Weather_Overcast_Sky_Sunrise_Color,197,190,180
fallback=Weather_Overcast_Sky_Day_Color,154,180,200
fallback=Weather_Overcast_Sky_Sunset_Color,050,043,046
fallback=Weather_Overcast_Sky_Night_Color,001,001,001
fallback=Weather_Overcast_Fog_Sunrise_Color,153,092,031
fallback=Weather_Overcast_Fog_Day_Color,174,201,218
fallback=Weather_Overcast_Fog_Sunset_Color,128,092,043
fallback=Weather_Overcast_Fog_Night_Color,021,026,036
fallback=Weather_Overcast_Ambient_Sunrise_Color,072,075,078
fallback=Weather_Overcast_Ambient_Day_Color,080,090,95
fallback=Weather_Overcast_Ambient_Sunset_Color,039,035,034
fallback=Weather_Overcast_Ambient_Night_Color,026,026,031
fallback=Weather_Overcast_Sun_Sunrise_Color,087,125,163
fallback=Weather_Overcast_Sun_Day_Color,107,144,153
fallback=Weather_Overcast_Sun_Sunset_Color,085,103,157
fallback=Weather_Overcast_Sun_Night_Color,003,003,003
fallback=Weather_Overcast_Sun_Disc_Sunset_Color,128,118,118
fallback=Weather_Overcast_Land_Fog_Day_Depth,.70
fallback=Weather_Overcast_Land_Fog_Night_Depth,.70
fallback=Weather_Overcast_Wind_Speed,.2
fallback=Weather_Overcast_Cloud_Speed,1.5
fallback=Weather_Overcast_Glare_View,.05
fallback=Weather_Overcast_Ambient_Loop_Sound_ID,None
fallback=Weather_Ashstorm_Cloud_Texture,Tx_Sky_Ashstorm.tga
fallback=Weather_Ashstorm_Clouds_Maximum_Percent,1.0
fallback=Weather_Ashstorm_Transition_Delta,.030
fallback=Weather_Ashstorm_Sky_Sunrise_Color,091,056,051
fallback=Weather_Ashstorm_Sky_Day_Color,126,104,082
fallback=Weather_Ashstorm_Sky_Sunset_Color,121,051,040
fallback=Weather_Ashstorm_Sky_Night_Color,020,021,022
fallback=Weather_Ashstorm_Fog_Sunrise_Color,087,053,049
fallback=Weather_Ashstorm_Fog_Day_Color,038,032,025
fallback=Weather_Ashstorm_Fog_Sunset_Color,101,052,039
fallback=Weather_Ashstorm_Fog_Night_Color,010,010,010
fallback=Weather_Ashstorm_Ambient_Sunrise_Color,026,020,018
fallback=Weather_Ashstorm_Ambient_Day_Color,130,120,120
fallback=Weather_Ashstorm_Ambient_Sunset_Color,074,010,009
fallback=Weather_Ashstorm_Ambient_Night_Color,035,013,014
fallback=Weather_Ashstorm_Sun_Sunrise_Color,184,091,071
fallback=Weather_Ashstorm_Sun_Day_Color,228,139,114
fallback=Weather_Ashstorm_Sun_Sunset_Color,185,086,057
fallback=Weather_Ashstorm_Sun_Night_Color,000,000,000
fallback=Weather_Ashstorm_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Ashstorm_Land_Fog_Day_Depth,1.1
fallback=Weather_Ashstorm_Land_Fog_Night_Depth,1.2
fallback=Weather_Ashstorm_Wind_Speed,.8
fallback=Weather_Ashstorm_Cloud_Speed,7
fallback=Weather_Ashstorm_Glare_View,0
fallback=Weather_Ashstorm_Ambient_Loop_Sound_ID,ashstorm
fallback=Weather_Ashstorm_Storm_Threshold,.70
fallback=Weather_Blight_Cloud_Texture,Tx_Sky_Blight.tga
fallback=Weather_Blight_Clouds_Maximum_Percent,1.0
fallback=Weather_Blight_Transition_Delta,.040
fallback=Weather_Blight_Sky_Sunrise_Color,002,000,000
fallback=Weather_Blight_Sky_Day_Color,019,018,018
fallback=Weather_Blight_Sky_Sunset_Color,004,000,000
fallback=Weather_Blight_Sky_Night_Color,003,000,000
fallback=Weather_Blight_Fog_Sunrise_Color,030,000,000
fallback=Weather_Blight_Fog_Day_Color,002,000,000
fallback=Weather_Blight_Fog_Sunset_Color,000,000,000
fallback=Weather_Blight_Fog_Night_Color,011,008,008
fallback=Weather_Blight_Ambient_Day_Color,255,240,200
fallback=Weather_Blight_Ambient_Sunset_Color,150,050,050
fallback=Weather_Blight_Ambient_Night_Color,030,010,010
fallback=Weather_Blight_Sun_Sunrise_Color,090,000,000
fallback=Weather_Blight_Sun_Day_Color,255,250,200
fallback=Weather_Blight_Sun_Sunset_Color,100,000,000
fallback=Weather_Blight_Sun_Night_Color,090,080,080
fallback=Weather_Blight_Sun_Disc_Sunset_Color,100,000,000
fallback=Weather_Blight_Land_Fog_Day_Depth,1.1
fallback=Weather_Blight_Land_Fog_Night_Depth,1.2
fallback=Weather_Blight_Wind_Speed,.9
fallback=Weather_Blight_Cloud_Speed,9
fallback=Weather_Blight_Glare_View,0
fallback=Weather_Blight_Ambient_Loop_Sound_ID,Blight
fallback=Weather_Blight_Storm_Threshold,.70
fallback=Weather_Blight_Disease_Chance,.10
fallback=Weather_Snow_Cloud_Texture,Tx_BM_Sky_Snow.tga
fallback=Weather_Snow_Clouds_Maximum_Percent,1.0
fallback=Weather_Snow_Transition_Delta,.015
fallback=Weather_Snow_Sky_Sunrise_Color,100,090,080
fallback=Weather_Snow_Sky_Day_Color,202,202,202
fallback=Weather_Snow_Sky_Sunset_Color,096,115,134
fallback=Weather_Snow_Sky_Night_Color,021,025,029
fallback=Weather_Snow_Fog_Sunrise_Color,106,091,091
fallback=Weather_Snow_Fog_Day_Color,155,155,155
fallback=Weather_Snow_Fog_Sunset_Color,115,096,091
fallback=Weather_Snow_Fog_Night_Color,031,035,039
fallback=Weather_Snow_Ambient_Sunrise_Color,092,084,084
fallback=Weather_Snow_Ambient_Day_Color,093,096,105
fallback=Weather_Snow_Ambient_Sunset_Color,070,079,087
fallback=Weather_Snow_Ambient_Night_Color,006,007,009
fallback=Weather_Snow_Sun_Sunrise_Color,141,109,109
fallback=Weather_Snow_Sun_Day_Color,163,169,183
fallback=Weather_Snow_Sun_Sunset_Color,101,121,141
fallback=Weather_Snow_Sun_Night_Color,055,066,077
fallback=Weather_Snow_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Snow_Land_Fog_Day_Depth,1.0
fallback=Weather_Snow_Land_Fog_Night_Depth,1.2
fallback=Weather_Snow_Wind_Speed,0
fallback=Weather_Snow_Cloud_Speed,1.5
fallback=Weather_Snow_Glare_View,0
fallback=Weather_Snow_Snow_Diameter,800
fallback=Weather_Snow_Snow_Height_Min,400
fallback=Weather_Snow_Snow_Height_Max,700
fallback=Weather_Snow_Snow_Entrance_Speed,6
fallback=Weather_Snow_Max_Snowflakes,750
fallback=Weather_Snow_Ambient_Loop_Sound_ID,None
fallback=Weather_Snow_Snow_Threshold,0.5
fallback=Weather_Blizzard_Cloud_Texture,Tx_BM_Sky_Blizzard.tga
fallback=Weather_Blizzard_Clouds_Maximum_Percent,1.0
fallback=Weather_Blizzard_Transition_Delta,.030
fallback=Weather_Blizzard_Sky_Sunrise_Color,091,099,106
fallback=Weather_Blizzard_Sky_Day_Color,121,133,145
fallback=Weather_Blizzard_Sky_Sunset_Color,108,115,121
fallback=Weather_Blizzard_Sky_Night_Color,027,029,031
fallback=Weather_Blizzard_Fog_Sunrise_Color,091,099,106
fallback=Weather_Blizzard_Fog_Day_Color,150,150,150
fallback=Weather_Blizzard_Fog_Sunset_Color,108,115,121
fallback=Weather_Blizzard_Fog_Night_Color,021,024,028
fallback=Weather_Blizzard_Ambient_Sunrise_Color,084,088,092
fallback=Weather_Blizzard_Ambient_Day_Color,093,096,105
fallback=Weather_Blizzard_Ambient_Sunset_Color,083,077,075
fallback=Weather_Blizzard_Ambient_Night_Color,006,007,009
fallback=Weather_Blizzard_Sun_Sunrise_Color,114,128,146
fallback=Weather_Blizzard_Sun_Day_Color,163,169,183
fallback=Weather_Blizzard_Sun_Sunset_Color,106,114,136
fallback=Weather_Blizzard_Sun_Night_Color,057,066,074
fallback=Weather_Blizzard_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Blizzard_Land_Fog_Day_Depth,0.8
fallback=Weather_Blizzard_Land_Fog_Night_Depth,1.0
fallback=Weather_Blizzard_Wind_Speed,.9
fallback=Weather_Blizzard_Cloud_Speed,7.5
fallback=Weather_Blizzard_Glare_View,0
fallback=Weather_Blizzard_Ambient_Loop_Sound_ID,BM Blizzard
fallback=Weather_Blizzard_Storm_Threshold,.50</code></pre>""",
            "extra_cfg_raw": """#
# Vurts Hi-res Skies and Weathers; put these
# after the last 'fallback=' line in your cfg.
#
fallback=Weather_Clear_Cloud_Texture,Tx_Sky_Clear.tga
fallback=Weather_Clear_Clouds_Maximum_Percent,1.0
fallback=Weather_Clear_Transition_Delta,.015
fallback=Weather_Clear_Sky_Sunrise_Color,100,140,205
fallback=Weather_Clear_Sky_Day_Color,115,156,220
fallback=Weather_Clear_Sky_Sunset_Color,196,141,170
fallback=Weather_Clear_Sky_Night_Color,000,000,010
fallback=Weather_Clear_Fog_Sunrise_Color,187,100,023
fallback=Weather_Clear_Fog_Day_Color,187,222,255
fallback=Weather_Clear_Fog_Sunset_Color,181,095,019
fallback=Weather_Clear_Fog_Night_Color,015,049,069
fallback=Weather_Clear_Ambient_Sunrise_Color,095,090,073
fallback=Weather_Clear_Ambient_Day_Color,184,178,117
fallback=Weather_Clear_Ambient_Sunset_Color,051,038,006
fallback=Weather_Clear_Ambient_Night_Color,030,030,050
fallback=Weather_Clear_Sun_Sunrise_Color,179,119,023
fallback=Weather_Clear_Sun_Day_Color,220,198,133
fallback=Weather_Clear_Sun_Sunset_Color,092,052,022
fallback=Weather_Clear_Sun_Night_Color,008,008,019
fallback=Weather_Clear_Sun_Disc_Sunset_Color,220,190,010
fallback=Weather_Clear_Land_Fog_Day_Depth,.34
fallback=Weather_Clear_Land_Fog_Night_Depth,.34
fallback=Weather_Clear_Wind_Speed,.1
fallback=Weather_Clear_Cloud_Speed,.50
fallback=Weather_Clear_Glare_View,1
fallback=Weather_Clear_Ambient_Loop_Sound_ID,None
fallback=Weather_Cloudy_Cloud_Texture,Tx_Sky_Cloudy.tga
fallback=Weather_Cloudy_Clouds_Maximum_Percent,1.0
fallback=Weather_Cloudy_Transition_Delta,.015
fallback=Weather_Cloudy_Sky_Sunrise_Color,049,078,120
fallback=Weather_Cloudy_Sky_Day_Color,059,088,130
fallback=Weather_Cloudy_Sky_Sunset_Color,102,073,089
fallback=Weather_Cloudy_Sky_Night_Color,005,005,009
fallback=Weather_Cloudy_Fog_Sunrise_Color,133,122,082
fallback=Weather_Cloudy_Fog_Day_Color,250,250,250
fallback=Weather_Cloudy_Fog_Sunset_Color,255,154,105
fallback=Weather_Cloudy_Fog_Night_Color,025,059,079
fallback=Weather_Cloudy_Ambient_Sunrise_Color,081,078,071
fallback=Weather_Cloudy_Ambient_Day_Color,245,255,255
fallback=Weather_Cloudy_Ambient_Sunset_Color,070,066,045
fallback=Weather_Cloudy_Ambient_Night_Color,023,026,029
fallback=Weather_Cloudy_Sun_Sunrise_Color,164,147,089
fallback=Weather_Cloudy_Sun_Day_Color,170,235,235
fallback=Weather_Cloudy_Sun_Sunset_Color,026,014,022
fallback=Weather_Cloudy_Sun_Night_Color,039,046,061
fallback=Weather_Cloudy_Sun_Disc_Sunset_Color,255,189,190
fallback=Weather_Cloudy_Land_Fog_Day_Depth,.72
fallback=Weather_Cloudy_Land_Fog_Night_Depth,.72
fallback=Weather_Cloudy_Wind_Speed,.2
fallback=Weather_Cloudy_Cloud_Speed,1
fallback=Weather_Cloudy_Glare_View,1
fallback=Weather_Cloudy_Ambient_Loop_Sound_ID,None
fallback=Weather_Foggy_Cloud_Texture,Tx_Sky_Foggy.tga
fallback=Weather_Foggy_Clouds_Maximum_Percent,1.0
fallback=Weather_Foggy_Transition_Delta,.015
fallback=Weather_Foggy_Sky_Sunrise_Color,150,150,150
fallback=Weather_Foggy_Sky_Day_Color,120,120,120
fallback=Weather_Foggy_Sky_Sunset_Color,030,030,030
fallback=Weather_Foggy_Sky_Night_Color,020,020,020
fallback=Weather_Foggy_Fog_Sunrise_Color,100,75,070
fallback=Weather_Foggy_Fog_Day_Color,190,190,190
fallback=Weather_Foggy_Fog_Sunset_Color,058,057,052
fallback=Weather_Foggy_Fog_Night_Color,023,023,033
fallback=Weather_Foggy_Ambient_Sunrise_Color,020,022,024
fallback=Weather_Foggy_Ambient_Day_Color,089,090,092
fallback=Weather_Foggy_Ambient_Sunset_Color,041,035,035
fallback=Weather_Foggy_Ambient_Night_Color,022,023,028
fallback=Weather_Foggy_Sun_Sunrise_Color,115,110,087
fallback=Weather_Foggy_Sun_Day_Color,111,131,141
fallback=Weather_Foggy_Sun_Sunset_Color,102,084,078
fallback=Weather_Foggy_Sun_Night_Color,008,006,006
fallback=Weather_Foggy_Sun_Disc_Sunset_Color,223,223,223
fallback=Weather_Foggy_Land_Fog_Day_Depth,1.0
fallback=Weather_Foggy_Land_Fog_Night_Depth,1.9
fallback=Weather_Foggy_Wind_Speed,0
fallback=Weather_Foggy_Cloud_Speed,.25
fallback=Weather_Foggy_Glare_View,0.15
fallback=Weather_Foggy_Ambient_Loop_Sound_ID,None
fallback=Weather_Thunderstorm_Cloud_Texture,Tx_Sky_Thunder.tga
fallback=Weather_Thunderstorm_Clouds_Maximum_Percent,0.66
fallback=Weather_Thunderstorm_Transition_Delta,.030
fallback=Weather_Thunderstorm_Sky_Sunrise_Color,024,024,024
fallback=Weather_Thunderstorm_Sky_Day_Color,016,016,017
fallback=Weather_Thunderstorm_Sky_Sunset_Color,022,021,021
fallback=Weather_Thunderstorm_Sky_Night_Color,011,011,014
fallback=Weather_Thunderstorm_Fog_Sunrise_Color,066,061,050
fallback=Weather_Thunderstorm_Fog_Day_Color,018,021,025
fallback=Weather_Thunderstorm_Fog_Sunset_Color,033,033,033
fallback=Weather_Thunderstorm_Fog_Night_Color,024,030,030
fallback=Weather_Thunderstorm_Ambient_Sunrise_Color,018,018,018
fallback=Weather_Thunderstorm_Ambient_Day_Color,050,050,060
fallback=Weather_Thunderstorm_Ambient_Sunset_Color,020,020,020
fallback=Weather_Thunderstorm_Ambient_Night_Color,010,010,014
fallback=Weather_Thunderstorm_Sun_Sunrise_Color,091,099,122
fallback=Weather_Thunderstorm_Sun_Day_Color,045,045,80
fallback=Weather_Thunderstorm_Sun_Sunset_Color,016,025,050
fallback=Weather_Thunderstorm_Sun_Night_Color,001,001,015
fallback=Weather_Thunderstorm_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Thunderstorm_Land_Fog_Day_Depth,1
fallback=Weather_Thunderstorm_Land_Fog_Night_Depth,1.15
fallback=Weather_Thunderstorm_Wind_Speed,.5
fallback=Weather_Thunderstorm_Cloud_Speed,3
fallback=Weather_Thunderstorm_Glare_View,0
fallback=Weather_Thunderstorm_Rain_Loop_Sound_ID,rain heavy
fallback=Weather_Thunderstorm_Using_Precip,1
fallback=Weather_Thunderstorm_Rain_Diameter,600
fallback=Weather_Thunderstorm_Rain_Height_Min,200
fallback=Weather_Thunderstorm_Rain_Height_Max,700
fallback=Weather_Thunderstorm_Rain_Threshold,0.6
fallback=Weather_Thunderstorm_Max_Raindrops,650
fallback=Weather_Thunderstorm_Rain_Entrance_Speed,5
fallback=Weather_Thunderstorm_Ambient_Loop_Sound_ID,None
fallback=Weather_Thunderstorm_Flash_Decrement,4
fallback=Weather_Rain_Cloud_Texture,Tx_Sky_Rainy.tga
fallback=Weather_Rain_Clouds_Maximum_Percent,0.66
fallback=Weather_Rain_Transition_Delta,.015
fallback=Weather_Rain_Sky_Sunrise_Color,071,074,075
fallback=Weather_Rain_Sky_Day_Color,083,083,083
fallback=Weather_Rain_Sky_Sunset_Color,033,033,033
fallback=Weather_Rain_Sky_Night_Color,010,010,013
fallback=Weather_Rain_Fog_Sunrise_Color,077,077,077
fallback=Weather_Rain_Fog_Day_Color,060,060,060
fallback=Weather_Rain_Fog_Sunset_Color,098,062,013
fallback=Weather_Rain_Fog_Night_Color,015,015,015
fallback=Weather_Rain_Ambient_Sunrise_Color,037,044,051
fallback=Weather_Rain_Ambient_Day_Color,040,040,052
fallback=Weather_Rain_Ambient_Sunset_Color,030,030,035
fallback=Weather_Rain_Ambient_Night_Color,016,017,020
fallback=Weather_Rain_Sun_Sunrise_Color,128,119,097
fallback=Weather_Rain_Sun_Day_Color,110,110,115
fallback=Weather_Rain_Sun_Sunset_Color,060,070,090
fallback=Weather_Rain_Sun_Night_Color,001,001,004
fallback=Weather_Rain_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Rain_Land_Fog_Day_Depth,.8
fallback=Weather_Rain_Land_Fog_Night_Depth,.8
fallback=Weather_Rain_Wind_Speed,.3
fallback=Weather_Rain_Cloud_Speed,2
fallback=Weather_Rain_Glare_View,0
fallback=Weather_Rain_Rain_Loop_Sound_ID,Rain
fallback=Weather_Rain_Using_Precip,1
fallback=Weather_Rain_Rain_Diameter,600
fallback=Weather_Rain_Rain_Height_Min,200
fallback=Weather_Rain_Rain_Height_Max,700
fallback=Weather_Rain_Rain_Threshold,0.6
fallback=Weather_Rain_Rain_Entrance_Speed,7
fallback=Weather_Rain_Ambient_Loop_Sound_ID,None
fallback=Weather_Rain_Max_Raindrops,2000
fallback=Weather_Overcast_Cloud_Texture,Tx_Sky_Overcast.tga
fallback=Weather_Overcast_Clouds_Maximum_Percent,1.0
fallback=Weather_Overcast_Transition_Delta,.015
fallback=Weather_Overcast_Sky_Sunrise_Color,197,190,180
fallback=Weather_Overcast_Sky_Day_Color,154,180,200
fallback=Weather_Overcast_Sky_Sunset_Color,050,043,046
fallback=Weather_Overcast_Sky_Night_Color,001,001,001
fallback=Weather_Overcast_Fog_Sunrise_Color,153,092,031
fallback=Weather_Overcast_Fog_Day_Color,174,201,218
fallback=Weather_Overcast_Fog_Sunset_Color,128,092,043
fallback=Weather_Overcast_Fog_Night_Color,021,026,036
fallback=Weather_Overcast_Ambient_Sunrise_Color,072,075,078
fallback=Weather_Overcast_Ambient_Day_Color,080,090,95
fallback=Weather_Overcast_Ambient_Sunset_Color,039,035,034
fallback=Weather_Overcast_Ambient_Night_Color,026,026,031
fallback=Weather_Overcast_Sun_Sunrise_Color,087,125,163
fallback=Weather_Overcast_Sun_Day_Color,107,144,153
fallback=Weather_Overcast_Sun_Sunset_Color,085,103,157
fallback=Weather_Overcast_Sun_Night_Color,003,003,003
fallback=Weather_Overcast_Sun_Disc_Sunset_Color,128,118,118
fallback=Weather_Overcast_Land_Fog_Day_Depth,.70
fallback=Weather_Overcast_Land_Fog_Night_Depth,.70
fallback=Weather_Overcast_Wind_Speed,.2
fallback=Weather_Overcast_Cloud_Speed,1.5
fallback=Weather_Overcast_Glare_View,.05
fallback=Weather_Overcast_Ambient_Loop_Sound_ID,None
fallback=Weather_Ashstorm_Cloud_Texture,Tx_Sky_Ashstorm.tga
fallback=Weather_Ashstorm_Clouds_Maximum_Percent,1.0
fallback=Weather_Ashstorm_Transition_Delta,.030
fallback=Weather_Ashstorm_Sky_Sunrise_Color,091,056,051
fallback=Weather_Ashstorm_Sky_Day_Color,126,104,082
fallback=Weather_Ashstorm_Sky_Sunset_Color,121,051,040
fallback=Weather_Ashstorm_Sky_Night_Color,020,021,022
fallback=Weather_Ashstorm_Fog_Sunrise_Color,087,053,049
fallback=Weather_Ashstorm_Fog_Day_Color,038,032,025
fallback=Weather_Ashstorm_Fog_Sunset_Color,101,052,039
fallback=Weather_Ashstorm_Fog_Night_Color,010,010,010
fallback=Weather_Ashstorm_Ambient_Sunrise_Color,026,020,018
fallback=Weather_Ashstorm_Ambient_Day_Color,130,120,120
fallback=Weather_Ashstorm_Ambient_Sunset_Color,074,010,009
fallback=Weather_Ashstorm_Ambient_Night_Color,035,013,014
fallback=Weather_Ashstorm_Sun_Sunrise_Color,184,091,071
fallback=Weather_Ashstorm_Sun_Day_Color,228,139,114
fallback=Weather_Ashstorm_Sun_Sunset_Color,185,086,057
fallback=Weather_Ashstorm_Sun_Night_Color,000,000,000
fallback=Weather_Ashstorm_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Ashstorm_Land_Fog_Day_Depth,1.1
fallback=Weather_Ashstorm_Land_Fog_Night_Depth,1.2
fallback=Weather_Ashstorm_Wind_Speed,.8
fallback=Weather_Ashstorm_Cloud_Speed,7
fallback=Weather_Ashstorm_Glare_View,0
fallback=Weather_Ashstorm_Ambient_Loop_Sound_ID,ashstorm
fallback=Weather_Ashstorm_Storm_Threshold,.70
fallback=Weather_Blight_Cloud_Texture,Tx_Sky_Blight.tga
fallback=Weather_Blight_Clouds_Maximum_Percent,1.0
fallback=Weather_Blight_Transition_Delta,.040
fallback=Weather_Blight_Sky_Sunrise_Color,002,000,000
fallback=Weather_Blight_Sky_Day_Color,019,018,018
fallback=Weather_Blight_Sky_Sunset_Color,004,000,000
fallback=Weather_Blight_Sky_Night_Color,003,000,000
fallback=Weather_Blight_Fog_Sunrise_Color,030,000,000
fallback=Weather_Blight_Fog_Day_Color,002,000,000
fallback=Weather_Blight_Fog_Sunset_Color,000,000,000
fallback=Weather_Blight_Fog_Night_Color,011,008,008
fallback=Weather_Blight_Ambient_Day_Color,255,240,200
fallback=Weather_Blight_Ambient_Sunset_Color,150,050,050
fallback=Weather_Blight_Ambient_Night_Color,030,010,010
fallback=Weather_Blight_Sun_Sunrise_Color,090,000,000
fallback=Weather_Blight_Sun_Day_Color,255,250,200
fallback=Weather_Blight_Sun_Sunset_Color,100,000,000
fallback=Weather_Blight_Sun_Night_Color,090,080,080
fallback=Weather_Blight_Sun_Disc_Sunset_Color,100,000,000
fallback=Weather_Blight_Land_Fog_Day_Depth,1.1
fallback=Weather_Blight_Land_Fog_Night_Depth,1.2
fallback=Weather_Blight_Wind_Speed,.9
fallback=Weather_Blight_Cloud_Speed,9
fallback=Weather_Blight_Glare_View,0
fallback=Weather_Blight_Ambient_Loop_Sound_ID,Blight
fallback=Weather_Blight_Storm_Threshold,.70
fallback=Weather_Blight_Disease_Chance,.10
fallback=Weather_Snow_Cloud_Texture,Tx_BM_Sky_Snow.tga
fallback=Weather_Snow_Clouds_Maximum_Percent,1.0
fallback=Weather_Snow_Transition_Delta,.015
fallback=Weather_Snow_Sky_Sunrise_Color,100,090,080
fallback=Weather_Snow_Sky_Day_Color,202,202,202
fallback=Weather_Snow_Sky_Sunset_Color,096,115,134
fallback=Weather_Snow_Sky_Night_Color,021,025,029
fallback=Weather_Snow_Fog_Sunrise_Color,106,091,091
fallback=Weather_Snow_Fog_Day_Color,155,155,155
fallback=Weather_Snow_Fog_Sunset_Color,115,096,091
fallback=Weather_Snow_Fog_Night_Color,031,035,039
fallback=Weather_Snow_Ambient_Sunrise_Color,092,084,084
fallback=Weather_Snow_Ambient_Day_Color,093,096,105
fallback=Weather_Snow_Ambient_Sunset_Color,070,079,087
fallback=Weather_Snow_Ambient_Night_Color,006,007,009
fallback=Weather_Snow_Sun_Sunrise_Color,141,109,109
fallback=Weather_Snow_Sun_Day_Color,163,169,183
fallback=Weather_Snow_Sun_Sunset_Color,101,121,141
fallback=Weather_Snow_Sun_Night_Color,055,066,077
fallback=Weather_Snow_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Snow_Land_Fog_Day_Depth,1.0
fallback=Weather_Snow_Land_Fog_Night_Depth,1.2
fallback=Weather_Snow_Wind_Speed,0
fallback=Weather_Snow_Cloud_Speed,1.5
fallback=Weather_Snow_Glare_View,0
fallback=Weather_Snow_Snow_Diameter,800
fallback=Weather_Snow_Snow_Height_Min,400
fallback=Weather_Snow_Snow_Height_Max,700
fallback=Weather_Snow_Snow_Entrance_Speed,6
fallback=Weather_Snow_Max_Snowflakes,750
fallback=Weather_Snow_Ambient_Loop_Sound_ID,None
fallback=Weather_Snow_Snow_Threshold,0.5
fallback=Weather_Blizzard_Cloud_Texture,Tx_BM_Sky_Blizzard.tga
fallback=Weather_Blizzard_Clouds_Maximum_Percent,1.0
fallback=Weather_Blizzard_Transition_Delta,.030
fallback=Weather_Blizzard_Sky_Sunrise_Color,091,099,106
fallback=Weather_Blizzard_Sky_Day_Color,121,133,145
fallback=Weather_Blizzard_Sky_Sunset_Color,108,115,121
fallback=Weather_Blizzard_Sky_Night_Color,027,029,031
fallback=Weather_Blizzard_Fog_Sunrise_Color,091,099,106
fallback=Weather_Blizzard_Fog_Day_Color,150,150,150
fallback=Weather_Blizzard_Fog_Sunset_Color,108,115,121
fallback=Weather_Blizzard_Fog_Night_Color,021,024,028
fallback=Weather_Blizzard_Ambient_Sunrise_Color,084,088,092
fallback=Weather_Blizzard_Ambient_Day_Color,093,096,105
fallback=Weather_Blizzard_Ambient_Sunset_Color,083,077,075
fallback=Weather_Blizzard_Ambient_Night_Color,006,007,009
fallback=Weather_Blizzard_Sun_Sunrise_Color,114,128,146
fallback=Weather_Blizzard_Sun_Day_Color,163,169,183
fallback=Weather_Blizzard_Sun_Sunset_Color,106,114,136
fallback=Weather_Blizzard_Sun_Night_Color,057,066,074
fallback=Weather_Blizzard_Sun_Disc_Sunset_Color,128,128,128
fallback=Weather_Blizzard_Land_Fog_Day_Depth,0.8
fallback=Weather_Blizzard_Land_Fog_Night_Depth,1.0
fallback=Weather_Blizzard_Wind_Speed,.9
fallback=Weather_Blizzard_Cloud_Speed,7.5
fallback=Weather_Blizzard_Glare_View,0
fallback=Weather_Blizzard_Ambient_Loop_Sound_ID,BM Blizzard
fallback=Weather_Blizzard_Storm_Threshold,.50""",
            "name": "Vurts Hi-res Skies and Weathers",
            "status": "live",
            "url": nexus_mod("28552"),
            "usage_notes": """This requires an old version of StarWarsGal's Skies 3, which is superceded by <a href='/mods/skies-iv/'>Skies .IV</a> - use that instead.""",
        },
        {
            "name": "New Starfields",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": skies,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2023-07-12 11:05:00 -0500",
            "description": "This is just a simple, high-definition replacer for the clear night sky texture. There are 8 different versions included in the download. Pick the one you like best.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43246"),
            "usage_notes": """The mod comes with a large number of choices for sky texture and opacity. The folder path shown is just a suggestion, you're encouraged to try several of them out and find the one that you like best.""",
        },
        {
            "name": "Meteors",
            "author": "Shanjaq",
            "category": skies,
            "date_added": "2021-08-29 11:40:19 -0500",
            "description": """<p>Every night a random max time interval is chosen, then all night random time intervals below the max are chosen to separate meteor launches.  They appear at a random distance from 5 random directions(Above, N, E, S, W) and travel on random paths.  Occasionally there will be Meteor Showers with unusual density from a specific direction.</p>

<p>console variables are:</p>

<p><code>MeteorChance</code> (set once every night, determines max time interval.  setting to 1 will have meteors firing at least once every second)</p>

<p><code>ShowerDir</code> (set to random every night, unless there's a meteor shower in progress.  max is 4)</p>

<p>if you want to see a meteor immediately, try the following console command at night: <code>placeatpc Meteor 1,0,0</code></p>""",
            "dl_url": "/files/ID4151_2_24_Meteors_1.2_20040609.zip",
            "is_active": False,
            "status": "live",
            "url": modhistory_archive_link("1-1407"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
