from momw.helpers import generate_mod, nexus_mod, nexus_user


shaders = "Shaders"

# Names of tags that are used in this file.
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_reshade = "Reshade"


def init():
    _mods = [
        {
            "alt_to": [
                "True Lights and Darkness",
                "True Lights and Darkness - Necro Edit",
            ],
            "author": "Vtastek",
            "category": shaders,
            "compat": "not working",
            "date_added": "2021-01-18 14:34:44 -0500",
            "description": """A highly advanced shader replacement that also revamps all lighting in the game.""",
            #             "extra_cfg": """<p>These values need to be added to <code>openmw.cfg</code>:</p>
            # <pre><code>fallback=LightAttenuation_UseConstant,0
            # fallback=LightAttenuation_ConstantValue,0.0
            # fallback=LightAttenuation_UseLinear,1
            # fallback=LightAttenuation_LinearMethod,1
            # fallback=LightAttenuation_LinearValue,3.0
            # fallback=LightAttenuation_LinearRadiusMult,1.0
            # fallback=LightAttenuation_UseQuadratic,0
            # fallback=LightAttenuation_QuadraticMethod,2
            # fallback=LightAttenuation_QuadraticValue,16.0
            # fallback=LightAttenuation_QuadraticRadiusMult,1.0
            # fallback=LightAttenuation_OutQuadInLin,0
            # fallback=Weather_Foggy_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Foggy_Sun_Day_Color,001,001,001
            # fallback=Weather_Foggy_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Foggy_Sun_Night_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Day_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Night_Color,001,001,001
            # fallback=Weather_Rain_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Rain_Sun_Day_Color,001,001,001
            # fallback=Weather_Rain_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Rain_Sun_Night_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Day_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Night_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Day_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Night_Color,001,001,001
            # fallback=Weather_Blight_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Blight_Sun_Day_Color,001,001,001
            # fallback=Weather_Blight_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Blight_Sun_Night_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Day_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Night_Color,001,001,001</code></pre>
            # <p>You can paste these just after the last <code>fallback=</code> line in your <code>openmw.cfg</code> file.</p>
            # <p>These values need to be added to <code>settings.cfg</code> under the <code>[Shaders]</code> section:</p>
            # <pre><code>force per pixel lighting = true
            # force shaders = true
            # clamp lighting = false
            # auto use object normal maps = true
            # auto use object specular maps = false
            # auto use terrain normal maps = true
            # auto use terrain specular maps = false
            # radial fog = true</code></pre>""",
            #             "extra_cfg_raw": """#
            # # Vtastek's Shaders
            # # (This goes into openmw.cfg)
            # #
            # fallback=LightAttenuation_UseConstant,0
            # fallback=LightAttenuation_ConstantValue,0.0
            # fallback=LightAttenuation_UseLinear,1
            # fallback=LightAttenuation_LinearMethod,1
            # fallback=LightAttenuation_LinearValue,3.0
            # fallback=LightAttenuation_LinearRadiusMult,1.0
            # fallback=LightAttenuation_UseQuadratic,0
            # fallback=LightAttenuation_QuadraticMethod,2
            # fallback=LightAttenuation_QuadraticValue,16.0
            # fallback=LightAttenuation_QuadraticRadiusMult,1.0
            # fallback=LightAttenuation_OutQuadInLin,0
            # fallback=Weather_Foggy_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Foggy_Sun_Day_Color,001,001,001
            # fallback=Weather_Foggy_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Foggy_Sun_Night_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Day_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Thunderstorm_Sun_Night_Color,001,001,001
            # fallback=Weather_Rain_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Rain_Sun_Day_Color,001,001,001
            # fallback=Weather_Rain_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Rain_Sun_Night_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Day_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Overcast_Sun_Night_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Day_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Ashstorm_Sun_Night_Color,001,001,001
            # fallback=Weather_Blight_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Blight_Sun_Day_Color,001,001,001
            # fallback=Weather_Blight_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Blight_Sun_Night_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Sunrise_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Day_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Sunset_Color,001,001,001
            # fallback=Weather_Blizzard_Sun_Night_Color,001,001,001
            # # NOTE: If you're using Skies .IV, then you need these
            # # after the Vtastek values above:
            # fallback=Weather_Clear_Cloud_Speed,1.25
            # fallback=Weather_Cloudy_Cloud_Speed,2
            # fallback=Weather_Foggy_Cloud_Speed,1.25
            # fallback=Weather_Thunderstorm_Cloud_Speed,3
            # fallback=Weather_Rain_Cloud_Speed,2
            # fallback=Weather_Overcast_Cloud_Speed,1.5
            # fallback=Weather_Ashstorm_Cloud_Speed,7
            # fallback=Weather_Blight_Cloud_Speed,9
            # fallback=Weather_Snow_Cloud_Speed,1.5
            # fallback=Weather_Blizzard_Cloud_Speed,7.5
            # # Vtastek's Shaders
            # # (These go into settings.cfg under [Shaders])
            # #
            # force per pixel lighting = true
            # force shaders = true
            # clamp lighting = false
            # auto use object normal maps = true
            # auto use object specular maps = false
            # auto use terrain normal maps = true
            # auto use terrain specular maps = false
            # radial fog = true""",
            "is_active": False,
            "name": "Vtastek's Light Shaders",
            "status": "live",
            "tags": [tag_high_res, tag_manual_edit_needed],
            # "url": "https://cdn.discordapp.com/attachments/381217735306248192/734530962024562720/openmwlightingbeta04.7z",
            "usage_notes": """<span class="bold">As of 2021-05, these shaders are not compatible with the lates 0.47.0 unreleased builds of OpenMW!</span>""",
            #             "usage_notes": """<p>Be sure to also download <a href="/files/vtastek-lighting-fix.zip">this file</a> for an updated file that works with the latest unreleased builds. If you're not following a mod list, you may also want to check out <a href="/mods/vtasteks-water-shaders/">Vtastek's Water Shaders</a> for enhanced water, including waves!</p>
            # <p>If you're also using <a href="/mods/skies-iv/">Skies .IV</a> (mod list followers: this includes you), you will want to use that mod's fallback values for weather:</p>
            # <pre><code>fallback=Weather_Clear_Cloud_Speed,0.25
            # fallback=Weather_Cloudy_Cloud_Speed,0.4
            # fallback=Weather_Foggy_Cloud_Speed,0.25
            # fallback=Weather_Thunderstorm_Cloud_Speed,0.6
            # fallback=Weather_Rain_Cloud_Speed,0.4
            # fallback=Weather_Overcast_Cloud_Speed,0.3
            # fallback=Weather_Ashstorm_Cloud_Speed,1.4
            # fallback=Weather_Blight_Cloud_Speed,1.8
            # fallback=Weather_Snow_Cloud_Speed,0.3
            # fallback=Weather_Blizzard_Cloud_Speed,1.5</code></pre>
            # <p>Alternatively, you can not use Skies .IV, but Vtastek's given values will make the sky textures move too fast if you don't use the Skies-provided fallbacks.</p>
            # <p>The shaders and related lighting changes require patching all cells in the game, you can find plugins for all lists on this site <a href="/files/momw-vtastek-lightfixes-2021-04.zip">here</a> (<a href="/files/momw-vtastek-lightfixes-2021-04.zip.sha256sum.txt">sha256sum</a>).  If you use any extra mods, you will want to generate your own patch.</p>
            # <p>Please see <a href="/tips/custom-shaders">this page</a> for a detailed guide on generating your own LightFixes plugin for these shaders.</p>""",
        },
        {
            "author": "Vtastek",
            "category": shaders,
            "date_added": "2021-04-09 13:24:33 -0500",
            "date_updated": "2021-08-05 17:34:03 -0500",
            "description": """<span class="bold">NOTE: A 0.47 build is required to use this, it is not compatible with 0.48 unreleased builds!</span>  An advanced water shader replacement that provides waves and other effects. Screenshots: <a href="/images/vtasteks-wavey-water-shader.png">one</a>, <a href="/images/vtasteks-wavey-water-shader2.png">two</a>, <a href="/images/vtasteks-wavey-water-shader3.png">three</a>, <a href="/images/vtasteks-wavey-water-shader4.png">four</a>, <a href="/images/vtasteks-wavey-water-shader5.png">five</a>.""",
            "is_active": False,
            "name": "Vtastek's Water Shaders",
            "status": "live",
            "tags": [tag_high_res, tag_manual_edit_needed],
            "url": "https://cdn.discordapp.com/attachments/814439771161886791/828348695300407296/watershaders0404.7z",
            "usage_notes": """<p>Please see <a href="/tips/custom-shaders/#installing">this page</a> for a guide on installing custom shaders.</p>""",
        },
        {
            "name": "Vanilla Inspired Water for OpenMW",
            "alt_to": ["Vtastek's Water Shaders"],
            "author": "DassiD - Vtastek - Left4No - PoodleSandwich",
            "category": shaders,
            "date_added": "2021-04-13 17:12:06 -0500",
            "description": """A more vanilla-looking water shader for OpenMW.""",
            "status": "live",
            "tags": [tag_manual_edit_needed],
            "url": nexus_mod("49391"),
            "usage_notes": """Please check the mod's description for installation instructions.""",
        },
        {
            "name": "Royale Reshade Demanufactured - OpenMW Preset",
            "author": nexus_user("81751823", "Demanufacturer87"),
            "category": shaders,
            "date_added": "2023-05-20 11:01:00 -0500",
            "description": """This is reshade preset modified by Demanufacturer87 for OpenMW engine. It was created originaly by <a href="https://www.nexusmods.com/elderscrollsonline/users/2555117">sneezes</a> for Elder Scrolls Online.""",
            "status": "live",
            "tags": [tag_reshade],
            "url": nexus_mod("52873"),
            "usage_notes": """Refer to the mod description for installation details.""",
        },
    ]

    for m in _mods:
        generate_mod(**m)
