from momw.helpers import generate_mod, nexus_mod, nexus_user


audio = "Audio"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_dev_build_only = "Dev Build Only"
tag_openmw_lua = "OpenMW Lua"


def init():
    # # print("START: Audio ... ", end="")

    _mods = [
        {
            "author": "JEKA and TJ",
            "category": audio,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-05-09 10:59:26 -0500",
            "description": 'This plugin changes default sound effects of Elder Scrolls III. All changes are divided into several groups - environment, items, doors, creatures, magic. This is not a simple "sound replacer". While working on "Better Sounds", we wanted to bring new life into TES III, to make it more atmospheric.',
            "name": "Better Sounds",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("9967"),
            "usage_notes": """<p>Do not use either plugin from this mod, instead use <a href='/mods/better-sounds-11-patch2/'>the patched plugin</a>.</p>

<p><span class="bold">NOTE TO NCGD USERS:</span> If you are following a list, then the list order will have you covered.  If you aren't following a list, please be sure to also use <a href="/mods/mbsp-better-sounds-ncgdmw-class-spec-fixed-again-r/">MBSP - Better Sounds - ncgdMW - Class-Spec - Fixed again Replacement Patch</a> (it is already a part of any mod list that uses this mod), which fixes this problem:  This plugin conflicts with <a href="/mods/natural-character-growth-and-decay-mw/">Natural Character Growth and Decay - MW</a> and <a href="/mods/magicka-based-skill-progression-ncgdmw-compatility/">Magicka Based Skill Progression - ncgdMW Compatility Version</a> such that your character\'s Destruction skill will not level up.</p>""",
        },
        {
            "name": "Voice Overhaul",
            "author": nexus_user("2250219", "Phoenix Rime"),
            "category": audio,
            "date_added": "2023-10-29 16:24:00 -0500",
            "description": """New voice line system made from scratch with great compatibility. Restored many unused audio files, increased the variety of voice lines. Lots of bug fixes and improvements.""",
            "status": "live",
            "url": nexus_mod("51215"),
        },
        {
            "name": "Quest Voice Greetings",
            "author": nexus_user("40926435", "Von Djangos"),
            "category": audio,
            "date_added": "2023-10-31 00:37:00 -0500",
            "description": """ElevenAI generated voices for quest-giving NPCs in Morrowind. Currently adds over 1400 original lines of voiced dialogue using the original actors voices.""",
            "status": "live",
            "url": nexus_mod("52273"),
        },
        {
            "name": "MAO Spell Sounds",
            "author": nexus_user("49943436", "Publicola") + " and PeterBitt",
            "category": audio,
            "date_added": "2023-10-31 00:44:00 -0500",
            "description": """This mod provides sound effects for each of the schools of magic as well as enchantments. These specific 'spell effect' sounds come from Morrowind Acoustic Overhaul by PeterBitt.""",
            "status": "live",
            "url": nexus_mod("50486"),
        },
        {
            "name": "Dynamic Music (OpenMW Only)",
            "author": nexus_user("39743235", "eMOElein"),
            "category": audio,
            "date_added": "2023-10-31 14:58:00 -0500",
            "description": "A configurable framework that provides support for situation specific custom ambient music.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53568"),
        },
        {
            "name": "Tamriel Rebuilt - Original Soundtrack",
            "author": nexus_user("6304834", "Rytelier") + " and ASKII",
            "category": audio,
            "date_added": "2023-11-06 12:58:00 -0500",
            "description": "Adds whole new soundtrack for Tamriel Rebuilt areas, with region based music, dungeons, cities and other. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("47254"),
        },
        {
            "name": "MUSE Music Expansion - Sixth House",
            "author": nexus_user("1756187", "Scipio219"),
            "category": audio,
            "date_added": "2023-11-06 13:07:00 -0500",
            "description": "A 6th House music expansion of original tracks for the Morrowind MUSE mod, created using modern production techniques. Featuring 5 new exploration tracks, 4 new combat tracks, and 1 boss fight track. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("51082"),
        },
        {
            "name": "MUSE Music Expansion - Daedric",
            "author": nexus_user("1756187", "Scipio219"),
            "category": audio,
            "date_added": "2023-11-06 13:18:00 -0500",
            "description": "A Daedric themed music expansion of original tracks by be created for the Morrowind MUSE mod. Featuring 5 new exploration tracks and 4 new combat tracks. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("51993"),
        },
        {
            "name": "MUSE Music Expansion - Dwemer",
            "author": nexus_user("1756187", "Scipio219"),
            "category": audio,
            "date_added": "2023-11-06 13:23:00 -0500",
            "description": "A Dwemer music expansion of original tracks by be created for the Morrowind MUSE mod. Featuring 5 new exploration tracks and 4 new combat tracks. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("51169"),
        },
        {
            "name": "MUSE Music Expansion - Empire",
            "author": nexus_user("1756187", "Scipio219"),
            "category": audio,
            "date_added": "2023-11-06 13:29:00 -0500",
            "description": "An Imperial music expansion of original tracks by me created for the Morrowind MUSE mod. Featuring 5 new exploration tracks and 3 new combat tracks. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52814"),
        },
        {
            "name": "MUSE Music Expansion - Tomb",
            "author": nexus_user("1756187", "Scipio219"),
            "category": audio,
            "date_added": "2023-11-06 13:35:00 -0500",
            "description": "An undead themed music expansion of original tracks by be created for the Morrowind MUSE mod. Featuring 5 new exploration tracks and 4 new combat tracks. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("51407"),
        },
        {
            "name": "Vindsvept Solstheim for Dynamic Music (OpenMW)",
            "author": nexus_user("29652720", "adventurer66")
            + ", eMOElein, and Vindsvept",
            "category": audio,
            "date_added": "2023-11-06 13:43:00 -0500",
            "description": "Adds Vindsvept's beautiful Celtic music to Solstheim only. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53597"),
        },
        {
            "name": "Songbook of the North - A music mod for Skyrim Home of the Nords",
            "author": nexus_user("51411351", "imsobadatnicknames"),
            "category": audio,
            "date_added": "2023-11-06 13:52:00 -0500",
            "description": "Songbook of the North is a MUSE plugin that adds 5 original tracks which will be heard exclusively in the territories added by SHotN. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("46936"),
        },
        {
            "name": "Songbook of the north - Dynamic Music",
            "author": nexus_user("1116176", "superliuk") + " and eMOElein",
            "category": audio,
            "date_added": "2023-11-06 13:51:00 -0500",
            "description": "This Dynamic Music script allows you to listen to the music from the 'songbook of the north' mod in Skyrim - Home of the Nords. Requires <a href='/mods/dynamic-music-openmw-only/'>Dynamic Music (OpenMW Only)</a>.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53635"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # # print("Done!")
