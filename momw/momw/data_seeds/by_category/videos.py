from momw.helpers import generate_mod, nexus_mod, nexus_user


videos = "Videos"


# Names of tags that are used in this file.
tag_high_res = "High Res"


def init():
    # print("START: {} ... ".format(videos), end="")

    _mods = [
        {
            "name": "HD Intro Cinematic - English",
            "author": nexus_user("187943", "Chesko"),
            "category": videos,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "An HD recreation of the original Intro Cinematic.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("39329"),
            "usage_notes": """This includes a file named <code>mw_intro_16x9_en.bik</code> which needs to be renamed to <code>mw_intro.bik</code> before it can be used.  The data path including the file should be like this: <code>HDIntroCinematicEnglish/Video/mw_intro.bik</code>.""",
        },
        {
            "name": "Subtle Animated Menu Background Assemble",
            "author": nexus_user("4655319", "Vegetto"),
            "category": videos,
            "date_added": "2021-09-01 18:34:36 -0500",
            "description": "A subtle animated menu background made of an assemble of Morrowind, ESO online, Legends and Morrowind arts in HD.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("50037"),
        },
        {
            "alt_to": ["Subtle Animated Menu Background Assemble"],
            "name": "Morrowind Definitive Menu Animation",
            "author": "Duo Dinamico",
            "category": videos,
            "date_added": "2021-09-01 18:45:52 -0500",
            "description": "The definitive animated menu animation for your Morrowind in 1080 HD.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("50080"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
