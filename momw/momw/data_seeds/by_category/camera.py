from momw.helpers import generate_mod


camera = "Camera"

tag_openmw_lua = "OpenMW Lua"
tag_gitlab_mods = "GitLab mods"


def init():
    _mods = [
        {
            "author": "johnnyhostile",
            "category": camera,
            "date_added": "2022-03-04 21:00:00 -0500",
            "description": """A camera mod for OpenMW that auto-swaps between first and third person cameras based on if combat or magic are readied.""",
            "is_featured": True,
            "name": "Action Camera Swap",
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/action-camera-swap/",
            "usage_notes": """<span class="bold">OpenMW 0.48 or newer is required!</span>""",
        },
        {
            "name": "Marksman's Eye",
            "author": "johnnyhostile",
            "category": camera,
            "date_added": "2022-11-13 12:57:00 -0500",
            "description": """Find a powerful artifact that magically enhances the player's vision with a zoom effect while aiming a bow or crossbow. Requires OpenMW-Lua. Based on <a href="https://gitlab.com/ptmikheev/openmw-lua-examples#advanced-camera">Advanced Camera / Bow Aiming by ptmikheev</a>.""",
            "is_featured": True,
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/marksmans-eye/",
            "usage_notes": """<span class="bold">OpenMW 0.48 or newer is required!</span>""",
        },
        {
            "name": "CameraHIM",
            "author": "urm",
            "category": camera,
            "date_added": "2022-11-13 13:27:00 -0500",
            "description": """Smooth camera for recording showcase and gameplay videos. Use its various features via the script settings menu (ESC >> Options >> Scripts).""",
            "is_featured": True,
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://gitlab.com/urm-openmw-mods/camerahim",
            "usage_notes": """<span class="bold">OpenMW 0.48 or newer is required!</span> Click <a href="https://gitlab.com/urm-openmw-mods/camerahim/-/archive/master/camerahim-master.zip">here</a> to download the mod as a zip file.""",
        },
        {
            "name": "OpenNevermind",
            "author": "<a href='https://gitlab.com/ptmikheev'>Petr Mikheev</a>",
            "category": camera,
            "date_added": "2022-11-13 16:38:00 -0500",
            "description": """Turns OpenMW into an engine for isometric RPGs.""",
            "is_featured": True,
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/opennevermind/",
            "usage_notes": """<span class="bold">OpenMW 0.48 or newer is required!</span> Consult the README bundled with the mod or <a href="https://modding-openmw.gitlab.io/opennevermind/readme/">on its website</a> for information about controls and usage.""",
        },
        {
            "name": "MOMW Camera",
            "author": "johnnyhostile",
            "category": camera,
            "date_added": "2022-11-19 20:31:00 -0500",
            "description": """Autoconfigure camera and headbobbing settings to recommended values!""",
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/momw-camera/",
            "usage_notes": """<p><span class="bold">OpenMW 0.48 or newer is required!</span> Each time you run OpenMW, the following camera and head bobbing settings will be set:</p>
<ul>
  <li> View over the shoulder: <span class="bold">Yes</span></li>
  <li>Auto switch shoulder: <span class="bold">Yes</span></li>
  <li>Zoom out when move coef: <span class="bold">60</span></li>
  <li>Preview if stand still: <span class="bold">Yes</span></li>
  <li>Ignore ‘No Collision’ flag: <span class="bold">Yes</span></li>
  <li>Smooth view change: <span class="bold">Yes</span></li>
  <li>Head bobbing enabled: <span class="bold">Yes</span></li>
  <li>Head bobbing base step length: <span class="bold">120</span></li>
  <li>Head bobbing step height: <span class="bold">6</span></li>
  <li>Head bobbing max roll angle: <span class="bold">0.5</span></li>
</ul>
<p class="bold">Disable this mod to adjust these settings to other values!</p>""",
        },
    ]

    for m in _mods:
        generate_mod(**m)
