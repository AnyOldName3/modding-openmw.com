from momw.helpers import generate_mod, nexus_mod, nexus_user


ui = "User Interface"


# Names of tags that are used in this file.
tag_high_res = "High Res"
tag_dev_build_only = "Dev Build Only"
tag_openmw_lua = "OpenMW Lua"
tag_gitlab_mods = "GitLab mods"

# Names of mods that are used as alts in this file.
arukinns_better_ui = "Arukinns Better UI"
chocolate_ui = "Chocolate UI"
monochrome_user_interface = "Monochrome User Interface"
morrowind_ui_revamped = "Morrowind UI Revamped"
skyrim_ui_overhaul_for_morrowind = "Skyrim UI Overhaul for Morrowind"


def init():
    for m in [
        {
            "author": nexus_user("4032923", "Dedaenc"),
            "category": ui,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": 'Removes all menu borders, giving a sleeker more appealing look to the UI.  I prefer the "Less Monochrome" configuration.',
            "extra_cfg": """<p>Add the below to your <code>openmw.cfg</code> for the "Monochrome" configuration:</p>

<p><pre><code>fallback=FontColor_color_normal,255,255,255
fallback=FontColor_color_normal_over,192,192,192
fallback=FontColor_color_normal_pressed,128,128,128
fallback=FontColor_color_active,255,255,255
fallback=FontColor_color_active_over,192,192,192
fallback=FontColor_color_active_pressed,128,128,128
fallback=FontColor_color_disabled,128,128,128
fallback=FontColor_color_disabled_over,192,192,292
fallback=FontColor_color_disabled_pressed,255,255,255
fallback=FontColor_color_link,105,129,255
fallback=FontColor_color_link_over,91,112,222
fallback=FontColor_color_link_pressed,97,106,156
fallback=FontColor_color_journal_link,37,49,112
fallback=FontColor_color_journal_link_over,58,77,175
fallback=FontColor_color_journal_link_pressed,112,126,207
fallback=FontColor_color_journal_topic,0,0,0
fallback=FontColor_color_journal_topic_over,58,77,175
fallback=FontColor_color_journal_topic_pressed,112,126,207
fallback=FontColor_color_answer,181,58,58
fallback=FontColor_color_answer_over,230,80,80
fallback=FontColor_color_answer_pressed,155,80,80
fallback=FontColor_color_header,255,255,255
fallback=FontColor_color_notify,100,100,100
fallback=FontColor_color_big_normal,200,200,200
fallback=FontColor_color_big_normal_over,255,255,255
fallback=FontColor_color_big_normal_pressed,150,150,15
fallback=FontColor_color_big_link,255,255,255
fallback=FontColor_color_big_link_over,255,255,255
fallback=FontColor_color_big_link_pressed,150,150,15
fallback=FontColor_color_big_answer,200,200,200
fallback=FontColor_color_big_answer_over,255,255,255
fallback=FontColor_color_big_answer_pressed,150,150,150
fallback=FontColor_color_big_header,200,200,200
fallback=FontColor_color_big_notify,200,200,200
fallback=FontColor_color_background,0,0,0
fallback=FontColor_color_focus,80,80,80
fallback=FontColor_color_health,255,255,255
fallback=FontColor_color_magic,255,255,255
fallback=FontColor_color_fatigue,255,255,255
fallback=FontColor_color_misc,255,255,255
fallback=FontColor_color_weapon_fill,255,255,255
fallback=FontColor_color_magic_fill,255,255,255
fallback=FontColor_color_positive,111,128,111
fallback=FontColor_color_negative,198,65,57
fallback=FontColor_color_count,255,255,255</code></pre></p>

<p>And this for the "Less Monochrome" configuration:</p>

<p><pre><code>fallback=FontColor_color_normal,255,255,255
fallback=FontColor_color_normal_over,192,192,192
fallback=FontColor_color_normal_pressed,128,128,128
fallback=FontColor_color_active,255,255,255
fallback=FontColor_color_active_over,192,192,192
fallback=FontColor_color_active_pressed,128,128,128
fallback=FontColor_color_disabled,128,128,128
fallback=FontColor_color_disabled_over,192,192,292
fallback=FontColor_color_disabled_pressed,255,255,255
fallback=FontColor_color_link,105,129,255
fallback=FontColor_color_link_over,91,112,222
fallback=FontColor_color_link_pressed,97,106,156
fallback=FontColor_color_journal_link,37,49,112
fallback=FontColor_color_journal_link_over,58,77,175
fallback=FontColor_color_journal_link_pressed,112,126,207
fallback=FontColor_color_journal_topic,0,0,0
fallback=FontColor_color_journal_topic_over,58,77,175
fallback=FontColor_color_journal_topic_pressed,112,126,207
fallback=FontColor_color_answer,230,80,80
fallback=FontColor_color_answer_over,181,58,58
fallback=FontColor_color_answer_pressed,155,80,80
fallback=FontColor_color_header,255,255,255
fallback=FontColor_color_notify,100,100,100
fallback=FontColor_color_big_normal,200,200,200
fallback=FontColor_color_big_normal_over,255,255,255
fallback=FontColor_color_big_normal_pressed,150,150,150
fallback=FontColor_color_big_link,255,255,255
fallback=FontColor_color_big_link_over,255,255,255
fallback=FontColor_color_big_link_pressed,150,150,150
fallback=FontColor_color_big_answer,200,200,200
fallback=FontColor_color_big_answer_over,255,255,255
fallback=FontColor_color_big_answer_pressed,150,150,150
fallback=FontColor_color_big_header,200,200,200
fallback=FontColor_color_big_notify,200,200,200
fallback=FontColor_color_background,0,0,0
fallback=FontColor_color_focus,80,80,80
fallback=FontColor_color_health,198,65,57
fallback=FontColor_color_magic,83,91,112
fallback=FontColor_color_fatigue,111,128,111
fallback=FontColor_color_misc,255,255,255
fallback=FontColor_color_weapon_fill,198,65,57
fallback=FontColor_color_magic_fill,83,91,112
fallback=FontColor_color_positive,111,128,111
fallback=FontColor_color_negative,198,65,57
fallback=FontColor_color_count,255,255,255</code></pre></p>""",
            "extra_cfg_raw": """#
# Monochrome UI, choose these or the values for "Less Monochrome" but not both.
# (This goes into openmw.cfg)
#
fallback=FontColor_color_normal,255,255,255
fallback=FontColor_color_normal_over,192,192,192
fallback=FontColor_color_normal_pressed,128,128,128
fallback=FontColor_color_active,255,255,255
fallback=FontColor_color_active_over,192,192,192
fallback=FontColor_color_active_pressed,128,128,128
fallback=FontColor_color_disabled,128,128,128
fallback=FontColor_color_disabled_over,192,192,292
fallback=FontColor_color_disabled_pressed,255,255,255
fallback=FontColor_color_link,105,129,255
fallback=FontColor_color_link_over,91,112,222
fallback=FontColor_color_link_pressed,97,106,156
fallback=FontColor_color_journal_link,37,49,112
fallback=FontColor_color_journal_link_over,58,77,175
fallback=FontColor_color_journal_link_pressed,112,126,207
fallback=FontColor_color_journal_topic,0,0,0
fallback=FontColor_color_journal_topic_over,58,77,175
fallback=FontColor_color_journal_topic_pressed,112,126,207
fallback=FontColor_color_answer,181,58,58
fallback=FontColor_color_answer_over,230,80,80
fallback=FontColor_color_answer_pressed,155,80,80
fallback=FontColor_color_header,255,255,255
fallback=FontColor_color_notify,100,100,100
fallback=FontColor_color_big_normal,200,200,200
fallback=FontColor_color_big_normal_over,255,255,255
fallback=FontColor_color_big_normal_pressed,150,150,15
fallback=FontColor_color_big_link,255,255,255
fallback=FontColor_color_big_link_over,255,255,255
fallback=FontColor_color_big_link_pressed,150,150,15
fallback=FontColor_color_big_answer,200,200,200
fallback=FontColor_color_big_answer_over,255,255,255
fallback=FontColor_color_big_answer_pressed,150,150,150
fallback=FontColor_color_big_header,200,200,200
fallback=FontColor_color_big_notify,200,200,200
fallback=FontColor_color_background,0,0,0
fallback=FontColor_color_focus,80,80,80
fallback=FontColor_color_health,255,255,255
fallback=FontColor_color_magic,255,255,255
fallback=FontColor_color_fatigue,255,255,255
fallback=FontColor_color_misc,255,255,255
fallback=FontColor_color_weapon_fill,255,255,255
fallback=FontColor_color_magic_fill,255,255,255
fallback=FontColor_color_positive,111,128,111
fallback=FontColor_color_negative,198,65,57
fallback=FontColor_color_count,255,255,255
#
# Less Monochrome UI, choose these or the values for "Monochrome" but not both.
# (This goes into openmw.cfg)
#
fallback=FontColor_color_normal,255,255,255
fallback=FontColor_color_normal_over,192,192,192
fallback=FontColor_color_normal_pressed,128,128,128
fallback=FontColor_color_active,255,255,255
fallback=FontColor_color_active_over,192,192,192
fallback=FontColor_color_active_pressed,128,128,128
fallback=FontColor_color_disabled,128,128,128
fallback=FontColor_color_disabled_over,192,192,292
fallback=FontColor_color_disabled_pressed,255,255,255
fallback=FontColor_color_link,105,129,255
fallback=FontColor_color_link_over,91,112,222
fallback=FontColor_color_link_pressed,97,106,156
fallback=FontColor_color_journal_link,37,49,112
fallback=FontColor_color_journal_link_over,58,77,175
fallback=FontColor_color_journal_link_pressed,112,126,207
fallback=FontColor_color_journal_topic,0,0,0
fallback=FontColor_color_journal_topic_over,58,77,175
fallback=FontColor_color_journal_topic_pressed,112,126,207
fallback=FontColor_color_answer,230,80,80
fallback=FontColor_color_answer_over,181,58,58
fallback=FontColor_color_answer_pressed,155,80,80
fallback=FontColor_color_header,255,255,255
fallback=FontColor_color_notify,100,100,100
fallback=FontColor_color_big_normal,200,200,200
fallback=FontColor_color_big_normal_over,255,255,255
fallback=FontColor_color_big_normal_pressed,150,150,150
fallback=FontColor_color_big_link,255,255,255
fallback=FontColor_color_big_link_over,255,255,255
fallback=FontColor_color_big_link_pressed,150,150,150
fallback=FontColor_color_big_answer,200,200,200
fallback=FontColor_color_big_answer_over,255,255,255
fallback=FontColor_color_big_answer_pressed,150,150,150
fallback=FontColor_color_big_header,200,200,200
fallback=FontColor_color_big_notify,200,200,200
fallback=FontColor_color_background,0,0,0
fallback=FontColor_color_focus,80,80,80
fallback=FontColor_color_health,198,65,57
fallback=FontColor_color_magic,83,91,112
fallback=FontColor_color_fatigue,111,128,111
fallback=FontColor_color_misc,255,255,255
fallback=FontColor_color_weapon_fill,198,65,57
fallback=FontColor_color_magic_fill,83,91,112
fallback=FontColor_color_positive,111,128,111
fallback=FontColor_color_negative,198,65,57
fallback=FontColor_color_count,255,255,255""",
            "name": monochrome_user_interface,
            "slug": "monochrome-user-interface",
            "status": "live",
            "url": nexus_mod("45174"),
            "usage_notes": """<p>This mod works by altering the various <code>FontColor_color</code> values your <code>openmw.cfg</code> should have after you import your <code>Morrowind.ini</code> file.  Remove the defaults from your cfg file, look for values that start out like <code>fallback=FontColor_color_</code>.</p>""",
        },
        {
            "alt_to": [monochrome_user_interface],
            "author": nexus_user("2334606", "Arukinn"),
            "category": ui,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "If the monochrome look isn't your thing, and/or you don't feel like tweaking too much in your <code>openmw.cfg</code> this is the best UI option out there.",
            "name": arukinns_better_ui,
            "slug": "arukinns_better_ui",
            "status": "live",
            "url": nexus_mod("42139"),
        },
        {
            "alt_to": [arukinns_better_ui, monochrome_user_interface],
            "author": nexus_user("159722", "Mikeandike"),
            "category": ui,
            "date_added": "2019-07-20 14:22:31 -0500",
            "description": "Replaces all of the tabled Morrowind UI with a simpler and more modern look like in Skyrim.",
            "extra_cfg": """<p>Add the below to your <code>openmw.cfg</code>:</p>

<p><pre><code>fallback=FontColor_color_normal,150,150,150
fallback=FontColor_color_normal_over,200,200,200
fallback=FontColor_color_normal_pressed,255,255,255
fallback=FontColor_color_active,255,255,255
fallback=FontColor_color_active_over, 150, 150,150
fallback=FontColor_color_active_pressed,255,255,255
fallback=FontColor_color_disabled,100,100,100
fallback=FontColor_color_disabled_over,140,140,140
fallback=FontColor_color_disabled_pressed,100,100,100
fallback=FontColor_color_link,58,78,181
fallback=FontColor_color_link_over,91,112,222
fallback=FontColor_color_link_pressed,97,106,156
fallback=FontColor_color_journal_link,37,49,112
fallback=FontColor_color_journal_link_over,58,77,175
fallback=FontColor_color_journal_link_pressed,112,126,207
fallback=FontColor_color_journal_topic,0,0,0
fallback=FontColor_color_journal_topic_over,58,77,175
fallback=FontColor_color_journal_topic_pressed,112,126,207
fallback=FontColor_color_answer, 181, 58,58
fallback=FontColor_color_answer_over,230,80,80
fallback=FontColor_color_answer_pressed,155,80,80
fallback=FontColor_color_header,255,255,255
fallback=FontColor_color_notify,100,100,100
fallback=FontColor_color_big_normal,200,200,200
fallback=FontColor_color_big_normal_over,255,255,255
fallback=FontColor_color_big_normal_pressed,150,150,150
fallback=FontColor_color_big_link,255,255,255
fallback=FontColor_color_big_link_over,255,255,255
fallback=FontColor_color_big_link_pressed,150,150,150
fallback=FontColor_color_big_answer, 200,200,200
fallback=FontColor_color_big_answer_over,255,255,255
fallback=FontColor_color_big_answer_pressed,150,150,150
fallback=FontColor_color_big_header,200,200,200
fallback=FontColor_color_big_notify,200,200,200
fallback=FontColor_color_background,0,0,0
fallback=FontColor_color_focus,80,80,80
fallback=FontColor_color_health,200,60,30
fallback=FontColor_color_magic,53,69,159
fallback=FontColor_color_fatigue,0,150,60
fallback=FontColor_color_misc,0,205,205
fallback=FontColor_color_weapon_fill,200,60,30
fallback=FontColor_color_magic_fill,200,60,30
fallback=FontColor_color_positive,35,219,109
fallback=FontColor_color_negative,200,60,30
fallback=FontColor_color_count,255,255,255</code></pre></p>

<p>And add this to your <code>settings.cfg</code>:</p>

<p><pre><code>stretch menu background = true</code></pre></p>""",
            "extra_cfg_raw": """#
# Skyrim UI Overhaul for Morrowind:
# This goes into settings.cfg under [GUI]!
#
stretch menu background = true
#
# Skyrim UI Overhaul for Morrowind
# This goes into openmw.cfg!
#
fallback=FontColor_color_normal,150,150,150
fallback=FontColor_color_normal_over,200,200,200
fallback=FontColor_color_normal_pressed,255,255,255
fallback=FontColor_color_active,255,255,255
fallback=FontColor_color_active_over, 150, 150,150
fallback=FontColor_color_active_pressed,255,255,255
fallback=FontColor_color_disabled,100,100,100
fallback=FontColor_color_disabled_over,140,140,140
fallback=FontColor_color_disabled_pressed,100,100,100
fallback=FontColor_color_link,58,78,181
fallback=FontColor_color_link_over,91,112,222
fallback=FontColor_color_link_pressed,97,106,156
fallback=FontColor_color_journal_link,37,49,112
fallback=FontColor_color_journal_link_over,58,77,175
fallback=FontColor_color_journal_link_pressed,112,126,207
fallback=FontColor_color_journal_topic,0,0,0
fallback=FontColor_color_journal_topic_over,58,77,175
fallback=FontColor_color_journal_topic_pressed,112,126,207
fallback=FontColor_color_answer, 181, 58,58
fallback=FontColor_color_answer_over,230,80,80
fallback=FontColor_color_answer_pressed,155,80,80
fallback=FontColor_color_header,255,255,255
fallback=FontColor_color_notify,100,100,100
fallback=FontColor_color_big_normal,200,200,200
fallback=FontColor_color_big_normal_over,255,255,255
fallback=FontColor_color_big_normal_pressed,150,150,150
fallback=FontColor_color_big_link,255,255,255
fallback=FontColor_color_big_link_over,255,255,255
fallback=FontColor_color_big_link_pressed,150,150,150
fallback=FontColor_color_big_answer, 200,200,200
fallback=FontColor_color_big_answer_over,255,255,255
fallback=FontColor_color_big_answer_pressed,150,150,150
fallback=FontColor_color_big_header,200,200,200
fallback=FontColor_color_big_notify,200,200,200
fallback=FontColor_color_background,0,0,0
fallback=FontColor_color_focus,80,80,80
fallback=FontColor_color_health,200,60,30
fallback=FontColor_color_magic,53,69,159
fallback=FontColor_color_fatigue,0,150,60
fallback=FontColor_color_misc,0,205,205
fallback=FontColor_color_weapon_fill,200,60,30
fallback=FontColor_color_magic_fill,200,60,30
fallback=FontColor_color_positive,35,219,109
fallback=FontColor_color_negative,200,60,30
fallback=FontColor_color_count,255,255,255""",
            "name": skyrim_ui_overhaul_for_morrowind,
            "status": "live",
            "url": nexus_mod("43039"),
        },
        {
            "alt_to": [
                arukinns_better_ui,
                monochrome_user_interface,
                skyrim_ui_overhaul_for_morrowind,
            ],
            "author": nexus_user("10857508", "Innicin"),
            "category": ui,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """Chocolate UI is an attempt to modernize the aging Morrowind UI while preserving the vanilla experience.""",
            "name": chocolate_ui,
            "status": "live",
            "url": nexus_mod("43076"),
        },
        {
            "alt_to": [
                chocolate_ui,
                arukinns_better_ui,
                monochrome_user_interface,
                skyrim_ui_overhaul_for_morrowind,
            ],
            "name": morrowind_ui_revamped,
            "author": "jiffyadvent",
            "category": ui,
            "compat": "partially working",
            "date_added": "2020-06-21 16:53:51 -0500",
            "date_updated": "2020-06-23 16:53:51 -0500",
            "description": """HD New look and overhaul to Morrowind's UI with keeping to the
base design scheme.  2 Main Theme choices to choose from; Classical (Morrowind-like) and Dark (Monochrome/Skyrim-like).""",
            "status": "live",
            "url": nexus_mod("43922"),
        },
        {
            "author": nexus_user("159722", "Mikeandike"),
            "category": ui,
            "compat": "not working",
            "date_added": "2019-07-20 14:40:14 -0500",
            "description": "Replaces the Morrowind main menu with an animated image made to fit the Skyrim UI for Morrowind mod. Requires MGXE.",
            "name": "Animated Main Menu for Morrowind",
            "status": "live",
            "url": nexus_mod("43341"),
        },
        {
            "name": "Title Screen and Logo Video Intro Reworked",
            "author": nexus_user("2470746", "Phobos"),
            "category": ui,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "HD reworkings of the title screen and the logo video.",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43657"),
        },
        {
            "name": "Widescreen Alaisiagae Splash Screens",
            "author": nexus_user("5648555", "Yingerman"),
            "category": ui,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": "This mod updates Alaisiagae's Bethesda-style Splash Screen Addon SSA from 4:3 ratio to 16:9 ratio for modern, widescreen displays. These new splash screens add a little more diversity to your loading screens while remaining lore friendly.",
            "status": "live",
            "url": nexus_mod("43021"),
        },
        {
            "name": "Morrowind Loading Screens Extended - v2.1",
            "author": nexus_user("45659397", "AlyMar1994"),
            "category": ui,
            "date_added": "2019-03-01 22:21:40 -0500",
            "description": "Extending the amount of Morrowind's loading screens since August of 2017.",
            "status": "live",
            "url": nexus_mod("45151"),
        },
        {
            "name": "HD Concept-art splash screen and main menu",
            "author": "Say",
            "category": ui,
            "date_added": "2019-03-02 16:21:40 -0500",
            "description": "Add 26 unique HD concept-art splash screens and main menu.",
            "status": "live",
            "url": nexus_mod("43081"),
        },
        {
            "name": "OpenHUD",
            "author": nexus_user("71679553", "wazabear"),
            "category": ui,
            "date_added": "2021-01-27 23:53:07 -0500",
            "date_updated": "2022-11-19 11:48:00 -0500",
            "description": "A Pluginless HUD replacer for OpenMW.",
            "status": "live",
            "url": nexus_mod("48893"),
            "usage_notes": """<p>Download the <code>OpenHUD</code> and <code>0.48 Dev Build Patch</code> files. To install:</p>
<ol>
  <li>Locate where you have installed OpenMW</li>
  <li>Backup the resources folder, this will make it very easy to uninstall later</li>
  <li>Copy and paste the resources and data folder from the mod archives into where OpenMW is installed, you should be prompted to overwrite and merge the two folders, accept</li>
</ol>""",
        },
        {
            "name": "Official Xbox Splash Screens",
            "author": "Bethesda",
            "category": ui,
            "date_added": "2021-08-29 14:06:40 -0500",
            "description": "The official Xbox loading screens converted to work with the desktop version.",
            "status": "live",
            "url": nexus_mod("46422"),
            "usage_notes": """These are all low resolution, so you may want to skip them if that bothers you.""",
        },
        {
            "name": "Big Icons",
            "author": nexus_user("44686767", "JaceyS"),
            "category": ui,
            "date_added": "2021-08-29 14:08:55 -0500",
            "description": "Replaces the small 16x16 spell effect icons with your choice of larger icons. Works with your choice of MWSE or OpenMW",
            "status": "live",
            "url": nexus_mod("49662"),
        },
        {
            "name": "Cantons on the Global Map",
            "author": nexus_user("102938538", "Hemaris"),
            "category": ui,
            "date_added": "2022-01-23 17:05:20 -0500",
            "description": "Vivec and Molag Mar no longer look like empty water on the global map.",
            "status": "live",
            "url": nexus_mod("50534"),
        },
        {
            "name": "Gonzo's Splash Screens",
            "author": nexus_user("4863777", "Gonzo"),
            "category": ui,
            "date_added": "2022-12-07 17:05:00 -0500",
            "date_updated": "2023-05-20 11:25:00 -0500",
            "description": "This pack contains 78 splash screens in a unified style. Pick and choose which you want to use. There are separate splash screen packs for OpenMW (2K/1440p and HD/1080p), and Vanilla(2:1). Part of the May Modathon Month 2023.",
            "status": "live",
            "url": nexus_mod("51667"),
        },
        {
            "name": "Simple HUD for OpenMW (with compass or minimap)",
            "author": nexus_user("545942", "MisterFlames"),
            "category": ui,
            "date_added": "2023-11-09 15:41:00 -0500",
            "description": "A better HUD for OpenMW with many different flavors; compass, vanilla, compact, skyrim-like.",
            "status": "live",
            "url": nexus_mod("53038"),
        },
        {
            "name": "UiModes",
            "author": '<a href="https://gitlab.com/ptmikheev">Petr Mikheev</a>',
            "category": ui,
            "date_added": "2023-09-10 16:41:00 -0500",
            "description": "Various gameplay and UI tweaks including menu hotkeys, no pause when menus are up and much more.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/ui-modes/",
        },
        {
            "name": "Morrowind HD Upscaled Menu",
            "author": nexus_user("81721973", "mio122"),
            "category": ui,
            "date_added": "2023-11-19 08:09:00 -0500",
            "description": "Simple HD vanilla upscale of the main menu.",
            "status": "live",
            "url": nexus_mod("49971"),
        },
        {
            "name": "Hit Chance UI",
            "author": nexus_user("17885684", "Safebox"),
            "category": ui,
            "date_added": "2024-01-11 17:06:00 -0500",
            "description": "Shows the hit chance of the player against the current enemy.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53930"),
        },
    ]:
        generate_mod(**m)
