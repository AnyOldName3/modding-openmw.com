from momw.helpers import generate_mod, nexus_mod, nexus_user, modhistory_archive_link


flora = "Flora"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_vanilla_friendly = "Vanilla Friendly"
tag_oaab_data = "OAAB Data"

# Names of mods that are used as alts in this file.
better_flora = "Better Flora"


def init():
    # print("START: {} ... ".format(flora), end="")

    _mods = [
        {
            "name": "Underwater Static Replacer v1.0",
            "author": "Slartibartfast",
            "category": flora,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This mod is a texture replacer for Morrowinds underwater static items ie, barnacles, kelp and kollops.",
            "is_active": False,
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly, tag_high_res],
            "url": modhistory_archive_link("56-11998"),
        },
        {
            "name": "Water Life",
            "author": nexus_user("38047", "abot"),
            "category": flora,
            "compat": "not working",
            "date_added": "2019-07-20 15:35:11 -0500",
            "description": 'Water Life is an attempt to mix the scripting techniques developed in the "Where are all bird going?" plugin with the best aquatic creature modding resources to bring more life into Morrowind waters.',
            "status": "live",
            "url": nexus_mod("42417"),
        },
        {
            "name": better_flora,
            "author": nexus_user("6676263", "Moranar raddimus"),
            "category": flora,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": 'This plugin replaces most of Ascadian Isles\'s (not only, these plants may be found in other regions too) plant meshes with smoother and high-poly versions, along with correct collisions. Included are also optimized vanilla meshes, which look like original, but have a lower polycount and correct collisions. These can be found in the directory "lowpoly".',
            "slug": "better-flora",
            "status": "live",
            "tags": [tag_high_res, tag_manual_edit_needed],
            "url": nexus_mod("43288"),
            "usage_notes": """<p>The below files must be deleted as they contain errors (and conflict with <a href="/mods/unification-compilation-nature/">UC: Nature</a>):</p>

<pre><code>Meshes/f/flora_tree_01.NIF
Meshes/f/flora_tree_02.NIF
Meshes/f/flora_tree_03.nif
Meshes/f/flora_tree_04.NIF
Meshes/f/flora_tree_ai_05.NIF
Meshes/f/flora_tree_ai_06.NIF</code></pre>

<p>If you aren't going for an HD look, the low poly meshes seem to have odd collision issues (you can't walk through grass, etc) so you may want to skip it.</p>""",
        },
        {
            "name": "Epic Plants",
            "alt_to": [better_flora],
            "author": "Articus - Qwertyquit - Pherim",
            "category": flora,
            "date_added": "2019-03-27 18:10:00 -0500",
            "date_updated": "2021-07-04 13:41:17 -0500",
            "description": "Replaces textures and models of various flora with HQ quality.",
            "status": "live",
            "tags": [tag_high_res, tag_manual_edit_needed],
            "url": nexus_mod("46180"),
            "usage_notes": """<p>Download the "HQ Plants" and "All-In-One" archives, and delete these files which conflict with <a href="/mods/comberry-bush-and-ingredient-replacer/">Comberry Bush and Ingredient Replacer</a>:

<pre><code>03 MWSE and OpenMW GH Patch/Meshes/f/flora_comberry_01.nif
00 Epic Plants/Meshes/n/ingred_comberry_01.NIF
00 Epic Plants/Meshes/f/flora_comberry_01.nif</code></pre>

<p>Also delete these wickwheat meshes because they cause a huge performance hit near Vos and the related region:</p>

<pre><code>Meshes/o/Flora_wickwheat_01.nif
Meshes/o/Flora_wickwheat_02.nif
Meshes/o/Flora_wickwheat_03.nif
Meshes/o/Flora_wickwheat_04.nif</code></pre>

<p>In the "All In One" package, you'll find these four meshes within the <code>03 MWSE and OpenMW GH Patch</code> and <code>00 Epic Plants</code> folders. For me, deleting these files took me from around 10FPS to 30FPS at the lowest. Simply run <code>coc "Vos"</code> in the console and turn around to test your performance.</p>

<p><a href="/mods/graphic-herbalism-mwse-and-openmw-edition/">Graphic Herbalism - MWSE and OpenMW Edition</a> has a <code>GH Patches and Replacers</code> download which includes a <code>06 Less Epic Plants</code> folder, this should serve as a suitable replacement for the stock Epic Plants wickwheat meshes. They still look very nice, and your framerate won't be nearing single digits. See that mod's usage notes for more information.</p>

<p><span class="bold">If you're following the Graphics Overhaul or Total Overhaul mod lists</span>: you can skip the <code>02 Ingredients</code> folder path (it gets replaced by <a href="/mods/ingredients-mesh-replacer/">Ingredients Mesh Replacer</a>).</p>""",
        },
        {
            "name": "Hackle-lo Fixed",
            "author": nexus_user("1592927", "Pherim"),
            "category": flora,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "A replacer for the hackle-lo plant with corrected meshes and improved textures.  Fixed meshes for Graphic Herbalism are also included. They use the leaf parts of the original [plugin-based] GH meshes.",
            "slug": "hackle-lo-fixed",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("42784"),
            "usage_notes": """If you're using <a href="/mods/graphic-herbalism-mwse-and-openmw-edition/">Graphic Herbalism - MWSE and OpenMW Edition</a> then don't install any meshes from this, only textures.""",
        },
        {
            "name": "Comberry Bush and Ingredient Replacer",
            "author": nexus_user("1592927", "Pherim"),
            "category": flora,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "High quality texture and model replacer.",
            "slug": "comberry-bush-and-ingredient-replacer",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("42586"),
            "usage_notes": """<p>Get the <code>Pherim Comberry Bush</code> download. From the <code>Data Files</code> directory in the root of the archive, extract the <code>Textures</code> directory to the root of this mod's data path. No meshes are needed, these are provided by <a href="/mods/graphic-herbalism-mwse-and-openmw-edition-addition/">Graphic Herbalism - MWSE and OpenMW Edition: Additional Patches</a>.</p>""",
        },
        {
            "name": "Pherim's Fire Fern - Plant and Ingredient",
            "author": nexus_user("1592927", "Pherim"),
            "category": flora,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2021-09-06 10:48:05 -0500",
            "description": "High quality texture and model replacer for the plant and ingredient.",
            "slug": "pherims-fire-fern-plant-and-ingredient",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("43568"),
            "usage_notes": """<p>If you're following a mod list, then you want to extract the <code>00 Default Red/icons</code> and <code>00 Default Red/textures</code> directories into this mod's data path.</p>

<p>If you're not following a mod list (and also not using <a href="/mods/graphic-herbalism-mwse-and-openmw-edition/">Grephic Herbalism</a>), then get the <code>meshes</code> directory too.</p>""",
        },
        {
            "name": "A Strange Plant",
            "author": nexus_user("4381248", "PeterBitt"),
            "category": flora,
            "date_added": "2019-03-01 18:10:00 -0500",
            "description": """<p>Brings the Nirnroot to Vvardenfell.  A simple, non intrusive and bug free quest mod.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("42009"),
            "usage_notes": """<p>There's an optional 'smaller textures' package for those who are looking for a more vanilla look.  Note that this mod suffers from the "Who's There?" problem, see <a href="https://www.tamriel-rebuilt.org/content/whos-there-not-knock-knock-joke-sadly">this article</a> on the Tamriel Rebuilt blog for more information.</p>""",
        },
        {
            "name": "Parasol Particles",
            "author": nexus_user("47755", "Melchior Dahrk"),
            "category": flora,
            "date_added": "2021-04-10 08:59:15 -0500",
            "date_updated": "2022-11-15 17:44:00 -0500",
            "description": "Adds falling spores particle effects to the iconic emperor parasol mushrooms.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("47755"),
            "usage_notes": """<p>For the <span class="bold">Graphics Overhaul and Total Overhaul mod lists</span>: use the <code>00 PeterBitt's Replacer</code> folder from the main archive.</p>

<p>For the <span class="bold">Expanded Vanilla</span> and <span class="bold">One Day Morrowind Modernization </span> mod lists: use the <code>00 Core</code> folder from the main archive.</p>

<p>All mod list users should also use the <code>01 OAAB_Data Patch</code> path.</p>""",
        },
        {
            "name": "Scum Retexture",
            "author": nexus_user("1600249", "Lougian"),
            "category": flora,
            "date_added": "2021-08-15 20:11:20 -0500",
            "description": "Retexture of the scum in the Bitter coast",
            "status": "live",
            "url": nexus_mod("42582"),
            "usage_notes": """If unsure, just go with the main download instead of the alternatives (it's what i prefer).""",
        },
        {
            "name": "Scummy Scum",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": flora,
            "date_added": "2021-08-15 20:13:42 -0500",
            "description": "Scummier scum texture for Lougian's Scum Retexture mod.",
            "status": "live",
            "url": nexus_mod("45802"),
        },
        {
            "name": "OAAB Saplings",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": flora,
            "date_added": "2021-10-21 19:22:45 -0500",
            "description": """This mod places saplings from OAAB_Data in most regions on Vvardenfell.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50334"),
            "usage_notes": """Only the <code>00 Merged</code> folder is needed.""",
        },
        {
            "name": "Thickle-Lo - The Succulent Hackle-Lo Mod",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": flora,
            "date_added": "2022-11-07 11:05:00 -0500",
            "description": """Hackle-lo leaves are described as "a tasty edible succulent leaf of the Grazelands..." And yet the leaves appear to be flat and not reminiscent of a succulent. This mod edits the plants and ingredients to be thicker. The ingredient is now a single leaf split in half to reveal the tasty flesh inside that locals like to chew after a long day's work.""",
            "status": "live",
            "url": nexus_mod("47502"),
        },
        {
            "name": "OAAB Pomegranates",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": flora,
            "date_added": "2022-11-09 08:33:00 -0500",
            "description": """Inspired by their mention of growing in the badlands of Morrowind in Sermon 12, a regional variety of pomegranates can now be found growing atop Mount Assarnibibi.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50726"),
            "usage_notes": """Mod list users: only the <code>00 Core</code> is needed.""",
        },
        {
            "name": "Perfect Scum",
            "author": nexus_user("2250219", "Phoenix Rime"),
            "category": flora,
            "date_added": "2022-11-20 14:39:00 -0500",
            "description": """Changes the location of the scum models to better match the water level. Makes all swamps perfect. OpenMW and MGE XE compatible.""",
            "status": "live",
            "url": nexus_mod("51355"),
        },
        {
            "name": "Mid Mushroom Overhaul",
            "author": nexus_user("58363776", "Juidius"),
            "category": flora,
            "date_added": "2023-09-17 10:43:00 -0500",
            "description": """Hand places OAAB's mid size parasol all across Vvardenfell.""",
            "status": "live",
            "url": nexus_mod("52760"),
        },
        {
            "name": "Unification Compilation: Nature",
            "author": "AOF, Apel, Ayse, Bloodinfested, CJW-Craigor, Earth_Wyrm, Evanmeisterx, Khalazza, Kieve, LestatDeLioncourt, Lord Gabryael, Nich, Papill6n, Plangkye, Qarl, Raptre, Rhymer, Slartibartfast, Taddeus, Vality, Vurt, Wollibeebee, Zuldazug, Compiled by Knots",
            "category": flora,
            "compat": "partially working",
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This is a compilation that includes most (if not all) of the best landscape replacers/retextures.",
            # TODO This archive is broken and contains only one file. Either remove the link or rehost the full mod while the WebArchive link still works.
            "dl_url": "/files/UnificationCompilationNature.zip",
            "needs_cleaning": True,
            "slug": "unification-compilation-nature",
            "status": "live",
            "tags": [tag_high_res, tag_game_expanding, tag_manual_edit_needed],
            "url": "https://web.archive.org/web/20161103125749/https://download.fliggerty.com/file.php?id=568",
            "usage_notes": """<p>While testing this mod I found that it caused horrible framerates in certain areas.  Even with just this mod, I'd get around 10FPS in the Ascadian Isles.  As such, use at your own discretion.</p>

<p>The file <code>Meshes/f/flora_bc_podplant_03.nif</code> has a <code>NiSourceTexture Tx_BC_podplant_03_g.tga</code>, but no <code>Tx_BC_podplant_03_g.tga</code> file is included in the mod texture files.  Removing all references (with Nifskope) to Tx_BC_podplant_03_g.tga fixes the issue.  A fixed mesh file can be downloaded from the below link "Download Link" on this page.</p>

<p>Be sure to use the OPTIONAL stuff if you also plan to use Vurts Grazeland Trees II!</p>""",
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
