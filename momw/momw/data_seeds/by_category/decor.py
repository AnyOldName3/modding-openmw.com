from momw.helpers import generate_mod

decor = "Decor"


def init():
    _mods = [
        {
            "name": "Mannequins for Sale",
            "description": "Portable mannequins which can be equipped with weapons and armor, and posed in about ten positions. I know there are other mannequin mods, but I wasn't happy with any that I tried. If you feel the same way, give mine a try. Visit the shop in Caldera to get started. Now with wigs!",
            "author": "ManaUser",
            "category": decor,
            "date_added": "2021-08-29 10:34:28 -0500",
            "needs_cleaning": True,
            #
            # $ tes3cmd clean "Mannequins for Sale.esp"
            #
            # CLEANING: "Mannequins for Sale.esp" ...
            # Loaded cached Master: <DATADIR>/morrowind.esm
            # Loaded cached Master: <DATADIR>/bloodmoon.esm
            #  Cleaned redundant AMBI,WHGT from CELL: caldera, falanaamo: clothier
            #  Cleaned redundant AMBI,WHGT from CELL: caldera, hodlismod: armorer
            #  Cleaned redundant AMBI,WHGT from CELL: caldera, irgola: pawnbroker
            #  Cleaned redundant AMBI,WHGT from CELL: caldera, north guard towers
            #  Cleaned redundant AMBI,WHGT from CELL: caldera, shenk's shovel
            # Output saved in: "Clean_Mannequins for Sale.esp"
            # Original unaltered: "Mannequins for Sale.esp"
            #
            # Cleaning Stats for "Mannequins for Sale.esp":
            #              redundant CELL.AMBI:     5
            #              redundant CELL.WHGT:     5
            #
            "status": "live",
            "url": "http://hostx.me/morrowind/#mannequins",
            "usage_notes": """<p>The mod README is viewable online at <a href="http://hostx.me/morrowind/files/MannequinsForSale-Readme.txt">this link</a>.</p>""",
        }
    ]

    for m in _mods:
        generate_mod(**m)
