from momw.helpers import generate_mod

# nexus_mod,
# nexus_user

travel = "Travel"

# Names of tags that are used in this file.
tag_dev_build_only = "Dev Build Only"
tag_openmw_lua = "OpenMW Lua"
tag_gitlab_mods = "GitLab mods"


def init():
    for m in [
        {
            "name": "Signpost Fast Travel",
            "author": "Johnnyhostile",
            "category": travel,
            "date_added": "2023-10-31 13:34:00 -0500",
            "description": "Fast travel from signposts to their locations if you have already been there before, using procedurally-generated target points. Includes an optional menu-based travel UI.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/signpost-fast-travel/",
        },
    ]:
        generate_mod(**m)
