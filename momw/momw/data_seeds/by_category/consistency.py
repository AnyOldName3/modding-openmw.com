from momw.helpers import generate_mod, nexus_mod, nexus_user

consistency = "Consistency"


def init():
    _mods = [
        {
            "name": "FMI - Hospitality Papers Expanded",
            "description": "Expands quorn's HospitalityPapers Mod. Includes newly recorded lines from original Morrowind voice actor Jeff Baker.",
            "author": "quorn and PoodleSandwich - Jeff Baker - SuperQuail",
            "category": consistency,
            "date_added": "2021-08-29 14:29:58 -0500",
            "status": "live",
            "url": nexus_mod("46107"),
        },
        {
            "name": "FMI - Misc",
            "description": "Fixes a handful of small inconsistencies between lore and what is found in game.",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": consistency,
            "date_added": "2021-08-29 15:02:15 -0500",
            "status": "live",
            "url": nexus_mod("47637"),
            "usage_notes": """Download and extract both main files.""",
        },
        {
            "name": "FMI - Nice to Meet You",
            "description": """Tired of NPCs saying things like "Nice to meet you." after you've already met? Now they will say things like "Nice to see you." instead.""",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": consistency,
            "date_added": "2021-08-29 15:30:13 -0500",
            "status": "live",
            "url": nexus_mod("47329"),
        },
        {
            "name": "FMI - Caius Big Package",
            "description": """Give Caius Cosades a package worthy of the Grand Spymaster of Vvardenfell.""",
            "author": "Team Big Package",
            "category": consistency,
            "date_added": "2021-08-29 15:38:07 -0500",
            "status": "live",
            "url": nexus_mod("47580"),
        },
        {
            "name": "FMI - NotAllDunmer",
            "description": """Not all Dunmer are slavers. Not all Argonians are slaves. Idle dialogue filtering has been improved to reflect this.""",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": consistency,
            "date_added": "2021-08-29 15:43:49 -0500",
            "status": "live",
            "url": nexus_mod("47569"),
        },
        {
            "name": "FMI - Service Refusal and Contraband",
            "description": """Fixes lore to gameplay inconsistencies pertaining to contraband items and service refusal.""",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": consistency,
            "date_added": "2021-08-29 16:57:12 -0500",
            "status": "live",
            "url": nexus_mod("47456"),
        },
        {
            "name": "FMI - Sane Ordinators",
            "description": """Makes it so Ordinators will not kill you for wearing Indoril armor once you have been named Nerevarine by Vivec, or if you are Master or Patriarch of the Temple. """,
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": consistency,
            "date_added": "2021-09-08 11:21:12 -0500",
            "status": "live",
            "url": nexus_mod("47381"),
        },
        {
            "name": "FMI - Legion Dialogue",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": consistency,
            "date_added": "2023-11-10 19:21:00 -0500",
            "description": "Fixes several inconsistencies in dialogue spoken by members of the Imperial Legion.",
            "status": "live",
            "url": nexus_mod("47318"),
        },
    ]

    for m in _mods:
        generate_mod(**m)
