from momw.helpers import generate_mod, nexus_mod, nexus_user, modhistory_archive_link


quests = "Quests"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_manual_edit_needed = "Manual Edit Needed"
tag_normal_maps = "Normal Maps"
tag_oaab_data = "OAAB Data"
tag_openmw_lua = "OpenMW Lua"
tag_tamriel_data = "Tamriel Data"
tag_vanilla_friendly = "Vanilla Friendly"
tag_dev_build_only = "Dev Build Only"

# Names of mods that are used as alts in this file.
official_plugins_fixes_v11 = "Official Plugins Fixes v1.1"
unofficial_morrowind_official_plugins_patched = (
    "Unofficial Morrowind Official Plugins Patched"
)


def init():
    for m in [
        {
            "name": "Bethesda's Official Plugins",
            "alt_to": [
                official_plugins_fixes_v11,
                unofficial_morrowind_official_plugins_patched,
            ],
            "author": "Bethesda Softworks",
            "category": quests,
            "date_added": "2019-07-19 14:48:41 -0500",
            "description": """<p>Small mods released by Bethesda, also known as 'The Official Plugins'.</p>""",
            "is_active": False,
            "status": "live",
            "url": "https://wiki.openmw.org/index.php?title=Bethesda_Softworks_Mods",
            "usage_notes": """It's recommended to use <a href="/mods/unofficial-morrowind-official-plugins-patched/">Unofficial Morrowind Official Plugins Patched</a> instead of these.""",
        },
        {
            "name": "Gondolier's License",
            "author": '<a href="https://baturin.org/">dmbaturin</a>',
            "category": quests,
            "date_added": "2019-05-14 19:03:15 -0500",
            "description": "Acquire the unique shirt and helm of Vivec gondoliers by applying for a gondolier's license and taking a Vivec geography test.",
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly],
            "url": "https://baturin.org/misc/morrowind-mods/#gondoliers-license",
        },
        {
            "name": "Tavern of Broken Dreams",
            "author": '<a href="https://baturin.org/">dmbaturin</a>',
            "category": quests,
            "date_added": "2019-05-14 19:12:39 -0500",
            "description": """<p>There are lots of unused NPCs and objects in the Morrowind data files. It's as if the developers never made any attempts to clean up anything, you can even find their notes to themselves. That prompted me to make a mod inspired by the “Cafe of Broken Dreams” special encounter from Fallout 2.</p>

<p>A strange Nord is relaxing on a beach on the Azura's coast (coe 19, -3). He can take you to a place where non-existent characters gather to complain about their situation. All those characters retain their original ids and inventory, I only gave them custom dialog with their background as I imagine it.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "https://baturin.org/misc/morrowind-mods/#tavern-of-broken-dreams",
        },
        {
            "name": "Fetchers of Morrowind",
            "author": '<a href="https://baturin.org/">dmbaturin</a>',
            "category": quests,
            "date_added": "2019-01-01 21:12:49 -0500",
            "description": """<p>“Fetchers of Morrowind” is a company that finds items and delivers them to customers. They are looking for resourceful adventurers who can find unusual items and fulfill absurd orders.</p>

<p>The main motivation was to experiment with spicing up twenty bear asses type quests by giving characters some personality and motivation to want the items.</p>

<p>To start the quest line, talk to Marthe Rane in Vivec, Foreign Quarter Upper Waistworks. Talk to her about “Background” and “My trade”.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "https://baturin.org/misc/morrowind-mods/#fetchers-of-morrowind",
            "usage_notes": """Make sure to grab Abot's version, not the original.""",
        },
        {
            "author": nexus_user("558771", "dagothagahnim"),
            "category": quests,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Adds an operational Dwemer Cyborg factory to the north of Vos, filled with challenging enemies and powerful artifacts.",
            "name": "Sobitur Facility",
            "date_updated": "2023-01-16 10:12:00 -0500",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("42418"),
            "usage_notes": """<p>The following plugins are dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span>:</p>

<pre><code>DA_Sobitur_Facility_Clean.ESP
DA_Sobitur_Quest_Part_1 Clean.esp
DA_Sobitur_Repurposed_1.ESP</code></pre></p>

<details>
<summary>
<p>click to expand/collapse:</p>
</summary>

<pre><code>$ tes3cmd clean DA_Sobitur_Facility_Clean.ESP

CLEANING: "./DA_Sobitur_Facility_Clean.ESP" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
 Cleaned duplicate record (INGR): food_kwama_egg_01
 Cleaned duplicate record (MISC): misc_uni_pillow_02
 Cleaned duplicate object instance (TempleMarker FRMR: 270735) from CELL: balmora (-3, -2)
 Cleaned junk-CELL: bitter coast region (-12, 3)
 Cleaned junk-CELL: bitter coast region (-12, 2)
 Cleaned junk-CELL: bitter coast region (-12, 1)
 Cleaned junk-CELL: bitter coast region (-13, 4)
 Cleaned junk-CELL: bitter coast region (-13, 3)
 Cleaned junk-CELL: bitter coast region (-13, 2)
 Cleaned junk-CELL: bitter coast region (-13, 1)
 Cleaned junk-CELL: bitter coast region (-14, 5)
 Cleaned junk-CELL: bitter coast region (-14, 4)
 Cleaned junk-CELL: bitter coast region (-14, 2)
 Cleaned junk-CELL: west gash region (-15, 7)
 Cleaned junk-CELL: wilderness (-15, -8)
 Cleaned junk-CELL: west gash region (-16, 8)
 Cleaned junk-CELL: west gash region (-16, 7)
 Cleaned redundant AMBI,WHGT from CELL: ald daedroth, left wing
 Cleaned redundant AMBI,WHGT from CELL: ald sotha, lower level
 Cleaned redundant AMBI,WHGT from CELL: ald sotha, shrine
 Cleaned redundant AMBI,WHGT from CELL: arkngthand, deep ore passage
 Cleaned redundant AMBI,WHGT from CELL: arkngthand, land's blood gallery
 Cleaned redundant AMBI,WHGT from CELL: ashalmawia, shrine
 Cleaned redundant AMBI,WHGT from CELL: ashmelech
 Cleaned redundant AMBI,WHGT from CELL: bal ur, underground
 Cleaned redundant AMBI,WHGT from CELL: bthanchend
 Cleaned redundant AMBI,WHGT from CELL: bthuand
 Cleaned duplicate object instance (ingred_6th_corprusmeat_05 FRMR: 234609) from CELL: corprusarium bowels
 Cleaned duplicate object instance (food_kwama_egg_01 FRMR: 234611) from CELL: corprusarium bowels
 Cleaned duplicate object instance (de_r_closet_bagarn FRMR: 234603) from CELL: corprusarium bowels
 Cleaned duplicate object instance (dwrv_chest_divayth07 FRMR: 257760) from CELL: corprusarium bowels
 Cleaned duplicate object instance (bk_Yagrum's_Book FRMR: 437725) from CELL: corprusarium bowels
 Cleaned redundant AMBI,WHGT from CELL: corprusarium bowels
 Cleaned redundant AMBI,WHGT from CELL: druscashti, upper level
 Cleaned duplicate object instance (com_closet_01_cullian FRMR: 276904) from CELL: ebonheart, imperial chapels
 Cleaned redundant AMBI,WHGT from CELL: ebonheart, imperial chapels
 Cleaned redundant AMBI,WHGT from CELL: falasmaryon, upper level
 Cleaned redundant AMBI,WHGT from CELL: galom daeus, entry
 Cleaned duplicate object instance (indoril shield FRMR: 270852) from CELL: ghostgate, tower of dawn
 Cleaned redundant AMBI,WHGT from CELL: ghostgate, tower of dawn
 Cleaned duplicate object instance (com_chest_02_vault_01 FRMR: 386876) from CELL: ibar-dad
 Cleaned redundant AMBI,WHGT from CELL: ibar-dad
 Cleaned redundant AMBI,WHGT from CELL: mudan, central vault
 Cleaned redundant AMBI,WHGT from CELL: nchuleft
 Cleaned redundant AMBI,WHGT from CELL: nchuleftingth, lower levels
 Cleaned duplicate object instance (iron flameblade FRMR: 361257) from CELL: tel mora, berwen: trader
 Cleaned redundant AMBI,WHGT from CELL: tel mora, berwen: trader
 Cleaned duplicate object instance (misc_shears_01 FRMR: 361448) from CELL: tel mora, elegnan: clothier
 Cleaned redundant AMBI,WHGT from CELL: tel mora, elegnan: clothier
 Cleaned duplicate object instance (de_r_closet_01_mcloth04 FRMR: 281419) from CELL: tel vos, aryon's chambers
 Cleaned duplicate object instance (bk_notebyaryon FRMR: 281426) from CELL: tel vos, aryon's chambers
 Cleaned duplicate object instance (bk_NGastaKvataKvakis_o FRMR: 281442) from CELL: tel vos, aryon's chambers
 Cleaned duplicate object instance (bookskill_illusion4 FRMR: 483486) from CELL: tel vos, aryon's chambers
 Cleaned duplicate object instance (bookskill_enchant3 FRMR: 483487) from CELL: tel vos, aryon's chambers
 Cleaned redundant AMBI,WHGT from CELL: tel vos, aryon's chambers
 Cleaned duplicate object instance (bk_firmament FRMR: 370656) from CELL: tower of tel fyr, hall of fyr
 Cleaned redundant AMBI,WHGT from CELL: tower of tel fyr, hall of fyr
 Cleaned redundant AMBI,WHGT from CELL: vas, tower
 Cleaned redundant AMBI,WHGT from CELL: vivec, foreign quarter plaza
 Cleaned duplicate object instance (bk_AedraAndDaedra FRMR: 197040) from CELL: vivec, high fane
 Cleaned redundant AMBI,WHGT from CELL: vivec, high fane
 Cleaned redundant AMBI,WHGT from CELL: vivec, jobasha's rare books
 Cleaned redundant AMBI,WHGT from CELL: bamz-amschend, hearthfire hall
 Cleaned duplicate object instance (dwrv_mechrarm00 FRMR: 23872) from CELL: bamz-amschend, radac's forge
 Cleaned redundant AMBI,WHGT from CELL: bamz-amschend, radac's forge
 Cleaned junk-CELL: wilderness (-19, 15)
 Cleaned junk-CELL: wilderness (-19, 14)
 Cleaned junk-CELL: wilderness (-20, 15)
 Cleaned junk-CELL: wilderness (-28, 24)
 Cleaned junk-CELL: wilderness (-29, 23)
Output saved in: "./Clean_DA_Sobitur_Facility_Clean.ESP"
Original unaltered: "./DA_Sobitur_Facility_Clean.ESP"

Cleaning Stats for "./DA_Sobitur_Facility_Clean.ESP":
       duplicate object instance:    19
                duplicate record:     2
                       junk-CELL:    19
             redundant CELL.AMBI:    30
             redundant CELL.WHGT:    30

$ tes3cmd clean "DA_Sobitur_Quest_Part_1 Clean.esp"

CLEANING: "./DA_Sobitur_Quest_Part_1 Clean.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
 Cleaned duplicate record (DOOR): door_dwrv_loadup00
Output saved in: "./Clean_DA_Sobitur_Quest_Part_1 Clean.esp"
Original unaltered: "./DA_Sobitur_Quest_Part_1 Clean.esp"

Cleaning Stats for "./DA_Sobitur_Quest_Part_1 Clean.esp":
                duplicate record:     1

$ tes3cmd clean DA_Sobitur_Repurposed_1.ESP

CLEANING: "./DA_Sobitur_Repurposed_1.ESP" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
 Cleaned duplicate record (STAT): in_velothismall_corner_01
 Cleaned duplicate record (STAT): in_velothismall_ramp_01
 Cleaned duplicate record (STAT): in_velothismall_room_09
 Cleaned duplicate record (STAT): in_lavacave2_08
 Cleaned duplicate record (STAT): in_lavacave2_s_04
 Cleaned duplicate record (STAT): in_lavacave2_s_end
 Cleaned duplicate record (STAT): in_hlaalu_hall_corner_01
 Cleaned duplicate record (STAT): in_velothismall_dj_01
 Cleaned duplicate record (CONT): chest_small_01_goldr500
 Cleaned duplicate record (DOOR): door_dwrv_loadup00
 Cleaned duplicate record (CONT): com_basket_arille's
 Cleaned duplicate record (CONT): chest_01_v_potion_h_01
 Cleaned duplicate record (MISC): misc_uni_pillow_02
 Cleaned duplicate record (CONT): com_chest_02_galbedir
 Cleaned junk-CELL: sheogorad (1, 27)
 Cleaned junk-CELL: sheogorad (0, 27)
Output saved in: "./Clean_DA_Sobitur_Repurposed_1.ESP"
Original unaltered: "./DA_Sobitur_Repurposed_1.ESP"

Cleaning Stats for "./DA_Sobitur_Repurposed_1.ESP":
                duplicate record:    14
                       junk-CELL:     2</code></pre>
</details>

<p>If you're following a mod list: skip the <code>DA_Sobitur_TRIngred_Compat.ESP</code> plugin, a more updated version will be used later.</p>""",
        },
        {
            "name": "Magical Missions",
            "needs_cleaning": True,
            "author": nexus_user("109031", "Von Djangos"),
            "category": quests,
            "compat": "partially working",
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Members of the Mages Guild can now deliver a coded message, solve a murder, investigate a heist, barter with the Telvanni and retrieve a tome of dark power for the Master Wizard of Caldera!",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("38773"),
            "usage_notes": "Later missions might not fully work with OpenMW but it's still worth adding.  I've gotten reports that console fiddling is needed to complete some of the later missions...",
        },
        {
            "name": "Pearls and Pirates",
            "author": "The Mystical Beasts",
            "category": quests,
            "compat": "unknown",
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "In the Azura’s Coast lives a lowly fisherman who specialises in working with pearls. If you help him, he will share some information about a treasure.",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("45268"),
        },
        {
            "name": "The Rise of the Tribe Unmourned Revised",
            "author": "SammyB27 and Revised by Caeris",
            "category": quests,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2022-11-12 15:35:00 -0500",
            "description": "Dagoth Ur has been defeated, but the continued presence of the Sixth House still poses a threat to Morrowind. A lieutenant of the Tribe Unmourned has come out of the shadows in a sinister plot to resurrect Dagoth Ur.",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("45165"),
            "usage_notes": """Download the <code>Crown Edition</code> file.""",
        },
        {
            "name": "Main Quest Enhancers",
            "author": nexus_user("370317", "Trainwiz"),
            "category": quests,
            "date_added": "2019-03-01 18:20:00 -0500",
            "description": "Adds a greater 6th House presence to the game as the main quest progresses",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("41337"),
            "usage_notes": """<p>Mlox had the following to say about this mod:</p>

<p><span class="italic">"MQE_BlightedBeasties.ESP" (and so the merged "MQE_MainQuestEnhancers.ESP") uses AddToLev*/RemoveFromLev* functions to modify Bethesda's leveled lists. Using either of these plugins will store leveled lists in your savegame that will override lists in your merged leveled lists plugin (e.g. "Mashed Lists.esp"), if you have one.</span></p>

<p><span class="italic"><a href="http://www.uesp.net/wiki/Tes3Mod:Leveled_Lists">http://www.uesp.net/wiki/Tes3Mod:Leveled_Lists</a></span></p>

<p><span class="italic">abot has released a version of "MQE_MainQuestEnhancers.ESP" which does not use these deprecated functions, and makes some other improvements and fixes.</span></p>

<p><span class="italic">Download "Main Quest Enhancers 1.03" from abot's website - <a href="http://abitoftaste.altervista.org/morrowind/index.php?option=downloads&task=info&id=72&Itemid=50">http://abitoftaste.altervista.org/morrowind/index.php?option=downloads&task=info&id=72&Itemid=50</a></span></p>

<p>The plugin mentioned by the Mlox message is next up (if you're following <a href="/lists/total-overhaul/">the Total Overhaul list</a>)!  Since it's replaced by the patch, you technically don't need to download this mod.  I am keeping it listed as a reference.</p>""",
        },
        {
            "name": "To Save a Falling Wizard",
            "author": nexus_user("22736769", "Lady Phoenix Fire Rose"),
            "category": quests,
            "date_added": "2019-02-22 21:00:00 -0500",
            "description": """<p>Have you ever run into a falling wizard and wanted to save him, but if you do he had nothing to say to you? Well now you can, and have new thing to talk with him about and new quests!</p>

<p>This mod expands upon Tarhiel, the wizard falling from the sky just outside of Seyda Neen. Giving him new unique dialogue if you save his life, and two quests to complete. Also included is a new spell, and a new enchanted scroll.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("45637"),
            "usage_notes": """This is reported as working, but it may conflict with another mod (I'm unclear about which one, though).""",
        },
        {
            "name": "Illegal Items",
            "author": "Kindi",
            "category": quests,
            "date_added": "2019-04-25 21:19:57 -0500",
            "description": """The Imperial Legion needs your help. Collect Illegal Items such as Raw ebony, Raw Glass, and Dwemer items and hand them over to Knight Protectors. A very good immersion plugin, with very balanced rewards.""",
            "status": "live",
            "url": nexus_mod("44362"),
            "usage_notes": """There's two versions: v1.1 is compatible with the Dwemer Bow and Helmet mod (not yet featured on this site).  If you aren't using that mod then it's fine to get the version without compatibility for it.""",
        },
        {
            "author": "Fliggerty",
            "category": quests,
            "compat": "unknown",
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """<p>A series of quests involving the operation of an illegal skooma lab.  This mod involves the skooma production process, the political aspects of becoming a druglord, and the legal problems involved.</p>

<p>This mod was originally intended to be an entrepenuer/business mod, but managed to become a lot more.  After all the quests are done, you have the option to automate the lab, making it entirely self sufficient...all you have to do is stop every once in a while and collect your money.</p>""",
            "name": "Vvardenfell Druglord",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": modhistory_archive_link("68-10247"),
            "usage_notes": "Contains an optional 'Skooma Anonymous add-on where you work to become sober!",
        },
        {
            "author": "Team SSE",
            "category": quests,
            "compat": "partially working",
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """Sotha Sil was somewhat disappointing in vanilla Tribunal, so we've made a massive expansion that creates the Clockwork City as it was always meant to be. A sprawling dungeon, a topsy-turvy city, and the madness of the Clockwork God all stand to challenge you!""",
            "name": "Sotha Sil Expanded",
            "status": "live",
            "url": nexus_mod("42347"),
            "usage_notes": "Works with OpenMW with the exception of one part involving a platform, console commands must be used to progress.",
        },
        {
            "name": "Welcome to the Arena",
            "author": nexus_user("889608", "Kalamestari_69"),
            "category": quests,
            "compat": "partially working",
            "date_added": "2018-08-05 21:00:00 -0500",
            "date_updated": "2020-11-15 08:47:19 -0500",
            "description": """With this mod, you can fight and bet for fights in the Vivec's Arena. Like in the the Imperial City Arena in Oblivion, there are apparent teams of combatants and scheduled matches. To start you'r journey, talk to one of the arena masters in their bloodworks about "Join the arena". You need to be at least level 10 to join. After joining, you can fight in rank related matches or against monsters. Just ask about this from your arena master and of you go!""",
            "status": "live",
            "tags": [tag_game_expanding, tag_normal_maps],
            "url": nexus_mod("22002"),
            "usage_notes": """<p>Do not use the included esp!  This is listed as partially working because a patch is required for this to work with OpenMW, see <a href="/mods/welcome-to-the-arena-v67-patched-for-openmw/">Welcome to the Arena v6.7 patched for OpenMW</a> for the working plugin.</p>

<p>OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.</p>""",
        },
        {
            "name": "Wealth Within Measure",
            "author": nexus_user("38286905", "Texafornian"),
            "category": quests,
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """<p>Adds bankers to the House vaults in Vivec, the Imperial Commission in Ebonheart, and the Indoril Vault in Bosmora (TR). Open deposit accounts, take out loans, invest in House ventures, and buy traveler's notes.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("44725"),
        },
        {
            "name": "Immersive Imperial Skirt",
            "author": '<a href="https://baturin.org/">dmbaturin</a>',
            "category": quests,
            "date_added": "2020-05-06 08:40:47 -0500",
            "description": """<p>The imperial skirt worn by guards and the imperial templar skirt worn by some legion officers are one of the “common but unobtainable” items, just like the shirt and helm of the gondoliers. The guards must get those skirts somewhere, right?</p>

<p>This mod adds a small quest to restore the supply of skirts. Talk to any legion member about “latest rumors”, then go to Darius in Gnisis and agree to help him. Then you'll have to walk around and talk to clothiers. Two of them in the empire-friendly towns can help you.</p>""",
            "status": "live",
            "url": "https://baturin.org/misc/morrowind-mods/#imperial-skirt",
        },
        {
            "name": "Uncharted Artifacts",
            "author": nexus_user("109130978", "Xero Foxx"),
            "category": quests,
            "date_added": "2021-08-28 16:33:13 -0500",
            "description": """All shipwrecks carry a hidden map revealing the location of a unique, non-quest related artifact. Let the treasure hunting begin!""",
            "status": "live",
            "url": nexus_mod("49281"),
            "usage_notes": """If you're following a mod list you can skip the plugin from this (one that's been patched for compatibility will be provided <a href="/mods/oaab-shipwrecks-patches-uncharted-artifacts/">later</a>).""",
        },
        {
            "name": "To save a falling wizard (alternate) Fixed For OpenMW and Delta Plugin",
            "author": "ezze and Lady Phoenix Fire Rose",
            "category": quests,
            "date_added": "2021-08-28 22:37:38 -0500",
            "description": """The mod expands upon Tarhiel, the bosmer enchanter that fall to his death nearby Seyda Neen. With this mod, if you manage to rescue him he'll thank you and give a quest to learn the safe Icarian Flight spell.""",
            "status": "live",
            "url": nexus_mod("49685"),
        },
        {
            "name": "Ebonheart Underworks",
            "author": "Dandy Daedra",
            "category": quests,
            "date_added": "2021-08-28 22:47:53 -0500",
            "date_updated": "2022-12-11 09:28:00 -0500",
            "description": """Ebonheart now has its own sewage system. With filth, rats, people and quests.""",
            "status": "live",
            "url": nexus_mod("47272"),
            "usage_notes": """If you're following the <span class="bold">Total Overhaul</span> or <span class="bold">Expanded Vanilla</span> mod lists you should skip the plugin from this one (a patched version is provided later on). """,
        },
        {
            "name": "Bounty Hunter Assignments",
            "author": nexus_user("4709296", "AliceL93"),
            "category": quests,
            "date_added": "2021-08-28 22:53:38 -0500",
            "description": """Roleplay as a bounty hunter, and kill dangerous criminals and beasts for gold, all around Vvardenfell""",
            "status": "live",
            "url": nexus_mod("46928"),
        },
        {
            "name": "Bitter and Blighted",
            "author": nexus_user("44521922", "MrWillis"),
            "category": quests,
            "date_added": "2021-08-28 23:00:23 -0500",
            "description": """After the murder of the local taxman, rumors are running around that Arrille bought a deed to a local abandoned mine and is looking for someone to help clean it of the blight. See if you can take care of it.""",
            "needs_cleaning": True,
            #
            # $ tes3cmd clean BitterAndBlighted.ESP
            #
            # CLEANING: "BitterAndBlighted.ESP" ...
            # Loaded cached Master: <DATADIR>/morrowind.esm
            # Loaded cached Master: <DATADIR>/tribunal.esm
            # Loaded cached Master: <DATADIR>/bloodmoon.esm
            #  Cleaned redundant AMBI,WHGT from CELL: abaesen-pulu egg mine
            # Output saved in: "Clean_BitterAndBlighted.ESP"
            # Original unaltered: "BitterAndBlighted.ESP"
            #
            # Cleaning Stats for "BitterAndBlighted.ESP":
            #              redundant CELL.AMBI:     1
            #              redundant CELL.WHGT:     1
            #
            "status": "live",
            "url": nexus_mod("47052"),
        },
        {
            "name": "Frozen in Time",
            "author": nexus_user("30444", "Billyfighter"),
            "category": quests,
            "date_added": "2021-08-28 23:06:55 -0500",
            "description": """This quest will bring you back in time, to solve a mystery untouched for hundreds of years. You'll also face off against Sheogorath's powerful minions for his amusement. Level 15-20 is recommended. Vampire characters can also do this quest.""",
            "status": "live",
            "url": nexus_mod("50077"),
        },
        {
            "name": "Main Quest Overhaul",
            "author": nexus_user("4709296", "AliceL93"),
            "category": quests,
            "date_added": "2021-08-28 23:12:33 -0500",
            "description": """This mod overhauls the Main Quest. For a full list of features, please see the mod's page.""",
            "status": "live",
            "url": nexus_mod("46913"),
        },
        {
            "name": "The Vanilla Quest Tweaks RP Choices Consequences Super Mega Package - Ultimate Edition",
            "author": nexus_user("4709296", "AliceL93"),
            "category": quests,
            "date_added": "2021-08-29 08:48:31 -0500",
            "description": """A combination of 20 mods by AliceL93 from the vanilla quest tweaks/RP choices/consequences category.""",
            # "dl_url": "/files/Clean_The_Vanilla_Quest_Tweaks_RP_Choices_Consequences_Super_Mega_Package_-_Ultimate_Edition.zip",
            # "needs_cleaning": True,
            #
            # $ tes3cmd clean The_Vanilla_Quest_Tweaks_RP_Choices_Consequences_Super_Mega_Package_-_Ultimate_Edition.ESP
            #
            # CLEANING: "The_Vanilla_Quest_Tweaks_RP_Choices_Consequences_Super_Mega_Package_-_Ultimate_Edition.ESP" ...
            # Loaded cached Master: <DATADIR>/morrowind.esm
            # Loaded cached Master: <DATADIR>/tribunal.esm
            # Loaded cached Master: <DATADIR>/bloodmoon.esm
            #  Cleaned redundant AMBI,WHGT from CELL: ald velothi, outpost
            #  Cleaned redundant AMBI,WHGT from CELL: ald-ruhn, ald skar inn
            #  Cleaned redundant AMBI,WHGT from CELL: ald-ruhn, council club
            #  Cleaned redundant AMBI,WHGT from CELL: gnisis, madach tradehouse
            #  Cleaned redundant AMBI,WHGT from CELL: khuul, thongar's tradehouse
            #  Cleaned redundant AMBI,WHGT from CELL: maar gan, andus tradehouse
            #  Cleaned redundant AMBI,WHGT from CELL: moonmoth legion fort, interior
            #  Cleaned redundant AMBI,WHGT from CELL: moonmoth legion fort, prison towers
            #  Cleaned redundant AMBI,WHGT from CELL: pelagiad, halfway tavern
            #  Cleaned redundant AMBI,WHGT from CELL: sadrith mora, telvanni council house
            #  Cleaned redundant AMBI,WHGT from CELL: tel branora, upper tower: therana's chamber
            #  Cleaned redundant AMBI,WHGT from CELL: uveran ancestral tomb
            # Output saved in: "Clean_The_Vanilla_Quest_Tweaks_RP_Choices_Consequences_Super_Mega_Package_-_Ultimate_Edition.ESP"
            # Original unaltered: "The_Vanilla_Quest_Tweaks_RP_Choices_Consequences_Super_Mega_Package_-_Ultimate_Edition.ESP"
            #
            # Cleaning Stats for "The_Vanilla_Quest_Tweaks_RP_Choices_Consequences_Super_Mega_Package_-_Ultimate_Edition.ESP":
            #              redundant CELL.AMBI:    12
            #              redundant CELL.WHGT:    12
            #
            "status": "live",
            "url": nexus_mod("47466"),
            # "usage_notes": """The version from Nexus mods requires cleaning, the file linked above which is provided by me has been cleaned (log included).""",
        },
        {
            "name": "Sorcerer of Alteration",
            "author": nexus_user("7531974", "kindi"),
            "category": quests,
            "date_added": "2022-11-12 15:22:00 -0500",
            "description": """Journey through this quest mod to improve your alteration magic.""",
            "status": "live",
            "url": nexus_mod("51224"),
        },
        {
            "name": "Aspect of Azura",
            "author": nexus_user("962116", "MelchiorDahrk"),
            "category": quests,
            "date_added": "2022-11-12 15:29:00 -0500",
            "description": """Adds a Daedric helm bearing the aspect of Azura which you can acquire through a new quest which starts at the Holamayan Monastery near the Shrine of Azura. Explore the hidden depths below the shrine to become the Champion of Azura!""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51079"),
        },
        {
            "name": "Secrets of the Crystal City",
            "author": nexus_user("962116", "MelchiorDahrk"),
            "category": quests,
            "date_added": "2022-11-15 18:31:00 -0500",
            "date_updated": "2022-12-11 09:35:00 -0500",
            "description": """The lost "Crystal City" of Massama has been discovered by a desperate group of escaped slaves. Through a series of quests, players will learn some of the secrets behind The Curse of Massama while trying to keep the separate factions from killing each other over the raw glass they've mined.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("51932"),
        },
        {
            "name": "Glowing Secrets of the Crystal City",
            "author": nexus_user("1729697", "Damius"),
            "category": quests,
            "date_added": "2023-11-06 18:31:00 -0500",
            "description": """Glowmaps the glass and daedric items to match Glass Glowset and Daedric Weapons Enhanced.""",
            "status": "live",
            "url": nexus_mod("53482"),
        },
        {
            "name": "Investigations at Tel Eurus",
            "author": "Dreamy Dreamers",
            "category": quests,
            "date_added": "2022-11-15 18:39:00 -0500",
            "description": """Investigate a series of attacks on Tel Eurus, follow the clues to uncover the mastermind behind it. Level 10 or higher recommended.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("51938"),
        },
        {
            "name": "Something in the Water -- A Peryite Daedric Quest",
            "author": nexus_user("70112108", "GlitterGear") + " and DetailDevil",
            "category": quests,
            "date_added": "2023-09-29 15:11:00 -0500",
            "description": """There must be something in the water... Everyone who drinks out of a certain well in Pelagiad ends up sick! Features two new dungeons with over 2,000 objects hand-placed objects between them and two new quests""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53308"),
        },
        {
            "name": "Questline - Vivec Lighthouse Keeper",
            "author": nexus_user("24248299", "Dillonn241"),
            "category": quests,
            "date_added": "2023-09-29 15:33:00 -0500",
            "description": """A 4-part questline that lets you assist the lighthouse keeper at the Vivec Lighthouse south of Ebonheart. Perform hard work for the keeper, weather a thunderstorm, and uncover a dark secret.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52120"),
        },
        {
            "name": "Ancient Foes",
            "author": nexus_user("19250", "Darkelfguy"),
            "category": quests,
            "date_added": "2023-10-19 15:40:00 -0500",
            "description": """To the west and slightly north of Dagon Fel, you'll find a small cottage along the shores of the Sheogorad, where a young Nord spends his days longing for home. Will you help him find what he seeks? Or will you be the instrument of his destruction?""",
            "status": "live",
            "url": nexus_mod("44705"),
        },
        {
            "name": "Caldera Priory and the Depths of Blood and Bone",
            "author": nexus_user("4139826", "seelof"),
            "category": quests,
            "date_added": "2023-10-19 15:50:00 -0500",
            "description": """Caldera Priory has been overrun by the Undead. Do you have the courage and the will to venture into the Depths of Blood and Bone and vanquish the ancient Evil that has taken hold of the lands above?""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52898"),
        },
        {
            "name": "The Mysterious Affair of Sara Shenk",
            "author": nexus_user("1233897", "Danae123"),
            "category": quests,
            "date_added": "2023-10-19 16:13:00 -0500",
            "description": """In the quaint town of Caldera, the innkeeper, Shenk, becomes the centre of attention: the once-idyllic life of innkeeper Shenk takes a dark turn when his wife vanishes without a trace, leaving behind a trail of speculation and intrigue. Each step forward brings you closer to unravelling the enigma, but beware, for not all is as it seems.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52968"),
        },
        {
            "name": "AFFresh",
            "author": nexus_user("322665", "AFFA"),
            "category": quests,
            "date_added": "2023-10-29 14:22:00 -0500",
            "description": """This mod adds about 30 quests to NPCs that are relatively easy for new players to run across. Some quests are easy to find and some slightly less easy. Most of these quests are suitable for new characters and give opportunities for combat, persuasion, and security. Some of the quest rewards include interesting items for newer players.""",
            "status": "live",
            "url": nexus_mod("53006"),
        },
        {
            "name": "Fargoth Says Hello",
            "author": nexus_user("322665", "AFFA"),
            "category": quests,
            "date_added": "2023-10-29 14:39:00 -0500",
            "description": """Adds two quests to Fargoth depending on whether you helped Hrisskar find his secret hiding place or not.""",
            "status": "live",
            "url": nexus_mod("52751"),
        },
        {
            "name": "A Cold Cell",
            "author": nexus_user("1152341", "tewlwolow"),
            "category": quests,
            "date_added": "2023-10-29 14:49:00 -0500",
            "description": """Travel to Khuul and investigate the <span class="bold">rumors</span> about a smuggling operation nearby. Or perhaps it's more than that?""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52832"),
        },
        {
            "name": "The Search for the White Wave",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": quests,
            "date_added": "2023-10-29 15:36:00 -0500",
            "description": """A scientific expedition headed into the Sea of Ghosts has failed to report on their progress. An investigator from the Imperial Navy has been dispatched to locate the White Wave and is looking for a hand. Join his crew to unravel what happened to the White Wave in the frozen northern waters...""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52301"),
        },
        {
            "name": "The Patchwork Airship - Fleshing out a vanilla quest",
            "author": nexus_user("59284071", "RandomPal"),
            "category": quests,
            "date_added": "2023-10-29 15:58:00 -0500",
            "description": """Fleshes out the vanilla quest "The Patchwork Airship" by replacing the airship with a more fitting model, and giving it an interior you can explore.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53133"),
        },
        {
            "name": "The Mananaut's Message",
            "author": nexus_user("962116", "Melchior Dahrk")
            + " and "
            + nexus_user("322665", "AFFA"),
            "category": quests,
            "date_added": "2023-10-29 16:15:00 -0500",
            "description": """This mod adds Royal Imperial Mananaut Alberius outside of the Tower of Tel Fyr. He has a message that he wants you to deliver for him. At the end of the quest, you'll get your very own royal mananaut outfit!""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("52896"),
        },
        {
            "name": "Secret of Vilmuhn",
            "author": nexus_user("54525107", "GrumblingVomit") + " and MassiveJuice",
            "category": quests,
            "date_added": "2023-10-29 16:36:00 -0500",
            "description": """Far to the North lies a lonely island with a Dwemer ruin and a Nord barrow. Travel to Vilmuhn and figure out what dark forces are at play to stop them before things go too far. Suggested level is at least 15.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52977"),
        },
        {
            "name": "Galen's Quest for Truth",
            "author": "Katya and " + nexus_user("1233897", "Danae"),
            "category": quests,
            "date_added": "2023-10-29 16:47:00 -0500",
            "description": """A hermit on an island east of Vivec. Befriend him and help him uncover the truth. Or not.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("52884"),
        },
        {
            "name": "The Plague Doctor",
            "author": nexus_user("4876198", "Ruffin Vangarr") + " and Katya",
            "category": quests,
            "date_added": "2023-10-29 16:54:00 -0500",
            "description": """The plague doctor a dark alchemist of ill repute has come to Vvardenfel and brought his morbid experimentations with him. He hides away in a new cave within the Bitter coast.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("52885"),
        },
        {
            "name": "The Doors of Oblivion",
            "author": nexus_user("48605", "Ashstaar"),
            "category": quests,
            "date_added": "2023-10-29 17:01:00 -0500",
            "description": """Journey through the realms of Oblivion. New quests, items, characters, creatures, etc.""",
            "status": "live",
            "url": nexus_mod("44398"),
        },
        {
            "name": "Aether Pirate's Discovery",
            "author": nexus_user("30444", "Billyfighter"),
            "category": quests,
            "date_added": "2023-10-30 15:13:00 -0500",
            "description": """Find an Aethership over in the far north west of Vvardenfell, at the Arkngthunch-Sturdumz ruins and meet a pirate who has a mysterious trinket and new adventure to discover.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("53320"),
        },
        {
            "name": "Silent Island",
            "author": nexus_user("30444", "Billyfighter"),
            "category": quests,
            "date_added": "2023-10-30 15:33:00 -0500",
            "description": """This is a horror themed mod, with pirates, Sixth House, and romance. Though this mod plays fairly different, where many tough enemies can be killed by the environment. You can play the mod running and avoiding enemies the entire way, or kill every single one of them.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50274"),
        },
        {
            "name": "Mudcrab Imports",
            "author": nexus_user("30444", "Billyfighter"),
            "category": quests,
            "date_added": "2023-10-30 23:25:00 -0500",
            "description": """This mod adds new pirate themed quests, items and locations to Morrowind. Including an Imperial style tradehouse and a shady smugglers' den where you can embark on several quests and explore new underwater dungeons throughout the seas.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49463"),
        },
        {
            "name": "Apothecary's Demise",
            "author": nexus_user("29070750", "MatthewTheBagel"),
            "category": quests,
            "date_added": "2023-10-30 23:46:00 -0500",
            "description": """Apothecary's Demise is a quest mod about choice that starts in a quaint abode in the Ascadian Isles. Recommended level 11.""",
            "status": "live",
            "url": nexus_mod("44331"),
        },
        {
            "name": "Keelhouse - A quest and house mod for Tamriel Rebuilt",
            "author": nexus_user("59284071", "RandomPal"),
            "category": quests,
            "date_added": "2023-10-31 00:05:00 -0500",
            "description": """During your travels down the Thirr River, you stumble upon an archaeologist-wizard living on an island in a strange upside-down ship home.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53395"),
        },
        {
            "name": "Tamriel Rebuilt Introduction Quest - Help a Khajiit reach the City of Good People",
            "author": "Denis418",
            "category": quests,
            "date_added": "2023-10-31 00:11:00 -0500",
            "description": """Rescue a slave from Addamasartus and help him reach Old Ebonheart.""",
            "status": "live",
            "tags": [tag_tamriel_data],
            "url": nexus_mod("50445"),
        },
        {
            "name": "Early Transport to Mournhold",
            "author": nexus_user("70336838", "Necrolesian"),
            "category": quests,
            "date_added": "2023-10-31 00:21:00 -0500",
            "description": """This mod allows travel to Mournhold before the Dark Brotherhood attacks begin.""",
            "status": "live",
            "url": nexus_mod("47985"),
        },
        {
            "alt_to": ["Practical Necromancy (OpenMW)"],
            "name": "Black Soul Gems",
            "author": nexus_user("790766", "ZackHasACat"),
            "category": quests,
            "date_added": "2023-11-06 12:12:00 -0500",
            "description": """This mod will add a Necromancer's Altar to the Velothi tower Mawia, south of Molag Mar. Kill the necromancer and his minions, and you will be able to use the tower for your own necromancy of sorts.""",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("45902"),
        },
        {
            "name": "Lord of Rebirth",
            "author": "Team Twinkling Twilights: AnrohaNexus, Billyfighter, Katya, Ruffin Vangarr, and Seelof",
            "category": quests,
            "date_added": "2023-11-08 13:56:00 -0500",
            "description": """Explore a realm in conflict between undead void pirates and a group of alchemists with members from all over Tamriel. Join one of them or find an alternate path to end the stalemate. Includes new weapons and items to earn as rewards.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("53680"),
        },
        {
            "name": "Terror of Tel Amur",
            "author": "Team Kwamakaze Kagouti: Juidius, Kalinter, Lucevar, and tewlwolow",
            "category": quests,
            "date_added": "2023-11-08 14:06:00 -0500",
            "description": """The friendly, law-abiding citizens in Dirty Muriel's Cornerclub know what it means when Erer Darothril gets Ideas, and they know to stay safely away. But Erer still needs help, and if the regulars won't do it, perhaps a passing adventurer will. But is there a deeper meaning to Erer Darothril's seemingly frivolous hijinks?""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53673"),
        },
        {
            "name": "Greymarch Dawn - Whispers of Jyggalag",
            "author": "Team Veloth's Virtuosos: GlobeMallow, Hurdrax Custos, Lord Zircon, Markond, and Melchior Dahrk",
            "category": quests,
            "date_added": "2023-11-08 14:16:00 -0500",
            "description": """Plunge into an engaging prequel to the events of the TES4:Shivering Isles expansion as the world shivers with Whispers of Jyggalag. The familiar sights of sewers and Thieves Guild operatives quickly unravel as you find yourself caught between two opposing forces while traversing the deepest parts of Vvardenfell.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53679"),
        },
        {
            "name": "Clone",
            "author": "Team Verticality Gang: Jackimoff Wackimoff, Overseer, Qualia, TheDrunkenMudcrab, and ZackHasAcat",
            "category": quests,
            "date_added": "2023-11-16 13:29:02 +0100",
            "description": """Awaken the mad scientist in you and raise clones as both followers and flesh simulacrums!""",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53671"),
        },
        {
            "name": "The Arcane Academy of Venarius",
            "author": "Team Madgods Missing Marbles: Lady Phoenix Fire Rose, GrumblingVomit, S3ctor, Sjek, and Orion",
            "category": quests,
            "date_added": "2023-11-17 13:33:00 +0100",
            "description": """Find a worn-down mage's attempt at their own offshoot of the Mages' Guild. Explore new, unknown dimensions, rebuild the Arcane Academy of Venarius, and find all the secrets the school has to offer!""",
            "status": "live",
            "tags": [
                tag_dev_build_only,
                tag_openmw_lua,
                tag_tamriel_data,
                tag_oaab_data,
            ],
            "url": nexus_mod("53675"),
        },
        {
            "alt_to": ["Library of Vivec Enhanced"],
            "name": "ReadMe - Library of Vivec Overhaul",
            "author": nexus_user("4139826", "seelof"),
            "category": quests,
            "date_added": "2021-08-19 08:02:51 -0500",
            "description": "This mod redesigns the Library of Vivec from scratch to make it look and feel like a true library, with many books added from Tamriel_Data and OAAB_Data for you to read. There are also new NPCs for you to interact with, and two new quests that set you on the hunt for a unique book the resource for which was provided by RubberMan. ",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49916"),
        },
        {
            "name": "Master Index Redux",
            "author": nexus_user("44230747", "EndoranWest"),
            "category": quests,
            "date_added": "2024-01-26 19:30:28 +0100",
            "description": """The wizard Folms Mirel in Caldera is trying to unravel the secrets of the ancient Dunmer Propylon network on Vvardenfel. However, he is missing the indices and needs you to find them.""",
            "status": "live",
            "url": nexus_mod("48977"),
        },
    ]:
        generate_mod(**m)
