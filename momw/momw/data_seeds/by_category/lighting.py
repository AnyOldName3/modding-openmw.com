from momw.helpers import generate_mod, nexus_mod, nexus_user


lighting = "Lighting"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_oaab_data = "OAAB Data"
tag_openmw_lua = "OpenMW Lua"
tag_vanilla_friendly = "Vanilla Friendly"
tag_dev_build_only = "Dev Build Only"
tag_gitlab_mods = "GitLab mods"


windows_glow_v22 = "Windows Glow v. 2.2"
windows_glow_expansion = "Windows Glow Expansion"
windows_glow_tribunal = "Windows Glow - Tribunal"
windows_glow_bloodmoon = "Windows Glow - Bloodmoon (now with Raven Rock)"


def init():
    for m in [
        {
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": lighting,
            "date_added": "2019-03-30 13:31:29 -0500",
            "date_updated": "2020-11-22 11:31:12 -0500",
            "description": """<p>Flames are now glow mapped and/or properly illuminated.</p>""",
            "name": "Glowing Flames",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("46124"),
            "usage_notes": """<p>If using <a href="/mods/true-lights-and-darkness-necro-edit/">True Lights and Darkness - Necro Edit</a>, get a compatible version of <code>Glowing Flames - TrueLightsAndDarkness Tweaks.ESP</code> from <a href="/files/Glowing Flames TLAD Tweaks - TLAD Logical Flicker Necro Colors - Replacement Patch-48955-1-0-1606070256.7z">this link</a>. It has been patched to make three torches less flickery and reverts five lantern colors back to the "Necro" colors. Credit goes to <a href="https://www.nexusmods.com/morrowind/users/77253">alvazir</a> for creating <a href="https://www.nexusmods.com/morrowind/mods/48955?tab=files">the patched plugin</a>.</p>

<p>If you aren't using <a href="/cfg/">my CFG Generator</a>, please pay close attention to the load order notes in the README.</p>""",  # noqa; doesn't like the "escaped" dollar signs.
        },
        {
            "alt_to": [
                "Windows Glow v. 2.2",
                "Windows Glow Expansion",
                "Windows Glow - Tribunal",
                "Windows Glow - Bloodmoon (now with Raven Rock)",
            ],
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": lighting,
            "date_added": "2019-02-23 21:00:00 -0500",
            "date_updated": "2022-01-17 11:05:14 -0500",
            "description": """<p>This is a modern, pluginless replacement of the old Windows Glow mods with many more features besides! Windows Glow suffered from every mod needing a patch to work with it. Glow in the Dahrk handles this automatically through the meshes themselves with a powerful code by NullCascade which can switch the visible portion of meshes on the fly.</p>""",
            "name": "Glow in the Dahrk",
            "status": "live",
            "tags": [tag_game_expanding, tag_high_res],
            "url": nexus_mod("45886"),
            "usage_notes": """<p><span class="bold">OpenMW 0.46.0 or newer is required for this to work</span>.</p>

<p>At this time you are unable to disable light rays in the latest version, please use version 2.11.2 if you would like to play without that feature, and note that the folder paths on this page reflect the latest version.</p>

<p>The plugins are technically optional and add extra content, I do recommend using them no matter what your setup is.</p>

<p>If you're following the <span class="bold">Expanded Vanilla</span>, <span class="bold">Graphics Overhaul</span>, or <span class="bold">Total Overhaul</span> mod lists you can skip the <code>03 Telvanni Dormers on Vvardenfell</code> folder path.</p>

<p>If you're following the <span class="bold">One Day Modernization</span> list, you also want to include the <code>03 Nord Glass Windows Interior Sunrays</code> folder path.</p>""",
        },
        {
            "name": "Darknut's Lighted Dwemer Towers",
            "author": nexus_user("137283", "Darknut"),
            "category": lighting,
            "date_added": "2022-11-13 11:54:00 -0500",
            "date_updated": "2022-11-13 11:54:00 -0500",
            "description": """This mod adds windows to the Exteriors & Interiors of the Dwemer Towers & Entrances. All new MeshesAdds Lighted Windows to Dwemer ruins""",
            "status": "live",
            "url": nexus_mod("51358"),
            "usage_notes": """The Tamriel Rebuilt add-on is not compatible with the latest TR version and should not be used at this time.""",
        },
        {
            "name": "Improved Lights for All Shaders",
            "author": nexus_user("102938538", "Hemaris"),
            "category": lighting,
            "date_added": "2022-11-13 12:25:00 -0500",
            "description": """Adjusts every vanilla light mesh to enhance the effects of other shader and lighting mods. Compatible with MGE, OpenMW, ReShade, True Lights And Darkness, Enlightened Flames, and just about everything else.""",
            "status": "live",
            "url": nexus_mod("51463"),
        },
        {
            "name": "Dunmer Lanterns Replacer",
            "slug": "dunmer-lanterns-replacer",
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": lighting,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2022-11-13 12:34:00 -0500",
            "description": "This mod replaces all of the Dunmer lanterns (including the paper lanterns and streetlight) from the original game with smoother, more detailed versions (yet not with a ridiculous amount of new polies: they're optimized).",
            "status": "live",
            "tags": [tag_vanilla_friendly, tag_oaab_data],
            "url": nexus_mod("43219"),
        },
        {
            "name": "Logs on Fire",
            "author": nexus_user("4462276", "MwGek"),
            "category": lighting,
            "date_added": "2023-10-31 13:13:00 -0500",
            "description": "Changes the non-combustible vanilla logs to combustible ones. The new meshes are using a glowmap and some UV animation to give the illusion to be actually on fire.",
            "status": "live",
            "url": nexus_mod("51752"),
        },
        {
            "name": "Remove Negative Lights for OpenMW",
            "author": nexus_user("525383", "DeathDaisy") + " and Pharis",
            "category": lighting,
            "date_added": "2023-10-31 14:22:00 -0500",
            "description": "With per-pixel lighting, so called 'negative lights' in the game become dark voids. This Lua script removes them on the fly.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52487"),
        },
        {
            "name": "Harvest Lights",
            "author": "johnnyhostile",
            "category": lighting,
            "date_added": "2023-11-24 08:19:00 -0500",
            "description": " Disables lights around harvestable objects and re-enables them after the object has grown back. Dynamic, compatible with everything. OpenMW 0.49 or newer required!",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/harvest-lights/",
        },
    ]:
        generate_mod(**m)
