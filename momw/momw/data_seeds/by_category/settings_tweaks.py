from momw.helpers import generate_mod, nexus_mod, nexus_user


settings_tweaks = "Settings Tweaks"

tag_openmw_lua = "OpenMW Lua"
tag_dev_build_only = "Dev Build Only"


def init():
    # print("START: {} ... ".format(tools), end="")

    _mods = [
        {
            "name": "Distant Land And Objects",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 12:50:42 -0500",
            "description": "These settings will affect how the engine renders distant land and objects. They allow for very fine-grained control over how much distant stuff you see.",
            "url": "https://openmw.readthedocs.io/en/latest/reference/modding/settings/index.html",
            "usage_notes": """<p>To enable distant land and objects via OpenMW-Launcher:</p>

<ul>
  <li>Click the "Advanced" icon at the top of the launcher</li>
  <li>Click the "Visuals" tab</li>
  <li>Scroll down to the "Terrain" section, set "Viewing distance" to 10 cells (or any other value as desired)</li>
  <li>Set "Object paging min size" to <code>0.023</code> (this affects how much pop-in you have with distant objects)</li>
  <li>Check the box next to "Distant land"</li>
  <li>Check the box next to "Active grid object paging"</li>
</ul>

<p>To enable it via direct edits to the cfg file, ensure you've got this in your <code>settings.cfg</code>:</p>

<pre><code>[Terrain]
distant terrain = true
object paging active grid = true
object paging min size = 0.023</code></pre>

<p>Please see the <a href="/tips/performance/">Tips: Performance</a> page for more information about how to do performance tweaking and testing, as well as <a href="https://openmw.readthedocs.io/en/latest/reference/modding/settings/terrain.html?highlight=object%20paging#object-paging">the official documentation for object paging</a>, for more information about other related settings you can try to adjust for further performance gains.</p>""",
        },
        {
            "name": "Groundcover And Stomping",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 13:10:21 -0500",
            "description": "OpenMW has special settings for grass and and other types of groundcover.",
            "url": "https://openmw.readthedocs.io/en/latest/reference/modding/extended.html?highlight=groundcover#groundcover-support",
            "usage_notes": """<p>First, enable groundcover by adding <code>enabled = true</code> to the <code>[Groundcover]</code> section of your <code>settings.cfg</code> file (add the section if it isn't already there).</p>

<p>These settings can affect performance and should be adjusted carefully:</p>

<ul>
  <li>Set <code>density = 0.5</code> and adjust as desired to handle the density of your groundcover (up to <code>1.0</code>).</li>
  <li>Set <code>min chunk size = 1.0</code>, again adjusting as needed (up to <code>1.0</code>).</li>
  <li>Set <code>stomp mode = 2</code> and <code>stomp intensity = 2</code> for a "stomp" effect that's most noticeable.</li>
  <li>Setting <code>rendering distance = 6144.0</code> controls how far out the groundcover will be drawn (in this case, roughly one cell), this will likely have a noticeable impact on performance.</li>
</ul>

<p>Please see the <a href="/tips/performance/">Tips: Performance</a> page for more information about how to do performance tweaking and testing, as well as <a href="https://openmw.readthedocs.io/en/latest/reference/modding/extended.html?highlight=groundcover#groundcover-support">the official documentation for groundcover</a> (<a href="https://openmw.readthedocs.io/en/latest/reference/modding/settings/groundcover.html?highlight=groundcover">settings page</a>), for more information about other related settings.</p>""",
        },
        {
            "name": "Head Bobbing",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 13:10:21 -0500",
            "date_updated": "2022-11-19 20:25:00 -0500",
            "description": "Enable and configure a head bobbing effect for the first person view.",
            "tags": [tag_openmw_lua],
            "url": "",
            "usage_notes": """<p><span class="bold">OpenMW 0.48 or newer is required!</span> To adjust head bobbing settings:</p>

<ul>
  <li>Hit ESC</li>
  <li>Click "Options"</li>
  <li>Click "Scripts"</li>
  <li>Click "OpenMW Camera"</li>
  <li>Head bobbing settings are at the bottom, scroll down and change them as desired</li>
</ul>

<p class="bold">If you're using <a href="/mods/momw-camera/">MOMW Camera</a>, be sure to disable it if you want to change these values.</p>""",
        },
        {
            "name": "Shadows",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 13:10:21 -0500",
            "description": "Realtime shadow maps for (optionally) all game objects.",
            "url": "https://openmw.readthedocs.io/en/latest/reference/modding/settings/shadows.html?highlight=shadows",
            "usage_notes": """<p>To enable shadows via OpenMW-Launcher:</p>

<ul>
  <li>Click the "Graphics" icon at the top of the launcher</li>
  <li>Click the "Shadows" tab</li>
  <li>Check the box next to "Enable Player Shadows"</li>
  <li>Check the box next to "Enable Actor Shadows"</li>
  <li>Check the box next to "Enable Object Shadows"</li>
  <li>Check the box next to "Enable Terrain Shadows"</li>
  <!--<li>Check the box next to "Enable Indoor Shadows"</li>-->
  <li>Note that not all shadows settings are exposed via the launcher</li>
</ul>

<p>To enable shadows via direct edits to the cfg file, add these to the <code>[Shadows]</code> section of your <code>settings.cfg</code> file:</p>

<pre><code>enable shadows = true
actor shadows = true
player shadows = true
terrain shadows = true
object shadows = true</code></pre>

<p>Please see the <a href="/tips/performance/">Tips: Performance</a> page for more information about shadows performance tweaking and testing, as well as <a href="https://openmw.readthedocs.io/en/latest/reference/modding/settings/shadows.html?highlight=shadows">the official documentation for shadows</a> for more information about related settings.</p>""",
        },
        {
            "name": "Third Person Camera Improvements",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 13:10:21 -0500",
            "date_updated": "2022-11-19 20:21:00 -0500",
            "description": "OpenMW offers an optional, improved third person camera that has a more modern look and feel from the default.",
            "tags": [tag_openmw_lua],
            "url": "",
            "usage_notes": """<p><span class="bold">OpenMW 0.48 or newer is required!</span> To adjust third person camera settings:</p>

<ul>
  <li>Hit ESC</li>
  <li>Click "Options"</li>
  <li>Click "Scripts"</li>
  <li>Click "OpenMW Camera"</li>
  <li>Change settings as desired</li>
</ul>
<p class="bold">If you're using <a href="/mods/momw-camera/">MOMW Camera</a>, be sure to disable it if you want to change these values.</p>""",
        },
        {
            "name": "Water",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 13:10:21 -0500",
            "description": "OpenMW has a configurable water shader, with several parameters that can be adjusted.",
            "url": "https://openmw.readthedocs.io/en/latest/reference/modding/settings/water.html?highlight=water",
            "usage_notes": """<p>Please see <a href="/tips/performance/#water">the performance guide on configuring water</a> for detailed information about what you should do with this.</p>""",
        },
        {
            "name": "Gameplay Tweaks",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2021-05-08 13:10:21 -0500",
            "description": "OpenMW has many options for adjusting various game mechanics built in to the engine.",
            "url": "https://openmw.readthedocs.io/en/latest/reference/modding/settings/game.html",
            "usage_notes": """<p>Many of these settings are highly subjective, so it's hard for me to give a rundown of what you should or shouldn't use.</p>

<p>For the best experience:</p>

<ul>
  <li>Run the OpenMW-Launcher, click "Advanced", and take a look at each tab under that category. You can mouse over something to get a tooltip with more detailed information.</li>
  <li>Check out <a href="https://openmw.readthedocs.io/en/latest/reference/modding/settings/game.html">the official documentation</a> for these settings, as well as the rest of the documentation on settings to get a more complete idea of what can be done.</li>
</ul>""",
        },
        {
            "name": "Generate A Navmesh Cache",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2022-12-03 09:25:00 -0500",
            "description": "The Navmeshtool program comes with OpenMW and can be used to pregenerate navmeshes, thus avoiding loading pauses in game that might have been caused by doing that on the fly. It's an easy, convenient way to eliminate one category of stuttering (while continuing to enjoy the benefits of what caused it: dynamically generated navigation meshes).",
            "url": "",
            "usage_notes": """Please view <a href="/tips/navmeshtool/">the tips page for the navmeshtool program</a> for usage instructions.""",
        },
        {
            "name": "Weather Particle Occlusion",
            "author": "OpenMW Authors",
            "category": settings_tweaks,
            "date_added": "2023-11-09 15:30:00 -0500",
            "description": "Enables particle occlusion for rain and snow particle effects. When enabled, rain and snow will not clip through ceilings and overhangs. Currently this relies on an additional render pass, which may lead to a performance hit.",
            "tags": [tag_dev_build_only],
            "url": "",
        },
        {
            "name": "True Nights and Darkness",
            "author": nexus_user("45710542", "PoodleSandwich"),
            "category": settings_tweaks,
            "date_added": "2021-04-23 23:58:50 -0500",
            "description": """Darker night-time settings for OpenMW.""",
            "status": "live",
            "url": nexus_mod("46082"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
