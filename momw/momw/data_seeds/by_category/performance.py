from momw.helpers import generate_mod, nexus_mod


performance = "Performance"

# Names of tags that are used in this file.
tag_atlased_textures = "Atlased Textures"
tag_tes3mp_graphics = "TES3MP Graphics"


def init():
    for m in [
        {
            "author": "MOP Team",
            "category": performance,
            "date_added": "2018-06-10 21:00:00 -0500",
            "date_updated": "2022-12-01 17:10:00 -0500",
            "description": "Greatly improves performance and fixes some mesh errors.",
            "name": "Morrowind Optimization Patch",
            "status": "live",
            "tags": [tag_tes3mp_graphics],
            "url": nexus_mod("45384"),
        },
        {
            "author": "Project Atlas Team",
            "category": performance,
            "date_added": "2019-03-27 19:47:40 -0500",
            "date_updated": "2022-09-07 10:42:30 -0500",
            "description": """<p>The goal of Project Atlas is to identify the most performance heavy areas of vanilla Morrowind and some popular mods and target high usage/strain meshes in those areas for atlasing. This effort involves reworking the UVs for those meshes and creating texture atlases to cover various sets.</p>""",
            "name": "Project Atlas",
            "status": "live",
            "tags": [tag_atlased_textures, tag_tes3mp_graphics],
            "url": "https://github.com/MelchiorDahrk/Project-Atlas/archive/refs/heads/master.zip",
        },
    ]:
        generate_mod(**m)
