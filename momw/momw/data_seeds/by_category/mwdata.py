from momw.helpers import generate_mod


mwdata = "Modding Resources"


def init():
    _mods = [
        {
            "author": "Bethesda Softworks",
            "category": mwdata,
            "date_added": "2023-07-27 18:51:00 -0500",
            "description": """TODO.""",
            "name": "Morrowind",
            "status": "live",
            "url": "#TODO",
        }
    ]

    for m in _mods:
        generate_mod(**m)
