from momw.helpers import generate_mod, nexus_mod


fonts: str = "Fonts"

# Names of tags that are used in this file.
tag_high_res: str = "High Res"
tag_tes3mp_graphics: str = "TES3MP Graphics"


def init():
    mods: list = [
        {
            "author": "Isaskar and Georgd",
            "category": fonts,
            "date_added": "2019-12-25 16:40:45 -0500",
            "date_updated": "2023-01-16 11:11:00 -0500",
            "description": """<p>Pre-defined setup of TrueType fonts for OpenMW.</p>

<p>Requires OpenMW 0.45 or newer, optional hi-res UI button textures require 0.46 or newer.</p>""",
            "extra_cfg": """<p>Add this to your <code>openmw.cfg</code> after all the other <code>fallback=</code> lines:</p>

<pre><code>fallback=Fonts_Font_0,pelagiad
fallback=Fonts_Font_2,ayembedt</code></pre>""",
            "extra_cfg_raw": """#
# TrueType fonts for OpenMW
# (this goes into openmw.cfg after
# all the other fallback= lines)
#
fallback=Fonts_Font_0,pelagiad
fallback=Fonts_Font_2,ayembedt""",
            "is_featured": True,
            "name": "TrueType fonts for OpenMW",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics, "Manual Edit Needed"],
            "url": nexus_mod("46854"),
            "usage_notes": """<p>Instructions for OpenMW 0.48 and newer:</p>
<ul>
  <li>Download and extract the main file as well as the optional "HD texture buttons" file for the language of your choosing.
    <ul>
      <li>Font files such as <code>.ttf</code> and <code>.omwfont</code> files should go into a <code>Fonts</code> folder in the root of this mod's data path.</li>
    </ul>
  </li>
  <li>Rename the provided <code>Fonts/openmw_font.xml</code> file to <code>Fonts/pelagiad.omwfont</code>.</li>
  <li>Create another file: <code>Fonts/ayembedt.omwfont</code> and put the following contents into it:
    <pre><code>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;MyGUI type="Resource" version="1.1"&gt;
    &lt;Resource type="ResourceTrueTypeFont" name="Daedric"&gt;
        &lt;Property key="Source" value="Ayembedt.ttf"/&gt;
        &lt;Property key="Antialias" value="false"/&gt;
        &lt;Property key="TabWidth" value="8"/&gt;
        &lt;Property key="OffsetHeight" value="0"/&gt;
        &lt;Codes&gt;
            &lt;Code range="32 126"/&gt;
        &lt;/Codes&gt;
    &lt;/Resource&gt;
&lt;/MyGUI&gt;</code></pre>
  </li>
  <li>Add the snippet from <a href="#extra-cfg">the extra cfg section</a> to your <code>openmw.cfg</code>.</li>
  <li>The new fonts and high-res buttons should now work as expected.  Please see <a href="https://openmw.readthedocs.io/en/master/reference/modding/font.html#truetype-fonts">the official OpenMW documentation</a> as a reference for how to install custom fonts such as these.</li>
</ul>""",
        },
        {
            "alt_to": ["TrueType fonts for OpenMW"],
            "name": "Alternative TrueType Fonts",
            "author": "Isaskar and Georgd",
            "category": fonts,
            "date_added": "2023-12-23 09:54:23 +0100",
            "description": """Fonts to replace the default fonts used by OpenMW: Pelagiad by Isak Larborn and Ayembedt by Georg Duffner.""",
            "status": "live",
            "tags": [tag_high_res, tag_tes3mp_graphics],
            "url": "",
            "dl_url": "/files/AlternativeTrueTypeFonts.7z",
        },
    ]

    for m in mods:
        generate_mod(**m)
