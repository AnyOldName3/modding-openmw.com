from momw.helpers import generate_category, generate_mod, read_toml_data, read_yaml_data
from momw.models import DataPath, ExtraCfg, Mod, ModList, ModPlugin, Tag, UsageNotes
from ..mod_lists import (
    generate_list,
    generate_list_with_sublists,
    generate_mod_list_obj,
    set_parent_orders,
)


test = "Test"


def init():
    # CATEGORIES
    generate_category("Test", "test", "These are test mods for use in tests.")

    # TAGS
    Tag(
        name="Test Tag",
        description="These are test tags for test mods for use in tests.",
    )

    # MODS
    for mod in [
        {
            "author": "Hodd Toward",
            "category": test,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2023-04-13 21:00:00 -0500",
            "description": "This is test mod #1's description!",
            "dl_url": "/files/TestMod1.zip",
            "is_active": True,
            "name": "Test Mod 1",
            "status": "live",
            # TODO: test tag
            "url": "https://gitlab.com/modding-openmw/build-openmw",
            "custom_folder": "MyCustomFolder",
        },
        {
            "alt_to": ["Test Mod 1"],
            "author": "Hodd Toward",
            "category": test,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2023-04-13 21:00:00 -0500",
            "description": "This is test mod #2's description!",
            "dl_url": "/files/TestMod2.zip",
            "is_active": True,
            "name": "Test Mod 2",
            "status": "live",
            "url": "https://gitlab.com/modding-openmw/build-openmw",
        },
        {
            "author": "Hodd Toward",
            "category": test,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2023-04-13 21:00:00 -0500",
            "description": "This is test mod #3's description!",
            "dl_url": "/files/TestMod3.zip",
            "is_active": True,
            "name": "Test Mod 3",
            "status": "live",
            "url": "https://gitlab.com/modding-openmw/build-openmw",
        },
        {
            "author": "Hodd Toward",
            "category": test,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2023-04-13 21:00:00 -0500",
            "description": "This is test mod #4's description!",
            "dl_url": "/files/TestMod4.zip",
            "is_active": True,
            "name": "Test Mod 4",
            "status": "live",
            "url": "https://gitlab.com/modding-openmw/build-openmw",
        },
        {
            "author": "Hodd Toward",
            "category": test,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2023-04-13 21:00:00 -0500",
            "description": "This is test mod #5's description!",
            "dl_url": "/files/TestMod5.zip",
            "is_active": True,
            "name": "Test Mod 5",
            "status": "live",
            "url": "https://gitlab.com/modding-openmw/build-openmw",
        },
        {
            "author": "Hodd Toward",
            "category": test,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2023-04-13 21:00:00 -0500",
            "description": "This is test mod #6's description!",
            "dl_url": "/files/TestMod6.zip",
            "is_active": True,
            "name": "Test Mod 6",
            "status": "live",
            "url": "https://gitlab.com/modding-openmw/build-openmw",
        },
    ]:
        generate_mod(**mod)

    # MOD LISTS
    with_sublists_list = read_toml_data("test2")
    with_sublists_obj = generate_list_with_sublists(with_sublists_list)
    set_parent_orders(ModList.objects.get(id=with_sublists_obj.id))

    flat_obj = generate_mod_list_obj(
        **{"description": "TEST 1 DESC", "status": ModList.LIVE, "title": "TEST 1"}
    )
    flat_list = read_yaml_data("test1")
    generate_list(flat_list, flat_obj)

    m1 = Mod.objects.get(slug="test-mod-1")
    m2 = Mod.objects.get(slug="test-mod-2")
    m3 = Mod.objects.get(slug="test-mod-3")
    m4 = Mod.objects.get(slug="test-mod-4")
    m5 = Mod.objects.get(slug="test-mod-5")
    m6 = Mod.objects.get(slug="test-mod-6")

    # PLUGINS
    count = ModPlugin.objects.count() + 1
    p1 = ModPlugin(
        order_number=count,
        file_name="test2.esp",
        for_mod=m2,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=False,
    )
    p1.save()
    p1.on_lists.add(flat_obj)

    count += 1
    p2 = ModPlugin(
        order_number=count,
        file_name="test2.bsa",
        for_mod=m2,
        needs_cleaning=False,
        is_bsa=True,
        is_groundcover=False,
    )
    p2.save()
    p2.on_lists.add(flat_obj)

    count += 1
    p3 = ModPlugin(
        order_number=count,
        file_name="test2gc.esp",
        for_mod=m2,
        needs_cleaning=False,
        is_bsa=True,
        is_groundcover=False,
    )
    p3.save()
    p3.on_lists.add(flat_obj)

    count += 1
    p4 = ModPlugin(
        order_number=count,
        file_name="test5.esp",
        for_mod=m5,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=False,
    )
    p4.save()
    p4.on_lists.add(with_sublists_obj)

    count += 1
    p5 = ModPlugin(
        order_number=count,
        file_name="test6.esp",
        for_mod=m6,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=False,
    )
    p5.save()
    p5.on_lists.add(with_sublists_obj)

    count += 1
    p6 = ModPlugin(
        order_number=count,
        file_name="test1.esp",
        for_mod=m1,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=False,
    )
    p6.save()
    p6.on_lists.add(with_sublists_obj)

    count += 1
    p7 = ModPlugin(
        order_number=count,
        file_name="test1.bsa",
        for_mod=m1,
        needs_cleaning=False,
        is_bsa=True,
        is_groundcover=False,
    )
    p7.save()
    p7.on_lists.add(with_sublists_obj)

    count += 1
    p8 = ModPlugin(
        order_number=count,
        file_name="test1gc.esp",
        for_mod=m1,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=True,
    )
    p8.save()
    p8.on_lists.add(with_sublists_obj)

    count += 1
    p9 = ModPlugin(
        order_number=count,
        file_name="test1.omwaddon",
        for_mod=m1,
        needs_cleaning=True,
        is_bsa=False,
        is_groundcover=False,
    )
    p9.save()

    count += 1
    p10 = ModPlugin(
        order_number=count,
        file_name="1bsa.bsa",
        for_mod=m1,
        needs_cleaning=True,
        is_bsa=True,
        is_groundcover=False,
    )
    p10.save()

    count += 1
    ModPlugin(
        order_number=count,
        file_name="test1gc2.esp",
        for_mod=m1,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=True,
    ).save()

    count += 1
    ModPlugin(
        order_number=count,
        file_name="test6gc.esp",
        for_mod=m6,
        needs_cleaning=False,
        is_bsa=False,
        is_groundcover=True,
    ).save()

    count += 1
    ModPlugin(
        order_number=count,
        file_name="2test6.bsa",
        for_mod=m6,
        needs_cleaning=False,
        is_bsa=True,
        is_groundcover=False,
    ).save()

    # DATA PATHS
    count = DataPath.objects.count() + 1
    dp1 = DataPath(order_number=count, for_mod=m1)
    dp1.extra_dirs = ["00 Core"]
    dp1.save()
    dp1.on_lists.add(with_sublists_obj)

    count += 1
    dp2 = DataPath(order_number=count, for_mod=m1)
    dp2.extra_dirs = ["One", "Two"]
    dp2.save()

    count += 1
    DataPath(order_number=count, for_mod=m2).save()

    count += 1
    DataPath(order_number=count, for_mod=m3).save()

    count += 1
    DataPath(order_number=count, for_mod=m4).save()

    count += 1
    DataPath(order_number=count, for_mod=m5).save()

    count += 1
    DataPath(order_number=count, for_mod=m6).save()
    # TODO: extra_dirs

    # USAGE NOTES
    un1 = UsageNotes(
        for_mod=m5, text="THIS IS THE USAGE NOTES FOR Test Mod 5 ON TEST2!"
    )
    un1.save()
    un1.on_lists.add(with_sublists_obj)

    un2 = UsageNotes(
        for_mod=m5, text="THIS IS THE USAGE NOTES FOR Test Mod 5 ON NO MOD LISTS!"
    )
    un2.save()

    un3 = UsageNotes(
        for_mod=m1, text="THIS IS THE USAGE NOTES FOR Test Mod 1 ON NO MOD LISTS!"
    )
    un3.save()

    # EXTRA CFG
    ec1 = ExtraCfg(text="EXTRA CFG GOES HERE!", in_settings=True)
    ec1.save()
    ec1.on_lists.add(with_sublists_obj)
    ec1.for_mods.add(m6)

    ec2 = ExtraCfg(text="OTHER EXTRA CFG GOES HERE!", in_settings=False)
    ec2.save()
    ec2.on_lists.add(with_sublists_obj)
    ec2.for_mods.add(m6)

    ec3 = ExtraCfg(text="EXTRA CFG GOES HERE!", in_settings=False)
    ec3.save()
    ec3.for_mods.add(m4)

    ec4 = ExtraCfg(text="EXTRA CFG GOES HERE!", in_settings=True)
    ec4.save()
    ec4.for_mods.add(m3)

    ec5 = ExtraCfg(text="EXTRA CFG GOES HERE!", in_settings=False)
    ec5.save()
    ec5.for_mods.add(m1)

    ec6 = ExtraCfg(text="EXTRA CFG GOES HERE!", in_settings=True)
    ec6.save()
    ec6.for_mods.add(m1)

    # TODO: CHANGELOGS
