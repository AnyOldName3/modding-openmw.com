from momw.helpers import generate_mod, nexus_mod, nexus_user

# Names of tags that are used in this file.
tag_oaab_data = "OAAB Data"
tag_tamriel_data = "Tamriel Data"
tag_normal_maps = "Normal Maps"

interiors = "Interiors"


def init():
    for m in [
        {
            "name": "Library of Vivec Enhanced",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": interiors,
            "date_added": "2023-11-04 12:21:00 -0500",
            "description": "Overhaul of the Library of Vivec to make it the interesting place it was meant to be.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("50181"),
        },
        {
            "name": "Almalexia's Chamber Overhaul",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": interiors,
            "date_added": "2023-12-11 4:54:00 -0500",
            "description": "Overhaul of Almalexia's High Chapel in Mournhold",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data, tag_normal_maps],
            "url": nexus_mod("52737"),
        },
        {
            "name": "Nordic Solstheim - Solstheim Interiors Overhaul",
            "author": nexus_user("59284071", "RandomPal"),
            "category": interiors,
            "date_added": "2023-09-19 12:27:00 -0500",
            "description": """Reclutters nordic interiors on Sosltheim in true nordic style.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53121"),
        },
        {
            "author": nexus_user("6219130", "Digmen"),
            "category": interiors,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Dagoth Ur made special arrangements for a friend.  Adds some decor to Dagoth Ur's chamber that's fitting for a meeting between two old friends...",
            "name": "Dagoth Ur Welcomes You",
            "status": "live",
            "url": nexus_mod("44204"),
        },
        {
            "name": "Akulakhan's best chamber",
            "author": "Duo Dinamico",
            "category": interiors,
            "date_added": "2021-08-19 08:35:46 -0500",
            "date_updated": "2022-11-07 14:27:00 -0500",
            "description": "A fully animated overhaul of Akulakhan's chamber.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("49644"),
        },
        {
            "name": "Azura's Shrine Overhaul",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": interiors,
            "date_added": "2023-10-29 18:06:00 -0500",
            "description": "Overhaul of both the exterior and interior of the Shrine of Azura.",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("53277"),
        },
        {
            "name": "The magic rock of Maar Gan",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": interiors,
            "date_added": "2023-09-12 12:00:00 -0500",
            "description": """Experience the Maar Gan shrine as the sacred place it should have been.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("50763"),
        },
        {
            "name": "Ald'ruhn-under-Skar",
            "author": "Nwahs and Mushrooms Team",
            "category": interiors,
            "date_added": "2021-10-21 20:18:58 -0500",
            "date_updated": "2023-10-19 14:23:00 -0500",
            "description": """More atmospheric Manor District. Nothing superfluous, just decorations.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("50332"),
        },
        {
            "name": "Tamriel Rebuilt - Hall of Justice Overhaul",
            "author": "DuoDinamico aka RandomPal and Vegetto",
            "category": interiors,
            "date_added": "2023-10-19 14:47:00 -0500",
            "description": """Overhaul the Hall of Justice in Old Ebonheart.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52224"),
        },
        {
            "name": "Ald'Ruhn Manor Banners",
            "author": "Caleb C",
            "category": interiors,
            "date_added": "2022-11-07 15:19:00 -0500",
            "description": "Adds new banners displaying the relevant lord's surname to Ald'Ruhn's manor district so you can actually tell what manor is which without having to approach the door.",
            "status": "live",
            "url": nexus_mod("51527"),
        },
        {
            "name": "Ald-Ruhn Mages Guild Expansion",
            "author": nexus_user("82168478", "Endify"),
            "category": interiors,
            "date_added": "2023-09-26 12:32:00 -0500",
            "description": """This mod is a light and lore-friendly expansion to the Ald-Ruhn Guild of Mages.""",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("48321"),
        },
        {
            "author": nexus_user("44525932", "Mordaxis"),
            "category": interiors,
            "date_added": "2018-04-13 21:00:00 -0500",
            "date_updated": "2020-11-22 11:59:47 -0500",
            "description": "Expands the Vivec Mages Guild to make it feel more like the regional hub of such a large institution while keeping the same feel of the original space.",
            "name": "Vivec Guild of Mages Expansion",
            "status": "live",
            "url": nexus_mod("44935"),
        },
    ]:
        generate_mod(**m)
