from momw.helpers import generate_mod, nexus_mod, nexus_user


tools = "Tools"

tag_gitlab_mods = "GitLab mods"


def init():
    # print("START: {} ... ".format(tools), end="")

    _mods = [
        {
            "name": "Build-OpenMW",
            "author": "johnnyhostile",
            "category": tools,
            "date_added": "2018-04-13 21:00:00 -0500",
            "description": "This is a helper script for compiling OpenMW and dependencies from source.  Currently known to support Debian and Void Linux.",
            "is_featured": True,
            "status": "live",
            "url": "https://github.com/hristoast/build-openmw",
        },
        {
            "name": "OpenMW-ModChecker",
            "author": "johnnyhostile",
            "category": tools,
            "date_added": "2018-07-04 21:00:00 -0500",
            "description": """<p class="bold">This has been superceded by my new <a href="/mods/openmw-validator/"><code>openmw-validator</code></a> tool! Please check that out instead, this is no longer an active project.</p>

<p>This command-line script will determine if a loaded mod is made obsolete by another mod later in the load order.</p>

<p>I wrote this script to scan my own load order to determine if any mods in this site's recommended list were in fact not needed at all due to being overwritten.  This should be useful for anyone who's got a mod list that includes things not explicitly listed in the recommended list.</p>""",
            "is_active": False,
            "status": "live",
            "url": "https://github.com/hristoast/OpenMW-ModChecker",
            #             "usage_notes": """<ol>
            #     <li>Clone the repository: <code>git clone https://github.com/hristoast/OpenMW-ModChecker.git</code></li>
            #     <li>Inspect the script (if desired, it's not huge).</li>
            #     <li>See how it works: <code>./openmw-modchecker.py --help</code></li>
            # </ol>
            # <p>Python 3 is required, this should run on any OS but has only been used on Linux.  License is <a href='https://www.gnu.org/licenses/gpl-3.0.txt'>GPLv3</a> (or greater).</p>""",
        },
        {
            "name": "Project Atlas Scripts",
            "author": "johnnyhostile",
            "category": tools,
            "date_added": "2019-03-10 19:03:00 -0500",
            "description": """<p>Bash script variant of the <code>.bat</code> files that come with <a href="/mods/project-atlas/">Project Atlas</a> and compatible mods, for those of us that aren't using Windows.</p>""",
            "is_featured": True,
            "status": "live",
            "url": "https://github.com/hristoast/atlas-project-scripts",
            "usage_notes": """<p>Consult <a href="https://github.com/hristoast/atlas-project-scripts#usage">the README</a> for full usage instructions.</p>""",
        },
        {
            "name": "OMWLLF",
            "author": '<a href="https://github.com/jmelesky">John Melesky</a>',
            "category": tools,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2021-01-24 15:25:09 -0500",
            "description": "This tool is absolutely essential if you use a mod that deals with leveled lists, which is very common.  If you don't know what that is, and you use more than a handful of mods with plugins, it is probably worthwhile to use this.",
            "is_featured": True,
            "status": "live",
            "url": "https://github.com/jmelesky/omwllf/blob/master/README.md",
            "usage_notes": """<p><a href='https://github.com/jmelesky/omwllf/blob/7ca3a3ba88df21bd5d8e8183d0add186b09406c3/README.md#how-to-use-this'>This link</a> should have everything you need to know.</p>

<p>If you're also using <a href="/mods/delta-plugin/">Delta Plugin</a> be sure to disable that plugin before running OMWLLF.</p>""",
        },
        {
            "name": "tes3cmd",
            "author": '<a href="https://github.com/john-moonsugar">John Moonsugar</a>',
            "category": tools,
            "date_added": "2018-12-01 10:00:00 -0500",
            "date_updated": "2021-11-10 17:37:04 -0500",
            "description": "A command line tool to examine and manipulate plugins for the Elder Scrolls game Morrowind.",
            "dl_url": "/files/tes3cmd-0.37v-2013.10.06.7z",
            "is_featured": True,
            "status": "live",
            # "url": "http://www.abitoftaste.altervista.org/morrowind/index.php?option=downloads&Itemid=50&task=info&id=103",
        },
        {
            "name": "MGE XE",
            "author": nexus_user("843673", "Hrnchamd"),
            "category": tools,
            "compat": "not working",
            "date_added": "2019-02-25 21:15:00 -0500",
            "description": """<p>Morrowind Graphics Extender XE; allows Morrowind to render distant views, scenery shadows, high quality shaders and other features.</p>""",
            "status": "live",
            "url": nexus_mod("41102"),
        },
        {
            "name": "openmw-validator",
            "author": "johnnyhostile",
            "category": tools,
            "date_added": "2021-01-16 16:56:33 -0500",
            "description": """<p>Check an <code>openmw.cfg</code> file for:</p>

<ul>
  <li>Errors such as typos on data paths, folders that don't exist, or similar mistakes</li>
  <li>A map of anything in your load order that overwrites files from another earlier load order entry - including a record of specific files that have been replaced</li>
  <li>Load order conflicts or problems that may be hard to notice in game or otherwise</li>
</ul>

<p>Very useful for making sure your config doesn't have any unintended problems after setting up a large number of mods.</p>

<p>Supports: Linux, macOS, and Windows.  Source code available on <a href="https://gitlab.com/modding-openmw/openmw-validator">GitLab</a>.</p>

<p>An AUR package (for Arch Linux and friends) can be found <a href="https://aur.archlinux.org/packages/openmw-validator/">here</a>.</p>""",
            "is_featured": True,
            "status": "live",
            "tags": [tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/openmw-validator/",
        },
        {
            "name": "vtasteks-light-fixes.sh",
            "author": "johnnyhostile",
            "category": tools,
            "date_added": "2021-01-24 15:15:50 -0500",
            "description": """<p>Export your OpenMW load order to a folder that you can use with <code>tes3cmd</code>. Useful for things like <a href="/mods/vtasteks-shaders/">Vtastek's Shaders</a>' LightFixes patch.</p>""",
            "status": "live",
            "url": "",  # TODO: rehost this and add a new link
            "usage_notes": """<p><span class="bold">NOTE: Unfortunately, this tool does not support Windows at this time.</span> If you're a Windows user and a capable scripter, please reach out to me so we can fill this usage gap! In the meantime, here's what the script does that you'd need to do by hand:</p>

<ol>
  <li>Create a "fake" Morrowind install; meaning some root folder that contains an empty <code>Data Files</code> folder and a valid <code>Morrowind.ini</code> file.</li>
  <li>Copy each plugin from your OpenMW load order into that empty <code>Data Files</code> folder. Make sure that each are dated appropriately, such that <code>Morrowind.exe</code> would read them in the correct order (by timestamp).</li>
  <li>Add each plugin to the <code>Morrowind.ini</code> file, ordered correctly.</li>
  <li>Run the <code>tes3cmd</code> command specified in Vtastek's readme.html file.</li>
  <li>If all goes well, you should now have a LightFixes.esp plugin.</li>
</ol>

<!--<p>Usage instructions for this tool can be found <a href="https://git.modding-openmw.com/Modding-OpenMW.com/vtasteks-light-fixes.sh/src/branch/master/README.md#vtasteks-light-fixes-sh">here</a>.</p>-->""",
        },
        {
            "author": "Benjamin Winger",
            "category": tools,
            "date_added": "2021-03-15 15:15:50 -0500",
            "description": """<p>Transcoder for managing a form of markup-based minimal Elder Scrolls Plugin files and OMWAddon files</p>""",
            "name": "Delta Plugin",
            "status": "live",
            "url": "https://gitlab.com/bmwinger/delta-plugin/-/releases",
        },
        {
            "name": "OpenMW Quick switch between settings",
            "author": nexus_user("2225677", "napstrike"),
            "category": tools,
            "date_added": "2021-04-11 11:13:10 -0500",
            "description": """<p>This is a windows batch script that quickly switches between two versions of files. The second versions should have the prefix "transpose_" (without quotes).</p>""",
            "status": "live",
            "url": nexus_mod("49543"),
            "usage_notes": """<span class="bold">This tool is for Windows only!</span> Please see the mod description for detailed usage instructions.""",
        },
        {
            "name": "MW Mesh Generator",
            "author": nexus_user("56866", "Yacoby"),
            "category": tools,
            "date_added": "2021-09-11 23:55:44 -0500",
            "description": """<p>This a modders resource that allows a user to place meshes on the Morrowind landscape dependent on the ground texture.</p>""",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("23065"),
            "usage_notes": """This is a Windows executable but it should run well enough via wine. Please see <a href="/tips/creating-custom-groundcover/">this page</a> for details on how to use this for generating custom grass plugins. Source code is available <a href="https://github.com/Yacoby/mw-mesh-gen">here</a>.""",
        },
        {
            "name": "OpenMW AppImage",
            "author": "OpenMW Contributors",
            "category": tools,
            "date_added": "2022-11-19 10:27:00 -0500",
            "description": """A portable Linux executable for OpenMW Launcher, OpenMW CS, Navmeshtool, and the OpenMW engine executable. Source and build instructions can be found <a href="https://gitlab.com/modding-openmw/openmw-appimage#openmw-appimage">on GitLab</a>.""",
            "status": "live",
            "url": "",
        },
        {
            "name": "Groundcoverify",
            "author": "Benjamin Winger",
            "category": tools,
            "date_added": "2023-11-09 14:45:00 -0500",
            "description": """This is a simple python script which uses DeltaPlugin to turn regular groundcover in morrowind plugins into openmw-style groundcover.""",
            "dl_url": "https://gitlab.com/bmwinger/groundcoverify/-/archive/main/groundcoverify-main.zip",
            "status": "live",
            "url": "https://gitlab.com/bmwinger/groundcoverify",
        },
        {
            "name": "waza_lightfixes",
            "author": "wazabear",
            "category": tools,
            "date_added": "2023-11-09 15:21:00 -0500",
            "description": """This is a tool which generates a plugin which fixes lights.""",
            "status": "live",
            "url": "https://github.com/glassmancody/waza_lightfixes/releases",
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
