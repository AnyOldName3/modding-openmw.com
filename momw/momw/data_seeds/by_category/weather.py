from momw.helpers import generate_mod, nexus_mod, nexus_user

weather = "Weather"

# Names of tags that are used in this file.
# tag_high_res = "High Res"
# tag_tes3mp_graphics = "TES3MP Graphics"


def init():
    for m in [
        {
            "name": "Solthas Blight Weather Pack",
            "author": nexus_user("2497139", "Solthas"),
            "category": weather,
            "date_added": "2023-10-31 12:34:00 -0500",
            "description": "Harsher and more dynamic weather. Worsening blight storms, with roaming beasts and airborne diseases. Traveling through the Blight is especially dangerous. Modular and fully configurable.",
            "status": "live",
            "url": nexus_mod("52354"),
        },
        {
            "name": "Kirel's Interior Weather",
            "author": "Kirel",
            "category": weather,
            "date_added": "2023-10-31 13:03:00 -0500",
            "description": "Plays weather sound effects in interior cells, but not in places like caves, ruins, etc and places you wouldn't expect to hear them - all with little or no FPS hit.",
            "status": "live",
            "url": nexus_mod("49278"),
        },
    ]:
        generate_mod(**m)
