from momw.helpers import (
    generate_mod,
    nexus_mod,
    nexus_user,
)


weapons = "Weapons"

# Names of tags that are used in this file.
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_normal_maps = "Normal Maps"
tag_openmw_lua = "OpenMW Lua"
tag_specular_maps = "Specular Maps"
tag_gitlab_mods = "GitLab mods"

# Names of mods that are used as alts in this file.
uc_armory = "Unification Compilation: Armory"


def init():
    for m in [
        {
            "author": nexus_user("3241081", "Reizeron"),
            "category": weapons,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": 'This is how in-game dialogues describe iron warhammers: "The iron warhammer has a single head with a balancing spike to penetrate plate armor." But in truth, iron warhammers had two heads and no spikes at all.  This mod makes them look more like how the game describes them.',
            "name": "Correct Iron Warhammer",
            "slug": "correct-iron-warhammer",
            "status": "live",
            "url": nexus_mod("43576"),
        },
        {
            "name": "True Trueflame",
            "author": nexus_user("37580", "Plangkye"),
            "category": weapons,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Mesh and texture replacements for True Flame, giving it a much fancier and firey look.",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_manual_edit_needed],
            "url": nexus_mod("33432"),
            "usage_notes": 'OpenMW 0.46.0 or newer required.  Set <code>apply lighting to environment maps = true</code> in the <code>[Shaders]</code> section of your <code>settings.cfg</code> file to fix shiny meshes, see <a href="/tips/shiny-meshes/">this page about "shiny meshes"</a>, and <a href="/getting-started/settings/">this page about the settings file</a> for more information.',
        },
        {
            "name": "Oriental Mesh Improvements",
            "alt_to": [uc_armory],
            "author": nexus_user("68289", "Psymoniser"),
            "category": weapons,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This replaces the meshes of the oriental styled weapons in Morrowind with slightly higher polycount versions. They still use Bethesda stock textures so they look different depending on what texture replacer you are running.",
            "tags": [tag_high_res],
            "status": "live",
            "url": nexus_mod("29906"),
            "usage_notes": """This is included in <a href="/mods/unification-compilation-armory/">UC: Armory</a>.""",
        },
        {
            "name": "Weapon Sheathing",
            "author": "TES3 Community",
            "category": weapons,
            "date_added": "2019-03-27 18:00:00 -0500",
            "description": """<p>Makes unreadied weapons appear on the character's hip or back.</p>""",
            "extra_cfg": """<p><span class="bold">OpenMW 0.46.0 or newer is required for this to work</span>.  <span class="bold">MWSE is not needed</span>.  Additionally, the following needs to be added to your <code>[Game]</code> section in the <code>settings.cfg</code> file:</p>

<pre><code>shield sheathing = true
weapon sheathing = true
use additional anim sources = true</code></pre>""",
            "extra_cfg_raw": """#
# Weapon Sheathing:
# (This goes into settings.cfg under [Game])
#
shield sheathing = true
weapon sheathing = true
use additional anim sources = true""",
            "status": "live",
            "url": nexus_mod("46069"),
        },
        {
            "name": "Weapon Sheathing - Tamriel Rebuilt",
            "author": nexus_user("27841505", "Leyawynn"),
            "category": weapons,
            "date_added": "2019-05-12 17:08:55 -0500",
            "description": """<p>Adds sheathing support to some weapons added by Tamriel Rebuilt.</p>""",
            "extra_cfg": """<p><span class="bold">OpenMW 0.46.0 or newer is required for this to work</span>.  <span class="bold">MWSE is not needed</span>.  Additionally, the following needs to be added to your <code>[Game]</code> section in the <code>settings.cfg</code> file:</p>

<pre><code>shield sheathing = true
weapon sheathing = true
use additional anim sources = true</code></pre>""",
            "extra_cfg_raw": """#
# Weapon Sheathing:
# This goes into settings.cfg under [Game]!
#
shield sheathing = true
weapon sheathing = true
use additional anim sources = true""",
            "status": "live",
            "url": nexus_mod("46603"),
            "usage_notes": "This mod has been merged into Tamriel_Data and is no longer available on Nexus mods.",
        },
        {
            "name": "Auto Ammo Equip for OpenMW",
            "author": "Equinox",
            "category": weapons,
            "date_added": "2019-04-27 10:25:53 -0500",
            "date_updated": "2020-10-30 11:40:00 -0500",
            "description": """<p>When a crossbow or bow is equipped, any available arrows or bolts in the inventory will be automatically equipped.</p>""",
            "status": "live",
            "url": nexus_mod("45967"),
            "usage_notes": """If you're using OpenMW 0.46, the plugin will need to be renamed to remove the non-ascii <code>ò</code> character from the name (<code>Auto Ammò Equip for OpenMW.omwaddon</code> renamed to: <code>Auto Ammo Equip for OpenMW.omwaddon</code>). Users of 0.47 unreleased builds (or later) don't need to worry about this.""",
        },
        {
            "name": "Lysol's Steel Claymore Replacer",
            "author": nexus_user("34241390", "Lysol"),
            "category": weapons,
            "date_added": "2021-08-28 13:38:32 -0500",
            "description": """A model and texture replacer of the Steel Claymore in Morrowind, Includes a specular and normal map if you run OpenMW. I mostly recommend the mod to be used in OpenMW because of this.""",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps, tag_specular_maps],
            "url": nexus_mod("49425"),
        },
        {
            "name": "Magebane Replacer",
            "author": nexus_user("49330348", "GVALT"),
            "category": weapons,
            "date_added": "2021-08-28 13:57:03 -0500",
            "description": """Magebane replacer to replace Magebane with replaced model of Magebane.""",
            "status": "live",
            "url": nexus_mod("48748"),
        },
        {
            "name": "Rubber's Weapons Pack",
            "author": nexus_user("2929833", "RubberMan"),
            "category": weapons,
            "date_added": "2021-08-28 22:14:19 -0500",
            "description": """Replaces several unique weapons and shields that repeat models in the game with new ones. As requested.""",
            "status": "live",
            "url": nexus_mod("46891"),
        },
        {
            "name": "Eltonbrand Replacer",
            "author": "Trancemaster_1988",
            "category": weapons,
            "date_added": "2021-08-28 22:23:30 -0500",
            "description": """This mod replaces the unique artifact Bluebrand with unique textures.""",
            "status": "live",
            "url": nexus_mod("46695"),
        },
        {
            "name": "Killing Spree Helluva ways to kill",
            "author": "Sandman Danae Cognatogen Denina",
            "category": weapons,
            "date_added": "2021-08-28 22:30:15 -0500",
            "description": """This mod adds close to 500 weapons to the game, either via levelled lists or hand-placed (unique weapons)""",
            "status": "live",
            "url": nexus_mod("49668"),
        },
        {
            "name": "Smart Ammo for OpenMW-Lua",
            "alt_to": ["Auto Ammo Equip for OpenMW"],
            "author": "johnnyhostile",
            "category": weapons,
            "date_added": "2022-11-11 19:08:00 -0500",
            "description": """<p><span class="bold">Requires OpenMW 0.48.0 or newer!</span> Ammo autoequip while bow/crossbow/thrown weapon readied. It will remember and prefer last hand-picked ammo if pressing Alt while equipping it. An OpenMW-Lua port of <a href="https://www.nexusmods.com/morrowind/mods/47383">Abot's original</a>.</p>""",
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/smart-ammo/",
        },
        {
            "alt_to": ["Remiros' Mod Graveyard"],
            "name": "SM The Tools of Kagrenac",
            "author": nexus_user("3755459", "ShadowMimicry"),
            "category": weapons,
            "date_added": "2022-11-12 10:29:00 -0500",
            "description": """I [ShadowMimicry] tried to give the new models the most "vanilla" look. At the same time, they have a minimum number of shapes, and high-resolution textures.""",
            "status": "live",
            "tags": [tag_high_res, tag_normal_maps],
            "url": nexus_mod("50034"),
        },
        {
            "name": "Improved Thrown Weapon Projectiles",
            "author": nexus_user("3241081", "R-Zero"),
            "category": weapons,
            "date_added": "2022-11-12 10:51:00 -0500",
            "date_updated": "2023-07-02 12:20:00 -0500",
            "description": "Makes thrown weapon projectiles fly pointy end forward and, some of them, spin in the air.",
            "status": "live",
            "tags": [tag_manual_edit_needed],
            "url": nexus_mod("44763"),
        },
        {
            "name": "New Widowmaker",
            "author": nexus_user("889608", "Kalamestari_69"),
            "category": weapons,
            "date_added": "2022-11-12 13:21:00 -0500",
            "description": "High res replacer for the Widowmaker axe.",
            "status": "live",
            "url": nexus_mod("42493"),
        },
        {
            "name": "New Bow of Shadows",
            "author": nexus_user("889608", "Kalamestari_69"),
            "category": weapons,
            "date_added": "2022-11-12 13:25:00 -0500",
            "description": "Replaces Bow of Shadows with new high ress meshes and textures with normal and reflection maps.",
            "status": "live",
            "url": nexus_mod("34418"),
        },
        {
            "name": "Animated Staff of Magnus",
            "author": nexus_user("16269634", "PikachunoTM"),
            "category": weapons,
            "date_added": "2022-11-12 13:28:00 -0500",
            "description": "Animates the Staff of Magnus. Bonus, a similar treatment was given to the Iceblade of the Monarch with particle effects and gem transparency.",
            "status": "live",
            "url": nexus_mod("51402"),
        },
        {
            "name": "Skullcrusher Redone",
            "author": nexus_user("1734939", "Billyro"),
            "category": weapons,
            "compat": "partially working",
            "date_added": "2022-11-12 13:39:00 -0500",
            "date_updated": "2022-12-05 17:29:00 -0500",
            "description": "A remake of the artifact Skullcrusher - smaller, metallic, but still reminisce of the original design.",
            "status": "live",
            "tags": [tag_manual_edit_needed],
            "url": nexus_mod("47114"),
        },
        {
            "name": "Trueflame Re-forged",
            "author": nexus_user("42970660", "Lorkhansheart"),
            "category": weapons,
            "date_added": "2022-11-20 14:54:00 -0500",
            "description": "A texture and mesh edit to True Trueflame, making the sword much more bronze to reflect the Dwarven make.",
            "status": "live",
            "tags": [tag_manual_edit_needed],
            "url": nexus_mod("50559"),
        },
        {
            "name": "Daedric Crescent Replacer",
            "author": nexus_user("65357811", "natinnet"),
            "category": weapons,
            "date_added": "2023-09-26 14:16:00 -0500",
            "description": "A mesh and texture replacer for the Daedric Crescent weapon.",
            "status": "live",
            "url": nexus_mod("46439"),
        },
        {
            "name": "Daedric Maul",
            "author": nexus_user("101892", "DagothVulgtm"),
            "category": weapons,
            "date_added": "2023-09-26 14:38:00 -0500",
            "description": "Adds a two-handed Daedric Maul to existing daedric leveled lists, as well as manually placed in two dungeons.",
            "status": "live",
            "url": nexus_mod("53218"),
        },
    ]:
        generate_mod(**m)
