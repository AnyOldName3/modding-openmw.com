from momw.helpers import generate_mod, nexus_mod, nexus_user


mod_managers = "Mod Managers"


def init():
    # print("START: {} ... ".format(mod_managers), end="")

    _mods = [
        {
            "author": "Portmod Authors",
            "category": mod_managers,
            "date_added": "2019-02-26 11:55:00 -0500",
            "description": """<p>A CLI tool for Linux, macOS, and Windows to manage mods for OpenMW.</p>""",
            "name": "Portmod",
            "status": "live",
            "url": "https://forum.openmw.org/viewtopic.php?f=40&t=5875",
            "usage_notes": """<p>See <a href="https://gitlab.com/portmod/portmod/-/wikis/Home#guide">the Portmod wiki</a> for the official usage guide.</p>""",
        },
        {
            "alt_to": ["Portmod"],
            "author": nexus_user("5699344", "AnyOldName3"),
            "category": mod_managers,
            "compat": "partially working",
            "date_added": "2019-02-26 12:01:00 -0500",
            "description": """<p>A Mod Organizer plugin to export your VFS, plugin selection and load order to OpenMW.</p>""",
            "name": "ModOrganizer-to-OpenMW",
            "status": "live",
            "url": nexus_mod("45642"),
            "usage_notes": """<iframe width="560" height="315" src="https://www.youtube.com/embed/cJvzXVi3lAg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  <p>
    The video above was produced by Rob of <a href="https://www.youtube.com/c/RobsRedHotSpot/videos">Rob's Red Hot Spot</a>, a general outline of its contents is below:
  </p>

  <p>
    If you are using any modern variant of Microsoft Windows you may choose to use <a href="https://github.com/ModOrganizer2/modorganizer/releases">Mod Organizer 2</a> and <a href="/mods/modorganizer-to-openmw/">ModOrganizer-to-OpenMW</a>.
  </p>

  <h5 id="mo2-warning" class="bold"><a href="#mo2-warning">A Warning</a></h5>

  <p>
    Please note though: MO2 doesn't fully support OpenMW, even with the plugin. So be aware that:
  </p>

  <ul>
    <li>It doesn't recognize OpenMW-native plugins (<code>omwgame</code>, <code>omwaddon</code>, <code>omwscripts</code>) and will erase them from your load order. Refer to my <a href="/cfg/">CFG Generator</a> for a complete and correct plugin load order.</li>
    <li>It treats groundcover plugins as content files, which causes serious performance issues. Be sure to disable any groundcover plugins that are added this way, and <a href="https://openmw.readthedocs.io/en/latest/reference/modding/extended.html?highlight=groundcover#groundcover-support">properly enable them as groundcover</a>.</li>
  </ul>

  <h5 id="mo2-setup" class="bold"><a href="#mo2-setup">Setup</a></h5>

  <p>
    Below is a step-by-step guide on using Mod Organizer 2 to streamline the download and installation of mods:
  </p>

  <ol>
    <li>Install <a href="https://github.com/ModOrganizer2/modorganizer/releases">Mod Organizer 2</a> and the <a href="https://www.nexusmods.com/morrowind/mods/45642">Export to OpenMW plugin</a>. Point this to your mods folder (e.g. <code>C:\\games\\OpenMWMods</code> or whatever you are using).</li>
    <li>
      Install mods in order, use any of <a href="/lists/">this website's lists</a> as a reference or use your own list/load order. Mod Organizer 2 can be used to:
      <ul>
        <li>Run <a href="http://wiki.tesnexus.com/index.php/BAIN_an_understanding">BAIN installers</a></li>
        <li>Select data folders within mod <code>.zip</code> files</li>
        <li>Merge patches with core mods</li>
        <li>Delete unwanted or conflicting files</li>
        <li>Create folders as needed</li>
        <li>Rename files as needed</li>
      </ul>
    </li>
    <li><a href="/tips/register-bsas/">Register BSAs</a> the normal way</li>
    <li>Ensure your load order matches what's given by <a href="/cfg-generator/">the CFG Generator</a>, or use Mlox to verify if using a custom list.</li>
    <li>Optional but potentially useful: rename <code>.omwaddon</code> files to <code>.esp</code>, so that Mod Organizer 2 will recognize them.</li>
    <li>Close Mod Organizer 2, run the OpenMW-Launcher and double check your plugin load order</li>
    <li><a href="https://github.com/jmelesky/omwllf/blob/master/README.md#how-to-use-this">Run OMWLLF</a> as you would normally</li>
    <li>Add the generated OMWLLF plugin to the end of your load order</li>
    <li>Run OpenMW from the launcher and enjoy!</li>
  </ol>""",
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
