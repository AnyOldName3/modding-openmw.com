from momw.helpers import generate_mod, nexus_mod, nexus_user


books = "Books"

tag_gitlab_mods = "GitLab mods"


def init():
    # print("START: {} ... ".format(ui), end="")

    _mods = [
        {
            "name": "An N'wah's Guide To Modern Culture And Mythology",
            "author": "johnnyhostile",
            "category": books,
            "date_added": "2023-05-27 12:37:00 -0500",
            "description": """<p>A companion book for the Expanded Vanilla and Total Overhaul mod lists of Modding-OpenMW.com.</p>

<p>An odd stranger greets you in Seyda Neen and gives you a “guide book” as a gift.</p>

<p>The process of going through the mod lists on Modding-OpenMW.com is a long one, and by the time you’ve finished you may have lost track of all the new content you now have to see. That’s where “The N’Wah’s Guide” comes in.</p>

<p>I created this mod with the goal of giving players an in-game guide to the new content they added via the mod lists.</p>

<p>This mod is potentially immersion-breaking and not lore friendly.</p>""",
            "status": "live",
            "tags": [tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/an-nwahs-guide-to-modern-culture-and-mythology/",
            "usage_notes": """<p><a class="bold" href="https://www.nexusmods.com/morrowind/mods/52942">Nexus mods download link</a>. Note that this mod is 100% optional for all mod list users!</p>""",
        },
        {
            "name": "All Books Color-Coded and Designed",
            "author": nexus_user("109130978", "Xero Foxx"),
            "category": books,
            "date_added": "2023-10-19 15:10:00 -0500",
            "description": """All Books each have their own unique customized design & are color-coded by subject.""",
            "status": "live",
            "url": nexus_mod("50536"),
        },
        {
            "name": "Audiobooks of Morrowind",
            "author": nexus_user("4614844", "frogstat"),
            "category": books,
            "compat": "fully working",
            "date_added": "2023-05-07 11:21:00 -0500",
            "date_updated": "2023-10-29 13:27:00 -0500",
            "description": "Turn the books in Morrowind into listenable audiobooks!",
            "status": "live",
            "url": nexus_mod("52458"),
        },
    ]

    for m in _mods:
        generate_mod(**m)

    # print("Done!")
