from momw.helpers import generate_mod, nexus_mod, nexus_user


groundcover = "Groundcover"

# Names of tags that are used in this file.
tag_game_expanding = "Game Expanding"
tag_grass = "Grass"
tag_high_res = "High Res"
tag_oaab_data = "OAAB Data"

# Names of mods that are used as alts in this file.
vurts_groundcover_for_openmw = "Vurt's Groundcover for OpenMW"


def init():
    for m in [
        {
            "name": vurts_groundcover_for_openmw,
            "author": nexus_user("6987427", "x79")
            + " and "
            + nexus_user("41808", "Vurt"),
            "category": groundcover,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2021-09-12 00:10:01 -0500",
            "description": """<p>This is Vurts Groundcover patched to work with OpenMW. Everything is included. You do not need the original mod.  I did not create these textures or meshes.   All I did is add the no collision flag to each of the 40 meshes.</p>

<p>Note:</p>

<p>This is what I did to each mesh one by one:</p>

<ol>
    <li>Open NifSkope</li>
    <li>open mesh</li>
    <li>Click NiNode</li>
    <li>Under Block Details (bottom left), Click the + next to ExtraData -> NiS... -> NiStringExtraData</li>
    <li>Under Block Details, double click the blank value column next to String Data and type NOC</li>
    <li>Save mesh</li>
<ol>

<p class="bold">ALL CREDITS GO TO VURT FOR CREATING THIS EPIC MOD.</p>
    """,
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_high_res],
            "url": nexus_mod("45464"),
        },
        {
            "name": "Ozzy's Grass Merged (OpenMW compatible)",
            "alt_to": [vurts_groundcover_for_openmw],
            "author": "Ozzy",
            "category": groundcover,
            "date_added": "2019-02-25 21:10:00 -0500",
            "description": """<p>This is a merged version of Ozzy's Grass mod for use with MGE.</p>

<p>It adds groundcover to:</p>

<ul>
    <li>Ascadian Isles</li>
    <li>Bitter Coast</li>
    <li>Grazelands</li>
    <li>Solstheim</li>
    <li>West Gash</li>
</ul>

<p>I extracted the .bsa and merged the single plugins into a single one. Ozzy gave full permission to edit and upload this mod in his original readme which I added. Full credit goes to Ozzy.</p>""",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_high_res],
            "url": "https://www.moddb.com/mods/ozzys-grass-merged-openmw-compatible",
        },
        {
            "name": "Remiros' Groundcover",
            "alt_to": [
                vurts_groundcover_for_openmw,
                "Ozzy's Grass Merged (OpenMW compatible)",
            ],
            "author": "Remiros, Vtastek, Hrnchamd",
            "category": groundcover,
            "date_added": "2021-01-27 21:02:31 -0500",
            "date_updated": "2023-05-27 11:12:00 -0500",
            "description": """This mod adds high quality grass and groundcover to almost all regions in the game as well as all Tamriel Rebuilt areas.""",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_high_res],
            "url": nexus_mod("46733"),
        },
        {
            "name": "Aesthesia Groundcover - grass mod",
            "alt_to": [
                vurts_groundcover_for_openmw,
                "Ozzy's Grass Merged (OpenMW compatible)",
                "Remiros' Groundcover",
            ],
            "author": nexus_user("6304834", "Rytelier"),
            "category": groundcover,
            "date_added": "2021-01-27 21:38:27 -0500",
            "description": """Grass for Morrowind - vanilla and Tamriel Rebuilt support. Made for every region. Makes every landscape covered in rich grass, made with vanilla aesthetics in HD. Lands will feel fuller and more lively with minimal to no performance cost on average PC.""",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_high_res],
            "url": nexus_mod("46377"),
        },
        {
            "name": "Aesthesia Groundcover Reworked",
            "alt_to": [
                vurts_groundcover_for_openmw,
                "Ozzy's Grass Merged (OpenMW compatible)",
                "Remiros' Groundcover",
                "Aesthesia Groundcover - grass mod",
            ],
            "author": nexus_user("86600168", "revenorror"),
            "category": groundcover,
            "compat": "partially working",
            "date_added": "2021-04-28 21:25:20 -0500",
            "date_updated": "2021-07-04 14:44:30 -0500",
            "description": """Reworked meshes for <a href="/mods/aesthesia-groundcover-grass-mod/">Aesthesia Groundcover</a>, with the addition of small mushrooms and rocks from <a href="/mods/remiros-groundcover/">Remiros' Groundcover</a>.""",
            "status": "live",
            "tags": [tag_game_expanding, tag_grass, tag_high_res],
            "url": nexus_mod("49144"),
        },
        {
            "name": "Turn Normal Grass and Kelp into Groundcover",
            "author": nexus_user("102938538", "Hemaris"),
            "category": groundcover,
            "date_added": "2023-01-16 13:31:00 -0500",
            "description": """Renders all the grass, ferns, and kelp in the base game as groundcover instead of statics, which looks better and improves performance. Combine it with any other grass mod for better FPS, or use it alone for a nice vanilla+ look.""",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("50020"),
        },
        {
            "name": "Lush Synthesis",
            "author": nexus_user("5210688", "acidzebra"),
            "category": groundcover,
            "date_added": "2023-06-03 13:08:00 -0500",
            "date_updated": "2023-07-02 11:03:00 -0500",
            "description": """A modular remix of popular grass mods with some new plants and flowers for Ascadian Isles, Bitter Coast, Grazelands, West Gash, and all rivers/lakes/seas. BCOM/vanilla compatible + TR addon (22.11). Uses LawnMower™ technology: 99% less clipping compared to other grass mods, guaranteed (or your money back).""",
            "status": "live",
            "tags": [tag_grass],
            "url": nexus_mod("52931"),
        },
        {
            "alt_to": ["OAAB Saplings"],
            "name": "OAAB Saplings OpenMW Groundcover Patch",
            "author": nexus_user("153862138", "MasssiveJuice")
            + ", Melchior Dahrk,"
            + " and Sophie",
            "category": groundcover,
            "date_added": "2023-11-10 20:13:00 -0500",
            "description": """Replacement patch for OAAB Saplings enabling OpenMW to use it as groundcover (OpenMW 0.47+ only). Also includes sapling groundcover patches for various tree replacers.""",
            "status": "live",
            "tags": [tag_grass, tag_oaab_data],
            "url": nexus_mod("52351"),
        },
    ]:
        generate_mod(**m)
