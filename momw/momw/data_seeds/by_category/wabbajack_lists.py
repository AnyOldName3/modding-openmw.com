from momw.helpers import generate_mod, nexus_mod, nexus_user


wabbajack_lists = "Wabbajack Lists"


def init():
    _mods = [
        {
            "name": "OpenMW Enhanced",
            "author": """<a href="https://ko-fi.com/leokhi">Okhi</a>""",
            "category": wabbajack_lists,
            "date_added": "2023-09-02 12:31:00 -0500",
            "description": """<a href="https://discord.gg/gDDnzsyY9u">Discord</a> - OpenMW Enhanced overhauls everything, graphics have been revamped and replaced along with tons of extra content, ranging from Tamriel Rebuilt and Skyrim: Home of the Nords.""",
            "status": "live",
            "url": "https://github.com/LeArby/OpenMW-Enhanced#-openmw-enhanced-",
        },
        {
            "name": "Path of the Incarnate - A Wabbajack Modlist for OpenMW",
            "author": nexus_user("9016530", "Clayby - Boneyard Creations"),
            "category": wabbajack_lists,
            "date_added": "2023-09-02 11:57:00 -0500",
            "description": """<a href="https://discord.gg/43EhRjU">Discord</a> - Path of the Incarnate is an original Wabbajack modlist for OpenMW, aiming to offer a fresh, stable, and immersive Morrowind experience.""",
            "status": "live",
            "url": nexus_mod("53437"),
        },
    ]

    for m in _mods:
        generate_mod(**m)
