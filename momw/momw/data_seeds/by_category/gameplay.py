from momw.helpers import generate_mod, nexus_mod, nexus_user, modhistory_archive_link


gameplay = "Gameplay"

# Names of tags that are used in this file.
tag_building = "Building"
tag_game_expanding = "Game Expanding"
tag_high_res = "High Res"
tag_manual_edit_needed = "Manual Edit Needed"
tag_oaab_data = "OAAB Data"
tag_openmw_lua = "OpenMW Lua"
tag_vanilla_friendly = "Vanilla Friendly"
tag_dev_build_only = "Dev Build Only"
tag_gitlab_mods = "GitLab mods"
tag_tamriel_data = "Tamriel Data"

# Names of mods that are used as alts in this file.
better_regen = "Better Regen"
birthsigns_ar_more_fun = "Birthsigns Are More Fun BAMF"
dark_brotherhood_armor_replacer_expanded = "Dark Brotherhood Armor Replacer Expanded"
delayed_db_attack_v2 = "Delayed DB Attack V2"
epic_plants = "Epic Plants"
graphic_herbalism = "Graphic Herbalism"
graphic_herbalism_add_on_no_glow = "Graphic Herbalism ADD_ON (No Glow)"
graphic_herbalism_extra_mining = "Graphic Herbalism Extra (Mining)"
graphic_herbalism_tamriel_rebuilt_add_on = "Graphic Herbalism Tamriel Rebuilt Add-on"
graphic_herbalism_tamriel_rebuilt_extra = (
    "Graphic Herbalism Tamriel Rebuilt Add-on Extra (minerals and egg clusters)"
)
house_dagoth_difficulty_mod = "House Dagoth Difficulty Mod"
magic_diversity = "Magic Diversity -COMPLETE-"
magicka_based_skill_progression_ncgd = (
    "Magicka Based Skill Progression - ncgdMW Compatibility Version"
)
magicka_based_skill_progression_better_sounds_ncgd = (
    "Magicka Based Skill Progression - Better Sounds/ncgdMW Compatible"
)
personal_effects = "Personal Effects"
races_are_more_fun = "Races Are More Fun RAMF"
tougher_sixth_house = "Tougher Sixth House"
vanilla_birthsigns_for_openmw = "Vanilla Birthsigns for OpenMW by Klorax"


def init():
    for m in [
        {
            "name": "Comfy Pillow for a restful sleep",
            "author": nexus_user("5617738", "redondepremiere"),
            "category": gameplay,
            "date_added": "2019-05-14 19:27:01 -0500",
            "description": """<p>Tired of your vampire getting their rest disturbed by Vaermina's intrusions? Are you looking for a roleplay-friendly solution without completely disabling/reducing the nightmares?</p>

<p>Comfy Pillow for a restful sleep offers such an option by giving some use to a lesser known unique object from the base game: Drarayne Thelas' extra-comfy pillow.</p>

<p>From now on, sleeping as a vampire with the pillow in your inventory will disable the nightmares. Get rid of the pillow to experience them again. Simple as that.</p>""",
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly],
            "url": nexus_mod("46613"),
            "usage_notes": """You will want to use the contents of the <code>00 Base version</code> folder, unless you are using Trainwiz's Truly Disturbing Vampire Dreams (not yet featured on this site).""",
        },
        {
            "author": nexus_user("1233897", "Danae"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Adds wearable backpacks that use left pauldron slot and grant a small feather buff.",
            "name": "Adventurer's Backpacks",
            "slug": "adventurers-backpacks",
            "status": "live",
            "url": nexus_mod("43213"),
            "usage_notes": "Be sure to read the included README for some usage tips!",
        },
        {
            "author": "Syclonix",
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Allows you to use alchemy tools while they are placed in the world by activating them, rather than them needing to be in one's inventory.",
            "is_active": False,
            "name": "At Home Alchemy",
            "slug": "at-home-alchemy",
            "status": "live",
            "url": modhistory_archive_link("87-9765"),
        },
        {
            "author": nexus_user("4504245", "Androl"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Adjusts the fatigue drain on various weapos based on their size (e.g. large, heavy weapons use more than short blades and so on).",
            "name": "Better Fatigue Usage",
            "slug": "better-fatigue-usage",
            "status": "live",
            "url": nexus_mod("43036"),
        },
        {
            "author": nexus_user("22851484", "Greywander"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Tweaks the magica regen rates for characters with at least 50 Willpower, and adds slight health regen for characters with over 50 Endurance.",
            "is_featured": True,
            "name": better_regen,
            "slug": "better-regen",
            "status": "live",
            "url": nexus_mod("44969"),
        },
        {
            "alt_to": [better_regen],
            "author": "Smokeyninja",
            "category": gameplay,
            "date_added": "2018-11-29 21:00:00 -0500",
            "description": "Designed for OpenMW. Fully configurable regen for health, stamina, and regen. Some documented gmst edits to balance stamina. includes a version without most of the gmst edits in case you want to use something else that uses the same GMSTs.",
            "is_featured": True,
            "name": "Regen And Stamina Overhaul",
            "status": "live",
            "url": nexus_mod("46179"),
            "usage_notes": "A nice alternative to <a href='/mods/better-regen/'>Better Regen</a>, don't use the two together!",
        },
        {
            # TODO: non ncgdmw version as an alt
            "author": "HotFusion4 - edited by " + nexus_user("22851484", "Greywander"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This mod makes magical skill progression based on the amount of magicka used, instead of number of spells cast. An edit of HotFusion4's original mod to be more compatible with my leveling mod, ncgdMW.",
            "name": magicka_based_skill_progression_ncgd,
            "status": "live",
            "url": nexus_mod("44973"),
            "usage_notes": """<span class="bold">Check the Spellbook item to see your magic skill progression!</span>""",
        },
        {
            "alt_to": [magicka_based_skill_progression_ncgd],
            "author": "HotFusion4 - edited by "
            + nexus_user("22851484", "Greywander")
            + " and Joao Almeida",
            "category": gameplay,
            "date_added": "2020-01-07 21:00:00 -0500",
            "description": 'This is a patched version of <a href="/mods/magicka-based-skill-progression-ncgdmw-compatility/">Magicka Based Skill Progression - ncgdMW Compatility Version</a> that will work with <a href="/mods/better-sounds/">Better Sounds</a> (and <a href="/mods/better-sounds-11-patch2/">the related patch</a>) and <a href="/mods/natural-character-growth-and-decay-mw/">Natural Character Growth and Decay - MW</a>.',
            "dl_url": "/files/MBSP-BetterSounds+ncgdMW.zip",
            "name": magicka_based_skill_progression_better_sounds_ncgd,
            "status": "live",
            # "url": nexus_mod("44973"),
            "usage_notes": """<span class="bold">Check the Spellbook item to see your magic skill progression!</span>""",
        },
        {
            "author": nexus_user("22851484", "Greywander"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Allows you to carry more and run faster, at the expense of greater fatigue usage.  An interesting take on the default fatigue mechanics.",
            "is_featured": True,
            "name": "CarryOn",
            "slug": "carryon",
            "status": "live",
            "url": nexus_mod("44972"),
        },
        {
            "author": nexus_user("2223453", "SilentNightxxx"),
            "category": gameplay,
            "date_added": "2019-05-14 20:40:32 -0500",
            "description": "Modernizes combat by removing the dice roll hit chance and changing parts of the game that were built around it. If it looks like an attack will hit then it will, assuming it doesn't get blocked. Other popular gameplay tweaks people use to modernize the game are also included.",
            "name": "Better Balanced Combat",
            "status": "live",
            "url": nexus_mod("46596"),
            "usage_notes": """<p>Take a moment to read the full description in order to find out what exactly this mod does and how it works.</p>

    <p>There are compatibility plugins for Tamriel Rebuilt and Siege at Firemoth, as well as an auto-patcher that can be used to update any mod that may need it.  Please see <a href='/tips/bbc-patching/'>this page</a> for more information about using the Auto Patcher.</p>""",
        },
        {
            "author": nexus_user("4381248", "PeterBitt"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "This mod replaces the Master Alchemy Set in the tower of the Caldera Mages Guild with a Journeyman's Alchemy Set. I dont like to be able to get such high level stuff without any effort at Level 1.",
            "name": "Caldera Apparatus",
            "slug": "caldera-apparatus",
            "status": "live",
            "url": nexus_mod("42306"),
        },
        {
            "author": nexus_user("4381248", "PeterBitt"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Disables trading with Creeper and the Mudcrab, for those who see this as an exploit and not a feature.",
            "name": "Creeper and Mudcrab No Trade",
            "status": "live",
            "url": nexus_mod("42306"),
        },
        {
            "name": "Price Balance",
            "author": nexus_user("1501467", "Dungeom"),
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Smooths the price curve of expensive items, prevents gold exploits, makes selling items easier and balances economy.",
            "status": "live",
            "url": nexus_mod("45529"),
        },
        {
            "name": "Clear Your Name",
            "author": nexus_user("3099525", "Lucevar"),
            "category": gameplay,
            "date_added": "2019-06-05 17:12:28 -0500",
            "description": "In vanilla Morrowind, you have two options for clearing a bounty: turning yourself in to the guards or joining the Thieves Guild and paying them to make the problem go away. This mod adds more options, all RP friendly.",
            "status": "live",
            "url": nexus_mod("43786"),
            "usage_notes": "There is a Polish translation available for this mod.",
        },
        {
            "author": nexus_user("1134695", "vojteek"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Based on original Avenger's Delayed DB Attack. Makes Tribunal main quest start only after significant progress in MW main quest. Optimized for performance.",
            "name": delayed_db_attack_v2,
            "status": "live",
            "url": nexus_mod("14891"),
        },
        {
            "alt_to": [delayed_db_attack_v2],
            "author": nexus_user("33332", "Dimitri Mazieres"),
            "category": gameplay,
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """<p>This is a mod that changes the Dark Brotherhood assassin's armor to one that looks more like the one in Oblivion's concept art (in my opinion, of course ;-).  None of the original DB armor statistics have been altered. All that was done was replace the relevant meshes and textures, as well as adding the mask to the NPCs inventory.  Male and female versions of the armor are included.</p>

<p>Also, there is a folder named AltTextures which contains different sets of alternative textures for the cuirass and mask, so that you can choose which kind of replacer you want.</p>

<p>Finally, new weapons have been added to replace the Dark brotherhood's arsenal, as well as customizing Dandras Vules (head of the DB in the game) with a unique look: new face, armor, and a special version of Vagabond Angel's Hissyo katana.</p>""",
            "name": dark_brotherhood_armor_replacer_expanded,
            "status": "live",
            "tags": [tag_vanilla_friendly],
            "url": nexus_mod("2678"),
            "usage_notes": """The included README states that this mod has an optional feature to delay the DB attacks, making this an alternative to standalone mods that do the same.""",
        },
        {
            "alt_to": [dark_brotherhood_armor_replacer_expanded, delayed_db_attack_v2],
            "author": nexus_user("1134695", "Cine"),
            "category": gameplay,
            "date_added": "2019-02-25 21:05:00 -0500",
            "description": """<p>Stops the Dark Assassins from attacking until you\'ve completed the main quest.</p>""",
            "name": "Tribunal Delayed",
            "status": "live",
            "tags": [tag_vanilla_friendly],
            "url": nexus_mod("33973"),
        },
        {
            "alt_to": ["FMI - Service Refusal and Contraband"],
            "name": "Dwemer and Ebony Service Refusal",
            "needs_cleaning": True,
            "author": nexus_user("8222409", "KiraMarshiku"),
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "In vanilla Morrowind it is mentioned in dialogue that Dwemer Artifacts and raw ebony are very much illegal to sell and trade. This mod makes it so that various Dwemer items and raw ebony cannot be sold or traded, with the exception of Weapons, armor, and keys.",
            "status": "live",
            "url": nexus_mod("44739"),
        },
        {
            "name": "Faction Service - Beds",
            "author": "kindi",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This mod makes it so that you need to either pay to sleep in a faction-owned bed, or be high enough rank that you can sleep for free.",
            "status": "live",
            "url": nexus_mod("45196"),
        },
        {
            "author": nexus_user("15809479", "thelawfull"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "It's not hard to end up with a strong enough character to make this fight insanely hard or flat out impossible. This mod deletes the script part that makes her level with player.",
            "name": "Gedna No Longer Immortal",
            "status": "live",
            "url": nexus_mod("44121"),
        },
        {
            "name": graphic_herbalism,
            "needs_cleaning": True,
            "author": "ManaUser",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "As in most herbalism mods, you pick plants just by activating them instead of opening them like a container. But in this mod only the part you pick disappears instead of the whole plant. Includes a purist version and a modular esp with slightly more daring changes. Has a patch supporting Tamriel Rebuilt.",
            "status": "live",
            "url": nexus_mod("43140"),
            "usage_notes": "Absolutely essential - this site will soon include a guide on how to patch for Tamriel Rebuilt.",
        },
        {
            "name": graphic_herbalism_extra_mining,
            "needs_cleaning": True,
            "author": "ManaUser",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "An add-on for Graphic Herbalism that enables support for mining containers such as ores.",
            "status": "live",
            "url": nexus_mod("43140"),
        },
        {
            "name": graphic_herbalism_tamriel_rebuilt_add_on,
            "author": "ManaUser",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Tamriel Rebuilt compatibility for Graphic Herbalism!",
            "status": "live",
            "url": nexus_mod("43140"),
            "usage_notes": """<p>This plugin needs to be patched with the latest Tamriel Rebuilt patcher -- please see <a href='/tips/tr-patcher/'>this page</a> for more information.</p>""",
        },
        {
            "name": graphic_herbalism_tamriel_rebuilt_extra,
            "author": "ManaUser",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "An extra for an add-on! Adds even more compatibility with Tamriel Rebuilt.",
            "status": "live",
            "url": nexus_mod("43140"),
            "usage_notes": """<p>The file to download is at the bottom of the files tab.  It plugin needs to be patched with the latest Tamriel Rebuilt patcher -- please see <a href='/tips/tr-patcher/'>this page</a> for more information.</p>""",
        },
        {
            "alt_to": [
                epic_plants,
                graphic_herbalism,
                graphic_herbalism_add_on_no_glow,
                graphic_herbalism_extra_mining,
                graphic_herbalism_tamriel_rebuilt_add_on,
                graphic_herbalism_tamriel_rebuilt_extra,
            ],
            "name": "Graphic Herbalism - MWSE and OpenMW Edition",
            "author": "Stuporstar and Greatness7",
            "category": gameplay,
            "date_added": "2019-05-04 15:36:05 -0500",
            "date_updated": "2022-11-15 20:08:00 -0500",
            "description": "Automatically harvests herbs just like the old Graphic Herbalism, but using the container's leveled lists -- no plugins or scripts are required!",
            "status": "live",
            "tags": [tag_high_res],
            "url": nexus_mod("46599"),
        },
        {
            "name": house_dagoth_difficulty_mod,
            "author": "relap5e",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Increases the difficulty of Sixth House enemies by significantly upping their stats.",
            "is_active": False,
            "status": "live",
            "url": modhistory_archive_link("90-15348"),
            "usage_notes": "If you've played the main quest countless times and want a new challenge, or if you just like your game more on the brutal side, this is essential!",
        },
        {
            "name": tougher_sixth_house,
            "author": "Sabregirl",
            "category": gameplay,
            "date_added": "2019-03-30 13:49:44 -0500",
            "description": """<p>This mod modifies the Dwemer strongholds of Venymal and Odrosal to be much more difficult. Finding Kragrenac's tools is no longer easy!</p>

<p>A dwemer ruin dungeon generator was used to create the new areas - 9 new cells for each dwemer ruin. All of them are interconnected and make a true dungeon maze worthy of the Nerevarine. Originally I wanted to do Dagoth Ur as well but I ran out of steam.</p>

<p>I HAND placed all of the new creatures in the pre-generated cells.  They are THICK and include the original dwemer creatures as well!</p>

<p>NEW and more powerful versions of the Sixth house creatures have been added to the new areas and some of the old ones.</p>

<p>Nearly all Dagoths (anything with the name "Dagoth")  have been made more powerful (higher attack values and soul gem values) and a script to teleport the remaining ash vampires to the heart chamber for the final fight - Borrowed from Sinner's Sixth house advanced mod. Due to the fact that the two mods do similar things this mod *may* conflict with Sinner's Sixth House Advanced. They may be okay if this mod loads second but it has not been tested!</p>

<p>Note: This mod has been made by Sabregirl. I've only reuploaded it.</p>""",
            "is_active": False,
            "needs_cleaning": True,
            "status": "live",
            "url": nexus_mod("6046"),
            "usage_notes": """The README states this has beeb 'cleaned with TESAME', but when I ran <code>tes3cmd</code> it did find junk records.""",
        },
        {
            "name": "Lower First Person Sneak Mode",
            "author": "Androl",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "This simple mod lowers the position of the first person camera when sneaking/crouching.",
            "status": "live",
            "url": nexus_mod("43108"),
        },
        {
            "name": "Marksman Overhaul",
            "needs_cleaning": True,
            "author": "Xenn",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Adds new Longbows, Short Bows, and Crossbows, as well as rebalances Morrowind's default bow stats.",
            "status": "live",
            "url": nexus_mod("22933"),
        },
        {
            "name": "Projectile Overhaul - Modular",
            "author": nexus_user("75950", "Mr.Magic"),
            "category": gameplay,
            "date_added": "2019-03-02 18:33:00 -0500",
            "description": "Faster projectiles and/or magic missiles, higher recovery chance, weightless arrows. Pick and choose.",
            "status": "live",
            "url": nexus_mod("43195"),
        },
        {
            "name": "Morrowind Go To Jail",
            "author": nexus_user("1553552", "LegoManIAm94"),
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2022-01-23 12:58:22 -0500",
            "description": "When you get arrested by a guard he will take you to jail and you can choose to serve your time by resting in the bedroll or try to escape.",
            "status": "live",
            "tags": [tag_vanilla_friendly],
            "url": nexus_mod("37792"),
            "usage_notes": """<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean "Go To Jail.esp"

CLEANING: "Go To Jail.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
 Cleaned redundant AMBI,WHGT from CELL: buckmoth legion fort, prison
 Cleaned redundant AMBI,WHGT from CELL: ebonheart, hawkmoth legion garrison
 Cleaned redundant AMBI,WHGT from CELL: moonmoth legion fort, prison towers
 Cleaned redundant AMBI,WHGT from CELL: pelagiad, south wall
 Cleaned redundant AMBI,WHGT from CELL: mournhold, royal palace: jail
 Cleaned redundant AMBI,WHGT from CELL: fort frostmoth, prison
Output saved in: "Clean_Go To Jail.esp"
Original unaltered: "Go To Jail.esp"

Cleaning Stats for "Go To Jail.esp":
             redundant CELL.AMBI:     6
             redundant CELL.WHGT:     6</code></pre>
</details>""",
        },
        {
            "name": "Morrowind Go To Jail Mournhold and Solstheim",
            "needs_cleaning": True,
            "author": nexus_user("1553552", "LegoManIAm94"),
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2022-01-23 12:58:02 -0500",
            "description": "Compatibility plugin for the two official expansion packs.",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("37792"),
            "usage_notes": """<p>This mod no longer exists.  Instead, you want <a href="/mods/morrowind-go-to-jail/">Morrowind Go To Jail</a> which was updated to support Tribunal and Bloodmoon.</p></p>""",
        },
        {
            "name": "Wait and Sleep for OpenMW",
            "author": "kindi",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "date_updated": "2020-04-10 21:00:00 -0500",
            "description": """A newer and revamped version of the mod "No Resting Outdoors" that is now renamed to "Wait and Sleep for OpenMW".""",
            "status": "live",
            "url": nexus_mod("45198"),
            "usage_notes": """I recommend the "Cot Version", but please be sure to check out <a href="https://www.nexusmods.com/morrowind/mods/45198?tab=docs">the documentation</a> for a full description of what the options are.""",
        },
        {
            "name": "On the move - the Ashlander Tent Deluxe remod",
            "author": nexus_user("1134695", "vojteek"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "date_updated": "2020-06-01 08:37:48 -0500",
            "description": 'Remake of the classic "Ashlander Tent DX" mod, which added a portable nomadic dwelling.',
            "status": "live",
            "url": nexus_mod("45035"),
            "usage_notes": """<p>The TR compatibility plugin is not needed, use <a href="/mods/portable-homes-openmw-script-override/">Portable Homes OpenMW script override</a> instead (list followers, it's coming up).</p>

<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean "On the Move.esp"

CLEANING: "On the Move.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
Loaded cached Master: <DATADIR>/tribunal.esm
Loaded cached Master: <DATADIR>/bloodmoon.esm
 Cleaned redundant WHGT from CELL: seyda neen, arrille's tradehouse
Output saved in: "Clean_On the Move.esp"
Original unaltered: "On the Move.esp"

Cleaning Stats for "On the Move.esp":
             redundant CELL.WHGT:     1</code></pre>
</details>""",
        },
        {
            "author": nexus_user("6953055", "Centrello"),
            "category": gameplay,
            "date_added": "2018-04-11 21:00:00 -0500",
            "description": "Want to see how much real time Morrowind (on OpenMW) is taking from your real life? This OpenMW mod tracks real time played on a character, with (default) hourly notices. A console script allows manual checking of character age.",
            "name": "Played Time for OpenMW",
            "status": "live",
            "url": nexus_mod("45125"),
        },
        {
            "name": "TimeScale Change - OpenMW",
            "author": nexus_user("6903201", "HunterSpark"),
            "category": gameplay,
            "date_added": "2019-02-25 22:00:00 -0500",
            "description": """<p>This is simply a TimeScale Change mod, which changes the Timescale from 30:1 to 15:1.</p>""",
            "status": "live",
            "url": nexus_mod("46332"),
        },
        {
            "name": "Speechcraft Rebalance",
            "author": "Androl",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Changes a few game settings related to Speechcraft. With this mod, you should succeed at Speechcraft at least half of the time, regardless of the NPC's disposition, but your Speechcraft skill and the corresponding die roll now has a bigger impact on the NPC's disposition change.",
            "status": "live",
            "url": nexus_mod("43113"),
        },
        {
            "name": "Talrivian's State-Based HP Mod",
            "author": "Talrivian & HotFusion",
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Changes how player health is generated.  Conflicts with <a href='/mods/natural-character-growth-and-decay-mw/'>Natural Character Growth and Decay</a> so it's not on the recommended list.",
            "is_active": False,
            "status": "live",
            "url": modhistory_archive_link("98-6521"),
        },
        {
            "name": "Quill of Feyfolken - Scroll Enchanting",
            "author": "Dormouse",
            "category": gameplay,
            "date_added": "2018-08-05 21:00:00 -0500",
            "date_updated": "2020-10-30 12:00:00 -0500",
            "description": "This is a scroll making mod that will make enchanting scrolls a viable option in Morrowind. Without this mod, enchanting scrolls is just as difficult as enchanting jewelry, but they can only be cast once. This makes enchanting scrolls pointless. The purpose of this mod is to counter these limitations.",
            "dl_url": "/files/Quill_of_Feyfolken_20.7z",
            "is_active": False,
            "status": "live",
            "tags": [tag_game_expanding],
            "url": "",
            "usage_notes": """<ol>
    <li>Download the original archive from the link above (the "Download Link" link).</li>
    <li>Download the patched plugin from <a href="https://www.nexusmods.com/morrowind/mods/48955?tab=files&file_id=1000021374">Nexus Mods (Various patches by alvazir)</a>.</li>
    <li>Install the mod as you normally would, be sure to load the patched plugin after the original (the load order advised by this website takes care of that).</li>
</ol>
<p>The original plugin does not fully work with OpenMW due to script errors, this is why the patched plugin is required.  See <a href='https://forum.openmw.org/viewtopic.php?f=40&t=5321'>this thread</a> for more details.</p>

<details>
<summary>
<p>The plugin is dirty <span class="bold">but will be auto-cleaned by <a href="/mods/delta-plugin/">DeltaPlugin</a></span> (click to expand/collapse):</p>
</summary>

<pre><code>$ tes3cmd clean "Quill of Feyfolken 2.0.esp"

CLEANING: "./Quill of Feyfolken 2.0.esp" ...
Loaded cached Master: <DATADIR>/morrowind.esm
 Cleaned duplicate object instance (llirala sendas FRMR: 150564) from CELL: llirala's shack
 Cleaned duplicate object instance (de_r_chest_01_sanctus FRMR: 150559) from CELL: llirala's shack
 Cleaned redundant AMBI,WHGT from CELL: llirala's shack
Output saved in: "./Clean_Quill of Feyfolken 2.0.esp"
Original unaltered: "./Quill of Feyfolken 2.0.esp"

Cleaning Stats for "./Quill of Feyfolken 2.0.esp":
       duplicate object instance:     2
             redundant CELL.AMBI:     1
             redundant CELL.WHGT:     1</code></pre>
</details>""",
        },
        {
            "name": personal_effects,
            "author": nexus_user("3241081", "R-Zero"),
            "category": gameplay,
            "compat": "not working",
            "date_added": "2019-08-03 17:31:01 -0500",
            "description": """Seyda Neen Census and Excise now returns your character's personal effects upon release. What items you get depends on your character's class.""",
            "status": "live",
            "url": nexus_mod("45338"),
            "usage_notes": """This mod is known to have issues with OpenMW, see <a href="https://gitlab.com/OpenMW/openmw/issues/4594">this</a> and <a href="https://gitlab.com/OpenMW/openmw/issues/4885">this</a> GitLab issue<!-- TODO dig up this issue (as well as <a href="https://git.modding-openmw.com/Modding-OpenMW.com/momw/issues/367">this site's own issue on it</a>)-->.""",
        },
        {
            "name": "Classed Effects",
            "alt_to": [personal_effects],
            "author": "EquiNox",
            "category": gameplay,
            "date_added": "2019-08-03 15:13:30 -0500",
            "description": """Get personal items based on your class. Choosing pre-made classes is now much more considerable. Start your character with some extra fun and make your roleplay more interesting.""",
            "status": "live",
            "url": nexus_mod("46916"),
            "usage_notes": """In my testing, there seemed to be some compatibility issues between this and <a href="/mods/better-chargen/">Better Chargen</a><!-- TODO dig up this issue, see <a href="https://git.modding-openmw.com/Modding-OpenMW.com/momw/issues/385">this link</a> for more details-->.""",
        },
        {
            "name": "True Lights and Darkness",
            "author": nexus_user("876720", "Booze"),
            "category": gameplay,
            "date_added": "2018-04-21 21:00:00 -0500",
            "description": "Darkens all interior cells and increases radius of all light sources. Light and shadows become more dynamic, locations that had very little light are a lot darker now, and places where there are no light sources at all are now truly dark! There is also sunlight though (some) windows.",
            "extra_cfg": """<p>Be sure that <code>clamp lighting = false</code> is in your settings under the <code>[Shaders]</code> section, or else lights won't look right.  Additionally, these default <code>openmw.cfg</code> values need to be changed for the mod to work properly:</p>

<pre><code>fallback=LightAttenuation_UseConstant,0
fallback=LightAttenuation_ConstantValue,0.0
fallback=LightAttenuation_UseLinear,1
fallback=LightAttenuation_LinearMethod,1
fallback=LightAttenuation_LinearValue,3.0
fallback=LightAttenuation_LinearRadiusMult,1.0
fallback=LightAttenuation_UseQuadratic,0
fallback=LightAttenuation_QuadraticMethod,2
fallback=LightAttenuation_QuadraticValue,16.0
fallback=LightAttenuation_QuadraticRadiusMult,1.0
fallback=LightAttenuation_OutQuadInLin,0</code></pre>

<p>Need to be changed to:</p>

<pre><code>fallback=LightAttenuation_UseConstant,1
fallback=LightAttenuation_ConstantValue,0.382
fallback=LightAttenuation_UseLinear,1
fallback=LightAttenuation_LinearMethod,1
fallback=LightAttenuation_LinearValue,1
fallback=LightAttenuation_LinearRadiusMult,1.0
fallback=LightAttenuation_UseQuadratic,1
fallback=LightAttenuation_QuadraticMethod,2
fallback=LightAttenuation_QuadraticValue,2.619
fallback=LightAttenuation_QuadraticRadiusMult,1.0
fallback=LightAttenuation_OutQuadInLin,0</code></pre>""",
            "extra_cfg_raw": """#
# True Lights and Darkness
# (This goes into openmw.cfg)
#
fallback=LightAttenuation_UseConstant,1
fallback=LightAttenuation_ConstantValue,0.382
fallback=LightAttenuation_UseLinear,1
fallback=LightAttenuation_LinearMethod,1
fallback=LightAttenuation_LinearValue,1
fallback=LightAttenuation_LinearRadiusMult,1.0
fallback=LightAttenuation_UseQuadratic,1
fallback=LightAttenuation_QuadraticMethod,2
fallback=LightAttenuation_QuadraticValue,2.619
fallback=LightAttenuation_QuadraticRadiusMult,1.0
fallback=LightAttenuation_OutQuadInLin,0
#
# True Lights and Darkness
# (This goes into settings.cfg, under [Shaders])
#
clamp lighting = false""",
            "needs_cleaning": True,
            "status": "live",
            "url": nexus_mod("39605"),
            "usage_notes": """<p>It's in the description but this is worth repeating: <span class="bold">this mod makes the game actually dark!</span>.  Torches or magic spells are fully needed in areas with no light.</p>

<p>Unfortunately, since the game was not designed with realistic lighting in mind, many interior cells are way too dark (Vivec and Ald-Ruhn Mage's Guilds, for instance).  Despite that, I fully recommend this mod!</p>

<p>Be sure to apply the extra configs that are required for this mod to work properly!</p>""",
        },
        {
            "author": nexus_user("52373446", "Klorax"),
            "category": gameplay,
            "date_added": "2019-02-23 21:00:00 -0500",
            "description": """<p>Vanilla Birthsigns for OpenMW by Klorax changes the behavior of the attribute enhancing birthsigns (the Lady, the Lover, and the Steed) to match that of original Morrowind.</p>

<p>This is accomplished by replacing the constant magic effect with base attributes upon finishing character generation (i.e., when leaving the census office building).  When this happens the player is notified.  Thus you need to start a new game to use this addon.</p>

<p>In original Morrowind these constant magic effects for attributes are treated as base attributes.  This is inconsistent to any other constant magic effect (i.e., spell or enchant) for attributes.  OpenMW removes this inconsistency, which is good IMHO, but this makes the original birthsigns behave differently (mostly less powerful), and this addon aims to fix this.</p>

<p>This means that the provided endurance will now count for level up health.  All relevant attributes (agility, endurance, personality, and speed) will now count towards skill level caps for governed skills.  The attribute base values (through leveling) will not be able to get past 100.</p>""",
            "name": vanilla_birthsigns_for_openmw,
            "status": "live",
            "tags": [tag_game_expanding, tag_vanilla_friendly],
            "url": nexus_mod("46033"),
        },
        {
            "alt_to": [vanilla_birthsigns_for_openmw],
            "author": nexus_user("610764", "Alaisiagae"),
            "category": gameplay,
            "date_added": "2019-02-25 21:00:00 -0500",
            "description": """<p>I wanted to make the original birthsigns a bit more fun and interesting to play. They are now more powerful than the original abilities/powers/spells because I felt that, at higher character levels, a birthsign could be replaced by a piece of enchanted clothing or a custom spell. Hopefully, the new abilities/powers/spells are also balanced and thus are not overpowering. I have drawn inspiration (directly, and modified) from Tyrthyllanos's 'Expanded Birthsigns for Purists' mod; also, I was inspired by Doy and IceNine0's 'The Definitive Birthsigns Pack' mod. !!!This mod does not appear to work correctly with existing characters.</p>""",
            "name": birthsigns_ar_more_fun,
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("17888"),
        },
        {
            "author": nexus_user("610764", "Alaisiagae"),
            "category": gameplay,
            "date_added": "2019-02-25 21:00:00 -0500",
            "description": """<p>A lore-friendly mod that changes the Races's attributes, skill bonuses, powers, abilities, and spells to be more true to lore - and more fun.</p>""",
            "name": races_are_more_fun,
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("21875"),
        },
        {
            "name": "Multiple Teleport Marking (OpenMW and Tamriel Rebuilt)",
            "author": "Marcel Hesselbarth - rot-edit",
            "category": gameplay,
            "date_added": "2019-03-01 18:15:00 -0500",
            "date_updated": "2023-07-12 11:42:00 -0500",
            "description": """<p>Replaces the built-in mark & recall spells with a version that allows marking up to 12 different places.</p>""",
            "status": "live",
            "tags": [tag_game_expanding],
            "url": nexus_mod("44825"),
            "usage_notes": """<p>Download these files:</p>

<ul>
  <li>Multiple Teleport Marking Mod (OpenMW and TR and provinces) TR 2101 update (Note that this is not compatible with the latest TR release)</li>
  <li>MultiMark - Mysticism Balance</li>
  <!--<li>Province Cyrodiil Stirk August 2018 Plugin</li>-->
  <li>Skyrim HotN January 2021 Plugin (Skip if following the One Day Morrowind Modernization mod list)</li>
  <li>Uvirith's Legacy plugin (Skip if following the One Day Morrowind Modernization mod list)</li>
</ul>""",
        },
        {
            "author": nexus_user("84095", "Arkann"),
            "category": gameplay,
            "date_added": "2019-03-01 18:05:00 -0500",
            "description": """<p>Magic Diversity's aim is to make your experience as a mage more interesting by giving every magic effect a new, unique HD look (this includes casting animations, magic bolts and hit/area effects).</p>""",
            "name": magic_diversity,
            "status": "live",
            "tags": [tag_game_expanding, tag_high_res],
            "url": nexus_mod("43380"),
            "usage_notes": """<p>Use the "Vanilla Sounds" option if you plan to also use <a href="/mods/better-sounds/">Better Sounds</a>, and any of the extra shield options you see fit.</p>""",
        },
        {
            "alt_to": [magic_diversity],
            "author": nexus_user("180261", "F.I.M."),
            "category": gameplay,
            "date_added": "2019-03-30 14:07:34 -0500",
            "description": """<p>spell effect texture replacer</p>""",
            "name": "FIMs Better Spell Effects",
            "status": "live",
            "tags": [tag_game_expanding, tag_high_res],
            "url": nexus_mod("37072"),
        },
        {
            "author": nexus_user("26153919", "NullCascade"),
            "category": gameplay,
            "compat": "not working",
            "date_added": "2018-08-05 21:00:00 -0500",
            "description": """This simple mod attempts to balance Morrowind's alchemy system by restricting how many potions the player can consume at once. By default this uses the same cooldown that NPCs must use, but supports custom consumption modules. It comes bundled with a vanilla NPC configuration and an Oblivion-style configuration.""",
            "name": "Controlled Consumption",
            "status": "live",
            "url": nexus_mod("45624"),
            "usage_notes": "This mod requires MWSE, which is why it doesn't work for OpenMW.  Future compatibility is uncertain.",
        },
        {
            "author": "Dave Humphrey & Erstam",
            "category": gameplay,
            "date_added": "2019-08-04 15:52:33 -0500",
            "description": """This plugin adds over 400 furniture items that you can pick up and move, allowing you to completely alter your own house layout while in the game... the only thing you gotta find is an empty house (now supplied as well). Items include all the typical house furnishings (tables, chairs, beds, etc...), plants and trees for decorating outside your house, and even placeable containers so you can store all your loot.""",
            "is_active": False,
            "name": "Dave Humphrey's Furniture Store v2.01",
            "status": "live",
            "tags": [tag_building],
            "url": modhistory_archive_link("72-7264"),
            "usage_notes": """If you're using this mod as a companion to <a href="/mods/morrowind-constructoropenmw-only/">Morrowind Constructor</a>, make sure to use the <code>dh_furn.esp</code>,<code>dh_furn_stores.esp</code>, and <code>dh_thriftshop.esp</code> plugins.""",
        },
        {
            "author": nexus_user("790766", "thewiz1"),
            "category": gameplay,
            "date_added": "2019-08-04 15:57:14 -0500",
            "description": """<p>Mod that allows you to build a house, foundation, even Vivec Cantons, anywhere you want!</p>
        <p>Requires you to run the OpenMW engine.</p>""",
            "name": "Morrowind Constructor(OpenMW Only)",
            "status": "live",
            "tags": [tag_building],
            "url": nexus_mod("46425"),
            "usage_notes": """Goes great with a furniture mod of some kind, such as <a href="/mods/dave-humphreys-furniture-store/">Dave Humphrey's Furniture Store</a>.""",
        },
        {
            "alt_to": [
                birthsigns_ar_more_fun,
                races_are_more_fun,
                vanilla_birthsigns_for_openmw,
            ],
            "author": nexus_user("1856623", "King Ink"),
            "category": gameplay,
            "date_added": "2020-01-19 12:02:09 -0500",
            "description": """Makes sensible changes to birthsigns and races to make each one unique and interesting while sticking to the original concept, staying reasonably lore-friendly, and made with the attitude, "If it ain't broke, don't fix it." Currently OpenMW-exclusive.""",
            "name": "Sensible Races and Birthsigns",
            "status": "live",
            "url": nexus_mod("47115"),
        },
        {
            "alt_to": [
                magicka_based_skill_progression_ncgd,
                magicka_based_skill_progression_better_sounds_ncgd,
            ],
            "author": "HotFusion4 - edits by Greywander, Joao Almeida, and p4r4digm",
            "category": gameplay,
            "date_added": "2020-05-05 21:41:34 -0500",
            "description": """This mod makes magical skill progression based on the amount of magicka used, instead of number of spells cast.""",
            "name": "Magicka Based Skill Progression - Better Sounds - ncgdMW Compatible - Class-Spec Benefit Fix",
            "status": "live",
            "url": nexus_mod("47812"),
            "usage_notes": """<span class="bold">Check the Spellbook item to see your magic skill progression!</span> The description contains extensive notes about how the mod works and differs from other versions.  Please take the time to read through it, in particular the part labeled "Opinions Addendum, updated with play-testing", where the author talks about growth rates and what works for whom.""",
        },
        {
            "alt_to": [house_dagoth_difficulty_mod, tougher_sixth_house],
            "author": nexus_user("4138441", "mort"),
            "category": gameplay,
            "date_added": "2020-05-06 08:10:29 -0500",
            "description": """Makes the Sixth House, properly, the most difficult content in the game. Intended for use with my other rebalance mods.""",
            "name": "Beware the Sixth House (Sixth House Overhaul)",
            "status": "live",
            "url": nexus_mod("46036"),
            "usage_notes": """Unless you want to use a modular setup for this mod, just use the <code>Beware the Sixth House.ESP</code> plugin to get all features of this mod.""",
        },
        {
            "author": nexus_user("4138441", "mort"),
            "category": gameplay,
            "date_added": "2020-05-06 08:14:45 -0500",
            "description": """This mod is intended to rebalance the Tribunal expansion in a variety of ways, most importantly by editing the stats of creatures and npcs.  This mod rebalances Tribunal as if it shipped with Morrowind.""",
            "name": "Tribunal Rebalance",
            "status": "live",
            "url": nexus_mod("45713"),
        },
        {
            "author": nexus_user("4138441", "mort"),
            "category": gameplay,
            "date_added": "2020-05-06 08:26:49 -0500",
            "description": """This mod is intended to rebalance the Bloodmoon expansion in a variety of ways, most importantly by editing the stats of creatures and npcs.  This mod rebalances Bloodmoon as if it shipped with Morrowind.""",
            "name": "Bloodmoon Rebalance",
            "status": "live",
            "url": nexus_mod("45714"),
        },
        {
            "name": "Persistent Corpse Disposal",
            "author": '<a href="https://baturin.org/">dmbaturin</a>',
            "category": gameplay,
            "date_added": "2020-05-06 11:05:59 -0500",
            "description": """Makes corpses that are persistent for quest purposes actually disappear once they aren't needed for their respective quest.""",
            "status": "live",
            "url": "https://baturin.org/misc/morrowind-mods/#persistent-corpse-disposal",
        },
        {
            "name": "Greetings for No Lore",
            "author": nexus_user("43442372", "Caeris"),
            "category": gameplay,
            "date_added": "2020-05-06 18:07:17 -0500",
            "description": """Replaces the three standard No Lore greetings with over sixty new ones.""",
            "status": "live",
            "url": nexus_mod("46063"),
        },
        {
            "name": "Expansions Integrated",
            "author": nexus_user("70336838", "Necrolesian"),
            "category": gameplay,
            "date_added": "2020-05-06 18:08:17 -0500",
            "description": """Integrates much of the content of the Tribunal and Bloodmoon expansions within the rest of the game world.""",
            "status": "live",
            "url": nexus_mod("47861"),
        },
        {
            "name": "NPC Schedule",
            "author": "Grumpy",
            "category": gameplay,
            "compat": "partially working",
            "date_added": "2020-05-08 13:12:55 -0500",
            "date_updated": "2021-11-15 17:36:52 -0500",
            "description": """Fixed version of Grumpy's amazing NPC Schedule mod.""",
            "dl_url": "/files/NPC_Schedule(beta) - Patch-48955-1-3-1636907427.7z",
            "status": "live",
            "url": nexus_mod("45525"),
            "usage_notes": """Download the patch plugin from <a href="/files/NPC_Schedule(beta)-OpenMW-Patch-48955-1-0-1603050298.7z">the Download Link above</a> and load it after the main plugin that comes with the mod from Nexus. Credit for the patch file goes to <a href="https://www.nexusmods.com/morrowind/users/77253">alvazir</a>""",
        },
        {
            "name": "Nighttime Door Locks",
            "author": "LDones",
            "category": gameplay,
            "date_added": "2020-05-08 13:13:34 -0500",
            "description": """Fixed version of LDones beautiful Nighttime Door Locks mod. Does what it says.""",
            "status": "live",
            "url": nexus_mod("45526"),
            "usage_notes": """Download the "NighttimeDoorLocks-LD Purists Version" file, under "Optional Files".""",
        },
        {
            "alt_to": [better_regen, "Regen And Stamina Overhaul"],
            "name": "ZEF Focus Magicka",
            "author": nexus_user("92843188", "Zefirotte"),
            "category": gameplay,
            "date_added": "2020-06-24 19:13:34 -0500",
            "date_updated": "2022-12-10 09:00:00 -0500",
            "description": """Add a custom spell named Focus that allow the player to recover it's magicka while standing still and meditating. Automatic learning depending on player Willpower.""",
            "status": "live",
            "url": nexus_mod("48431"),
        },
        {
            "name": "Active Wait",
            "author": nexus_user("92843188", "Zefirotte"),
            "category": gameplay,
            "date_added": "2020-07-04 10:43:30 -0500",
            "description": """Allow to speed the game for a short amount of time to wait instead of using the vanilla wait menu. Use either a free custom spell or stand still for 5 seconds to start the Active Wait.""",
            "status": "live",
            "url": nexus_mod("48492"),
            "usage_notes": """There are two versions of the mod, be sure to read the mod description to find out about each one.""",
        },
        {
            "alt_to": ["True Lights and Darkness"],
            "name": "True Lights and Darkness - Necro Edit",
            "author": "Cool Demon and B00ze and Necrolesian",
            "category": gameplay,
            "date_added": "2020-11-22 10:54:52 -0500",
            "description": 'Modified version of <a href="/mods/true-lights-and-darkness/">True Lights and Darkness</a> that reverts some of TLAD\'s more radical color changes, removes the flicker effect from most lights, plus a few other changes.',
            "dl_url": "/files/True Lights and Darkness - Necro Edit-47133-3-0-1-1602901876.7z",
            "status": "live",
            "url": nexus_mod("47133"),
        },
        {
            "author": nexus_user("962116", "Melchior Dahrk"),
            "category": gameplay,
            "date_added": "2021-04-10 07:29:16 -0500",
            "description": """To align with what the in game dialogue suggests, the entrance to the Cavern of the Incarnate will now only be visible during the magical hours of twilight.""",
            "name": "The Dream is the Door",
            "status": "live",
            "tags": [tag_vanilla_friendly],
            "url": nexus_mod("47423"),
        },
        {
            "name": "Morrowind Anti-Cheese",
            "author": "Remiros and Half11",
            "category": gameplay,
            "date_added": "2021-08-28 09:37:25 -0500",
            "description": """Fixes the biggest exploits and balance issues in the game.""",
            "status": "live",
            "url": nexus_mod("47305"),
        },
        {
            "alt_to": ["Sensible Races and Birthsigns"],
            "name": "Eye of Night Toggle",
            "author": nexus_user("3531126", "Vinifera7"),
            "category": gameplay,
            "date_added": "2021-08-28 12:02:55 -0500",
            "description": """This mod turns the Eye of Night spell available to the Khajiit race into a toggled effect.""",
            "status": "live",
            "url": nexus_mod("43139"),
        },
        {
            "name": "No Pushover Merchants",
            "author": nexus_user("34365690", "ComradeSokami"),
            "category": gameplay,
            "date_added": "2021-08-28 12:13:36 -0500",
            "description": """A mod that boost the Mercantile and Speechcraft of all Traders in Vanilla Morrowind and its expansions to be on par with their Personality attribute. By default, most NPC traders have 20 merc or less, leading to serious game breaking exploits. This mod rectifies this exploit and makes Bartering an immersive challenge to old and new players alike!""",
            "status": "live",
            "url": nexus_mod("47604"),
        },
        {
            "name": "The Publicans",
            "author": nexus_user("36879320", "Half11"),
            "category": gameplay,
            "date_added": "2021-08-28 12:21:22 -0500",
            "description": """The Publicans fixes several places in the vanilla game that are set up like inns, but in which Bethesda for some reason has forgotten to add the option to rent a room to.""",
            "status": "live",
            "url": nexus_mod("45410"),
        },
        {
            "alt_to": ["The Publicans"],
            "name": "Improved Inns Expanded",
            "author": nexus_user("59284071", "RandomPal"),
            "category": gameplay,
            "date_added": "2021-08-28 12:29:07 -0500",
            "description": """Adds the functionality of Improved Inns to the new rentable rooms added by the Publicans and Tamriel Rebuilt. Adds the option to "entertain the patrons" to the same inns.""",
            "status": "live",
            "url": nexus_mod("48610"),
        },
        {
            "name": "No more Dr. Nerevarine",
            "author": nexus_user("4709296", "AliceL93"),
            "category": gameplay,
            "date_added": "2021-08-28 12:37:58 -0500",
            "description": """Upon catching a disease, you will no longer know its type. You will have to visit a healer (or try medicine randomly) to get cured.""",
            "status": "live",
            "url": nexus_mod("45861"),
        },
        {
            "name": "Another Armor n Weapon Rebalance",
            "author": "ezze and Quorn",
            "category": gameplay,
            "date_added": "2021-08-28 13:08:29 -0500",
            "date_updated": "2022-11-05 09:00:00 -0500",
            "description": """Rebalances armors and weapons to reduce the quality jump between armors and increase variety.""",
            "is_active": False,
            "status": "live",
            "url": nexus_mod("50053"),
            "usage_notes": """This mod has been set to hidden by its author.""",
        },
        {
            "alt_to": [
                magicka_based_skill_progression_ncgd,
                magicka_based_skill_progression_better_sounds_ncgd,
                "Magicka Based Skill Progression - Better Sounds - ncgdMW Compatible - Class-Spec Benefit Fix",
            ],
            "name": "MBSP - Better Sounds - ncgdMW - Class-Spec - Fixed again Replacement Patch",
            "author": "HotFusion4 - edits by Greywander, Joao Almeida, p4r4digm, and alvazir",
            "category": gameplay,
            "date_added": "2021-09-01 17:50:46 -0500",
            "description": """<p>A refined version of <a href="/mods/magicka-based-skill-progression-better-sounds-ncgd/">Magicka Based Skill Progression - Better Sounds - ncgdMW Compatible - Class-Spec Benefit Fix</a>, by alvazir.</p>
<p>This replacement patch makes just 2 changes:</p>
<ol>
  <li>Fix "Demoralize Humanoid". It is now a pure Illusion spell. Before it was from Mysticism school, but trained illusion.</li>
  <li> Remove Better Sounds patch from masters to allow using any Better Sounds patch.</li>
</ol>""",
            "status": "live",
            "url": nexus_mod("48955"),
            "usage_notes": """<span class="bold">Check the Spellbook item to see your magic skill progression!</span>""",
        },
        {
            "alt_to": ["At Home Alchemy"],
            "name": "At Home Alchemy - Finished",
            "author": "Syclonix, " + nexus_user("77253", "alvazir"),
            "category": gameplay,
            "date_added": "2021-09-04 16:48:15 -0500",
            "description": """<p>A completed version of <a href="/mods/at-home-alchemy/">At Home Alchemy</a> featuring:</p>

<ul>
  <li>You can no longer use other people's apparatuses</li>
  <li>Skooma pipes are now supported</li>
  <li>Patch for Purists changes included</li>
</ul>""",
            "status": "live",
            "url": nexus_mod("48955"),
        },
        {
            "name": "Pickpocket Fix",
            "author": "TheOne&Only",
            "category": gameplay,
            "date_added": "2022-01-23 17:14:46 -0500",
            "description": """<p>Fixes bugs associated with pickpocketing.</p>

<p>Changes the fPickPocketMod GMST from 0.3 to -3 causing the odds of a successful pickpocketing to increase instead of decrease based on sneak skill level.</p>

<p>A character with minimal sneak skill will have the default 5% minimum chance as before but higher skill levels now increase the odds of success toward the default 75% maximum.</p>""",
            "status": "live",
            "url": modhistory_archive_link("13-6311"),
        },
        {
            "alt_to": [
                magicka_based_skill_progression_ncgd,
                magicka_based_skill_progression_better_sounds_ncgd,
                "Magicka Based Skill Progression - Better Sounds - ncgdMW Compatible - Class-Spec Benefit Fix",
                "MBSP - Better Sounds - ncgdMW - Class-Spec - Fixed again Replacement Patch",
            ],
            "name": "MBSP - ncgdMW - Class-Spec - Fixed again Replacement Patch",
            "author": "HotFusion4 - edits by Greywander, Joao Almeida, p4r4digm, and alvazir",
            "category": gameplay,
            "date_added": "2022-11-11 18:49:00 -0500",
            "description": """<p>This mod makes magical skill progression based on the amount of magicka used, instead of number of spells cast.</p>

<p>Yet another version of <a href="/mods/magicka-based-skill-progression-better-sounds-ncgd/">Magicka Based Skill Progression - Better Sounds - ncgdMW Compatible - Class-Spec Benefit Fix</a>, by alvazir; This removes all mentions of Better Sounds</p>""",
            "dl_url": "/files/MBSPncgdMWClassSpecFixedAgainReplacementPatch.zip",
            "status": "live",
            "url": "",
            "usage_notes": """<p><span class="bold">Check the Spellbook item to see your magic skill progression!</span></p>

<p>See this mod in action <a href="https://youtu.be/UvPPn5UHH2c?t=245">in the Expanded Vanilla: Intro video</a>.</p>""",
        },
        {
            "name": "Pursuit (OpenMW)",
            "author": nexus_user("7531974", "kindi"),
            "category": gameplay,
            "date_added": "2022-11-11 19:20:00 -0500",
            "description": """Actors can travel through cells to pursue their target.""",
            "status": "live",
            "tags": [tag_openmw_lua],
            "url": nexus_mod("50271"),
            "usage_notes": """<span class="bold">Requires OpenMW 0.48.0 or newer!</span>""",
        },
        {
            "name": "Pickpocket Rebalance (OpenMW Compatible)",
            "author": nexus_user("2957184", "Cresix96"),
            "alt_to": ["Pickpocket Fix"],
            "category": gameplay,
            "date_added": "2022-11-11 19:26:00 -0500",
            "date_updated": "2023-07-02 12:47:00 -0500",
            "description": """Rebalance for Pickpocketing in TES3:Morrowind, makes pickpocketing possible, but not broken. Sets max chance to 100%.""",
            "status": "live",
            "url": nexus_mod("51558"),
        },
        {
            "name": "OAAB Integrations",
            "author": "OAAB_Data Team",
            "category": gameplay,
            "date_added": "2022-11-09 10:24:00 -0500",
            "description": "These mods add some of the new items in OAAB_Data to appropriate creatures and leveled lists and adds OAAB creatures to vanilla leveled creature lists so that you will find them in the world as you explore. See the readmes for a full list of additions.",
            "status": "live",
            "tags": [tag_oaab_data],
            "url": nexus_mod("49045"),
        },
        {
            "name": "Alchemical Hustle",
            "author": nexus_user("88683753", "Rosynant"),
            "category": gameplay,
            "date_added": "2022-11-12 08:25:00 -0500",
            "date_updated": "2023-12-26 16:21:59 +0100",
            "description": "OpenMW-friendly alchemy rebalance. Features toxicity mechanics, prevents common exploits and scales down effects of many overpowered potions. Fully modular, compatible with Tamriel Rebuilt.",
            "status": "live",
            "tags": [tag_tamriel_data],
            "url": nexus_mod("51050"),
        },
        {
            "name": "CarryOn Tweaks",
            "author": "johnnyhostile",
            "category": gameplay,
            "date_added": "2022-11-13 21:21:00 -0500",
            "description": "An extension for CarryOn designed to make walking and running use less fatigue.",
            "status": "live",
            "url": nexus_mod("45212"),
        },
        {
            "name": "Wandering Creature Merchants",
            "author": "ezze",
            "category": gameplay,
            "date_added": "2022-11-20 14:27:00 -0500",
            "description": "This mod makes the creature merchants, everyone favorite Mudcrab and Scamp plus a wholly new merchant know as Griewk the Cliff Racer, roam the map.",
            "status": "live",
            "url": nexus_mod("51511"),
        },
        {
            "alt_to": ["Speechcraft Rebalance"],
            "name": "Speechcraft Rebalance (OpenMW)",
            "author": nexus_user("53177", "Aphain"),
            "category": gameplay,
            "date_added": "2023-09-27 12:31:00 -0500",
            "description": "This mod changes several game modifiers to make leveling speechcraft a more worthwhile and (hopefully) less broken experience. It also dramatically increases the worth of speechcraft as a major or minor skill by nerfing 'ye olde throw money at the problem' method.",
            "status": "live",
            "url": nexus_mod("53177"),
        },
        {
            "name": "For the Right Price",
            "author": nexus_user("88683753", "Necrolesian Rosynant"),
            "category": gameplay,
            "date_added": "2023-09-27 13:15:00 -0500",
            "description": "OpenMW-friendly economy rebalance based on Economy Adjuster Adjustments. Prevents getting very rich quickly (or getting very rich at all) by improving NPCs' trade skills and increasing prices of services. All done in an immersive, reasonable way. Fully modular, compatible with Tamriel Rebuilt.",
            "status": "live",
            "url": nexus_mod("52668"),
        },
        {
            "alt_to": ["Container Ownership (From Half11's Misc Mods)"],
            "name": "Ownership Overhaul",
            "author": nexus_user("70336838", "Necrolesian"),
            "category": gameplay,
            "date_added": "2023-09-27 13:27:00 -0500",
            "description": "This mod assigns ownership to the many, many items and containers in Morrowind that rightly should be owned but weren't, and otherwise cleans up and adjusts item ownership.",
            "status": "live",
            "url": nexus_mod("48051"),
        },
        {
            "name": "Real Disposition",
            "author": nexus_user("2250219", "Phoenix Rime"),
            "category": gameplay,
            "date_added": "2023-09-27 13:37:00 -0500",
            "description": "The modification makes the disposition of NPCs towards the player more realistic. Increases the number of amazing voice lines you'll hear per game.",
            "status": "live",
            "url": nexus_mod("51427"),
        },
        {
            "alt_to": [
                "CarryOn",
                "CarryOn Tweaks",
                "Better Fatigue Usage",
            ],
            "name": "Fatigue and Speed and Carryweight Rebalance (OpenMW)",
            "author": nexus_user("545942", "MisterFlames"),
            "category": gameplay,
            "date_added": "2023-09-27 13:48:00 -0500",
            "description": "Carefully tweaked values to increase speed and carry weight. Fatigue-management overhauled. No more quickly burning through stamina while running, but fatigue is still a factor.",
            "status": "live",
            "url": nexus_mod("52981"),
        },
        {
            "alt_to": [
                "Sensible Races and Birthsigns",
                races_are_more_fun,
                "Eye of Night Toggle",
            ],
            "name": "Races RESPECted - A Race Skill and Attribute Rebalance Mod",
            "author": nexus_user("3890785", "DisQualia")
            + ", Petethegoat, and Hrnchamd",
            "category": gameplay,
            "compat": "partially working",
            "date_added": "2023-09-27 14:06:00 -0500",
            "description": "This mod tweaks racial attributes, skill bonuses, and abilities to make the races' unique identities as described in lore and through their vanilla bonuses clearer and better translate them into gameplay.",
            "status": "live",
            "url": nexus_mod("52967"),
        },
        {
            "alt_to": ["Pickpocket Rebalance (OpenMW Compatible)"],
            "name": "Pickpocket Rebalance (OpenMW)",
            "author": nexus_user("6075441", "Aphain"),
            "category": gameplay,
            "date_added": "2023-09-27 14:44:00 -0500",
            "description": "Makes pickpocketing useful by setting the maximum success chance to 100% and changing the <code>fPickPocketMod</code> modifer from <code>0,3</code> to <code>0,05</code>. This makes pickpocketing actually useful and allows you to steal valuable/heavy items with enough Sneak/Luck.",
            "status": "live",
            "url": nexus_mod("53174"),
        },
        {
            "name": "Better Bound Items",
            "author": nexus_user("60401096", "EndinessDragoon"),
            "category": gameplay,
            "date_added": "2023-09-27 14:58:00 -0500",
            "description": "This OpenMW mod tries to balance and make bound items more enjoyable to play with. Adding a variety of effects to the bound weapons and armor to make a new playthrough feel fresh.",
            "status": "live",
            "url": nexus_mod("53149"),
        },
        {
            "name": "Pharis' Magicka Regeneration",
            "author": nexus_user("127856113", "Pharis"),
            "category": gameplay,
            "date_added": "2023-09-27 15:08:00 -0500",
            "description": "Magicka regeneration Lua mod for all actors based on Willpower, current Fatigue ratio, and optionally current Magicka ratio.",
            "status": "live",
            "url": nexus_mod("52779"),
        },
        {
            "name": "Spell Effects Rebalance",
            "author": nexus_user("41304635", "puyopuyohero"),
            "category": gameplay,
            "date_added": "2023-09-27 15:17:00 -0500",
            "description": "Changes the casting cost of spell effects that are strictly more expensive/cheaper than other spell effects with the same practical benefits (ie: Feather vs. Fortify Attribute). Also fixes Demoralize Humanoid to be the intended school of Illusion.",
            "status": "live",
            "url": nexus_mod("52858"),
        },
        {
            "name": "MDMD - More Deadly Morrowind Denizens",
            "author": nexus_user("45815197", "AutumnBramble"),
            "category": gameplay,
            "date_added": "2023-09-27 15:30:00 -0500",
            "description": "230+ NPCs are given unique spells, enchantments, and items, to make Morrowind deadlier AND more varied. Faction bosses, Artifact owners, and Daedric cultists have been given customized, flavorful make-overs to become more difficult AND more memorable. Players are encouraged to carry resists, dispels, restores, and prepare ahead for big fights.",
            "status": "live",
            "url": nexus_mod("48745"),
        },
        {
            "name": "Better Blight",
            "author": nexus_user("92681153", "Cognatogen"),
            "category": gameplay,
            "date_added": "2023-10-06 14:23:00 -0500",
            "description": "Changes the effects of Blight Diseases to make them more interesting and unique, rather than just more potent versions of the common diseases.",
            "status": "live",
            "url": nexus_mod("48395"),
        },
        {
            "name": "Practical Necromancy (OpenMW)",
            "author": nexus_user("74168263", "Seth"),
            "category": gameplay,
            "date_added": "2023-10-06 14:50:00 -0500",
            "description": "With this mod, players may learn to raise permanent undead followers and trap the souls of NPCs using Black Soul Gems. A skilled enough Necromancer can learn to transcend the bounds of mortality and become a powerful undead Lich.",
            "status": "live",
            "url": nexus_mod("47390"),
        },
        {
            "name": "Light Hotkey - OpenMW Lua",
            "author": nexus_user("127856113", "Pharis"),
            "category": gameplay,
            "date_added": "2023-10-06 15:00:00 -0500",
            "description": "Equip your light with a hotkey and automatically re-equip your shield when the light is unequipped.",
            "status": "live",
            "url": nexus_mod("51981"),
        },
        {
            "name": "Detect All Keys",
            "author": nexus_user("790766", "ZackHasACat"),
            "category": gameplay,
            "date_added": "2023-10-31 14:45:00 -0500",
            "description": "Some keys, such as slave keys, are not associated with any door or container, which makes them not detected by the 'detect key' spell. This fixes those.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52778"),
        },
        {
            "name": "Guards' Hounds",
            "author": nexus_user("66357466", "ezze"),
            "category": gameplay,
            "date_added": "2023-11-06 11:26:00 -0500",
            "description": "This is a small mod that gives to some guards the ability to summon a dog (actually a Wolf Creature), something larger (a Bear), or more rarely a Dwemer Animunculi to help them to keep the peace.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53346"),
        },
        {
            "name": "Devilish Vampire Overhaul",
            "author": nexus_user("5708545", "DetailDevil"),
            "category": gameplay,
            "date_added": "2023-11-06 12:32:00 -0500",
            "description": "This mod alters vampire mechanics. There are now blood drinking effects, varying stages of vampiric hunger, and enabled NPC interactions if you're satiated.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52969"),
        },
        {
            "name": "No Witness - No Bounty",
            "author": nexus_user("790766", "ZackHasACat"),
            "category": gameplay,
            "date_added": "2023-11-06 12:44:00 -0500",
            "description": "When you commit a crime, you will need to kill all of your witnesses to make sure you aren't reported. This will be anyone who saw you commit the crime but may also include those who saw you in the area. Anyone who attacks you upon committing a crime will be counted as a witness. Leaving the building that you are in will make any crimes you've committed permanent. Player followers are not counted as witnesses.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53384"),
        },
        {
            "name": "Protective Guards (OpenMW)",
            "author": "kindi",
            "category": gameplay,
            "date_added": "2023-11-10 19:50:00 -0500",
            "description": """Guards now protect against hostile NPCs, that includes NPCs that have high fight ratings, NPCs that attack you first, and even the guards themselves. Taunting NPCs is now a new weapon. Can't fight them? Just taunt them, and let the guards take care of it.""",
            "status": "live",
            "tags": [tag_openmw_lua],
            "url": nexus_mod("46992"),
        },
        {
            "name": "Shield Unequipper",
            "author": "johnnyhostile",
            "category": gameplay,
            "date_added": "2023-11-10 20:04:00 -0500",
            "description": """Inspired by the MCP feature "Two-handed weapon removes shield". Un-equips the shield when equipping any two-handed weapon. You no longer can benefit from the armor rating of the shield while both hands are occupied.""",
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/shield-unequipper/",
        },
        {
            "name": "MOMW Gameplay",
            "author": "johnnyhostile",
            "category": gameplay,
            "date_added": "2023-11-09 15:18:00 -0500",
            "description": "This mod will, at game startup, set various settings across several mods to values that I’ve found to work well after a bit of playtesting. This mod can be disabled to stop the changes or re-enabled to resume them at any time.",
            "status": "live",
            "tags": [tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/momw-gameplay/",
        },
        {
            "name": "Ashlander Architect",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": gameplay,
            "date_added": "2023-11-16 19:42:00 -0500",
            "description": "Ashlander Architect adds a building system with lua scripting in OpenMW. It allows you to place objects, and pick up any object.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52879"),
        },
        {
            "name": "Smooth Master Index",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": gameplay,
            "date_added": "2023-11-29 09:33:00 -0500",
            "description": "This mod makes the usage of the master index a little more convenient, not requiring the use of dialog, but not involving any new menus.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52991"),
        },
        {
            "alt_to": ["At Home Alchemy - Finished"],
            "name": "Tabletop Alchemy",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": gameplay,
            "date_added": "2023-11-29 09:36:00 -0500",
            "description": "This mod allows you to enter the alchemy interface by activating alchemy apparatus in the world, so you don't have to pick it up first.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("52891"),
        },
        {
            "name": "No Sneaky Sleeping",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": gameplay,
            "date_added": "2023-11-29 09:39:00 -0500",
            "description": "Makes it so that you cannot sleep in beds that are owned. Because it doesn't make sense to be able to sneak around a house to sleep in it.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53539"),
        },
        {
            "name": "Corporeal Carryable Containers",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": gameplay,
            "date_added": "2023-11-29 09:40:00 -0500",
            "description": "Adds carryable containers that can store items for organizational purposes. Includes a keychain use.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53772"),
        },
        {
            "name": "Quicktrain",
            "author": nexus_user("790766", "ZackHasaCat"),
            "category": gameplay,
            "date_added": "2023-11-29 09:42:00 -0500",
            "description": "Adds a note that saves known trainer locations, and makes training itself a bit less repetitive.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53771"),
        },
        {
            "name": "CO.RE - Comprehensive Rebalance",
            "author": nexus_user("2031404", "Sarge945"),
            "category": gameplay,
            "date_added": "2023-11-29 09:44:00 -0500",
            "description": "A mod designed to fix many longstanding balance and gameplay issues present in Morrowind in a configurable and seamless way. OpenMW only",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53663"),
        },
        {
            "name": "Perfumed Vampires",
            "author": nexus_user("4513134", "urm"),
            "category": gameplay,
            "date_added": "2023-11-29 09:48:00 -0500",
            "description": '"Perfumed Vampires" allows the player to mask their undead scent by applying perfumes. The effect lasts as long as the perfume itself.',
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53714"),
        },
        {
            "name": "Most Wanted Nerevarine",
            "author": nexus_user("4513134", "urm"),
            "category": gameplay,
            "date_added": "2023-11-29 09:50:00 -0500",
            "description": "Shows your current crime status in a HUD overlay reminiscent of open world crime games, such as GTA or Cyberpunk.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53717"),
        },
        {
            "name": "Convenient Thief Tools",
            "author": nexus_user("127856113", "Pharis"),
            "category": gameplay,
            "date_added": "2023-12-11 08:52:23 +0100",
            "description": "Quickly equip and cycle through thief tools (lockpicks/probes) with configurable hotkeys. Tools can also be automatically equipped when the player activates a locked/trapped object.",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("53094"),
        },
        {
            "name": "Wares Ultimate",
            "author": nexus_user("1233897", "Danae Aurel"),
            "category": gameplay,
            "date_added": "2022-12-01 19:22:00 -0500",
            "date_updated": "2023-01-16 10:55:00 -0500",
            "description": """Modular versions of WARES for Vvardenfell, Tamriel Rebuilt, Skyrim Home of the Nords, and Province Cyrodiil including items, traders, npcs, Hold it and containers.""",
            "status": "live",
            "tags": [tag_oaab_data, tag_tamriel_data],
            "url": nexus_mod("52013"),
        },
        {
            "name": "Wares",
            "author": nexus_user("1233897", "Danae Aurel"),
            "category": gameplay,
            "date_added": "2021-08-22 15:46:05 -0500",
            "date_updated": "2022-11-15 17:37:00 -0500",
            "description": """Wares gives traders more Wares (duh). Includes Wares from other mods (such as Tamriel Rebuilt). Includes traders in the Mainland.""",
            "is_active": False,
            "status": "live",
            "url": "",
        },
        {
            "name": "Perfect Placement",
            "author": nexus_user("843673", "Hrnchamd"),
            "category": gameplay,
            "date_added": "2023-12-23 17:11:00 -0500",
            "description": """Adds interactive placement and rotation.""",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua],
            "url": nexus_mod("46562"),
        },
        {
            "name": "Bound Balance",
            "author": "johnnyhostile",
            "category": gameplay,
            "date_added": "2024-01-03 17:41:00 -0500",
            "description": """Balances bound items by adjusting them based on the player’s conjuration skill level.""",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/bound-balance/",
        },
        {
            "name": "Go Home!",
            "author": "johnnyhostile",
            "category": gameplay,
            "date_added": "2024-01-08 17:55:00 -0500",
            "description": """Gives NPCs a schedule. Dynamically supports everything with many exceptions to avoid conflicts. Highly configurable.""",
            "status": "live",
            "tags": [tag_dev_build_only, tag_openmw_lua, tag_gitlab_mods],
            "url": "https://modding-openmw.gitlab.io/go-home/",
            "alt_to": ["NPC Schedule"],
        },
    ]:
        generate_mod(**m)
