from django.contrib.sitemaps.views import sitemap
from django.urls import include, path
from django.views.decorators.cache import cache_page
from chroniko.views import EntryDetailView, entry_edit
from utilz.views import app_info, app_info_json
from .feeds import LatestModsFeed
from .settings import CACHE_MIDDLEWARE_SECONDS, DEBUG, USE_ROBOTS
from .sitemaps import sitemap_dict
from .views import search
from .views.dynamicpages import (
    all_mods,
    all_fallback_archives,
    all_groundcover,
    all_plugins,
    categories,
    category_detail,
    compat_unknown,
    example_mod_page,
    fully_working,
    gs_directories,
    index,
    ip_address,
    latest_mods,
    mod_alts,
    mod_detail,
    mod_list_changelog,
    mod_list_detail,
    mod_list_final,
    mod_list_step_detail,
    mod_lists,
    no_plugin,
    not_working,
    partial_working,
    rn,
    tag_detail,
    tags,
    tips_cleaning,
    user_settings,
    users_guide,
    with_plugin,
)
from .views.forms import cfg_generator, cfg_blank
from .views.utility import robots_txt, legacy_url_redirect, static_view, webmanifest


urlpatterns = [
    path("rn", rn, name="rn"),
    path("manifest.webmanifest", webmanifest, name="webmanifest"),
    path("_app/", app_info, name="app-info"),
    path("_stat/", app_info_json, name="app-info-json"),
    path("about/", static_view, {"template": "about.html"}, name="about"),
    path("all-fallback-archives/", all_fallback_archives),
    path("all-groundcover/", all_groundcover),
    path("all-plugins/", all_plugins),
    path("blog/", include("chroniko.urls", namespace="blog")),
    path("cfg-generator/", cfg_blank, name="cfg_generator"),
    path("cfg-generator/<slug:preset>/", cfg_generator, name="cfg_generator"),
    path("cfg/generator/", cfg_blank, name="cfg_generator2"),
    path("cfg/generator/<slug:preset>/", cfg_generator, name="cfg_generator2"),
    path("cfg/", cfg_blank, name="cfg_generator3"),
    path("cfg/<slug:preset>/", cfg_generator, name="cfg_generator3"),
    path(
        "changelogs/", static_view, {"template": "changelogs.html"}, name="changelogs"
    ),
    path(
        "changelogs/website/",
        static_view,
        {"template": "changelogs-website.html"},
        name="changelogs-website",
    ),
    path(
        "compatibility/",
        static_view,
        {"template": "compatibility.html"},
        name="compatibility",
    ),
    path("compat/fully-working/", fully_working, name="fully_working"),
    path("compat/partially-working/", partial_working, name="partial_working"),
    path("compat/not-working/", not_working, name="not_working"),
    path("compat/unknown/", compat_unknown, name="compat_unknown"),
    path("settings/", user_settings, name="user_settings"),
    path("subscribe/", static_view, {"template": "subscribe.html"}, name="subscribe"),
    # path("contact/", contact, name="contact"),
    path("cookie/", static_view, {"template": "cookie.html"}, name="cookie"),
    path(
        "enable-in-cfg-generator-button/",
        static_view,
        {"template": "enable-in-cfg-generator-button.html"},
        name="enable-in-cfg-generator-button",
    ),
    path("faq/", static_view, {"template": "faq.html"}, name="faq"),
    # path("feedback/", feedback, name="feedback"),
    path(
        "feeds/mods/",
        cache_page(CACHE_MIDDLEWARE_SECONDS)(LatestModsFeed()),
        name="mod_feed",
    ),
    path(
        "site-update-2022-06-01/",
        static_view,
        {"template": "site-update-2022-06-01.html"},
        name="future_plans",
    ),
    path(
        "getting-started/",
        static_view,
        {"template": "getting_started.html"},
        name="getting_started",
    ),
    path(
        "getting-started/buy/", static_view, {"template": "gs_buy.html"}, name="gs-buy"
    ),
    path(
        "getting-started/install/",
        static_view,
        {"template": "gs_install.html"},
        name="gs-install",
    ),
    path(
        "getting-started/settings/",
        static_view,
        {"template": "gs_settings.html"},
        name="gs-settings",
    ),
    path("getting-started/managing-mods/", gs_directories, name="gs-directories"),
    # TODO: make this a redirect
    path("getting-started/directories/", gs_directories, name="gs-directories-old"),
    path(
        "getting-started/tips/",
        static_view,
        {"template": "gs_tips.html"},
        name="gs-tips",
    ),
    path("guides/", static_view, {"template": "guides.html"}, name="guides"),
    path(
        "guides/developers/",
        static_view,
        {"template": "guides_developers.html"},
        name="guides-developers",
    ),
    path(
        "guides/modders/",
        static_view,
        {"template": "guides_modders.html"},
        name="guides-modders",
    ),
    path("guides/users/", users_guide, name="guides-users"),
    path("ip/", ip_address, name="ip"),
    path("lists/", mod_lists, name="mod-lists"),
    path("lists/add/", static_view, {"template": "add_a_list.html"}, name="add-a-list"),
    # START legacy list URLs
    path(
        "lists/androidswitch/",
        legacy_url_redirect,
        {"new_slug": "i-heart-vanilla-directors-cut", "new_view": "mod-list-detail"},
        name="old-androidswitch-list",
    ),
    path(
        "lists/tes3mp-friendly/",
        legacy_url_redirect,
        {"new_slug": "tes3mp-server", "new_view": "mod-list-detail"},
        name="old-tes3mp-list",
    ),
    # "Legacy" lists; point them to the actual new ones
    path(
        "lists/legacy-expanded-vanilla/",
        legacy_url_redirect,
        {"new_slug": "expanded-vanilla", "new_view": "mod-list-detail"},
        name="legacy-lists-expanded-vanilla",
    ),
    path(
        "lists/legacy-graphics-overhaul/",
        legacy_url_redirect,
        {"new_slug": "graphics-overhaul", "new_view": "mod-list-detail"},
        name="legacy-lists-graphics-overhaul",
    ),
    path(
        "lists/legacy-total-overhaul/",
        legacy_url_redirect,
        {"new_slug": "total-overhaul", "new_view": "mod-list-detail"},
        name="legacy-lists-total-overhaul",
    ),
    # END legacy list URLs
    path("lists/<slug:slug>/", mod_list_detail, name="mod-list-detail"),
    path("lists/<slug:slug>/changelog", mod_list_changelog, name="mod-list-changelog"),
    path("lists/<slug:slug>/final", mod_list_final, name="mod-list-final"),
    path(
        "lists/<slug:slug>/<int:step>/",
        mod_list_step_detail,
        name="mod-list-step-detail",
    ),
    path(
        "load-order/", static_view, {"template": "load_order.html"}, name="load_order"
    ),
    # path('logout/', logout, {'template_name': 'logout.html'},
    #     name='logout'),
    path("media/", include("media.urls", namespace="media")),
    path("mods/", all_mods, name="mod_index"),
    path(
        "mods/add",
        static_view,
        {"template": "how-to-get-a-mod-added.html"},
        name="how-to-get-a-mod-added",
    ),
    path(
        "mods/todo/",
        legacy_url_redirect,
        {"new_slug": "total-overhaul", "new_view": "mod-list-detail"},
        name="mod_todo",
    ),
    path("mods/all/", all_mods, name="all_mods"),
    path("mods/alts/", mod_alts, name="mod_alts"),
    path("mods/category/all/", categories, name="mod_categories"),
    # START preserve old category names
    path(
        "mods/category/followers/",
        legacy_url_redirect,
        {"new_slug": "companions", "new_view": "mod_category_detail"},
        name="followers-redirect",
    ),
    # END preserve old category names
    path("mods/category/<slug:slug>/", category_detail, name="mod_category_detail"),
    path("mods/latest/", latest_mods, name="latest_mods"),
    # START preserve old list URLs and ?step=100 endpoints
    path(
        "mods/list/",
        legacy_url_redirect,
        {"new_slug": "total-overhaul", "new_view": "mod-list-detail"},
        name="old-step1",
    ),
    path(
        "mods/steps/",
        # The legacy_url_redirect is not used here because the
        # redirect is handled in the mod_list_step_detail view.
        # This is to allow for handling of args like: ?step=42
        mod_list_step_detail,
        {"slug": "total-overhaul", "step": 1},
        name="old-step2",
    ),
    # Expanded vanilla:
    path(
        "mods/tag/expanded-vanilla/",
        legacy_url_redirect,
        {"new_slug": "expanded-vanilla", "new_view": "mod-list-detail"},
        name="old-step3",
    ),
    # TES3MP Friendly:
    path(
        "mods/tag/tes3mp-friendly/",
        legacy_url_redirect,
        {"new_slug": "tes3mp-server", "new_view": "mod-list-detail"},
        name="old-step4",
    ),
    # END preserve old list URLs and ?step=100 endpoints
    path("mods/tag/", tags, name="mod_tags"),
    # Preserve bad, old, typo, other links that are
    # now "gone" for any other reason.
    path(
        "mods/ast-eds-remastered/",
        legacy_url_redirect,
        {"new_slug": "ast-beds-remastered"},
        name="ast_beds_redirect",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-patch-for-pu/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_1",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-oaab-foyada-/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_2",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-concept-art-/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_3",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-siege-at-fir/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_4",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-tr_travels/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_5",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-skyrim-home-/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_6",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-province-cyr/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_7",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-npc-schedule/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_8",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-velothi-wall/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_9",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-morrowind-an/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_10",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-welcome-to-t/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_11",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-ebonheart-un/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_12",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-rise-of-hous/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_13",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-uvirith-lega/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_14",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-remiros-grou/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_15",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-gitd-telvann/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_16",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-ghastly-glow/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_17",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-oaab-pomegra/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_18",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-improved-inn/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_19",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-store-entran/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_20",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-bounty-hunte/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_21",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-tamriel-rebu/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_22",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-cephalopod-a/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_23",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-yet-another-/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_24",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-imperial-leg/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_25",
    ),
    path(
        "mods/beautiful-cities-of-morrowind-patches-area-of-effe/",
        legacy_url_redirect,
        {"new_slug": "beautiful-cities-of-morrowind"},
        name="bcom_patch_num_26",
    ),
    path(
        "mods/blademeister-openmw/",
        legacy_url_redirect,
        {"new_slug": "blademeister"},
        name="blademeister_openmw_redirect",
    ),
    path(
        "mods/caldera-mine-expanded-fixed-plugin/",
        legacy_url_redirect,
        {"new_slug": "caldera-mine-expanded"},
        name="caldera_mine_expanded_fixed_plugin_redirect",
    ),
    path(
        "mods/concept-arts-plantations-bcom-groundcover-patch/",
        legacy_url_redirect,
        {"new_slug": "concept-arts-plantations"},
        name="cap_bcom_patch_redirect",
    ),
    path(
        "mods/creeper-and-mudcrap-no-trade/",
        legacy_url_redirect,
        {"new_slug": "creeper-and-mudcrab-no-trade"},
        name="creeper_mudcrab_redirect",
    ),
    path(
        "mods/darknuts-world-textures-10/",
        legacy_url_redirect,
        {"new_slug": "darknuts-world-textures"},
        name="darknuts_world_textures_redirect",
    ),
    path(
        "mods/chrysamere-pbr/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave1_redirect",
    ),
    path(
        "mods/clannfear-replacer/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave2_redirect",
    ),
    path(
        "mods/dwemer-puzzle-box-replacer/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave3_redirect",
    ),
    path(
        "mods/ebony-mail-replacer/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave4_redirect",
    ),
    path(
        "mods/ice-blade-of-the-monarch-pbr/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave5_redirect",
    ),
    path(
        "mods/ice-blade-of-the-monarch-v2/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave6_redirect",
    ),
    path(
        "mods/lily-pad-replacer/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave7_redirect",
    ),
    path(
        "mods/mehrunes-razor-pbr/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave8_redirect",
    ),
    path(
        "mods/remiros-alcohol-hd/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave9_redirect",
    ),
    path(
        "mods/robe-overhaul/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave10_redirect",
    ),
    path(
        "mods/the-tools-of-kagrenac/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave11_redirect",
    ),
    path(
        "mods/unique-jewelry-redone/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave12_redirect",
    ),
    path(
        "mods/volendrung-hd/",
        legacy_url_redirect,
        {"new_slug": "remiros-mod-graveyard"},
        name="remi_grave13_redirect",
    ),
    path(
        "mods/daedric-ruins-half-revamped-caverns-revamp-patch/",
        legacy_url_redirect,
        {"new_slug": "daedric-ruins-half-revamped"},
        name="drhr_patch_redirect",
    ),
    path(
        "mods/directional-attack-stat-buffs-openmw-lua/",
        legacy_url_redirect,
        {"new_slug": "solthas-combat-pack-openmw-lua"},
        name="solthas_combat_pack_redirect",
    ),
    path(
        "mods/ebonheart-underworks-momw-patch/",
        legacy_url_redirect,
        {"new_slug": "momw-patches"},
        name="momw_patches_redirect",
    ),
    path(
        "mods/fm-unique-items-compilation-patch-for-purists/",
        legacy_url_redirect,
        {"new_slug": "alvazirs-various-patches"},
        name="alvazir_patch1_redirect",
    ),
    path(
        "mods/foyada-mamaea-overhaul/",
        legacy_url_redirect,
        {"new_slug": "oaab-foyada-mamaea"},
        name="foyada_mamaea_overhaul_redirect",
    ),
    path(
        "mods/graphic-herbalism-mwse-and-openmw-edition-project-/",
        legacy_url_redirect,
        {"new_slug": "graphic-herbalism-mwse-and-openmw-edition"},
        name="gh_patch_num_1",
    ),
    path(
        "mods/graphic-herbalism-mwse-and-openmw-edition-addition/",
        legacy_url_redirect,
        {"new_slug": "graphic-herbalism-mwse-and-openmw-edition"},
        name="gh_patch_num_2",
    ),
    path(
        "mods/graphic-herbalism-mwse-and-openmw-edition-glass-gl/",
        legacy_url_redirect,
        {"new_slug": "graphic-herbalism-mwse-and-openmw-edition"},
        name="gh_patch_num_3",
    ),
    path(
        "mods/hotv-solstheim-tomb-of-the-snow-prince/",
        legacy_url_redirect,
        {"new_slug": "solstheim-tomb-of-the-snow-prince"},
        name="totsp_redirect",
    ),
    path(
        "mods/imperial-towns-revamp-gitd-patches/",
        legacy_url_redirect,
        {"new_slug": "imperial-towns-revamp"},
        name="itr_patch_redirect",
    ),
    path(
        "mods/magicka-based-skill-progression-ncgdmw-compatility/",
        legacy_url_redirect,
        {"new_slug": "magicka-based-skill-progression-ncgdmw-compatibili"},
        name="mbsp_redirect",
    ),
    path(
        "mods/morrowind-optimization-patch-graphic-herbalism-pat/",
        legacy_url_redirect,
        {"new_slug": "morrowind-optimization-patch"},
        name="mop_patch_num_1",
    ),
    path(
        "mods/morrowind-optimization-patch-weapon-sheathing-patc/",
        legacy_url_redirect,
        {"new_slug": "morrowind-optimization-patch"},
        name="mop_patch_num_2",
    ),
    path(
        "mods/no-resting-outdoors/",
        legacy_url_redirect,
        {"new_slug": "wait-and-sleep-for-openmw"},
        name="no_resting_outdoors_redirect",
    ),
    path(
        "mods/oaab_data-sm_bitter-coast-trees-patch/",
        legacy_url_redirect,
        {"new_slug": "oaab-data"},
        name="oaabdata_patch_num_1",
    ),
    path(
        "mods/oaab_data-glass-glowset-patch/",
        legacy_url_redirect,
        {"new_slug": "oaab-data"},
        name="oaabdata_patch_num_2",
    ),
    path(
        "mods/oaab-integrations-oaab-leveled-lists/",
        legacy_url_redirect,
        {"new_slug": "oaab-integrations"},
        name="oaab_stuff_patch1_redirect",
    ),
    path(
        "mods/oaab-integrations-oaab-golden-reeds/",
        legacy_url_redirect,
        {"new_slug": "oaab-integrations"},
        name="oaab_stuff_patch2_redirect",
    ),
    path(
        "mods/oaab-shipwrecks-patches-uncharted-artifacts/",
        legacy_url_redirect,
        {"new_slug": "oaab-shipwrecks"},
        name="oaab_shipwrecks_patch_redirect",
    ),
    path(
        "mods/openmw-mod-manager/",
        legacy_url_redirect,
        {"new_slug": "portmod"},
        name="openmmm_redirect",
    ),
    path(
        "mods/orcish-retexture-v12/",
        legacy_url_redirect,
        {"new_slug": "orcish-retexture"},
        name="orcish_retexture_redirect",
    ),
    path(
        "mods/persistent-corpse-disposal-alvazirs-patch/",
        legacy_url_redirect,
        {"new_slug": "alvazirs-various-patches"},
        name="alvazir_patch3_redirect",
    ),
    path(
        "mods/project-atlas-glow-in-the-dahrk-patch/",
        legacy_url_redirect,
        {"new_slug": "project-atlas"},
        name="pa_patch_redirect",
    ),
    path(
        "mods/remiros-groundcover-openmw-patch/",
        legacy_url_redirect,
        {"new_slug": "remiros-groundcover"},
        name="remis_openmw_patch_redirect",
    ),
    path(
        "mods/staff-spell-stat-buffs-openmw-lua/",
        legacy_url_redirect,
        {"new_slug": "solthas-combat-pack-openmw-lua"},
        name="solthas_combat_pack_redirect_2",
    ),
    path(
        "mods/the-bounty-hunter-bundle/",
        legacy_url_redirect,
        {"new_slug": "bounty-hunter-assignments"},
        name="the_bounty_hunter_bundle_redirect",
    ),
    path(
        "mods/total-overhaul-patches-bcomghastlyggdynamicdistant/",
        legacy_url_redirect,
        {"new_slug": "momw-patches"},
        name="to_patches_redirect",
    ),
    path(
        "mods/uviriths-legacy-35/",
        legacy_url_redirect,
        {"new_slug": "uviriths-legacy"},
        name="uviriths_legacy_redirect",
    ),
    path(
        "mods/viewing-distance/",
        legacy_url_redirect,
        {"new_slug": "distant-land-and-objects"},
        name="viewing_distance_redirect",
    ),
    path(
        "mods/vivec-voice-addon-refreshed/",
        legacy_url_redirect,
        {"new_slug": "alvazirs-various-patches"},
        name="alvazir_patch2_redirect",
    ),
    path(
        "mods/weighty-charged-attacks-openmw-lua/",
        legacy_url_redirect,
        {"new_slug": "solthas-combat-pack-openmw-lua"},
        name="solthas_combat_pack_redirect_3",
    ),
    path(
        "mods/tag/essential/",
        legacy_url_redirect,
        {"new_slug": "expanded-vanilla", "new_view": "mod-list-detail"},
        name="tag_essential_redirect",
    ),
    path(
        "mods/vtasteks-shaders/",
        legacy_url_redirect,
        {"new_slug": "vtasteks-light-shaders"},
        name="vtasteks_shaders_redirect",
    ),
    # End preserving "gone" links.
    path("mods/tag/<slug:slug>/", tag_detail, name="mod_tag_detail"),
    path("mods/no-plugin/", no_plugin, name="no_plugin"),
    path("mods/with-plugin/", with_plugin, name="with_plugin"),
    path("mods/example-mod-page/", example_mod_page, name="example-mod-page"),
    path("mods/<slug:slug>/", mod_detail, name="mod_detail"),
    path("privacy/", static_view, {"template": "privacy.html"}, name="privacy"),
    path("resources/", static_view, {"template": "resources.html"}, name="resources"),
    path("search/", search, name="search"),
    path(
        "site-versions/",
        static_view,
        {"template": "site-versions.html"},
        name="site-versions",
    ),
    path(
        "sitemap.xml",
        sitemap,
        {"sitemaps": sitemap_dict},
        name="django.contrib.sitemaps.views.sitemap",
    ),
    path(
        "tes3mp/", static_view, {"template": "tes3mp_server.html"}, name="tes3mp_server"
    ),
    path(
        "tes3mp-server/",
        static_view,
        {"template": "tes3mp_server.html"},
        name="tes3mp_server2",
    ),
    path("tips/", static_view, {"template": "tips.html"}, name="tips"),
    path(
        "tips/atlased-meshes/",
        static_view,
        {"template": "tips_atlased_meshes.html"},
        name="tips-atlased-meshes",
    ),
    path(
        "tips/bbc-patching/",
        static_view,
        {"template": "tips_bbc_patching.html"},
        name="tips-bbc-patching",
    ),
    path("tips/cleaning-with-tes3cmd/", tips_cleaning, name="tips-cleaning"),
    path(
        "tips/creating-custom-groundcover/",
        static_view,
        {"template": "tips_creating_custom_groundcover.html"},
        name="tips-creating-custom-groundcover",
    ),
    path(
        "tips/file-renames/",
        static_view,
        {"template": "tips_file_renames.html"},
        name="tips-file-renames",
    ),
    path(
        "tips/ini-importer/",
        static_view,
        {"template": "tips_ini_importer.html"},
        name="tips-ini-importer",
    ),
    path(
        "tips/merging-objects/",
        static_view,
        {"template": "tips_merging_objects.html"},
        name="tips-merging-objects",
    ),
    path(
        "tips/navmeshtool/",
        static_view,
        {"template": "tips_navmeshtool.html"},
        name="tips-navmeshtool",
    ),
    path(
        "tips/openmw-physics-fps/",
        static_view,
        {"template": "tips_openmw_physics_fps.html"},
        name="tips-openmw-physics-fps",
    ),
    path(
        "tips/performance/",
        static_view,
        {"template": "tips_performance.html"},
        name="tips-performance",
    ),
    path(
        "tips/portable-install/",
        static_view,
        {"template": "tips_portable_install.html"},
        name="tips-portable-install",
    ),
    path(
        "tips/register-bsas/",
        static_view,
        {"template": "tips_register_bsas.html"},
        name="tips-register-bsas",
    ),
    path(
        "tips/shiny-meshes/",
        static_view,
        {"template": "tips_shiny_meshes.html"},
        name="tips-shiny-meshes",
    ),
    path(
        "tips/tr-patcher/",
        static_view,
        {"template": "tips_tr_patcher.html"},
        name="tips-tr-patcher",
    ),
    path(
        "tips/custom-shaders/",
        static_view,
        {"template": "tips_custom_shaders.html"},
        name="tips-custom-shaders",
    ),
    # path('whatismyip/', static_view, {"template": "ip.html"}, name='whatismyip'),
    # path('zEndusal/', include("zadmin.urls", namespace='zadmin')),
    path("", index, name="index"),
    path("<slug:slug>/", EntryDetailView.as_view(), name="slug_root"),
    path("<slug:slug>/edit/", entry_edit, name="slug_entry_edit"),
]


if DEBUG:
    try:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
    except ImportError:
        # Not installing or using debug toolbar.
        pass

if USE_ROBOTS:
    urlpatterns = [path("robots.txt", robots_txt)] + urlpatterns
