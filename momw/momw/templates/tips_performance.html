{% extends 'base.html' %}
{% block title %}Tips: Performance{% endblock %}
{% block content %}

  <h3 class="center">Tips: <span class="bold">Performance</span></h3>

  {% include "tips-nav.html" with cur_page="tips-performance" %}

  <p>
    Making a twenty-plus year-old game look like a modern one while still having good performance can be tricky. Thankfully, OpenMW offers many different settings options for managing performance in various ways.
  </p>

  <p>This page will discuss specific strategies for gaining those precious FPS.</p>

  <h4>Table Of Contents</h4>

  <ul>
    <li><a href="#how-to-test">How To Test Performance</a></li>
    <li><a href="#async-physics">Async Physics</a></li>
    <li><a href="#fov">Field Of View</a></li>
    <li><a href="#groundcover">Groundcover</a></li>
    <li><a href="#mods">Mods</a></li>
    <li><a href="#object-paging">Object Paging</a></li>
    <li><a href="#shadows">Shadows</a></li>
    <li><a href="#viewing-distance">Viewing Distance</a></li>
    <li><a href="#water">Water</a></li>
    <li><a href="#os-gpu-specific-tips">OS/GPU-Specific Tips</a></li>
    <li><a href="#steam-deck-tips">Steam Deck Tips</a></li>
    <li><a href="#perf-data">Performance Data</a></li>
  </ul>

  <h4 id="how-to-test"><a href="#how-to-test">How To Test Performance</a></h4>

  <p>
    It's good to establish a testing pattern that you can repeat in order to quickly change and test settings values. Something like this:
  </p>

  <ol>
    <li>Edit your settings as desired. Do not edit the settings file with the OpenMW-Launcher running.</li>
    <li>Run OpenMW-Launcher.</li>
    <li>Under the "Advanced" tab, click "Testing".</li>
    <li>Check the box next to where it says "Skip menu and generate default character".</li>
    <li>Pick a busy scene to test against (Balmora and Ebonheart both have spots that are very demanding). Type the cell name into the box next to where it says "Start default character at".</li>
    <li>Create a file in the root of your base mods folder and call it <code>openmw-test.txt</code>. Type this into that file:
      <pre><code>tgm
tcl
player->setspeed 400  ; Or a higher value as desired</code></pre>
      <ul><li>This will enable godmode, disable clipping, and set your player speed to something high so you can fly around very quickly when the game loads.</li></ul>
    </li>
    <li>Back to the launcher, where it says "Run script after startup:", click the button labeled "Browse..." next to the input box and select the <code>openmw-test.txt</code> file you created before.</li>
    <li>Click "Play" in the launcher, and the game will load with your chosen scene.</li>
    <li>Repeat this process as you try different values.</li>
  </ol>

  <p>
    After a bit of testing it's possible to arrive at settings that run well for your system, it just takes some patience.
  </p>

  <h4 id="async-physics"><a href="#async-physics">Async Physics</a></h4>

  <p>
    Set <code>async num threads</code> under the <code>[Physics]</code> section from <code>0</code> to <code>1</code>.
  </p>

  <p>
    Advanced tip: If you know you've got multi-threading built into your bullet (this may require compiling it yourself), then you can go ahead and set that to <code>2</code>. When setting this higher than <code>1</code>, it's good to do some benchmarks to make sure you benefit from the change.
  </p>

  <p>
    To do a quick benchmark:
  </p>

  <ul>
    <li>Go to a city such as Balmora (paste <code>coc "balmora, guild of mages"</code> into the console, press Enter, close the console, then walk outside)</li>
    <li>Spawn in 20 or so NPCs (paste <code>placeatpc "imperial guard" 20 20 1</code> into the console and press Enter)</li>
    <li>Press F3 twice to see the performance graphs, note the parts that indicate physics to see how things are going</li>
    <li>Adjust your settings as needed and repeat</li>
  </ul>

  <h4 id="fov"><a href="#fov">Field Of View</a></h4>

  <p>
    Personally, I love using a wide FOV in games that allow it. But I've found the performance cost can be significant, and actually nowadays just play with the default FOV.
  </p>

  <p>
    If you've got a widened FOV, and want to get more performance out of your setup, then do try using the default FOV.
  </p>

  <h4 id="groundcover"><a href="#groundcover">Groundcover</a></h4>

  <p>
    On this website, I suggest somewhat conservative values for groundcover settings but it may be possible to push that a bit further. For example: I've noticed that <code>rendering distance = 12288.0</code> doesn't have too much of an impact on most scenes, and the pop-in is greatly reduced.
  </p>

  <p>
    On a powerful machine, you can try to set <code>density</code> and <code>min chunk size</code> to <code>1.0</code> for more dense groundcover.
  </p>

  <h4 id="mods"><a href="#mods">Mods</a></h4>

  <p>
    There are several modding projects that are dedicated to improving the performance of the vanilla Morrowind assets. These include <a href="/mods/morrowind-optimization-patch/">Morrowind Optimization Patch</a> and <a href="/mods/project-atlas/">Project Atlas</a>. These can especially help on lower-end systems.
  </p>

  <h4 id="object-paging"><a href="#object-paging">Object Paging</a></h4>

  <p>
    OpenMW's object paging system is very powerful, highly configurable, and <a href="https://openmw.readthedocs.io/en/latest/reference/modding/settings/terrain.html?highlight=object%20paging#object-paging">very well-documented</a>. It gives you fine-grained control over how much distant stuff you see in-game. In order to maximize performance with it, you need to find the right amount of distant objects your system can handle.
  </p>

  <p>
    The most powerful option for controlling this is <code>object paging min size</code> (it goes under the <code>[Terrain]</code> section), which sets the size of things that will get paged (and thus, rendered in the distance). On my system with a powerful GPU, I set this to <code>0.023</code>, but you could raise it higher for more serious gains (at the cost of more pop-in, and less distant objects overall).
  </p>

  <h4 id="shadows"><a href="#shadows">Shadows</a></h4>

  <p>
    I've had the most success with <code>maximum shadow map distance</code> for getting performance out of shadows. I play with <code>4096</code>, which is half the default value. It's worth tweaking this one for potential gains.
  </p>

  <h4 id="viewing-distance"><a href="#viewing-distance">Viewing Distance</a></h4>

  <p>
    The <code>viewing distance</code> setting will have a major impact on your performance (combined with object paging).
  </p>

  <p>
    I've found that a value of <code>71680</code>, or just under 10 cells, is about as far I can go without having major drops in the busier scenes. If you have a less powerful GPU, experiment with 7 cells or lower, and a higher value for <code>object paging min size</code>.
  </p>

  <h4 id="water"><a href="#water">Water</a></h4>

  <p>
    Water settings can have a huge impact on performance without it being very obvious. Some things that can affect performance:
  </p>

  <ul>
    <li><span class="bold">Texture quality:</span> It can be tempting to set this to "High", but the difference between that and "Low" is actually not too noticeable in practice. In any case, it may be a worthy tradeoff to simply set this to "Low".</li>
    <li><span class="bold">Reflection shader detail:</span> This is another one that may be tempting to max out, but really beyond "World" reflections a lot of the extra detail is hard to notice and very expensive to render.</li>
    <li><span class="bold">Refraction:</span> Like the above reflection option, this one requires the engine to draw the entire scene twice which results in a decent performance hit. Disable this if you want to gain some FPS.</li>
  </ul>

  <h4 id="os-gpu-specific-tips"><a href="#os-gpu-specific-tips">OS/GPU-Specific Tips</a></h4>

  <p>
    If you're using a modern AMD GPU on a modern Linux system (e.g., I've got a Polaris 10 card on a rolling release distro), it might be a good idea to try running OpenMW with the <code>OSG_VERTEX_BUFFER_HINT=VERTEX_BUFFER_OBJECT</code> environment variable. On my own setup, this doubled my average FPS with very high settings.
  </p>

  <p>
    This is known to not make any difference with Nvidia GPUs (and may have a negative impact on performance), but it is unknown to me if it makes a difference with AMD GPUs on Windows.
  </p>

  <h4 id="steam-deck-tips"><a href="#steam-deck-tips">Steam Deck Tips</a></h4>

  <p>
    It's possible to run any of the mod lists featured on this website using <a href="https://www.steamdeck.com/">Valve's Steam Deck</a>, but some considerations will need to be made because of the device's limitations.
  </p>

  <ul>
    <li>For the <span class="bold">Total Overhaul</span> and <span class="bold">Graphics Overhaul</span> lists in particular, you'll need to use a reasonably low viewing distance so that you don't use too much RAM and crash OpenMW. I've been using <code>viewing distance = 40960</code> with a lot of success (no more crashing due to OOM).</li>
    <li>This applies to all setups but especially the more graphically intense - another way to use less memory overall is to raise the <code>object paging min size</code> value if you have not already done so. A value of <code>object paging min size = 0.023</code> has worked well for me.</li>
    <li>Use a lower <code>shadow map resolution</code> value, <code>shadow map resolution = 1024</code> for example.</li>
    <li>Run the game at a lower than native resolution such as 928x580, then use the Steam Deck's builtin FSR controls to raise the quality on top of that. I've noticed a lower power draw when doing this (anywhere from 1-4w give or take).</li>
    <li>As mentioned above in the "Water" section, those settings can gain or cost you FPS.</li>
    <li>Not strictly related to performance, but <a href="/tips/portable-install/">OpenMW's portable install feature</a> is useful for having simultaneous entries for multiple mod lists and setups in your Steam Deck game UI.</li>
    <li>Also not strictly related to performance, <a href="/mods/openmw-appimage/">OpenMW AppImages</a> are an easy way to get releases and development builds onto your Steam Deck.</li>
  </ul>

  <div class="center">
    <img class="pic" src="/images/steamdeck-openmw-totaloverhaul.jpg" />
    <figcaption>The <a href="/lists/total-overhaul/">Total Overhaul mod list</a> on Steam Deck.</figcaption>
  </div>


  <h4 id="perf-data"><a href="#perf-data">Performance Data</a></h4>

  <p>
    A collection of benchmarks I've done, with visuals provided by Flightless Mango and the excellent mangohud software.
  </p>

  <ul>
    <li><a href="https://flightlessmango.com/games/15511/logs/1370">OpenMW 0.47 (unreleased version: b286397dd4): Vanilla Morrowind vs Vanilla + MOP vs Vanilla + Project Atlas vs Vanilla + MOP + Project Atlas</a></li>
    <li><a href="https://flightlessmango.com/games/15511/logs/1470">OpenMW 0.47 (unreleased version: 2bfee281fd): Benchmarking the Total Overhaul list in a busy scene at 3, 5, and 10 cells viewing distance</a></li>
    <li><a href="https://flightlessmango.com/games/15511/logs/1471">OpenMW 0.47 (unreleased version: 2bfee281fd): Benchmarking the Total Overhaul list in a busy scene with different <code>object paging min size</code> values</a> and a 10-cell viewing distance</li>
  </ul>

  {% include "tips-nav.html" with cur_page="tips-performance" %}

  {% include "main-nav.html" with cur_page="tips-performance" prev_link=False next_link=False %}

{% endblock %}
