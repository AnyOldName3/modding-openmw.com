from django.conf import settings
from utilz.git import APP_SHA, GIT_TAG, is_tag_release


def momw_ver(request):
    dev = "-dev" in APP_SHA
    if is_tag_release():
        momw_rev = GIT_TAG
    elif dev:
        momw_rev = APP_SHA[:10] + "-dev"
    else:
        momw_rev = APP_SHA[:10]
    return {
        "momw_rev": momw_rev,
        "momw_sha": APP_SHA.replace("-dev", ""),
        "momw_ver": settings.MOMW_VER,
    }
