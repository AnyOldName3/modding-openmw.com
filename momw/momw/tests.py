import os

from django.contrib.auth.models import AnonymousUser
from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site

# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.shortcuts import reverse
from django.test import RequestFactory, TestCase
from django.utils.html import strip_tags

# from selenium.webdriver.firefox.webdriver import WebDriver as FFDriver
from momw.data_seeds.mod_categories import mod_cats
from momw.data_seeds.mod_data import mods
from momw.data_seeds.mod_datapaths import data_paths
from momw.data_seeds.mod_lists import mod_lists as mod_lists_dataseed
from momw.data_seeds.mod_plugin_order import plugins_load_order
from momw.data_seeds.mod_tags import mod_tags

# from utilz.tests import BaseSelenium, start_driver_wrapper
# from .cfg import generate_cfg
from .feeds import LatestModsFeed
from .models import ListedMod, Mod, ModList, Category, Tag
from .sitemaps import sitemap_dict
from .views.dynamicpages import (
    all_mods,
    categories,
    category_detail,
    compat_unknown,
    fully_working,
    gs_directories,
    latest_mods,
    mod_alts,
    mod_detail,
    mod_list_detail,
    mod_list_final,
    mod_list_step_detail,
    mod_lists,
    no_plugin,
    not_working,
    partial_working,
    tag_detail,
    tags,
    tips_cleaning,
    with_plugin,
)
from .views.forms import cfg_generator
from .views.utility import static_view
from .settings import MOMW_VER


HTTP_OK = 200
User = get_user_model()


class MomwBaseSeleniumTests:
    def setUp(self):
        os.environ["MOMW_TESTING"] = "True"
        settings.DEBUG = True
        super().setUp()
        self.f = RequestFactory()

        self.s = Site.objects.create(name="test", domain="localhost")
        if not User.objects.all():
            self.u = User.objects.create(
                first_name="First Name",
                last_name="Last Name",
                username="first-last-jaja-lala",
                password="12345",
                is_superuser=True,
            )
        # TODO: There has to be a way to persist data in between tests without duplicating it as json fixtures.
        # https://docs.djangoproject.com/en/3.0/topics/testing/tools/#liveservertestcase
        # The LiveServerTestCase and Static variant both truncate the db after each test..
        mods(for_tests=True)

    def test_about(self):
        u = "about"
        v = static_view
        self.check_http_status_code(u, v, template="about.html")

    def test_mod_category_detail(self):
        u = "mod_category_detail"
        v = category_detail
        for _cat in Category.objects.all():
            _cat_mods = Mod.objects.filter(category=_cat)
            self.check_http_status_code(u, v, slug=_cat.slug)
            self.check_for_text(u, _cat.title, slug=_cat.slug)
            for _mod in _cat_mods:
                self.check_for_text(u, _mod.name, slug=_cat.slug)

    def test_mod_detail(self):
        u = "mod_detail"
        v = mod_detail
        for _mod in Mod.objects.all():
            if _mod.category.slug == "tes3mp":
                self.check_for_text(
                    u, "Suggested folder path", exists=False, slug=_mod.slug
                )
            self.check_http_status_code(u, v, slug=_mod.slug)
            self.check_for_text(u, _mod.category.title, slug=_mod.slug)
            self.check_for_text(u, _mod.name, slug=_mod.slug)
            if _mod.in_lists:
                for ml in _mod.in_lists:
                    ordnum = ListedMod.objects.get(mod=_mod, modlist=ml).order_number
                    if ml.is_parent:
                        # Sublists aren't shown in the "Featured in..." area
                        self.check_for_text(u, str(ml.title), slug=_mod.slug)
                    self.check_for_text(u, str(ordnum), slug=_mod.slug)
            self.check_for_text(
                u,
                _mod.COMPAT_CHOICES[_mod.compat - len(_mod.COMPAT_CHOICES)][1],
                slug=_mod.slug,
            )
            if _mod.tags.count() > 0:
                _tags = _mod.tags.all()
                for _t in _tags:
                    self.check_for_text(u, _t.name, slug=_mod.slug)

    def test_sitemap(self):
        u = "django.contrib.sitemaps.views.sitemap"
        v = sitemap
        self.check_http_status_code(u, v, sitemaps=sitemap_dict)

    def test_latest_mods(self):
        u = "latest_mods"
        v = latest_mods
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Latest 100 Mod Additions/Updates")
        self.check_for_text(u, "Below are the 100 most recently added or updated mods!")

    def test_fully_working(self):
        u = "fully_working"
        v = fully_working
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Compatibility: Fully Working")
        self.check_for_text(u, "Below is a list of ")
        self.check_for_text(u, " mods that are known to be fully working with OpenMW.")

    def test_partial_working(self):
        u = "partial_working"
        v = partial_working
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Compatibility: Partially Working")
        self.check_for_text(u, "Below is a list of ")
        self.check_for_text(
            u,
            " mods that are known to be partially working with OpenMW, some tweaks may be needed.",
        )

    def test_not_working(self):
        u = "not_working"
        v = not_working
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Compatibility: Not Working")
        self.check_for_text(u, "Below is a list of ")
        self.check_for_text(u, " mods that are known to not work with OpenMW.")

    def test_compat_unknown(self):
        u = "compat_unknown"
        v = compat_unknown
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Compatibility: Unknown")
        self.check_for_text(u, "Below is a list of ")
        self.check_for_text(u, " mods that are of unknown compatibility.")

    def test_getting_started(self):
        u = "getting_started"
        v = static_view
        self.check_http_status_code(u, v, template="getting_started.html")
        self.check_for_text(u, "Setup Steps")
        self.check_for_text(
            u,
            "A series of pages written to help get you set up with Morrowind via OpenMW:",
        )

    def test_guides(self):
        u = "guides"
        v = static_view
        self.check_http_status_code(u, v, template="guides.html")
        self.check_for_text(u, "Guides")
        self.check_for_text(
            u, "A series of guides to help you get the most out of this website."
        )

    def test_guides_developers(self):
        u = "guides-developers"
        v = static_view
        self.check_http_status_code(u, v, template="guides_developers.html")
        self.check_for_text(u, "Developers' Guide")
        # TODO: self.check_for_text(u, "")

    def test_guides_modders(self):
        u = "guides-modders"
        v = static_view
        self.check_http_status_code(u, v, template="guides_modders.html")
        self.check_for_text(u, "Modders' Guide")
        # TODO: self.check_for_text(u, "")

    def test_guides_users(self):
        u = "guides-users"
        v = static_view
        self.check_http_status_code(u, v, template="guides_users.html")
        self.check_for_text(u, "Users' Guide")
        # TODO: self.check_for_text(u, "")

    def test_gs_buy(self):
        u = "gs-buy"
        v = static_view
        self.check_http_status_code(u, v, template="gs_buy.html")
        self.check_for_text(u, "Getting Started: Buy Morrowind")
        self.check_for_text(
            u,
            "GOG.com: DRM-free, includes the Construction Set, Tribunal, Bloodmoon, and the official plugins -- as well as a PDF version of the official strategy guide.",
        )

    def test_gs_install(self):
        u = "gs-install"
        v = static_view
        self.check_http_status_code(u, v, template="gs_install.html")
        self.check_for_text(u, "Getting Started: Install OpenMW")
        self.check_for_text(u, "Should I Use A Dev Build?")

    def test_gs_settings(self):
        u = "gs-settings"
        v = static_view
        self.check_http_status_code(u, v, template="gs_settings.html")
        self.check_for_text(u, "Getting Started: Tweak Settings")
        self.check_for_text(
            u, "All of the settings mentioned on this page should go into a file named"
        )

    def test_gs_tips(self):
        u = "gs-tips"
        v = static_view
        self.check_http_status_code(u, v, template="gs_tips.html")
        self.check_for_text(u, "Getting Started: Tips")
        self.check_for_text(
            u,
            "Before getting into the giant list of mods that awaits you, feel free to check out some of the below tips.",
        )

    def test_how_to_get_a_mod_added(self):
        u = "how-to-get-a-mod-added"
        v = static_view
        self.check_http_status_code(u, v, template="how-to-get-a-mod-added.html")
        self.check_for_text(u, "How To Get A Mod Into The Site Database")
        self.check_for_text(
            u,
            "Is there a mod you use or have used that isn't listed on this site? Are you interested in helping get it on here? Then please, read on!",
        )

    def test_gs_directories(self):
        u = "gs-directories"
        v = gs_directories
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Getting Started: Managing Mods")
        self.check_for_text(
            u,
            "Before you get started into a huge list of mods, it's important to have a way to be organized. There's a few ways to go about this depending on your personal preference and possibly your OS.",
        )

    def test_mod_alts(self):
        u = "mod_alts"
        v = mod_alts
        alts = Mod.alts.all()
        self.check_http_status_code(u, v)
        for a in alts:
            self.check_for_text(u, a.name)
        self.check_for_text(u, "Alternatives")
        self.check_for_text(u, "The below list of ")
        self.check_for_text(
            u,
            " mods represents the cast of alternatives to the Total Overhaul list of mods.",
        )

    def test_add_a_list(self):
        u = "add-a-list"
        v = static_view
        self.check_http_status_code(u, v, template="add_a_list.html")
        self.check_for_text(u, "How To Become A Mod List Curator")
        self.check_for_text(
            u,
            "Mod lists are, of course, lists of mods that folks use. The ones featured on this site are (presently, at least) all put together by me (the site admin), of course with the help of countless individuals from the community. It's my intention that this site should have lists by other folks, for other tastes and styles.",
        )

    def test_all_mods(self):
        u = "all_mods"
        v = all_mods
        mods = Mod.objects.all()
        self.check_http_status_code(u, v)
        for m in mods:
            self.check_for_text(u, m.name)
        self.check_for_text(u, "All mods")
        self.check_for_text(u, "All mods in the site database.")

    def test_mod_categories(self):
        u = "mod_categories"
        v = categories
        self.check_http_status_code(u, v)
        for c in Category.objects.all():
            if c.mod_count > 0:
                self.check_for_text(u, c.title)
        self.check_for_text(u, "All Categories")
        self.check_for_text(u, "Category Name")

    def test_mod_tags(self):
        u = "mod_tags"
        v = tags
        self.check_http_status_code(u, v)
        for t in Tag.objects.all():
            if t.tagged_mods:
                self.check_for_text(u, t.name)
        self.check_for_text(u, "Tags")
        self.check_for_text(u, "Tag Name")

    def test_mod_tag_detail(self):
        t = Tag.objects.get(name="Game Expanding")
        u = "mod_tag_detail"
        v = tag_detail
        mods = Mod.objects.filter(tags__slug=t.slug)
        self.check_http_status_code(u, v, slug=t.slug)
        self.check_for_text(u, t.name, slug=t.slug)
        for m in mods:
            self.check_for_text(u, m.name, slug=t.slug)
        self.check_for_text(u, "Tag: Game Expanding", slug=t.slug)
        self.check_for_text(u, "Expands the game world.", slug=t.slug)
        self.assertEqual(t.get_absolute_url(), "/mods/tag/game-expanding/")

    def test_mod_list_detail(self):
        u = "mod-list-detail"
        v = mod_list_detail
        for ml in ModList.objects.all():
            if ml.mod_count > 1:
                self.check_http_status_code(u, v, slug=ml.slug)
                if ml.is_parent or not ml.parent_list:
                    self.check_for_text(u, "Mod List: " + ml.title, slug=ml.slug)
                else:
                    self.check_for_text(u, "SubList: " + ml.title, slug=ml.slug)
                self.check_for_text(u, str(ml.mod_count) + " mods", slug=ml.slug)
            elif ml.mod_count == 1:
                self.check_for_text(u, str(ml.mod_count) + " mod", slug=ml.slug)

    def test_mod_list_step_detail(self):
        u = "mod-list-step-detail"
        v = mod_list_step_detail

        for ml in ModList.objects.all():
            for step in range(1, ml.mod_count):
                self.check_http_status_code(u, v, slug=ml.slug, step=step)
                if ml.is_parent:
                    # TODO: Somehow test parent lists
                    pass

                else:
                    self.check_for_text(
                        u,
                        ListedMod.objects.get(modlist=ml, order_number=step).mod.name,
                        slug=ml.slug,
                        step=step,
                    )
                self.check_for_text(u, ml.title, slug=ml.slug, step=step)

    def test_mod_lists(self):
        u = "mod-lists"
        v = mod_lists
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Curated Mod Lists")
        self.check_for_text(
            u,
            "Various mod lists are provided below, click on one for more information about it and to browse as well. Want to host your own list here? Please see the short guide on how to add your list if you're interested.",
        )
        self.check_for_text(u, "List Name")
        self.check_for_text(u, "Description")
        self.check_for_text(u, "Mod Count")
        for ml in ModList.objects.all():
            if ml.is_parent:
                self.check_for_text(u, ml.title)
                self.check_for_text(u, strip_tags(ml.short_description))
                self.check_for_text(u, str(ml.mod_count))

    def test_load_order(self):
        u = "load_order"
        v = static_view
        self.check_http_status_code(u, v, template="load_order.html")
        self.check_for_text(u, "Load Order")
        self.check_for_text(
            u,
            "To ensure you have the right load order for your game, please visit the CFG Generator page. There you can select the mods you want to use and get the right data and plugin load orders for them.",
        )
        self.check_for_text(u, "How I produce my load order")

    def test_resources(self):
        u = "resources"
        v = static_view
        self.check_http_status_code(u, v, template="resources.html")
        self.check_for_text(u, "Resources")
        self.check_for_text(u, "OpenMW documentation on installing and using mods")

    def test_faq(self):
        u = "faq"
        v = static_view
        self.check_http_status_code(u, v, template="faq.html")
        self.check_for_text(u, "Frequently Asked Questions")
        self.check_for_text(u, "Got a question? Ask it!")

    def test_index(self):
        u = "index"
        v = static_view
        self.check_http_status_code(u, v, template="index.html")
        self.check_for_text(
            u, "A website dedicated to modding and modernizing Morrowind with OpenMW!"
        )
        self.check_for_text(u, "Choose a mod list, or browse available lists:")
        self.check_for_text(u, MOMW_VER.upper())

    def test_privacy(self):
        u = "privacy"
        v = static_view
        self.check_http_status_code(u, v, template="privacy.html")
        self.check_for_text(u, "Privacy Policy")
        self.check_for_text(
            u,
            """I collect general visitor stats for this website using GoatCounter, this data is publicly viewable.""",
        )

    def test_cookie(self):
        u = "cookie"
        v = static_view
        self.check_http_status_code(u, v, template="cookie.html")
        self.check_for_text(u, "Cookie Policy")
        self.check_for_text(
            u,
            "This website uses cookies to ensure user security when submitting forms, namely the Feedback form. In this context, cookies are used to prevent cross-site request forgery attacks.",
        )

    def test_cfg_generator(self):
        u = "cfg_generator"
        v = cfg_generator
        self.check_http_status_code(u, v)
        self.check_for_text(u, "CFG Generator")
        self.check_for_text(
            u,
            "Generates a working load order for your openmw.cfg BSA and plugin sections. This feature is a work-in-progress.",
        )
        self.check_for_text(
            u,
            "Choose a preset from one of the preset buttons below or select your own loadout using the multiple select form, then copy and paste the results into your own cfg file. Any selections made, either by hand or from a preset, are still selected when a query is submitted.",
        )

    def test_subscribe(self):
        u = "subscribe"
        v = static_view
        self.check_http_status_code(u, v, template="subscribe.html")
        self.check_for_text(u, "Subscribe!")

    def test_tes3mp(self):
        u = "tes3mp_server"
        v = static_view
        self.check_http_status_code(u, v, template="tes3mp_server.html")
        self.check_for_text(u, "TES3MP @ ")
        # self.check_for_text(
        #     u,
        #     "TES3MP servers hosted by your site admin. Featuring one vanilla server and one lightly modded server, each containing an assortment of server-side scripts to add features and improve player quality of life.",
        # )

    def test_tips(self):
        u = "tips"
        v = static_view
        self.check_http_status_code(u, v, template="tips.html")
        self.check_for_text(u, "Tips")
        self.check_for_text(
            u,
            "This section, intended for advanced users, will describe various topics relating to modding with OpenMW in detail.",
        )

    def test_tips_atlased_meshes(self):
        u = "tips-atlased-meshes"
        v = static_view
        self.check_http_status_code(u, v, template="tips_atlased_meshes.html")
        self.check_for_text(u, "Tips: Atlased Meshes And Textures")
        self.check_for_text(
            u,
            "Atlased meshes and textures in Morrowind are an idea pioneered by the Project Atlas Team, which includes many longtime content creators. The description on their mod page describes the purpose well:",
        )

    def test_tips_bbc_patching(self):
        u = "tips-bbc-patching"
        v = static_view
        self.check_http_status_code(u, v, template="tips_bbc_patching.html")
        self.check_for_text(u, "Tips: Better Balanced Combat Auto Patcher")
        self.check_for_text(
            u,
            """The popular Better Balanced Combat mod has some compatibility issues with certain mods. The mod author offers an "Auto Patcher" tool, found in the mod's 7z archive that can do the work of enabling compatibility with trouble mods.""",
        )

    def test_tips_cleaning_with_tes3cmd(self):
        u = "tips-cleaning"
        v = tips_cleaning
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Tips: Cleaning Plugins With TES3CMD")
        self.check_for_text(
            u,
            """A "dirty" plugin, or one that contains GMST contamination, is created by default when using the vanilla construction kit. Any content creator that uses it unknowingly creates plugins with bad or undesirable changes.""",
        )

    def test_tips_creating_custom_groundcover(self):
        u = "tips-creating-custom-groundcover"
        v = static_view
        self.check_http_status_code(
            u, v, template="tips_creating_custom_groundcover.html"
        )
        self.check_for_text(u, "Tips: Creating Custom Groundcover")
        self.check_for_text(
            u,
            """The nature of how MGE:XE groundcover (also known as "legacy groundcover") works creates problems when you use any mod which changes the landscape. You'll end up with missing or floating groundcover items. It is possible to generate custom groundcover for your load order, but it is not a trivial process.""",
        )

    def test_tips_file_renames(self):
        u = "tips-file-renames"
        v = static_view
        self.check_http_status_code(u, v, template="tips_file_renames.html")
        self.check_for_text(u, "Tips: File Renames")
        self.check_for_text(
            u,
            "Many texture mods that use special maps (normal, specular, etc) and are made for vanilla Morrowind have a filename like this:",
        )

    def test_tips_performance(self):
        u = "tips-performance"
        v = static_view
        self.check_http_status_code(u, v, template="tips_performance.html")
        self.check_for_text(u, "Tips: Performance")
        self.check_for_text(
            u,
            "Making a twenty-plus year-old game look like a modern one while still having good performance can be tricky. Thankfully, OpenMW offers many different settings options for managing performance in various ways.",
        )

    def test_tips_portable_install(self):
        u = "tips-portable-install"
        v = static_view
        self.check_http_status_code(u, v, template="tips_portable_install.html")
        self.check_for_text(u, "Tips: Portable Install")
        self.check_for_text(
            u,
            """OpenMW 0.48 added the ability for a "portable install"; that is: one where all the cfg files and related things that normally live amid your user documents instead live in a separate folder of your choosing.""",
        )

    def test_custom_shaders(self):
        u = "tips-custom-shaders"
        v = static_view
        self.check_http_status_code(u, v, template="tips_custom_shaders.html")
        self.check_for_text(u, "Tips: Custom Shaders")
        self.check_for_text(
            u,
            "So, you want to use custom shaders with OpenMW? What is the benefit of doing this? They have the potential to adjust how the game looks in various ways, sometimes with great effect.",
        )

    def test_tips_cleaning(self):
        u = "tips-cleaning"
        v = tips_cleaning
        self.check_http_status_code(u, v)
        self.check_for_text(u, "Tips: Cleaning Plugins With TES3CMD")
        self.check_for_text(
            u,
            'A "dirty" plugin, or one that contains GMST contamination, is created by default when using the vanilla construction kit. Any content creator that uses it unknowingly creates plugins with bad or undesirable changes.',
        )

    def test_tips_ini_importer(self):
        u = "tips-ini-importer"
        v = static_view
        self.check_http_status_code(u, v, template="tips_ini_importer.html")
        self.check_for_text(u, "Tips: INI Importer")
        self.check_for_text(
            u,
            'The OpenMW Launcher has a handy "Settings Importer" that will take the values in a Morrowind.ini file and convert them to a usable openmw.cfg. To use this:',
        )

    def test_tips_merging_objects(self):
        u = "tips-merging-objects"
        v = static_view
        self.check_http_status_code(u, v, template="tips_merging_objects.html")
        self.check_for_text(u, "Tips: Merging Objects")
        self.check_for_text(
            u,
            "Sometimes, two mods might provide the same object. This is where object merging comes in handy.",
        )

    def test_tips_navmeshtool(self):
        u = "tips-navmeshtool"
        v = static_view
        self.check_http_status_code(u, v, template="tips_navmeshtool.html")
        self.check_for_text(u, "Tips: Navmeshtool")
        self.check_for_text(
            u,
            """The Navmeshtool program comes with OpenMW and can be used to pregenerate navmeshes, thus avoiding loading pauses in game that might have been caused by doing that on the fly. It's an easy, convenient way to eliminate one category of stuttering (while continuing to enjoy the benefits of what caused it: dynamically generated navigation meshes).""",
        )

    def test_tips_openmw_physics_fps(self):
        u = "tips-openmw-physics-fps"
        v = static_view
        self.check_http_status_code(u, v, template="tips_openmw_physics_fps.html")
        self.check_for_text(u, "The content below is now out of date!")
        self.check_for_text(u, "Tips: OPENMW_PHYSICS_FPS")
        self.check_for_text(u, "Quoting David C. of the TES3MP project:")

    def test_tips_register_bsas(self):
        u = "tips-register-bsas"
        v = static_view
        self.check_http_status_code(u, v, template="tips_register_bsas.html")
        self.check_for_text(u, "Tips: Register BSAs")
        self.check_for_text(
            u,
            """When a mod comes with a .bsa file (such as Tamriel Rebuilt), it needs to be 'activated' or else the game will exhibit unwanted behavior (such as the "yellow exclamation points" often seen.)""",
        )

    def test_tips_shiny_meshes(self):
        u = "tips-shiny-meshes"
        v = static_view
        self.check_http_status_code(u, v, template="tips_shiny_meshes.html")
        self.check_for_text(u, "Tips: Shiny Meshes")
        self.check_for_text(u, 'How To Fix "Shiny Meshes"')

    def test_tips_tr_patcher(self):
        u = "tips-tr-patcher"
        v = static_view
        self.check_http_status_code(u, v, template="tips_tr_patcher.html")
        self.check_for_text(u, "Tips: TR Patcher")
        self.check_for_text(
            u,
            "The TR Patcher (mentioned briefly in their FAQ) is a small program that updates an .esp or .ess file to ensure it's compatible with data paths and etc in Tamriel Rebuilt.",
        )

    def test_site_versions(self):
        u = "site-versions"
        v = static_view
        self.check_http_status_code(u, v, template="site-versions.html")
        self.check_for_text(u, "Beta what?")
        self.check_for_text(
            u, 'Please click here to go to the "released" version of the site.'
        )

    def test_no_plugin(self):
        u = "no_plugin"
        v = no_plugin
        mods = Mod.objects.filter(has_plugin=False)
        self.check_http_status_code(u, v)
        for m in mods:
            self.check_for_text(u, m.name)
        self.check_for_text(u, "Mods With No Plugin")
        self.check_for_text(u, "Below are ")
        self.check_for_text(
            u,
            " mods that do not require a plugin file (.esp, .omwaddon, or otherwise).",
        )

    def test_with_plugin(self):
        u = "with_plugin"
        v = with_plugin
        mods = Mod.objects.filter(has_plugin=True)
        self.check_http_status_code(u, v)
        for m in mods:
            self.check_for_text(u, m.name)
        self.check_for_text(u, "Mods That Require A Plugin")
        self.check_for_text(u, "Below are ")
        self.check_for_text(
            u, " mods that require a plugin file (.esp, .omwaddon, or otherwise)."
        )

    def test_compatibility(self):
        u = "compatibility"
        v = static_view
        self.check_http_status_code(u, v, template="compatibility.html")
        self.check_for_text(u, "Mod Compatibility")
        self.check_for_text(
            u, "Below are the compatibility ratings you might find on a mod."
        )

    def test_mod_feed(self):
        u = "mod_feed"
        v = LatestModsFeed()
        self.check_http_status_code(u, v)

    def test_user_settings(self):
        u = "user_settings"
        v = static_view
        self.check_http_status_code(u, v, template="settings.html")
        self.check_for_text(u, "User Settings")
        self.check_for_text(u, "Settings for your Modding-OpenMW.com experience.")

    def test_list_finals(self):
        u = "mod-list-final"
        v = mod_list_final
        for ml in ModList.objects.all():
            if ml.mod_count > 1:
                if ml.slug != "tes3mp-server":
                    self.check_http_status_code(u, v, slug=ml.slug)
                    self.check_for_text(u, "Final Checklist: " + ml.title, slug=ml.slug)


# class MomwFirefoxTestCase(
#     BaseSelenium,
#     MomwBaseSeleniumTests,
#     StaticLiveServerTestCase,
# ):
#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         start_driver_wrapper(cls, FFDriver)


class MomwTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.s = Site.objects.create(name="test", domain="localhost")
        cls.u = User.objects.get_or_create(
            first_name="First Name",
            last_name="Last Name",
            username="first-last-jaja-lala",
            password="12345",
            is_superuser=True,
        )

        mod_cats()
        mod_tags()
        mods()
        mod_lists_dataseed()
        data_paths()
        plugins_load_order(testing=True)
        cls.c = Category.objects.get(title="Fixes")
        cls.m = Mod.objects.get(name="Patch for Purists")
        cls.m_with_bsa = Mod.objects.get(name="Tamriel Data")
        cls.m_with_p = Mod.objects.get(name="Natural Character Growth and Decay - MW")
        cls.m_with_fp = Mod.objects.get(
            name="Graphic Herbalism - MWSE and OpenMW Edition"
        )
        cls.t = Tag.objects.get(name="TES3MP Lua")

    def setUp(self):
        self.f = RequestFactory()

    def test_cat_get_absolute_url(self):
        self.assertEqual(self.c.get_absolute_url(), "/mods/category/fixes/")

    def test_mod_clean_name(self):
        self.assertEqual(self.m.clean_name, "PatchforPurists")

    def test_mod_compat_string(self):
        self.assertEqual(self.m.compat_string, "Fully Working")

    def test_mod_compat_link(self):
        self.assertEqual(
            self.m.compat_link,
            '<a href="/compatibility/#fully-working">Fully Working</a>',
        )

    def test_mod_dl_item(self):
        self.assertEqual(self.m_with_p.dl_item(), "ncgdMW.zip")

    def test_mod_get_absolute_url(self):
        self.assertEqual(self.m.get_absolute_url(), "/mods/patch-for-purists/")

    def test_mod_get_status_string(self):
        self.assertEqual(self.m.get_status_string(), "Live")

    # def test_mod_plugins(self):
    #     self.assertEqual(self.m_with_p.plugins[0], "ncgdMW.omwaddon")

    # def test_mod_bsa(self):
    #     self.assertEqual(self.m_with_bsa.bsa, "PC_Data.bsa, TR_Data.bsa, Sky_Data.bsa")

    # def test_mod_get_moddir_android(self):
    #     pass

    # def test_mod_get_moddir_linux(self):
    #     self.assertEqual(
    #         self.m.get_moddir_linux,
    #         "/home/username/games/OpenMWMods/Patches/PatchforPurists",
    #     )

    # def test_mod_get_moddir_macos(self):
    #     self.assertEqual(
    #         self.m.get_moddir_macos,
    #         "/Users/username/games/OpenMWMods/Patches/PatchforPurists",
    #     )

    # def test_mod_get_moddir_windows(self):
    #     self.assertEqual(
    #         self.m.get_moddir_windows, "C:\\games\\OpenMWMods\\Patches\\PatchforPurists"
    #     )

    # def test_mod_get_moddir_linux_multi_0(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_linux[0],
    #         "/home/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/00 Core + Vanilla Meshes",
    #     )

    # def test_mod_get_moddir_linux_multi_1(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_linux[1],
    #         "/home/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/01 Optional - Smoothed Meshes",
    #     )

    # def test_mod_get_moddir_linux_multi_2(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_linux[2],
    #         "/home/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/00 Correct UV Ore + README",
    #     )

    # def test_mod_get_moddir_linux_multi_3(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_linux[3],
    #         "/home/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/06 Less Epic Plants",
    #     )

    # def test_mod_get_moddir_macos_multi_0(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_macos[0],
    #         "/Users/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/00 Core + Vanilla Meshes",
    #     )

    # def test_mod_get_moddir_macos_multi_1(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_macos[1],
    #         "/Users/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/01 Optional - Smoothed Meshes",
    #     )

    # def test_mod_get_moddir_macos_multi_2(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_macos[2],
    #         "/Users/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/00 Correct UV Ore + README",
    #     )

    # def test_mod_get_moddir_macos_multi_3(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_macos[3],
    #         "/Users/username/games/OpenMWMods/Gameplay/GraphicHerbalismMWSEandOpenMWEdition/06 Less Epic Plants",
    #     )

    # def test_mod_get_moddir_windows_multi_0(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_windows[0],
    #         "C:\\games\\OpenMWMods\\Gameplay\\GraphicHerbalismMWSEandOpenMWEdition\\00 Core + Vanilla Meshes",
    #     )

    # def test_mod_get_moddir_windows_multi_1(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_windows[1],
    #         "C:\\games\\OpenMWMods\\Gameplay\\GraphicHerbalismMWSEandOpenMWEdition\\01 Optional - Smoothed Meshes",
    #     )

    # def test_mod_get_moddir_windows_multi_2(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_windows[2],
    #         "C:\\games\\OpenMWMods\\Gameplay\\GraphicHerbalismMWSEandOpenMWEdition\\00 Correct UV Ore + README",
    #     )

    # def test_mod_get_moddir_windows_multi_3(self):
    #     self.assertEqual(
    #         self.m_with_fp.get_moddir_windows[3],
    #         "C:\\games\\OpenMWMods\\Gameplay\\GraphicHerbalismMWSEandOpenMWEdition\\06 Less Epic Plants",
    #     )

    def test_tag_get_absolute_url(self):
        self.assertEqual(self.t.get_absolute_url(), "/mods/tag/tes3mp-lua/")

    # def test_cfg_generator_data_paths_are_sequential_preset(self):
    #     get_data = {"preset": "total overhaul"}
    #     os = "linux"
    #     bsa_mods, plugin_mods, used_mods, data_paths, extra_cfg, preset = generate_cfg(
    #         get_data, os
    #     )
    #     count = 1
    #     for mod in Mod.total_overhaul.all().order_by("datapathloadorder__order_number"):
    #         if mod.category.title not in settings.IGNORED_CATS:
    #             if isinstance(mod.get_moddir_linux, str):
    #                 mod_string = '"' + mod.get_moddir_linux + '"' + "\n"
    #             if isinstance(mod.get_moddir_linux, list):
    #                 break
    #             data_path_string = data_paths[count]
    #             self.assertEqual(mod_string, data_path_string)
    #             count += 1

    #     self.assertEqual(preset, "total overhaul")

    # def test_cfg_generator_data_paths_are_sequential_user_input(self):
    #     # http://0.0.0.0:8666/cfg-generator/?mod=graphic-herbalism-mwse-and-openmw-edition&mod=morrowind-optimization-patch

    #     get_data = {
    #         "mod": [
    #             "graphic-herbalism-mwse-and-openmw-edition",
    #             "morrowind-optimization-patch",
    #         ]
    #     }
    #     os = "linux"
    #     bsa_mods, plugin_mods, used_mods, data_paths, extra_cfg, preset = generate_cfg(
    #         get_data, os
    #     )

    #     count = 1
    #     for mod in used_mods:
    #         if mod.category.title not in settings.IGNORED_CATS:
    #             if isinstance(mod.get_moddir_linux, str):
    #                 mod_string = '"' + mod.get_moddir_linux + '"' + "\n"
    #             if isinstance(mod.get_moddir_linux, list):
    #                 for d in mod.get_moddir_linux:
    #                     _data_path_string = data_paths[count]
    #                     self.assertEqual('"' + d + '"' + "\n", _data_path_string)
    #                     count += 1
    #                 break
    #             data_path_string = data_paths[count]

    #             self.assertEqual(mod_string, data_path_string)
    #             count += 1

    # TODO: for sure need a new equivalent for this!
    # def test_ensure_all_mods_are_in_data_path_load_order(self):
    #     no_dpo = []
    #     for m in Mod.objects.all():
    #         if (
    #             m.category.title not in settings.IGNORED_CATS
    #             and m.category.slug != "settings-tweaks"
    #             and m.name not in settings.IGNORED_MODS
    #         ):
    #             if m.datapathloadorder_set.count() == 0:
    #                 no_dpo.append(m.name)
    #     self.assertEqual(no_dpo, [])

    # def test_mod_bsa_load_order_is_sequential(self):
    #     bsa_dict = {}
    #     mods = Mod.with_bsa.all()
    #     for m in mods:
    #         bsa_dict.update(**m.has_bsa)

    #     nomatch = False
    #     count = 1
    #     for key in sorted(bsa_dict.keys()):
    #         if "." not in key:
    #             if int(key) != count:
    #                 print("Key didnt match count: ", key, count)
    #                 nomatch = True
    #             # only increment when key is not a float
    #             count += 1
    #     self.assertFalse(nomatch)

    # def test_mod_plugin_load_order_is_sequential(self):
    #     plugin_dict = {}
    #     mods = Mod.with_plugins.all()
    #     for m in mods:
    #         plugin_dict.update(**m.has_plugin)

    #     nomatch = False
    #     count = 1
    #     for key in sorted(plugin_dict.keys()):
    #         if "." not in key:
    #             if int(key) != count:
    #                 # TODO: skip two at 566 due to horrible horrible hacks
    #                 if count == 566:
    #                     count += 2
    #                 else:
    #                     print("Key didnt match count: ", key, count)
    #                     nomatch = True
    #             # only increment when key is not a float
    #             count += 1
    #     self.assertFalse(nomatch)

    # TODO: Do we need tests that read each mod list yaml/toml file and check for dupes?
    # TODO: The following data order tests should implicitly prevent this from happening
    # TODO: too, so maybe the specific test isn't needed..

    # def test_data_order_total_overhaul(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.total_overhaul.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "Total Overhaul mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    # def test_data_order_graphics_overhaul(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.graphics_overhaul.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "Graphics Overhaul mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    # def test_data_order_one_day_modernize(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.one_day_modernize.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "One Day Modernize mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    # def test_data_order_expanded_vanilla(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.expanded_vanilla.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "Expanded Vanilla mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    # def test_data_order_i_heart_vanilla(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.i_heart_vanilla.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "I Heart Vanilla mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    # def test_data_order_starwind_modded(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.starwind_modded.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "Starwind Modded mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    # def test_data_order_i_heart_vanilla_dc(self):
    #     full_data_path_order = DataPathLoadOrder.objects.all()
    #     listed_mods = ListedMod.i_heart_vanilla_dc.exclude(
    #         mod__category__title="Settings Tweaks"
    #     )
    #     mods = []
    #     dp = []
    #     out_of_order = 0

    #     for m in listed_mods:
    #         mods.append(m.mod)

    #     for m in full_data_path_order:
    #         if m.mod in mods:
    #             dp.append(m.mod)

    #     for n in range(0, len(mods) - 1):
    #         if dp[n] != mods[n]:
    #             print(
    #                 "I Heart Vanilla Director's Cut mod #{n} out of order: {m}".format(
    #                     n=n, m=mods[n].name
    #                 )
    #             )
    #             print("====> Should be: " + dp[n].name)
    #             out_of_order += 1

    #     self.assertEqual(out_of_order, 0)

    def test_about_page(self):
        r = self.f.get(reverse("about"))
        r.user = AnonymousUser()
        response = static_view(r, "about.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_changelogs_website(self):
        r = self.f.get(reverse("changelogs-website"))
        r.user = AnonymousUser()
        response = static_view(r, "changelogs-website.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_index(self):
        r = self.f.get(reverse("index"))
        r.user = AnonymousUser()
        response = static_view(r, "index.html")
        self.assertEqual(response.status_code, HTTP_OK)

    def test_privacy_page(self):
        r = self.f.get(reverse("privacy"))
        r.user = AnonymousUser()
        response = static_view(r, "privacy.html")
        self.assertEqual(response.status_code, HTTP_OK)

    # TODO: can this even work?
    # def test_login_page_loads(self):
    #     r = self.f.get(reverse("login"))
    #     # r.user = AnonymousUser()
    #     print(Site.objects.all())
    #     response = login(r)
    #     self.assertEqual(response.status_code, HTTP_OK)
