import os

from pathlib import Path


if os.getenv("MOMW_PROD"):
    from .settings_prod import *  # NOQA
elif os.getenv("MOMW_STAGING"):
    from .settings_staging import *  # NOQA
elif os.getenv("MOMW_BETA"):
    from .settings_beta import *  # NOQA
elif os.getenv("MOMW_TESTING"):
    from .settings_testing import *  # NOQA
else:
    from .settings_local import *  # NOQA

if os.getenv("MOMW_VER"):
    MOMW_VER = os.getenv("MOMW_VER")
elif not os.getenv("MOMW_PROD") or not os.getenv("MOMW_STAGING"):
    try:
        MOMW_VER
    except NameError:
        MOMW_VER = "wtf"

if PROJECT_HOSTNAME != "0.0.0.0":  # NOQA
    ALLOWED_HOSTS = [
        PROJECT_HOSTNAME,  # NOQA
    ]
PROJECT_NAME = "momw"
SITE_ID = 1
ADMINS = (("Admin", "admin@modding-openmw.com"),)
BASE_DIR = Path(__file__).resolve().parent.parent
BLOG_RSS_DESCRIPTION = "Modding-OpenMW.com news"
BLOG_RSS_TITLE = "{} News and Updates".format(SITE_NAME)  # NOQA
CSP_CHILD_SRC = ("modding-openmw.com/",)
CSRF_FAILURE_VIEW = "{}.errors.csrf_failure_view".format(PROJECT_NAME)
CACHE_MIDDLEWARE_KEY_PREFIX = "momw_is_rad"
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "momw-db.sqlite3",
    }
}
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.sitemaps",
]
if DEBUG:  # NOQA
    INSTALLED_APPS.append("debug_toolbar")
for m in ["chroniko", "media", "momw", "taggit", "utilz"]:
    INSTALLED_APPS.append(m)
INTERNAL_IPS = ["127.0.0.1", "0.0.0.0"]
LANGUAGE_CODE = "en-us"
LOGIN_REDIRECT_URL = "/zEndusal/home"  # TODO
LOGIN_URL = "/"
LOGOUT_URL = "/"
MANAGERS = ADMINS
for m in [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "utilz.middleware.ReferrerPolicyMiddleware",
    "utilz.middleware.AllYourBaseMiddleware",
]:
    MIDDLEWARE.append(m)  # NOQA
MONTH_FORMAT = "%m"
PAGINATE_BY = 5
ROOT_URLCONF = "{}.urls".format(PROJECT_NAME)
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_SECONDS = 31536000
SECRET_KEY = "-a6n&n$*x+fgg90w&d5j0_hzlq+m!8!yyhkwiu6#wl1bn@p52+"
STATIC_URL = "/static/"
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"),)
WSGI_APPLICATION = "{}.wsgi.application".format(PROJECT_NAME)
TEMPLATE_DEBUG = False
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            os.path.join(BASE_DIR, "templates"),
            os.path.join(BASE_DIR, "momw", "data_seeds", "data"),
            os.path.join(BASE_DIR, "momw", "data_seeds", "data", "changelogs"),
        ],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "{0}.context_processors.{0}_ver".format(PROJECT_NAME),
                "utilz.context_processors.site_wide",
            ]
        },
    }
]
TIME_ZONE = "America/Chicago"
TIME_ZONE_OFFSET = "-0500"
USE_ASCIINEMA = True
USE_I18N = True
USE_L10N = True
USE_TZ = True

IGNORED_CATS = ("Collections", "Mod Managers", "Settings Tweaks", "TES3MP", "Tools")
IGNORED_MODS = (
    "Controlled Consumption",
    "MGE XE",
    "Morrowind Code Patch",
    "Bloom Linear",
)
