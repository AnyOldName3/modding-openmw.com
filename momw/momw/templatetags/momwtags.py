from django import template
from django.utils.html import format_html
from django.utils.text import slugify
from ..models import Mod

register = template.Library()


@register.simple_tag
def mod_link(title):
    try:
        return format_html(
            "<a href='{}'>{}</a>", Mod.objects.get(name=title).get_absolute_url(), title
        )
    except Mod.DoesNotExist:
        # This mod was removed from the DB but had a changelog at one point
        # TODO: make a tuple containing all known mods that have been removed but would show up here
        return format_html("<a href='{}'>{}</a>", slugify(title), title)


@register.simple_tag
def issue_link(number):
    return format_html(
        """<a href="https://gitlab.com/modding-openmw/modding-openmw.com/-/issues/{}/">#{}</a>""",
        number,
        number,
    )


@register.simple_tag
def nexus_mod(modid):
    return format_html(
        """<a href="https://www.nexusmods.com/morrowind/mods/{}">Nexus Mods Link</a>""",
        modid,
    )
