import os
import sys
import toml
import yaml

from datetime import datetime
from django.contrib.auth import get_user_model
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.query import QuerySet
from django.template.exceptions import TemplateSyntaxError
from django.template.loader import get_template
from django.utils.text import slugify
from .models import Category, Mod, Tag


User = get_user_model()


def generate_category(title: str, slug: str, description: str) -> Category:
    new_category = {"title": title, "slug": slug, "description": description}
    c = Category(**new_category)
    c.save()
    return c


def generate_mod(
    alt_to=[],
    author=None,
    category=None,
    compat=None,
    date_added=None,
    date_updated=None,
    description=None,
    dl_url=None,
    is_active=None,
    is_featured=False,
    name=None,
    slug=None,
    status=None,
    tags=[],
    url=None,
    usage_notes=None,
    needs_cleaning=False,
    extra_cfg=False,
    extra_cfg_raw=None,
    folder_paths=None,
    custom_folder=None,
) -> Mod:
    datetime_format = "%Y-%m-%d %H:%M:%S %z"

    compat_map = {
        "fully working": Mod.FULLY_WORKING,
        "partially working": Mod.PARTIAL_WORKING,
        "not working": Mod.NOT_WORKING,
        "unknown": Mod.UNKNOWN,
    }

    status_map = {"live": Mod.LIVE, "draft": Mod.DRAFT, "hidden": Mod.HIDDEN}

    new_mod = {
        "author": author,
        "category": Category.objects.get(title=category),
        "date_added": datetime.strptime(date_added, datetime_format),
        "description": description,
        "name": name,
        "slug": slug,
    }

    # IsAlpha
    isalpha = ""
    for char in name:
        if char.isalpha():
            isalpha += char
    new_mod.update({"isalpha_name": isalpha})

    # Special2Spaces
    special2spaces = ""
    for char in name:
        if char.isalpha():
            special2spaces += char
        elif char != " ":
            special2spaces += " "
    new_mod.update({"special_to_spaces_name": special2spaces})

    # Abbrev
    abbrev = ""
    split_name = name.split()
    for word in split_name:
        abbrev += word[0]
    new_mod.update({"abbrev_name": abbrev})

    if compat:
        new_mod.update({"compat": compat_map[compat]})
    else:
        new_mod.update({"compat": Mod.FULLY_WORKING})

    if status:
        new_mod.update({"status": status_map[status]})
    else:
        new_mod.update({"status": Mod.HIDDEN})

    if date_updated:
        new_mod.update(
            {"date_updated": datetime.strptime(date_updated, datetime_format)}
        )
    else:
        new_mod.update({"date_updated": datetime.strptime(date_added, datetime_format)})

    if dl_url:
        new_mod.update({"dl_url": dl_url})
    if is_active is not None:
        new_mod.update({"is_active": is_active})
    if not slug:
        new_mod.update({"slug": make_slug(name)})
    if custom_folder:
        new_mod.update({"custom_folder": custom_folder})

    if not url:
        if dl_url:
            url = new_mod["dl_url"]
        else:
            url = "/mods/" + new_mod["slug"]

    new_mod.update({"url": url})

    m = Mod(**new_mod)
    m.save()

    if alt_to:
        for a in alt_to:
            try:
                m.alt_to.add(Mod.objects.get(name=a))
            except Mod.DoesNotExist:
                print()
                print("ERROR: No mod matches the name:", a)
                print("ERROR: Aborting alt_to addition for:", m.name)
                sys.exit(1)

    if tags:
        for t in tags:
            try:
                m.tags.add(Tag.objects.get(name=t))
            except Mod.DoesNotExist:
                print()
                print("ERROR: No tag matches the name:", t)
                print("ERROR: Aborting tag addition for:", m.name)
                sys.exit(1)

    m.save()
    return m


def int_to_str(i: int) -> str:
    """Turn an int into a string and pad it appropriately."""
    str_int = str(i)
    if len(str_int) == 1:
        return "00" + str_int
    elif len(str_int) == 2:
        return "0" + str_int
    return str_int


def make_slug(string: str) -> str:
    """
    Wrapper around Django's slugify() that limits length to 49 characters.
    """
    return slugify(string)[:50]


def moddinghall_mod(slug: str) -> str:
    return "https://mw.moddinghall.com/file/" + slug


def moddinghall_user(slug: str, username: str) -> str:
    return '<a href="https://mw.moddinghall.com/profile/{0}">{1}</a>'.format(
        slug, username
    )


def modhistory_archive_link(mod: str):
    return "https://web.archive.org/web/https://mw.modhistory.com/download-" + mod


def nexus_mod(modid: str) -> str:
    return "https://www.nexusmods.com/morrowind/mods/" + modid


def nexus_user(userid: str, username: str) -> str:
    return '<a href="https://www.nexusmods.com/morrowind/users/{0}">{1}</a>'.format(
        userid, username
    )


def paginator_for_queryset(request, mod_queryset, page_num):
    """
    Given an HTTP request 'request', a Queryset 'mod_queryset', and an
    integer 'page_num', calculate and return values for pagination ranges.
    """
    paginator = Paginator(mod_queryset, 1)

    try:
        mod = paginator.page(page_num)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        mod = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        mod = paginator.page(paginator.num_pages)

    pag_range = []
    gap = 10

    # TODO: Demystify this..
    pag_next = list(
        range(mod.number, mod.number + (1 + paginator.num_pages - mod.number))
    )
    pag_prev = list(
        range(mod.number - gap - (abs(len(pag_next) - gap)) - 1, mod.number)
    )

    added = 0
    if isinstance(pag_prev, list):
        for num in reversed(pag_prev):
            if num > 0 and added < gap:
                pag_range.insert(0, num)
                added += 1

    added = 0
    if isinstance(pag_next, list):
        for num in pag_next:
            if num > 0 and num <= paginator.num_pages and added < gap:
                pag_range.append(num)

                # Don't count this mod's number so there can be equal numbers on each side.
                if num != mod.number:
                    added += 1

    return pag_range, pag_prev, pag_next


def read_toml_data(filename: str, changelog=False) -> dict:
    """
    Read the file 'filename' as if it was a YAML file that is a single list.
    If successful, return the list as an actual Python list.
    """
    try:
        return toml.loads(get_template(f"{filename}.toml").render())
    except (TemplateSyntaxError, toml.decoder.TomlDecodeError) as e:
        print(f"There's a syntax error in **{filename}.toml**: {e}")
        sys.exit(1)


def read_yaml_data(filename: str):
    """
    Read the file 'filename' as if it was a YAML file that is a single list.
    If successful, return the list as an actual Python list.
    """
    this_dir = os.path.dirname(__file__)
    yaml_file = os.path.abspath(
        os.path.join(this_dir, "data_seeds", "data", filename + ".yml")
    )
    try:
        with open(yaml_file, "r") as f:
            dl = yaml.safe_load(f)
    except FileNotFoundError as e:
        print("ERROR:", e)
        sys.exit(1)
    return dl


def sorted_plugins_list(mods_with_plugins: QuerySet) -> list:
    """
    Takes a queryset of Mods with plugins, returns a sorted
    list of tuples containing order number/plugin name pairs.

    A copy of the dict of plugins and orders is made in order
    to produce a list that's free of orders with "." in them
    (for plugins that appear more than once.)
    """
    d = {}
    for m in mods_with_plugins:
        d.update(m.has_plugin)
    _d = d.copy()
    for k, v in _d.items():
        if "." in k:
            d.pop(k, v)
    return sorted(d.items())


def get_visitor_os(request) -> str:
    try:
        user_agent = request.META["HTTP_USER_AGENT"]
    except KeyError:
        return None
    user_os = None
    if "Macintosh" in user_agent:
        user_os = "macos"
    elif "Linux" in user_agent or "Ubuntu" in user_agent:
        user_os = "linux"
    elif "Windows" in user_agent:
        user_os = "windows"
    elif "Android" in user_agent:
        user_os = "android"
    return user_os
