from pathlib import Path

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.MemcachedCache",
        "LOCATION": "127.0.0.1:11211",
    }
}
CACHE_MIDDLEWARE_SECONDS = 3600  # 1 hour
DEBUG = False
EMAIL_HOST = "localhost"
EMAIL_PORT = 25
INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "django.contrib.sitemaps",
    "chroniko",
    "media",
    "momw",
    "taggit",
    "utilz",
)
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": Path.home() / "logs" / "django.log",
        }
    },
    "loggers": {
        "django.request": {"handlers": ["file"], "level": "DEBUG", "propagate": True}
    },
}
MIDDLEWARE = []
