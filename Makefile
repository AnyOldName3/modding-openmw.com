proj_dir := $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))

HOST ?= 127.0.0.1
PORT ?= 8000

DOCKER_VER ?= 0
MOMW_VER ?= docker
VIRTUAL_ENV ?= $(proj_dir)/venv

server:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py runserver $(HOST):$(PORT)

clean:
	if [ -f momw/momw-db.sqlite3 ]; then rm momw/momw-db.sqlite3; fi

# Docker stuff
docker-build:
	docker build . -t modding-openmw-dot-com:$(DOCKER_VER)

docker-rmi:
	docker rmi modding-openmw-dot-com:$(DOCKER_VER)

docker-reset-and-run: clean docker-build
	docker run -p $(PORT):$(PORT) -it -e HOST=0.0.0.0 -e MOMW_VER=$(MOMW_VER) -v $(proj_dir):/app --rm modding-openmw-dot-com:0 reset server

docker-run: docker-build
	docker run -p $(PORT):$(PORT) -it -e HOST=0.0.0.0 -e MOMW_VER=$(MOMW_VER) -v $(proj_dir):/app --rm modding-openmw-dot-com:0

docker-sh: docker-build
	docker run -p $(PORT):$(PORT) -it -e HOST=0.0.0.0 -e MOMW_VER=$(MOMW_VER) -v $(proj_dir):/app --rm --entrypoint bash modding-openmw-dot-com:0

docker-tests: docker-build
	docker run -p $(PORT):$(PORT) -it -v $(proj_dir):/app --rm modding-openmw-dot-com:0 reset test

docker-rebuild: docker-rmi docker-build

# Python stuff
venv:
	python3 -m venv $(VIRTUAL_ENV)

# Cursed Ubuntu stuff
ub-venv:
	python3 -m venv $(VIRTUAL_ENV) --without-pip --system-site-packages

migrate:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py migrate

makemigrations:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py makemigrations

shell:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py shell

seeddb:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/bin/seed_db.py

# THANKS https://www.b-list.org/weblog/2022/may/13/boring-python-dependencies/
pip-tools: venv
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location --upgrade pip-tools

pip-hashes: pip-tools
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m piptools compile --strip-extras --generate-hashes requirements/app.in --output-file requirements/app.txt
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m piptools compile --strip-extras --generate-hashes requirements/tests.in --output-file requirements/tests.txt

pip-requirements:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location -r requirements/app.in
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location -r requirements/tests.in

#TODO: make this work with our deps+versions...
_pip-requirements:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location -r requirements/app.txt
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" python -m pip install --no-warn-script-location -r requirements/tests.txt

test-black:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" black --check --diff --exclude migrations momw

test-flake8:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" flake8 momw

test-media:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py test media.tests

test-momw:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py test momw.tests

test-utilz:
	VIRTUAL_ENV=$(VIRTUAL_ENV) PATH="$$VIRTUAL_ENV/bin:$$PATH" $(proj_dir)/momw/manage.py test utilz.tests

# Groupings
reset: clean migrate seeddb

test-djangotests: test-media test-momw test-utilz

test: test-black test-flake8 test-momw

ifndef SKIPTESTS
test-local: test
else
test-local:
	@echo Skipped tests due to the presence of the SKIPTESTS env var!
endif

# Extra
start-xvfb-ci:
	$(proj_dir)/bin/start_xvfb.sh

smtpd:
	python3 -m smtpd -n -c DebuggingServer localhost:1025

ansible-tests:
	cd deploy && make

ansible-venv:
	cd deploy && make pip-requirements

testing-local:
	cd deploy && make testing-local

testing-local-noreset:
	cd deploy && make testing-local-noreset

beta-local:
	cd deploy && make beta-local

beta-local-noreset:
	cd deploy && make beta-local-noreset

staging-local:
	cd deploy && make staging-local

staging-local-noreset:
	cd deploy && make staging-local-noreset

prod-git:
	cd deploy && make prod-git
